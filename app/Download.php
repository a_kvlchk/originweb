<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    protected $fillable = ['mail', 'slug', 'file_id'];

    public function file()
    {
        return $this->belongsTo(File::class);
    }

    public function getPathAttribute()
    {
        return $this->file->path;
    }
}
