<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ApiController extends Controller
{
    public function loadImage()
    {

    }

    public function sendMail(Request $request)
    {
        if ($request->isMethod('POST')) {

            $response = [
                'success'      => true,
                'nameError'    => false,
                'emailError'   => false,
                'messageError' => false,
            ];

            if (!$request->has('name') || strlen($request->post('name')) < 2) {
                $response['success'] = false;
                $response['nameError'] = 'Name is required';
            }

            if (!$request->has('email') || !filter_var($request->post('email'), FILTER_VALIDATE_EMAIL)) {
                $response['success'] = false;
                $response['emailError'] = 'E-mail is invalid';
            }

            if (!$request->has('message') || strlen($request->post('message')) < 2) {
                $response['success'] = false;
                $response['messageError'] = 'Message is required';

            }

            if ($response['success']) {

                $user = User::find(1);

                \App\Mail::create([
                    'name'    => $request->post('name'),
                    'email'   => $request->post('email'),
                    'message' => $request->post('message')
                ]);

                Mail::raw($request->post('message'), function ($message) use ($user) {
                    $message->from($user['email']);
                    $message->subject('Message from website');
                    $message->to($user['email']);
                });

                if ($request->sendToMe) {
                    Mail::raw($request->post('message'), function ($message) use ($user) {
                        $message->from($user['email']);
                        $message->subject('Message from website');
                        $message->to(request()->post('email'));
                    });
                }
            }

        } else {
            $response = [
                'success' => false
            ];
        }

        return response()->json($response);

    }

    public function requestDownload()
    {

    }
}
