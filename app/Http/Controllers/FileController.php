<?php

namespace App\Http\Controllers;

use App\Download;
use App\File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class FileController extends Controller
{
    /**
     * Request a file to e-mail
     */
    public function request()
    {
        $data = \request()->validate([
            'email'   => 'required|email',
            'file_id' => 'required|integer'
        ]);

        $file = File::findOrFail($data['file_id']);
        $slug = str_random(10);

        Download::create([
            'mail'    => $data['email'],
            'slug'    => $slug,
            'file_id' => $file->id
        ]);

        $link = url('files/download', $slug);

        Mail::send([], [], function ($message) use ($data, $link) {
            $message->from('mail@originweb.ca');
            $message->subject('File Download');
            $message->to($data['email']);
            $message->setBody("Hey, here's your link: <a href='$link'>$link</a>. It will be available for next 24hrs.", 'text/html');
        });

        return response()->json(true);
    }

    /**
     * Download a file by slug
     */
    public function download($slug)
    {
        $file = Download::where([
            ['created_at', '>=', Carbon::now()->subDay()],
            ['slug', '=', $slug]
        ])->first();

        if ($file) {
            $file->downloaded = ++$file->downloaded;
            $file->save();

            return response()->download(storage_path('app/' . $file->path));
        } else {
            abort(404, "File wasn't found or link is outdated.");
        }
    }
}
