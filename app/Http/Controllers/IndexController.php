<?php

namespace App\Http\Controllers;

use App\Project;
use App\Testimonial;
use Spatie\Analytics\AnalyticsFacade;
use Spatie\Analytics\Period;

class IndexController extends Controller
{
    /**
     * HomePage
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projects = Project::latest()->limit(6)->get();

        $testimonials = Testimonial::latest()->get();

        return view('welcome', compact('projects', 'testimonials'));
    }

    public function admin()
    {
        $analytics = AnalyticsFacade::fetchVisitorsAndPageViews(Period::days(7));
        $projects = Project::latest()->get();
        $testimonials = Testimonial::latest()->get();

        return view('backdoor.admin', compact('analytics', 'projects', 'testimonials'));
    }

    /**
     * Single project
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function oneProject(Project $project)
    {
        return view('project', compact('project'));
    }
}
