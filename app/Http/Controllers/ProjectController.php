<?php

namespace App\Http\Controllers;

use App\Category;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ProjectController extends Controller
{
    /**
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Project $project)
    {
        $blade = $project->template;

        $next = Project::find($project->id + 1);
        $prev = Project::find($project->id - 1);

        return view('projects.' . $blade, compact('project', 'next', 'prev'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $bladeFiles = $this->getBladeFiles();

        return view('backdoor.projects.create', compact('bladeFiles'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name'       => 'required|string',
            'image'      => 'nullable|image',
            'file'       => 'nullable|file',
            'categories' => 'required',
            'year'       => 'required|integer',
            'short_desc' => 'present',
            'template'   => 'required'
        ]);

        $validatedData = $this->storeImage($validatedData);
        $validatedData = $this->saveFile($validatedData);

        Project::create($validatedData);

        return redirect('backdoor');
    }


    /**
     * @param Project $project
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Project $project)
    {
        $bladeFiles = $this->getBladeFiles();

        return view('backdoor.projects.edit', compact('project', 'bladeFiles'));
    }


    /**
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Project $project)
    {
        $validatedData = $request->validate([
            'name'       => 'required|string',
            'image'      => 'nullable|image',
            'file'       => 'nullable|file',
            'categories' => 'required|string',
            'year'       => 'required|integer',
            'short_desc' => 'present',
            'template'   => 'required'
        ]);

        // Saving image
        if (isset($validatedData['image'])) {
            if ($image = $project->image) {
                \File::delete(public_path('projects' . '/' . $image));
            }

            $validatedData = $this->storeImage($validatedData);
        }

        // Saving downloadable file
        $validatedData = $this->saveFile($validatedData);

        $project->update($validatedData);
        $project->save();

        return redirect('backdoor');
    }


    /**
     * @param Project $project
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete(Project $project)
    {
        if ($image = $project->image) {
            \File::delete(public_path('projects' . '/' . $image));
        }

        $project->delete();

        return redirect('backdoor');
    }


    /**
     * @param $validatedData
     * @return mixed
     */
    private function storeImage($validatedData)
    {
        $image = $validatedData['image'];

        $imageTitle = strtolower(str_replace(' ', '_', trim($validatedData['name']))) . '.' . $image->extension();

        $image->storeAs('', $imageTitle, 'projects');

        $imageThumb = Image::make($image)->fit(700)->save(public_path('projects') . DIRECTORY_SEPARATOR . 'thumb_' . $imageTitle);

        $validatedData['image'] = $imageTitle;

        return $validatedData;
    }


    /**
     * @param $validatedData
     * @return mixed
     */
    private function saveFile($validatedData)
    {
        if (isset($validatedData['file'])) {
            $file = $validatedData['file'];
            $path = $file->store('files');

            $file = File::create([
                'name' => $file->getClientOriginalName(),
                'path' => $path,
            ]);

            unset($validatedData['file']);
            $validatedData['file_id'] = $file->id;
        }

        return $validatedData;
    }


    /**
     * @return array
     */
    private function getBladeFiles()
    {
        $bladeFiles = File::files(base_path('resources/views/projects'));

        foreach ($bladeFiles as $key => $bladeFile) {
            $bladeFiles[$key] = $bladeFile->getFilename();
        }

        return $bladeFiles;
    }
}
