<?php

namespace App\Http\Controllers;

use App\Testimonial;

class TestimonialController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backdoor.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = \request()->validate([
            'name'    => 'required',
            'company' => 'required',
            'image'   => 'nullable|image',
            'text'    => 'required',
        ]);

        if (\request()->hasFile('image')) {
            $data = $this->saveFile($data);
        }

        Testimonial::create($data);

        return redirect('backdoor');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('backdoor.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Testimonial $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Testimonial $testimonial)
    {
        $data = \request()->validate([
            'name'    => 'required',
            'company' => 'required',
            'image'   => 'nullable|image',
            'text'    => 'required',
        ]);

        if (\request()->hasFile('image')) {
            $data = $this->saveFile($data);
        }

        $testimonial->update($data);
        $testimonial->save();

        return redirect('backdoor');
    }

    /**
     * Remove the specified resource from storage
     */
    public function destroy(Testimonial $testimonial)
    {
        $testimonial->delete();

        return redirect('backdoor');
    }

    private function saveFile($data)
    {
        $image = $data['image'];

        $data['image'] = $image->storeAs('', $image->hashName(), 'projects');

        return $data;
    }
}
