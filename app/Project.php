<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $appends = ['image_thumb', 'image_path'];

    /**$value
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getImagePathAttribute()
    {
        return asset('projects/' . $this->image);
    }

    /**
     * @return string
     */
    public function getCategoriesListAttribute()
    {
        $cats = $this->categories()->get();

        $result = [];

        foreach ($cats as $cat) {
            $result[] = $cat->name;
        }

        return implode(', ', $result);
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo(File::class);
    }

    /**
     * @return null|string
     */
    public function getImageThumbAttribute()
    {
        return $this->image ? asset('projects/thumb_' . $this->image) : null;
    }

    public function setTemplateAttribute($value)
    {
        $this->attributes['template'] = str_replace('.blade.php', '', $value);
    }
}
