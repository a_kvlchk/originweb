<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $guarded = [];

    public function getImageAttribute($value)
    {
        return $value ? asset('projects/'. $value) : null;
    }
}
