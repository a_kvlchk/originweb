const WOW = require('wowjs');

window.wow = window.WOW = WOW.WOW;

require('./main');

$(document).ready(function () {

    $('a.anchor')
    // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                }
            }
        });

    $('.materialboxed').materialbox();
    $('#file-request').on('submit', function (e) {
        e.preventDefault();

        let resultBlock = $('#file-request-result');

        resultBlock.removeClass();
        resultBlock.html('Sending...');

        let data = $(this).serialize();

        $.ajax({
            type: "POST",
            url: '/files/request',
            data: data,
            success: function (result) {
                console.log(result);
                resultBlock.removeClass();
                resultBlock.addClass('text-success');
                resultBlock.html('E-mail was sent');
            },
            error: function (result) {
                console.log(result);
                resultBlock.removeClass();
                resultBlock.addClass('text-danger');
                resultBlock.html('Sorry, undefined error');
            }
        });
    });
});

$('#contactForm').on('submit', e => {
    e.preventDefault();

    let button = $('#sendContactForm');
    button.text('Sending...');
    button.prop('disabled', true);

    let data = $('#contactForm').serialize();

    $.post('/api/mail', data)
        .then(() => {
            button.text('Sent');
            button.addClass('success');
        })
        .catch(resp => {
            console.log(resp);
            button.prop('disabled', false);
        });
});