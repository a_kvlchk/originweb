function _classCallCheck(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
}

!function (t, e) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
        if (!t.document) throw new Error("jQuery requires a window with a document");
        return e(t)
    } : e(t)
}("undefined" != typeof window ? window : this, function (t, e) {
    "use strict";

    function i(t, e) {
        var i = (e = e || Q).createElement("script");
        i.text = t, e.head.appendChild(i).parentNode.removeChild(i)
    }

    function n(t) {
        var e = !!t && "length" in t && t.length, i = nt.type(t);
        return "function" !== i && !nt.isWindow(t) && ("array" === i || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
    }

    function o(t, e) {
        return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
    }

    function r(t, e, i) {
        return nt.isFunction(e) ? nt.grep(t, function (t, n) {
            return !!e.call(t, n, t) !== i
        }) : e.nodeType ? nt.grep(t, function (t) {
            return t === e !== i
        }) : "string" != typeof e ? nt.grep(t, function (t) {
            return G.call(e, t) > -1 !== i
        }) : dt.test(e) ? nt.filter(e, t, i) : (e = nt.filter(e, t), nt.grep(t, function (t) {
            return G.call(e, t) > -1 !== i && 1 === t.nodeType
        }))
    }

    function s(t, e) {
        for (; (t = t[e]) && 1 !== t.nodeType;) ;
        return t
    }

    function a(t) {
        return t
    }

    function l(t) {
        throw t
    }

    function c(t, e, i, n) {
        var o;
        try {
            t && nt.isFunction(o = t.promise) ? o.call(t).done(e).fail(i) : t && nt.isFunction(o = t.then) ? o.call(t, e, i) : e.apply(void 0, [t].slice(n))
        } catch (t) {
            i.apply(void 0, [t])
        }
    }

    function u() {
        Q.removeEventListener("DOMContentLoaded", u), t.removeEventListener("load", u), nt.ready()
    }

    function h() {
        this.expando = nt.expando + h.uid++
    }

    function d(t, e, i) {
        var n;
        if (void 0 === i && 1 === t.nodeType) if (n = "data-" + e.replace(_t, "-$&").toLowerCase(), "string" == typeof(i = t.getAttribute(n))) {
            try {
                i = function (t) {
                    return "true" === t || "false" !== t && ("null" === t ? null : t === +t + "" ? +t : Tt.test(t) ? JSON.parse(t) : t)
                }(i)
            } catch (t) {
            }
            kt.set(t, e, i)
        } else i = void 0;
        return i
    }

    function p(t, e, i, n) {
        var o, r = 1, s = 20, a = n ? function () {
                return n.cur()
            } : function () {
                return nt.css(t, e, "")
            }, l = a(), c = i && i[3] || (nt.cssNumber[e] ? "" : "px"),
            u = (nt.cssNumber[e] || "px" !== c && +l) && Et.exec(nt.css(t, e));
        if (u && u[3] !== c) {
            c = c || u[3], i = i || [], u = +l || 1;
            do {
                u /= r = r || ".5", nt.style(t, e, u + c)
            } while (r !== (r = a() / l) && 1 !== r && --s)
        }
        return i && (u = +u || +l || 0, o = i[1] ? u + (i[1] + 1) * i[2] : +i[2], n && (n.unit = c, n.start = u, n.end = o)), o
    }

    function f(t, e) {
        for (var i, n, o = [], r = 0, s = t.length; r < s; r++) (n = t[r]).style && (i = n.style.display, e ? ("none" === i && (o[r] = Ct.get(n, "display") || null, o[r] || (n.style.display = "")), "" === n.style.display && $t(n) && (o[r] = function (t) {
            var e, i = t.ownerDocument, n = t.nodeName, o = Dt[n];
            return o || (e = i.body.appendChild(i.createElement(n)), o = nt.css(e, "display"), e.parentNode.removeChild(e), "none" === o && (o = "block"), Dt[n] = o, o)
        }(n))) : "none" !== i && (o[r] = "none", Ct.set(n, "display", i)));
        for (r = 0; r < s; r++) null != o[r] && (t[r].style.display = o[r]);
        return t
    }

    function g(t, e) {
        var i;
        return i = void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e || "*") : void 0 !== t.querySelectorAll ? t.querySelectorAll(e || "*") : [], void 0 === e || e && o(t, e) ? nt.merge([t], i) : i
    }

    function v(t, e) {
        for (var i = 0, n = t.length; i < n; i++) Ct.set(t[i], "globalEval", !e || Ct.get(e[i], "globalEval"))
    }

    function m(t, e, i, n, o) {
        for (var r, s, a, l, c, u, h = e.createDocumentFragment(), d = [], p = 0, f = t.length; p < f; p++) if ((r = t[p]) || 0 === r) if ("object" === nt.type(r)) nt.merge(d, r.nodeType ? [r] : r); else if (jt.test(r)) {
            for (s = s || h.appendChild(e.createElement("div")), a = (Mt.exec(r) || ["", ""])[1].toLowerCase(), l = Nt[a] || Nt._default, s.innerHTML = l[1] + nt.htmlPrefilter(r) + l[2], u = l[0]; u--;) s = s.lastChild;
            nt.merge(d, s.childNodes), (s = h.firstChild).textContent = ""
        } else d.push(e.createTextNode(r));
        for (h.textContent = "", p = 0; r = d[p++];) if (n && nt.inArray(r, n) > -1) o && o.push(r); else if (c = nt.contains(r.ownerDocument, r), s = g(h.appendChild(r), "script"), c && v(s), i) for (u = 0; r = s[u++];) It.test(r.type || "") && i.push(r);
        return h
    }

    function y() {
        return !0
    }

    function b() {
        return !1
    }

    function w() {
        try {
            return Q.activeElement
        } catch (t) {
        }
    }

    function x(t, e, i, n, o, r) {
        var s, a;
        if ("object" == typeof e) {
            "string" != typeof i && (n = n || i, i = void 0);
            for (a in e) x(t, a, i, n, e[a], r);
            return t
        }
        if (null == n && null == o ? (o = i, n = i = void 0) : null == o && ("string" == typeof i ? (o = n, n = void 0) : (o = n, n = i, i = void 0)), !1 === o) o = b; else if (!o) return t;
        return 1 === r && (s = o, (o = function (t) {
            return nt().off(t), s.apply(this, arguments)
        }).guid = s.guid || (s.guid = nt.guid++)), t.each(function () {
            nt.event.add(this, e, o, n, i)
        })
    }

    function C(t, e) {
        return o(t, "table") && o(11 !== e.nodeType ? e : e.firstChild, "tr") ? nt(">tbody", t)[0] || t : t
    }

    function k(t, e) {
        var i, n, o, r, s, a, l, c;
        if (1 === e.nodeType) {
            if (Ct.hasData(t) && (r = Ct.access(t), s = Ct.set(e, r), c = r.events)) {
                delete s.handle, s.events = {};
                for (o in c) for (i = 0, n = c[o].length; i < n; i++) nt.event.add(e, o, c[o][i])
            }
            kt.hasData(t) && (a = kt.access(t), l = nt.extend({}, a), kt.set(e, l))
        }
    }

    function T(t, e, n, o) {
        e = U.apply([], e);
        var r, s, a, l, c, u, h = 0, d = t.length, p = d - 1, f = e[0], v = nt.isFunction(f);
        if (v || d > 1 && "string" == typeof f && !it.checkClone && Ft.test(f)) return t.each(function (i) {
            var r = t.eq(i);
            v && (e[0] = f.call(this, i, r.html())), T(r, e, n, o)
        });
        if (d && (r = m(e, t[0].ownerDocument, !1, t, o), s = r.firstChild, 1 === r.childNodes.length && (r = s), s || o)) {
            for (l = (a = nt.map(g(r, "script"), function (t) {
                return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t
            })).length; h < d; h++) c = r, h !== p && (c = nt.clone(c, !0, !0), l && nt.merge(a, g(c, "script"))), n.call(t[h], c, h);
            if (l) for (u = a[a.length - 1].ownerDocument, nt.map(a, function (t) {
                var e = Vt.exec(t.type);
                return e ? t.type = e[1] : t.removeAttribute("type"), t
            }), h = 0; h < l; h++) c = a[h], It.test(c.type || "") && !Ct.access(c, "globalEval") && nt.contains(u, c) && (c.src ? nt._evalUrl && nt._evalUrl(c.src) : i(c.textContent.replace(Qt, ""), u))
        }
        return t
    }

    function _(t, e, i) {
        for (var n, o = e ? nt.filter(e, t) : t, r = 0; null != (n = o[r]); r++) i || 1 !== n.nodeType || nt.cleanData(g(n)), n.parentNode && (i && nt.contains(n.ownerDocument, n) && v(g(n, "script")), n.parentNode.removeChild(n));
        return t
    }

    function S(t, e, i) {
        var n, o, r, s, a = t.style;
        return (i = i || Ut(t)) && ("" !== (s = i.getPropertyValue(e) || i[e]) || nt.contains(t.ownerDocument, t) || (s = nt.style(t, e)), !it.pixelMarginRight() && Xt.test(s) && Bt.test(e) && (n = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = i.width, a.width = n, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s
    }

    function E(t, e) {
        return {
            get: function () {
                if (!t()) return (this.get = e).apply(this, arguments);
                delete this.get
            }
        }
    }

    function A(t) {
        var e = nt.cssProps[t];
        return e || (e = nt.cssProps[t] = function (t) {
            if (t in te) return t;
            for (var e = t[0].toUpperCase() + t.slice(1), i = Kt.length; i--;) if ((t = Kt[i] + e) in te) return t
        }(t) || t), e
    }

    function $(t, e, i) {
        var n = Et.exec(e);
        return n ? Math.max(0, n[2] - (i || 0)) + (n[3] || "px") : e
    }

    function P(t, e, i, n, o) {
        var r, s = 0;
        for (r = i === (n ? "border" : "content") ? 4 : "width" === e ? 1 : 0; r < 4; r += 2) "margin" === i && (s += nt.css(t, i + At[r], !0, o)), n ? ("content" === i && (s -= nt.css(t, "padding" + At[r], !0, o)), "margin" !== i && (s -= nt.css(t, "border" + At[r] + "Width", !0, o))) : (s += nt.css(t, "padding" + At[r], !0, o), "padding" !== i && (s += nt.css(t, "border" + At[r] + "Width", !0, o)));
        return s
    }

    function D(t, e, i) {
        var n, o = Ut(t), r = S(t, e, o), s = "border-box" === nt.css(t, "boxSizing", !1, o);
        return Xt.test(r) ? r : (n = s && (it.boxSizingReliable() || r === t.style[e]), "auto" === r && (r = t["offset" + e[0].toUpperCase() + e.slice(1)]), (r = parseFloat(r) || 0) + P(t, e, i || (s ? "border" : "content"), n, o) + "px")
    }

    function O(t, e, i, n, o) {
        return new O.prototype.init(t, e, i, n, o)
    }

    function M() {
        ie && (!1 === Q.hidden && t.requestAnimationFrame ? t.requestAnimationFrame(M) : t.setTimeout(M, nt.fx.interval), nt.fx.tick())
    }

    function I() {
        return t.setTimeout(function () {
            ee = void 0
        }), ee = nt.now()
    }

    function N(t, e) {
        var i, n = 0, o = {height: t};
        for (e = e ? 1 : 0; n < 4; n += 2 - e) o["margin" + (i = At[n])] = o["padding" + i] = t;
        return e && (o.opacity = o.width = t), o
    }

    function j(t, e, i) {
        for (var n, o = (H.tweeners[e] || []).concat(H.tweeners["*"]), r = 0, s = o.length; r < s; r++) if (n = o[r].call(i, e, t)) return n
    }

    function H(t, e, i) {
        var n, o, r = 0, s = H.prefilters.length, a = nt.Deferred().always(function () {
            delete l.elem
        }), l = function () {
            if (o) return !1;
            for (var e = ee || I(), i = Math.max(0, c.startTime + c.duration - e), n = 1 - (i / c.duration || 0), r = 0, s = c.tweens.length; r < s; r++) c.tweens[r].run(n);
            return a.notifyWith(t, [c, n, i]), n < 1 && s ? i : (s || a.notifyWith(t, [c, 1, 0]), a.resolveWith(t, [c]), !1)
        }, c = a.promise({
            elem: t,
            props: nt.extend({}, e),
            opts: nt.extend(!0, {specialEasing: {}, easing: nt.easing._default}, i),
            originalProperties: e,
            originalOptions: i,
            startTime: ee || I(),
            duration: i.duration,
            tweens: [],
            createTween: function (e, i) {
                var n = nt.Tween(t, c.opts, e, i, c.opts.specialEasing[e] || c.opts.easing);
                return c.tweens.push(n), n
            },
            stop: function (e) {
                var i = 0, n = e ? c.tweens.length : 0;
                if (o) return this;
                for (o = !0; i < n; i++) c.tweens[i].run(1);
                return e ? (a.notifyWith(t, [c, 1, 0]), a.resolveWith(t, [c, e])) : a.rejectWith(t, [c, e]), this
            }
        }), u = c.props;
        for (function (t, e) {
            var i, n, o, r, s;
            for (i in t) if (n = nt.camelCase(i), o = e[n], r = t[i], Array.isArray(r) && (o = r[1], r = t[i] = r[0]), i !== n && (t[n] = r, delete t[i]), (s = nt.cssHooks[n]) && "expand" in s) {
                r = s.expand(r), delete t[n];
                for (i in r) i in t || (t[i] = r[i], e[i] = o)
            } else e[n] = o
        }(u, c.opts.specialEasing); r < s; r++) if (n = H.prefilters[r].call(c, t, u, c.opts)) return nt.isFunction(n.stop) && (nt._queueHooks(c.elem, c.opts.queue).stop = nt.proxy(n.stop, n)), n;
        return nt.map(u, j, c), nt.isFunction(c.opts.start) && c.opts.start.call(t, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), nt.fx.timer(nt.extend(l, {
            elem: t,
            anim: c,
            queue: c.opts.queue
        })), c
    }

    function L(t) {
        return (t.match(mt) || []).join(" ")
    }

    function q(t) {
        return t.getAttribute && t.getAttribute("class") || ""
    }

    function z(t, e, i, n) {
        var o;
        if (Array.isArray(e)) nt.each(e, function (e, o) {
            i || fe.test(t) ? n(t, o) : z(t + "[" + ("object" == typeof o && null != o ? e : "") + "]", o, i, n)
        }); else if (i || "object" !== nt.type(e)) n(t, e); else for (o in e) z(t + "[" + o + "]", e[o], i, n)
    }

    function R(t) {
        return function (e, i) {
            "string" != typeof e && (i = e, e = "*");
            var n, o = 0, r = e.toLowerCase().match(mt) || [];
            if (nt.isFunction(i)) for (; n = r[o++];) "+" === n[0] ? (n = n.slice(1) || "*", (t[n] = t[n] || []).unshift(i)) : (t[n] = t[n] || []).push(i)
        }
    }

    function W(t, e, i, n) {
        function o(a) {
            var l;
            return r[a] = !0, nt.each(t[a] || [], function (t, a) {
                var c = a(e, i, n);
                return "string" != typeof c || s || r[c] ? s ? !(l = c) : void 0 : (e.dataTypes.unshift(c), o(c), !1)
            }), l
        }

        var r = {}, s = t === _e;
        return o(e.dataTypes[0]) || !r["*"] && o("*")
    }

    function F(t, e) {
        var i, n, o = nt.ajaxSettings.flatOptions || {};
        for (i in e) void 0 !== e[i] && ((o[i] ? t : n || (n = {}))[i] = e[i]);
        return n && nt.extend(!0, t, n), t
    }

    var V = [], Q = t.document, B = Object.getPrototypeOf, X = V.slice, U = V.concat, Y = V.push, G = V.indexOf, Z = {},
        J = Z.toString, K = Z.hasOwnProperty, tt = K.toString, et = tt.call(Object), it = {}, nt = function (t, e) {
            return new nt.fn.init(t, e)
        }, ot = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, rt = /^-ms-/, st = /-([a-z])/g;
    nt.fn = nt.prototype = {
        jquery: "3.2.1", constructor: nt, length: 0, toArray: function () {
            return X.call(this)
        }, get: function (t) {
            return null == t ? X.call(this) : t < 0 ? this[t + this.length] : this[t]
        }, pushStack: function (t) {
            var e = nt.merge(this.constructor(), t);
            return e.prevObject = this, e
        }, each: function (t) {
            return nt.each(this, t)
        }, map: function (t) {
            return this.pushStack(nt.map(this, function (e, i) {
                return t.call(e, i, e)
            }))
        }, slice: function () {
            return this.pushStack(X.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (t) {
            var e = this.length, i = +t + (t < 0 ? e : 0);
            return this.pushStack(i >= 0 && i < e ? [this[i]] : [])
        }, end: function () {
            return this.prevObject || this.constructor()
        }, push: Y, sort: V.sort, splice: V.splice
    }, nt.extend = nt.fn.extend = function () {
        var t, e, i, n, o, r, s = arguments[0] || {}, a = 1, l = arguments.length, c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || nt.isFunction(s) || (s = {}), a === l && (s = this, a--); a < l; a++) if (null != (t = arguments[a])) for (e in t) i = s[e], s !== (n = t[e]) && (c && n && (nt.isPlainObject(n) || (o = Array.isArray(n))) ? (o ? (o = !1, r = i && Array.isArray(i) ? i : []) : r = i && nt.isPlainObject(i) ? i : {}, s[e] = nt.extend(c, r, n)) : void 0 !== n && (s[e] = n));
        return s
    }, nt.extend({
        expando: "jQuery" + ("3.2.1" + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (t) {
            throw new Error(t)
        }, noop: function () {
        }, isFunction: function (t) {
            return "function" === nt.type(t)
        }, isWindow: function (t) {
            return null != t && t === t.window
        }, isNumeric: function (t) {
            var e = nt.type(t);
            return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t))
        }, isPlainObject: function (t) {
            var e, i;
            return !(!t || "[object Object]" !== J.call(t)) && (!(e = B(t)) || "function" == typeof(i = K.call(e, "constructor") && e.constructor) && tt.call(i) === et)
        }, isEmptyObject: function (t) {
            var e;
            for (e in t) return !1;
            return !0
        }, type: function (t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? Z[J.call(t)] || "object" : typeof t
        }, globalEval: function (t) {
            i(t)
        }, camelCase: function (t) {
            return t.replace(rt, "ms-").replace(st, function (t, e) {
                return e.toUpperCase()
            })
        }, each: function (t, e) {
            var i, o = 0;
            if (n(t)) for (i = t.length; o < i && !1 !== e.call(t[o], o, t[o]); o++) ; else for (o in t) if (!1 === e.call(t[o], o, t[o])) break;
            return t
        }, trim: function (t) {
            return null == t ? "" : (t + "").replace(ot, "")
        }, makeArray: function (t, e) {
            var i = e || [];
            return null != t && (n(Object(t)) ? nt.merge(i, "string" == typeof t ? [t] : t) : Y.call(i, t)), i
        }, inArray: function (t, e, i) {
            return null == e ? -1 : G.call(e, t, i)
        }, merge: function (t, e) {
            for (var i = +e.length, n = 0, o = t.length; n < i; n++) t[o++] = e[n];
            return t.length = o, t
        }, grep: function (t, e, i) {
            for (var n = [], o = 0, r = t.length, s = !i; o < r; o++) !e(t[o], o) !== s && n.push(t[o]);
            return n
        }, map: function (t, e, i) {
            var o, r, s = 0, a = [];
            if (n(t)) for (o = t.length; s < o; s++) null != (r = e(t[s], s, i)) && a.push(r); else for (s in t) null != (r = e(t[s], s, i)) && a.push(r);
            return U.apply([], a)
        }, guid: 1, proxy: function (t, e) {
            var i, n, o;
            if ("string" == typeof e && (i = t[e], e = t, t = i), nt.isFunction(t)) return n = X.call(arguments, 2), o = function () {
                return t.apply(e || this, n.concat(X.call(arguments)))
            }, o.guid = t.guid = t.guid || nt.guid++, o
        }, now: Date.now, support: it
    }), "function" == typeof Symbol && (nt.fn[Symbol.iterator] = V[Symbol.iterator]), nt.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (t, e) {
        Z["[object " + e + "]"] = e.toLowerCase()
    });
    var at = function (t) {
        function e(t, e, i, n) {
            var o, r, s, a, l, u, d, p = e && e.ownerDocument, f = e ? e.nodeType : 9;
            if (i = i || [], "string" != typeof t || !t || 1 !== f && 9 !== f && 11 !== f) return i;
            if (!n && ((e ? e.ownerDocument || e : H) !== $ && A(e), e = e || $, D)) {
                if (11 !== f && (l = pt.exec(t))) if (o = l[1]) {
                    if (9 === f) {
                        if (!(s = e.getElementById(o))) return i;
                        if (s.id === o) return i.push(s), i
                    } else if (p && (s = p.getElementById(o)) && N(e, s) && s.id === o) return i.push(s), i
                } else {
                    if (l[2]) return U.apply(i, e.getElementsByTagName(t)), i;
                    if ((o = l[3]) && y.getElementsByClassName && e.getElementsByClassName) return U.apply(i, e.getElementsByClassName(o)), i
                }
                if (y.qsa && !W[t + " "] && (!O || !O.test(t))) {
                    if (1 !== f) p = e, d = t; else if ("object" !== e.nodeName.toLowerCase()) {
                        for ((a = e.getAttribute("id")) ? a = a.replace(mt, yt) : e.setAttribute("id", a = j), r = (u = C(t)).length; r--;) u[r] = "#" + a + " " + h(u[r]);
                        d = u.join(","), p = ft.test(t) && c(e.parentNode) || e
                    }
                    if (d) try {
                        return U.apply(i, p.querySelectorAll(d)), i
                    } catch (t) {
                    } finally {
                        a === j && e.removeAttribute("id")
                    }
                }
            }
            return T(t.replace(nt, "$1"), e, i, n)
        }

        function i() {
            function t(i, n) {
                return e.push(i + " ") > b.cacheLength && delete t[e.shift()], t[i + " "] = n
            }

            var e = [];
            return t
        }

        function n(t) {
            return t[j] = !0, t
        }

        function o(t) {
            var e = $.createElement("fieldset");
            try {
                return !!t(e)
            } catch (t) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e), e = null
            }
        }

        function r(t, e) {
            for (var i = t.split("|"), n = i.length; n--;) b.attrHandle[i[n]] = e
        }

        function s(t, e) {
            var i = e && t, n = i && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
            if (n) return n;
            if (i) for (; i = i.nextSibling;) if (i === e) return -1;
            return t ? 1 : -1
        }

        function a(t) {
            return function (e) {
                return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && wt(e) === t : e.disabled === t : "label" in e && e.disabled === t
            }
        }

        function l(t) {
            return n(function (e) {
                return e = +e, n(function (i, n) {
                    for (var o, r = t([], i.length, e), s = r.length; s--;) i[o = r[s]] && (i[o] = !(n[o] = i[o]))
                })
            })
        }

        function c(t) {
            return t && void 0 !== t.getElementsByTagName && t
        }

        function u() {
        }

        function h(t) {
            for (var e = 0, i = t.length, n = ""; e < i; e++) n += t[e].value;
            return n
        }

        function d(t, e, i) {
            var n = e.dir, o = e.next, r = o || n, s = i && "parentNode" === r, a = q++;
            return e.first ? function (e, i, o) {
                for (; e = e[n];) if (1 === e.nodeType || s) return t(e, i, o);
                return !1
            } : function (e, i, l) {
                var c, u, h, d = [L, a];
                if (l) {
                    for (; e = e[n];) if ((1 === e.nodeType || s) && t(e, i, l)) return !0
                } else for (; e = e[n];) if (1 === e.nodeType || s) if (h = e[j] || (e[j] = {}), u = h[e.uniqueID] || (h[e.uniqueID] = {}), o && o === e.nodeName.toLowerCase()) e = e[n] || e; else {
                    if ((c = u[r]) && c[0] === L && c[1] === a) return d[2] = c[2];
                    if (u[r] = d, d[2] = t(e, i, l)) return !0
                }
                return !1
            }
        }

        function p(t) {
            return t.length > 1 ? function (e, i, n) {
                for (var o = t.length; o--;) if (!t[o](e, i, n)) return !1;
                return !0
            } : t[0]
        }

        function f(t, e, i, n, o) {
            for (var r, s = [], a = 0, l = t.length, c = null != e; a < l; a++) (r = t[a]) && (i && !i(r, n, o) || (s.push(r), c && e.push(a)));
            return s
        }

        function g(t, i, o, r, s, a) {
            return r && !r[j] && (r = g(r)), s && !s[j] && (s = g(s, a)), n(function (n, a, l, c) {
                var u, h, d, p = [], g = [], v = a.length, m = n || function (t, i, n) {
                        for (var o = 0, r = i.length; o < r; o++) e(t, i[o], n);
                        return n
                    }(i || "*", l.nodeType ? [l] : l, []), y = !t || !n && i ? m : f(m, p, t, l, c),
                    b = o ? s || (n ? t : v || r) ? [] : a : y;
                if (o && o(y, b, l, c), r) for (u = f(b, g), r(u, [], l, c), h = u.length; h--;) (d = u[h]) && (b[g[h]] = !(y[g[h]] = d));
                if (n) {
                    if (s || t) {
                        if (s) {
                            for (u = [], h = b.length; h--;) (d = b[h]) && u.push(y[h] = d);
                            s(null, b = [], u, c)
                        }
                        for (h = b.length; h--;) (d = b[h]) && (u = s ? G(n, d) : p[h]) > -1 && (n[u] = !(a[u] = d))
                    }
                } else b = f(b === a ? b.splice(v, b.length) : b), s ? s(null, a, b, c) : U.apply(a, b)
            })
        }

        function v(t) {
            for (var e, i, n, o = t.length, r = b.relative[t[0].type], s = r || b.relative[" "], a = r ? 1 : 0, l = d(function (t) {
                return t === e
            }, s, !0), c = d(function (t) {
                return G(e, t) > -1
            }, s, !0), u = [function (t, i, n) {
                var o = !r && (n || i !== _) || ((e = i).nodeType ? l(t, i, n) : c(t, i, n));
                return e = null, o
            }]; a < o; a++) if (i = b.relative[t[a].type]) u = [d(p(u), i)]; else {
                if ((i = b.filter[t[a].type].apply(null, t[a].matches))[j]) {
                    for (n = ++a; n < o && !b.relative[t[n].type]; n++) ;
                    return g(a > 1 && p(u), a > 1 && h(t.slice(0, a - 1).concat({value: " " === t[a - 2].type ? "*" : ""})).replace(nt, "$1"), i, a < n && v(t.slice(a, n)), n < o && v(t = t.slice(n)), n < o && h(t))
                }
                u.push(i)
            }
            return p(u)
        }

        var m, y, b, w, x, C, k, T, _, S, E, A, $, P, D, O, M, I, N, j = "sizzle" + 1 * new Date, H = t.document, L = 0,
            q = 0, z = i(), R = i(), W = i(), F = function (t, e) {
                return t === e && (E = !0), 0
            }, V = {}.hasOwnProperty, Q = [], B = Q.pop, X = Q.push, U = Q.push, Y = Q.slice, G = function (t, e) {
                for (var i = 0, n = t.length; i < n; i++) if (t[i] === e) return i;
                return -1
            },
            Z = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            J = "[\\x20\\t\\r\\n\\f]", K = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            tt = "\\[" + J + "*(" + K + ")(?:" + J + "*([*^$|!~]?=)" + J + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + K + "))|)" + J + "*\\]",
            et = ":(" + K + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + tt + ")*)|.*)\\)|)",
            it = new RegExp(J + "+", "g"), nt = new RegExp("^" + J + "+|((?:^|[^\\\\])(?:\\\\.)*)" + J + "+$", "g"),
            ot = new RegExp("^" + J + "*," + J + "*"), rt = new RegExp("^" + J + "*([>+~]|" + J + ")" + J + "*"),
            st = new RegExp("=" + J + "*([^\\]'\"]*?)" + J + "*\\]", "g"), at = new RegExp(et),
            lt = new RegExp("^" + K + "$"), ct = {
                ID: new RegExp("^#(" + K + ")"),
                CLASS: new RegExp("^\\.(" + K + ")"),
                TAG: new RegExp("^(" + K + "|[*])"),
                ATTR: new RegExp("^" + tt),
                PSEUDO: new RegExp("^" + et),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + J + "*(even|odd|(([+-]|)(\\d*)n|)" + J + "*(?:([+-]|)" + J + "*(\\d+)|))" + J + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + Z + ")$", "i"),
                needsContext: new RegExp("^" + J + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + J + "*((?:-\\d)?\\d*)" + J + "*\\)|)(?=[^-]|$)", "i")
            }, ut = /^(?:input|select|textarea|button)$/i, ht = /^h\d$/i, dt = /^[^{]+\{\s*\[native \w/,
            pt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ft = /[+~]/,
            gt = new RegExp("\\\\([\\da-f]{1,6}" + J + "?|(" + J + ")|.)", "ig"), vt = function (t, e, i) {
                var n = "0x" + e - 65536;
                return n != n || i ? e : n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
            }, mt = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, yt = function (t, e) {
                return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t
            }, bt = function () {
                A()
            }, wt = d(function (t) {
                return !0 === t.disabled && ("form" in t || "label" in t)
            }, {dir: "parentNode", next: "legend"});
        try {
            U.apply(Q = Y.call(H.childNodes), H.childNodes), Q[H.childNodes.length].nodeType
        } catch (t) {
            U = {
                apply: Q.length ? function (t, e) {
                    X.apply(t, Y.call(e))
                } : function (t, e) {
                    for (var i = t.length, n = 0; t[i++] = e[n++];) ;
                    t.length = i - 1
                }
            }
        }
        y = e.support = {}, x = e.isXML = function (t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return !!e && "HTML" !== e.nodeName
        }, A = e.setDocument = function (t) {
            var e, i, n = t ? t.ownerDocument || t : H;
            return n !== $ && 9 === n.nodeType && n.documentElement ? ($ = n, P = $.documentElement, D = !x($), H !== $ && (i = $.defaultView) && i.top !== i && (i.addEventListener ? i.addEventListener("unload", bt, !1) : i.attachEvent && i.attachEvent("onunload", bt)), y.attributes = o(function (t) {
                return t.className = "i", !t.getAttribute("className")
            }), y.getElementsByTagName = o(function (t) {
                return t.appendChild($.createComment("")), !t.getElementsByTagName("*").length
            }), y.getElementsByClassName = dt.test($.getElementsByClassName), y.getById = o(function (t) {
                return P.appendChild(t).id = j, !$.getElementsByName || !$.getElementsByName(j).length
            }), y.getById ? (b.filter.ID = function (t) {
                var e = t.replace(gt, vt);
                return function (t) {
                    return t.getAttribute("id") === e
                }
            }, b.find.ID = function (t, e) {
                if (void 0 !== e.getElementById && D) {
                    var i = e.getElementById(t);
                    return i ? [i] : []
                }
            }) : (b.filter.ID = function (t) {
                var e = t.replace(gt, vt);
                return function (t) {
                    var i = void 0 !== t.getAttributeNode && t.getAttributeNode("id");
                    return i && i.value === e
                }
            }, b.find.ID = function (t, e) {
                if (void 0 !== e.getElementById && D) {
                    var i, n, o, r = e.getElementById(t);
                    if (r) {
                        if ((i = r.getAttributeNode("id")) && i.value === t) return [r];
                        for (o = e.getElementsByName(t), n = 0; r = o[n++];) if ((i = r.getAttributeNode("id")) && i.value === t) return [r]
                    }
                    return []
                }
            }), b.find.TAG = y.getElementsByTagName ? function (t, e) {
                return void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t) : y.qsa ? e.querySelectorAll(t) : void 0
            } : function (t, e) {
                var i, n = [], o = 0, r = e.getElementsByTagName(t);
                if ("*" === t) {
                    for (; i = r[o++];) 1 === i.nodeType && n.push(i);
                    return n
                }
                return r
            }, b.find.CLASS = y.getElementsByClassName && function (t, e) {
                if (void 0 !== e.getElementsByClassName && D) return e.getElementsByClassName(t)
            }, M = [], O = [], (y.qsa = dt.test($.querySelectorAll)) && (o(function (t) {
                P.appendChild(t).innerHTML = "<a id='" + j + "'></a><select id='" + j + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && O.push("[*^$]=" + J + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || O.push("\\[" + J + "*(?:value|" + Z + ")"), t.querySelectorAll("[id~=" + j + "-]").length || O.push("~="), t.querySelectorAll(":checked").length || O.push(":checked"), t.querySelectorAll("a#" + j + "+*").length || O.push(".#.+[+~]")
            }), o(function (t) {
                t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var e = $.createElement("input");
                e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && O.push("name" + J + "*[*^$|!~]?="), 2 !== t.querySelectorAll(":enabled").length && O.push(":enabled", ":disabled"), P.appendChild(t).disabled = !0, 2 !== t.querySelectorAll(":disabled").length && O.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), O.push(",.*:")
            })), (y.matchesSelector = dt.test(I = P.matches || P.webkitMatchesSelector || P.mozMatchesSelector || P.oMatchesSelector || P.msMatchesSelector)) && o(function (t) {
                y.disconnectedMatch = I.call(t, "*"), I.call(t, "[s!='']:x"), M.push("!=", et)
            }), O = O.length && new RegExp(O.join("|")), M = M.length && new RegExp(M.join("|")), e = dt.test(P.compareDocumentPosition), N = e || dt.test(P.contains) ? function (t, e) {
                var i = 9 === t.nodeType ? t.documentElement : t, n = e && e.parentNode;
                return t === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(n)))
            } : function (t, e) {
                if (e) for (; e = e.parentNode;) if (e === t) return !0;
                return !1
            }, F = e ? function (t, e) {
                if (t === e) return E = !0, 0;
                var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return i || (1 & (i = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !y.sortDetached && e.compareDocumentPosition(t) === i ? t === $ || t.ownerDocument === H && N(H, t) ? -1 : e === $ || e.ownerDocument === H && N(H, e) ? 1 : S ? G(S, t) - G(S, e) : 0 : 4 & i ? -1 : 1)
            } : function (t, e) {
                if (t === e) return E = !0, 0;
                var i, n = 0, o = t.parentNode, r = e.parentNode, a = [t], l = [e];
                if (!o || !r) return t === $ ? -1 : e === $ ? 1 : o ? -1 : r ? 1 : S ? G(S, t) - G(S, e) : 0;
                if (o === r) return s(t, e);
                for (i = t; i = i.parentNode;) a.unshift(i);
                for (i = e; i = i.parentNode;) l.unshift(i);
                for (; a[n] === l[n];) n++;
                return n ? s(a[n], l[n]) : a[n] === H ? -1 : l[n] === H ? 1 : 0
            }, $) : $
        }, e.matches = function (t, i) {
            return e(t, null, null, i)
        }, e.matchesSelector = function (t, i) {
            if ((t.ownerDocument || t) !== $ && A(t), i = i.replace(st, "='$1']"), y.matchesSelector && D && !W[i + " "] && (!M || !M.test(i)) && (!O || !O.test(i))) try {
                var n = I.call(t, i);
                if (n || y.disconnectedMatch || t.document && 11 !== t.document.nodeType) return n
            } catch (t) {
            }
            return e(i, $, null, [t]).length > 0
        }, e.contains = function (t, e) {
            return (t.ownerDocument || t) !== $ && A(t), N(t, e)
        }, e.attr = function (t, e) {
            (t.ownerDocument || t) !== $ && A(t);
            var i = b.attrHandle[e.toLowerCase()],
                n = i && V.call(b.attrHandle, e.toLowerCase()) ? i(t, e, !D) : void 0;
            return void 0 !== n ? n : y.attributes || !D ? t.getAttribute(e) : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
        }, e.escape = function (t) {
            return (t + "").replace(mt, yt)
        }, e.error = function (t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }, e.uniqueSort = function (t) {
            var e, i = [], n = 0, o = 0;
            if (E = !y.detectDuplicates, S = !y.sortStable && t.slice(0), t.sort(F), E) {
                for (; e = t[o++];) e === t[o] && (n = i.push(o));
                for (; n--;) t.splice(i[n], 1)
            }
            return S = null, t
        }, w = e.getText = function (t) {
            var e, i = "", n = 0, o = t.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof t.textContent) return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling) i += w(t)
                } else if (3 === o || 4 === o) return t.nodeValue
            } else for (; e = t[n++];) i += w(e);
            return i
        }, (b = e.selectors = {
            cacheLength: 50,
            createPseudo: n,
            match: ct,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (t) {
                    return t[1] = t[1].replace(gt, vt), t[3] = (t[3] || t[4] || t[5] || "").replace(gt, vt), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
                }, CHILD: function (t) {
                    return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
                }, PSEUDO: function (t) {
                    var e, i = !t[6] && t[2];
                    return ct.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : i && at.test(i) && (e = C(i, !0)) && (e = i.indexOf(")", i.length - e) - i.length) && (t[0] = t[0].slice(0, e), t[2] = i.slice(0, e)), t.slice(0, 3))
                }
            },
            filter: {
                TAG: function (t) {
                    var e = t.replace(gt, vt).toLowerCase();
                    return "*" === t ? function () {
                        return !0
                    } : function (t) {
                        return t.nodeName && t.nodeName.toLowerCase() === e
                    }
                }, CLASS: function (t) {
                    var e = z[t + " "];
                    return e || (e = new RegExp("(^|" + J + ")" + t + "(" + J + "|$)")) && z(t, function (t) {
                        return e.test("string" == typeof t.className && t.className || void 0 !== t.getAttribute && t.getAttribute("class") || "")
                    })
                }, ATTR: function (t, i, n) {
                    return function (o) {
                        var r = e.attr(o, t);
                        return null == r ? "!=" === i : !i || (r += "", "=" === i ? r === n : "!=" === i ? r !== n : "^=" === i ? n && 0 === r.indexOf(n) : "*=" === i ? n && r.indexOf(n) > -1 : "$=" === i ? n && r.slice(-n.length) === n : "~=" === i ? (" " + r.replace(it, " ") + " ").indexOf(n) > -1 : "|=" === i && (r === n || r.slice(0, n.length + 1) === n + "-"))
                    }
                }, CHILD: function (t, e, i, n, o) {
                    var r = "nth" !== t.slice(0, 3), s = "last" !== t.slice(-4), a = "of-type" === e;
                    return 1 === n && 0 === o ? function (t) {
                        return !!t.parentNode
                    } : function (e, i, l) {
                        var c, u, h, d, p, f, g = r !== s ? "nextSibling" : "previousSibling", v = e.parentNode,
                            m = a && e.nodeName.toLowerCase(), y = !l && !a, b = !1;
                        if (v) {
                            if (r) {
                                for (; g;) {
                                    for (d = e; d = d[g];) if (a ? d.nodeName.toLowerCase() === m : 1 === d.nodeType) return !1;
                                    f = g = "only" === t && !f && "nextSibling"
                                }
                                return !0
                            }
                            if (f = [s ? v.firstChild : v.lastChild], s && y) {
                                for (b = (p = (c = (u = (h = (d = v)[j] || (d[j] = {}))[d.uniqueID] || (h[d.uniqueID] = {}))[t] || [])[0] === L && c[1]) && c[2], d = p && v.childNodes[p]; d = ++p && d && d[g] || (b = p = 0) || f.pop();) if (1 === d.nodeType && ++b && d === e) {
                                    u[t] = [L, p, b];
                                    break
                                }
                            } else if (y && (b = p = (c = (u = (h = (d = e)[j] || (d[j] = {}))[d.uniqueID] || (h[d.uniqueID] = {}))[t] || [])[0] === L && c[1]), !1 === b) for (; (d = ++p && d && d[g] || (b = p = 0) || f.pop()) && ((a ? d.nodeName.toLowerCase() !== m : 1 !== d.nodeType) || !++b || (y && ((u = (h = d[j] || (d[j] = {}))[d.uniqueID] || (h[d.uniqueID] = {}))[t] = [L, b]), d !== e));) ;
                            return (b -= o) === n || b % n == 0 && b / n >= 0
                        }
                    }
                }, PSEUDO: function (t, i) {
                    var o, r = b.pseudos[t] || b.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                    return r[j] ? r(i) : r.length > 1 ? (o = [t, t, "", i], b.setFilters.hasOwnProperty(t.toLowerCase()) ? n(function (t, e) {
                        for (var n, o = r(t, i), s = o.length; s--;) t[n = G(t, o[s])] = !(e[n] = o[s])
                    }) : function (t) {
                        return r(t, 0, o)
                    }) : r
                }
            },
            pseudos: {
                not: n(function (t) {
                    var e = [], i = [], o = k(t.replace(nt, "$1"));
                    return o[j] ? n(function (t, e, i, n) {
                        for (var r, s = o(t, null, n, []), a = t.length; a--;) (r = s[a]) && (t[a] = !(e[a] = r))
                    }) : function (t, n, r) {
                        return e[0] = t, o(e, null, r, i), e[0] = null, !i.pop()
                    }
                }), has: n(function (t) {
                    return function (i) {
                        return e(t, i).length > 0
                    }
                }), contains: n(function (t) {
                    return t = t.replace(gt, vt), function (e) {
                        return (e.textContent || e.innerText || w(e)).indexOf(t) > -1
                    }
                }), lang: n(function (t) {
                    return lt.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(gt, vt).toLowerCase(), function (e) {
                        var i;
                        do {
                            if (i = D ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (i = i.toLowerCase()) === t || 0 === i.indexOf(t + "-")
                        } while ((e = e.parentNode) && 1 === e.nodeType);
                        return !1
                    }
                }), target: function (e) {
                    var i = t.location && t.location.hash;
                    return i && i.slice(1) === e.id
                }, root: function (t) {
                    return t === P
                }, focus: function (t) {
                    return t === $.activeElement && (!$.hasFocus || $.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                }, enabled: a(!1), disabled: a(!0), checked: function (t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && !!t.checked || "option" === e && !!t.selected
                }, selected: function (t) {
                    return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected
                }, empty: function (t) {
                    for (t = t.firstChild; t; t = t.nextSibling) if (t.nodeType < 6) return !1;
                    return !0
                }, parent: function (t) {
                    return !b.pseudos.empty(t)
                }, header: function (t) {
                    return ht.test(t.nodeName)
                }, input: function (t) {
                    return ut.test(t.nodeName)
                }, button: function (t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && "button" === t.type || "button" === e
                }, text: function (t) {
                    var e;
                    return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                }, first: l(function () {
                    return [0]
                }), last: l(function (t, e) {
                    return [e - 1]
                }), eq: l(function (t, e, i) {
                    return [i < 0 ? i + e : i]
                }), even: l(function (t, e) {
                    for (var i = 0; i < e; i += 2) t.push(i);
                    return t
                }), odd: l(function (t, e) {
                    for (var i = 1; i < e; i += 2) t.push(i);
                    return t
                }), lt: l(function (t, e, i) {
                    for (var n = i < 0 ? i + e : i; --n >= 0;) t.push(n);
                    return t
                }), gt: l(function (t, e, i) {
                    for (var n = i < 0 ? i + e : i; ++n < e;) t.push(n);
                    return t
                })
            }
        }).pseudos.nth = b.pseudos.eq;
        for (m in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0}) b.pseudos[m] = function (t) {
            return function (e) {
                return "input" === e.nodeName.toLowerCase() && e.type === t
            }
        }(m);
        for (m in{submit: !0, reset: !0}) b.pseudos[m] = function (t) {
            return function (e) {
                var i = e.nodeName.toLowerCase();
                return ("input" === i || "button" === i) && e.type === t
            }
        }(m);
        return u.prototype = b.filters = b.pseudos, b.setFilters = new u, C = e.tokenize = function (t, i) {
            var n, o, r, s, a, l, c, u = R[t + " "];
            if (u) return i ? 0 : u.slice(0);
            for (a = t, l = [], c = b.preFilter; a;) {
                n && !(o = ot.exec(a)) || (o && (a = a.slice(o[0].length) || a), l.push(r = [])), n = !1, (o = rt.exec(a)) && (n = o.shift(), r.push({
                    value: n,
                    type: o[0].replace(nt, " ")
                }), a = a.slice(n.length));
                for (s in b.filter) !(o = ct[s].exec(a)) || c[s] && !(o = c[s](o)) || (n = o.shift(), r.push({
                    value: n,
                    type: s,
                    matches: o
                }), a = a.slice(n.length));
                if (!n) break
            }
            return i ? a.length : a ? e.error(t) : R(t, l).slice(0)
        }, k = e.compile = function (t, i) {
            var o, r = [], s = [], a = W[t + " "];
            if (!a) {
                for (i || (i = C(t)), o = i.length; o--;) (a = v(i[o]))[j] ? r.push(a) : s.push(a);
                (a = W(t, function (t, i) {
                    var o = i.length > 0, r = t.length > 0, s = function (n, s, a, l, c) {
                        var u, h, d, p = 0, g = "0", v = n && [], m = [], y = _, w = n || r && b.find.TAG("*", c),
                            x = L += null == y ? 1 : Math.random() || .1, C = w.length;
                        for (c && (_ = s === $ || s || c); g !== C && null != (u = w[g]); g++) {
                            if (r && u) {
                                for (h = 0, s || u.ownerDocument === $ || (A(u), a = !D); d = t[h++];) if (d(u, s || $, a)) {
                                    l.push(u);
                                    break
                                }
                                c && (L = x)
                            }
                            o && ((u = !d && u) && p--, n && v.push(u))
                        }
                        if (p += g, o && g !== p) {
                            for (h = 0; d = i[h++];) d(v, m, s, a);
                            if (n) {
                                if (p > 0) for (; g--;) v[g] || m[g] || (m[g] = B.call(l));
                                m = f(m)
                            }
                            U.apply(l, m), c && !n && m.length > 0 && p + i.length > 1 && e.uniqueSort(l)
                        }
                        return c && (L = x, _ = y), v
                    };
                    return o ? n(s) : s
                }(s, r))).selector = t
            }
            return a
        }, T = e.select = function (t, e, i, n) {
            var o, r, s, a, l, u = "function" == typeof t && t, d = !n && C(t = u.selector || t);
            if (i = i || [], 1 === d.length) {
                if ((r = d[0] = d[0].slice(0)).length > 2 && "ID" === (s = r[0]).type && 9 === e.nodeType && D && b.relative[r[1].type]) {
                    if (!(e = (b.find.ID(s.matches[0].replace(gt, vt), e) || [])[0])) return i;
                    u && (e = e.parentNode), t = t.slice(r.shift().value.length)
                }
                for (o = ct.needsContext.test(t) ? 0 : r.length; o-- && (s = r[o], !b.relative[a = s.type]);) if ((l = b.find[a]) && (n = l(s.matches[0].replace(gt, vt), ft.test(r[0].type) && c(e.parentNode) || e))) {
                    if (r.splice(o, 1), !(t = n.length && h(r))) return U.apply(i, n), i;
                    break
                }
            }
            return (u || k(t, d))(n, e, !D, i, !e || ft.test(t) && c(e.parentNode) || e), i
        }, y.sortStable = j.split("").sort(F).join("") === j, y.detectDuplicates = !!E, A(), y.sortDetached = o(function (t) {
            return 1 & t.compareDocumentPosition($.createElement("fieldset"))
        }), o(function (t) {
            return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function (t, e, i) {
            if (!i) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }), y.attributes && o(function (t) {
            return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
        }) || r("value", function (t, e, i) {
            if (!i && "input" === t.nodeName.toLowerCase()) return t.defaultValue
        }), o(function (t) {
            return null == t.getAttribute("disabled")
        }) || r(Z, function (t, e, i) {
            var n;
            if (!i) return !0 === t[e] ? e.toLowerCase() : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
        }), e
    }(t);
    nt.find = at, nt.expr = at.selectors, nt.expr[":"] = nt.expr.pseudos, nt.uniqueSort = nt.unique = at.uniqueSort, nt.text = at.getText, nt.isXMLDoc = at.isXML, nt.contains = at.contains, nt.escapeSelector = at.escape;
    var lt = function (t, e, i) {
            for (var n = [], o = void 0 !== i; (t = t[e]) && 9 !== t.nodeType;) if (1 === t.nodeType) {
                if (o && nt(t).is(i)) break;
                n.push(t)
            }
            return n
        }, ct = function (t, e) {
            for (var i = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && i.push(t);
            return i
        }, ut = nt.expr.match.needsContext, ht = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        dt = /^.[^:#\[\.,]*$/;
    nt.filter = function (t, e, i) {
        var n = e[0];
        return i && (t = ":not(" + t + ")"), 1 === e.length && 1 === n.nodeType ? nt.find.matchesSelector(n, t) ? [n] : [] : nt.find.matches(t, nt.grep(e, function (t) {
            return 1 === t.nodeType
        }))
    }, nt.fn.extend({
        find: function (t) {
            var e, i, n = this.length, o = this;
            if ("string" != typeof t) return this.pushStack(nt(t).filter(function () {
                for (e = 0; e < n; e++) if (nt.contains(o[e], this)) return !0
            }));
            for (i = this.pushStack([]), e = 0; e < n; e++) nt.find(t, o[e], i);
            return n > 1 ? nt.uniqueSort(i) : i
        }, filter: function (t) {
            return this.pushStack(r(this, t || [], !1))
        }, not: function (t) {
            return this.pushStack(r(this, t || [], !0))
        }, is: function (t) {
            return !!r(this, "string" == typeof t && ut.test(t) ? nt(t) : t || [], !1).length
        }
    });
    var pt, ft = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (nt.fn.init = function (t, e, i) {
        var n, o;
        if (!t) return this;
        if (i = i || pt, "string" == typeof t) {
            if (!(n = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : ft.exec(t)) || !n[1] && e) return !e || e.jquery ? (e || i).find(t) : this.constructor(e).find(t);
            if (n[1]) {
                if (e = e instanceof nt ? e[0] : e, nt.merge(this, nt.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : Q, !0)), ht.test(n[1]) && nt.isPlainObject(e)) for (n in e) nt.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                return this
            }
            return (o = Q.getElementById(n[2])) && (this[0] = o, this.length = 1), this
        }
        return t.nodeType ? (this[0] = t, this.length = 1, this) : nt.isFunction(t) ? void 0 !== i.ready ? i.ready(t) : t(nt) : nt.makeArray(t, this)
    }).prototype = nt.fn, pt = nt(Q);
    var gt = /^(?:parents|prev(?:Until|All))/, vt = {children: !0, contents: !0, next: !0, prev: !0};
    nt.fn.extend({
        has: function (t) {
            var e = nt(t, this), i = e.length;
            return this.filter(function () {
                for (var t = 0; t < i; t++) if (nt.contains(this, e[t])) return !0
            })
        }, closest: function (t, e) {
            var i, n = 0, o = this.length, r = [], s = "string" != typeof t && nt(t);
            if (!ut.test(t)) for (; n < o; n++) for (i = this[n]; i && i !== e; i = i.parentNode) if (i.nodeType < 11 && (s ? s.index(i) > -1 : 1 === i.nodeType && nt.find.matchesSelector(i, t))) {
                r.push(i);
                break
            }
            return this.pushStack(r.length > 1 ? nt.uniqueSort(r) : r)
        }, index: function (t) {
            return t ? "string" == typeof t ? G.call(nt(t), this[0]) : G.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (t, e) {
            return this.pushStack(nt.uniqueSort(nt.merge(this.get(), nt(t, e))))
        }, addBack: function (t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }
    }), nt.each({
        parent: function (t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null
        }, parents: function (t) {
            return lt(t, "parentNode")
        }, parentsUntil: function (t, e, i) {
            return lt(t, "parentNode", i)
        }, next: function (t) {
            return s(t, "nextSibling")
        }, prev: function (t) {
            return s(t, "previousSibling")
        }, nextAll: function (t) {
            return lt(t, "nextSibling")
        }, prevAll: function (t) {
            return lt(t, "previousSibling")
        }, nextUntil: function (t, e, i) {
            return lt(t, "nextSibling", i)
        }, prevUntil: function (t, e, i) {
            return lt(t, "previousSibling", i)
        }, siblings: function (t) {
            return ct((t.parentNode || {}).firstChild, t)
        }, children: function (t) {
            return ct(t.firstChild)
        }, contents: function (t) {
            return o(t, "iframe") ? t.contentDocument : (o(t, "template") && (t = t.content || t), nt.merge([], t.childNodes))
        }
    }, function (t, e) {
        nt.fn[t] = function (i, n) {
            var o = nt.map(this, e, i);
            return "Until" !== t.slice(-5) && (n = i), n && "string" == typeof n && (o = nt.filter(n, o)), this.length > 1 && (vt[t] || nt.uniqueSort(o), gt.test(t) && o.reverse()), this.pushStack(o)
        }
    });
    var mt = /[^\x20\t\r\n\f]+/g;
    nt.Callbacks = function (t) {
        t = "string" == typeof t ? function (t) {
            var e = {};
            return nt.each(t.match(mt) || [], function (t, i) {
                e[i] = !0
            }), e
        }(t) : nt.extend({}, t);
        var e, i, n, o, r = [], s = [], a = -1, l = function () {
            for (o = o || t.once, n = e = !0; s.length; a = -1) for (i = s.shift(); ++a < r.length;) !1 === r[a].apply(i[0], i[1]) && t.stopOnFalse && (a = r.length, i = !1);
            t.memory || (i = !1), e = !1, o && (r = i ? [] : "")
        }, c = {
            add: function () {
                return r && (i && !e && (a = r.length - 1, s.push(i)), function e(i) {
                    nt.each(i, function (i, n) {
                        nt.isFunction(n) ? t.unique && c.has(n) || r.push(n) : n && n.length && "string" !== nt.type(n) && e(n)
                    })
                }(arguments), i && !e && l()), this
            }, remove: function () {
                return nt.each(arguments, function (t, e) {
                    for (var i; (i = nt.inArray(e, r, i)) > -1;) r.splice(i, 1), i <= a && a--
                }), this
            }, has: function (t) {
                return t ? nt.inArray(t, r) > -1 : r.length > 0
            }, empty: function () {
                return r && (r = []), this
            }, disable: function () {
                return o = s = [], r = i = "", this
            }, disabled: function () {
                return !r
            }, lock: function () {
                return o = s = [], i || e || (r = i = ""), this
            }, locked: function () {
                return !!o
            }, fireWith: function (t, i) {
                return o || (i = [t, (i = i || []).slice ? i.slice() : i], s.push(i), e || l()), this
            }, fire: function () {
                return c.fireWith(this, arguments), this
            }, fired: function () {
                return !!n
            }
        };
        return c
    }, nt.extend({
        Deferred: function (e) {
            var i = [["notify", "progress", nt.Callbacks("memory"), nt.Callbacks("memory"), 2], ["resolve", "done", nt.Callbacks("once memory"), nt.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", nt.Callbacks("once memory"), nt.Callbacks("once memory"), 1, "rejected"]],
                n = "pending", o = {
                    state: function () {
                        return n
                    }, always: function () {
                        return r.done(arguments).fail(arguments), this
                    }, catch: function (t) {
                        return o.then(null, t)
                    }, pipe: function () {
                        var t = arguments;
                        return nt.Deferred(function (e) {
                            nt.each(i, function (i, n) {
                                var o = nt.isFunction(t[n[4]]) && t[n[4]];
                                r[n[1]](function () {
                                    var t = o && o.apply(this, arguments);
                                    t && nt.isFunction(t.promise) ? t.promise().progress(e.notify).done(e.resolve).fail(e.reject) : e[n[0] + "With"](this, o ? [t] : arguments)
                                })
                            }), t = null
                        }).promise()
                    }, then: function (e, n, o) {
                        function r(e, i, n, o) {
                            return function () {
                                var c = this, u = arguments, h = function () {
                                    var t, h;
                                    if (!(e < s)) {
                                        if ((t = n.apply(c, u)) === i.promise()) throw new TypeError("Thenable self-resolution");
                                        h = t && ("object" == typeof t || "function" == typeof t) && t.then, nt.isFunction(h) ? o ? h.call(t, r(s, i, a, o), r(s, i, l, o)) : (s++, h.call(t, r(s, i, a, o), r(s, i, l, o), r(s, i, a, i.notifyWith))) : (n !== a && (c = void 0, u = [t]), (o || i.resolveWith)(c, u))
                                    }
                                }, d = o ? h : function () {
                                    try {
                                        h()
                                    } catch (t) {
                                        nt.Deferred.exceptionHook && nt.Deferred.exceptionHook(t, d.stackTrace), e + 1 >= s && (n !== l && (c = void 0, u = [t]), i.rejectWith(c, u))
                                    }
                                };
                                e ? d() : (nt.Deferred.getStackHook && (d.stackTrace = nt.Deferred.getStackHook()), t.setTimeout(d))
                            }
                        }

                        var s = 0;
                        return nt.Deferred(function (t) {
                            i[0][3].add(r(0, t, nt.isFunction(o) ? o : a, t.notifyWith)), i[1][3].add(r(0, t, nt.isFunction(e) ? e : a)), i[2][3].add(r(0, t, nt.isFunction(n) ? n : l))
                        }).promise()
                    }, promise: function (t) {
                        return null != t ? nt.extend(t, o) : o
                    }
                }, r = {};
            return nt.each(i, function (t, e) {
                var s = e[2], a = e[5];
                o[e[1]] = s.add, a && s.add(function () {
                    n = a
                }, i[3 - t][2].disable, i[0][2].lock), s.add(e[3].fire), r[e[0]] = function () {
                    return r[e[0] + "With"](this === r ? void 0 : this, arguments), this
                }, r[e[0] + "With"] = s.fireWith
            }), o.promise(r), e && e.call(r, r), r
        }, when: function (t) {
            var e = arguments.length, i = e, n = Array(i), o = X.call(arguments), r = nt.Deferred(), s = function (t) {
                return function (i) {
                    n[t] = this, o[t] = arguments.length > 1 ? X.call(arguments) : i, --e || r.resolveWith(n, o)
                }
            };
            if (e <= 1 && (c(t, r.done(s(i)).resolve, r.reject, !e), "pending" === r.state() || nt.isFunction(o[i] && o[i].then))) return r.then();
            for (; i--;) c(o[i], s(i), r.reject);
            return r.promise()
        }
    });
    var yt = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    nt.Deferred.exceptionHook = function (e, i) {
        t.console && t.console.warn && e && yt.test(e.name) && t.console.warn("jQuery.Deferred exception: " + e.message, e.stack, i)
    }, nt.readyException = function (e) {
        t.setTimeout(function () {
            throw e
        })
    };
    var bt = nt.Deferred();
    nt.fn.ready = function (t) {
        return bt.then(t).catch(function (t) {
            nt.readyException(t)
        }), this
    }, nt.extend({
        isReady: !1, readyWait: 1, ready: function (t) {
            (!0 === t ? --nt.readyWait : nt.isReady) || (nt.isReady = !0, !0 !== t && --nt.readyWait > 0 || bt.resolveWith(Q, [nt]))
        }
    }), nt.ready.then = bt.then, "complete" === Q.readyState || "loading" !== Q.readyState && !Q.documentElement.doScroll ? t.setTimeout(nt.ready) : (Q.addEventListener("DOMContentLoaded", u), t.addEventListener("load", u));
    var wt = function (t, e, i, n, o, r, s) {
        var a = 0, l = t.length, c = null == i;
        if ("object" === nt.type(i)) {
            o = !0;
            for (a in i) wt(t, e, a, i[a], !0, r, s)
        } else if (void 0 !== n && (o = !0, nt.isFunction(n) || (s = !0), c && (s ? (e.call(t, n), e = null) : (c = e, e = function (t, e, i) {
                return c.call(nt(t), i)
            })), e)) for (; a < l; a++) e(t[a], i, s ? n : n.call(t[a], a, e(t[a], i)));
        return o ? t : c ? e.call(t) : l ? e(t[0], i) : r
    }, xt = function (t) {
        return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
    };
    h.uid = 1, h.prototype = {
        cache: function (t) {
            var e = t[this.expando];
            return e || (e = {}, xt(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
                value: e,
                configurable: !0
            }))), e
        }, set: function (t, e, i) {
            var n, o = this.cache(t);
            if ("string" == typeof e) o[nt.camelCase(e)] = i; else for (n in e) o[nt.camelCase(n)] = e[n];
            return o
        }, get: function (t, e) {
            return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][nt.camelCase(e)]
        }, access: function (t, e, i) {
            return void 0 === e || e && "string" == typeof e && void 0 === i ? this.get(t, e) : (this.set(t, e, i), void 0 !== i ? i : e)
        }, remove: function (t, e) {
            var i, n = t[this.expando];
            if (void 0 !== n) {
                if (void 0 !== e) {
                    i = (e = Array.isArray(e) ? e.map(nt.camelCase) : (e = nt.camelCase(e)) in n ? [e] : e.match(mt) || []).length;
                    for (; i--;) delete n[e[i]]
                }
                (void 0 === e || nt.isEmptyObject(n)) && (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando])
            }
        }, hasData: function (t) {
            var e = t[this.expando];
            return void 0 !== e && !nt.isEmptyObject(e)
        }
    };
    var Ct = new h, kt = new h, Tt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, _t = /[A-Z]/g;
    nt.extend({
        hasData: function (t) {
            return kt.hasData(t) || Ct.hasData(t)
        }, data: function (t, e, i) {
            return kt.access(t, e, i)
        }, removeData: function (t, e) {
            kt.remove(t, e)
        }, _data: function (t, e, i) {
            return Ct.access(t, e, i)
        }, _removeData: function (t, e) {
            Ct.remove(t, e)
        }
    }), nt.fn.extend({
        data: function (t, e) {
            var i, n, o, r = this[0], s = r && r.attributes;
            if (void 0 === t) {
                if (this.length && (o = kt.get(r), 1 === r.nodeType && !Ct.get(r, "hasDataAttrs"))) {
                    for (i = s.length; i--;) s[i] && 0 === (n = s[i].name).indexOf("data-") && (n = nt.camelCase(n.slice(5)), d(r, n, o[n]));
                    Ct.set(r, "hasDataAttrs", !0)
                }
                return o
            }
            return "object" == typeof t ? this.each(function () {
                kt.set(this, t)
            }) : wt(this, function (e) {
                var i;
                if (r && void 0 === e) {
                    if (void 0 !== (i = kt.get(r, t))) return i;
                    if (void 0 !== (i = d(r, t))) return i
                } else this.each(function () {
                    kt.set(this, t, e)
                })
            }, null, e, arguments.length > 1, null, !0)
        }, removeData: function (t) {
            return this.each(function () {
                kt.remove(this, t)
            })
        }
    }), nt.extend({
        queue: function (t, e, i) {
            var n;
            if (t) return e = (e || "fx") + "queue", n = Ct.get(t, e), i && (!n || Array.isArray(i) ? n = Ct.access(t, e, nt.makeArray(i)) : n.push(i)), n || []
        }, dequeue: function (t, e) {
            e = e || "fx";
            var i = nt.queue(t, e), n = i.length, o = i.shift(), r = nt._queueHooks(t, e);
            "inprogress" === o && (o = i.shift(), n--), o && ("fx" === e && i.unshift("inprogress"), delete r.stop, o.call(t, function () {
                nt.dequeue(t, e)
            }, r)), !n && r && r.empty.fire()
        }, _queueHooks: function (t, e) {
            var i = e + "queueHooks";
            return Ct.get(t, i) || Ct.access(t, i, {
                empty: nt.Callbacks("once memory").add(function () {
                    Ct.remove(t, [e + "queue", i])
                })
            })
        }
    }), nt.fn.extend({
        queue: function (t, e) {
            var i = 2;
            return "string" != typeof t && (e = t, t = "fx", i--), arguments.length < i ? nt.queue(this[0], t) : void 0 === e ? this : this.each(function () {
                var i = nt.queue(this, t, e);
                nt._queueHooks(this, t), "fx" === t && "inprogress" !== i[0] && nt.dequeue(this, t)
            })
        }, dequeue: function (t) {
            return this.each(function () {
                nt.dequeue(this, t)
            })
        }, clearQueue: function (t) {
            return this.queue(t || "fx", [])
        }, promise: function (t, e) {
            var i, n = 1, o = nt.Deferred(), r = this, s = this.length, a = function () {
                --n || o.resolveWith(r, [r])
            };
            for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; s--;) (i = Ct.get(r[s], t + "queueHooks")) && i.empty && (n++, i.empty.add(a));
            return a(), o.promise(e)
        }
    });
    var St = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Et = new RegExp("^(?:([+-])=|)(" + St + ")([a-z%]*)$", "i"),
        At = ["Top", "Right", "Bottom", "Left"], $t = function (t, e) {
            return "none" === (t = e || t).style.display || "" === t.style.display && nt.contains(t.ownerDocument, t) && "none" === nt.css(t, "display")
        }, Pt = function (t, e, i, n) {
            var o, r, s = {};
            for (r in e) s[r] = t.style[r], t.style[r] = e[r];
            o = i.apply(t, n || []);
            for (r in e) t.style[r] = s[r];
            return o
        }, Dt = {};
    nt.fn.extend({
        show: function () {
            return f(this, !0)
        }, hide: function () {
            return f(this)
        }, toggle: function (t) {
            return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
                $t(this) ? nt(this).show() : nt(this).hide()
            })
        }
    });
    var Ot = /^(?:checkbox|radio)$/i, Mt = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i, It = /^$|\/(?:java|ecma)script/i, Nt = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
    };
    Nt.optgroup = Nt.option, Nt.tbody = Nt.tfoot = Nt.colgroup = Nt.caption = Nt.thead, Nt.th = Nt.td;
    var jt = /<|&#?\w+;/;
    !function () {
        var t = Q.createDocumentFragment().appendChild(Q.createElement("div")), e = Q.createElement("input");
        e.setAttribute("type", "radio"), e.setAttribute("checked", "checked"), e.setAttribute("name", "t"), t.appendChild(e), it.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", it.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
    }();
    var Ht = Q.documentElement, Lt = /^key/, qt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        zt = /^([^.]*)(?:\.(.+)|)/;
    nt.event = {
        global: {}, add: function (t, e, i, n, o) {
            var r, s, a, l, c, u, h, d, p, f, g, v = Ct.get(t);
            if (v) for (i.handler && (i = (r = i).handler, o = r.selector), o && nt.find.matchesSelector(Ht, o), i.guid || (i.guid = nt.guid++), (l = v.events) || (l = v.events = {}), (s = v.handle) || (s = v.handle = function (e) {
                return void 0 !== nt && nt.event.triggered !== e.type ? nt.event.dispatch.apply(t, arguments) : void 0
            }), c = (e = (e || "").match(mt) || [""]).length; c--;) p = g = (a = zt.exec(e[c]) || [])[1], f = (a[2] || "").split(".").sort(), p && (h = nt.event.special[p] || {}, p = (o ? h.delegateType : h.bindType) || p, h = nt.event.special[p] || {}, u = nt.extend({
                type: p,
                origType: g,
                data: n,
                handler: i,
                guid: i.guid,
                selector: o,
                needsContext: o && nt.expr.match.needsContext.test(o),
                namespace: f.join(".")
            }, r), (d = l[p]) || ((d = l[p] = []).delegateCount = 0, h.setup && !1 !== h.setup.call(t, n, f, s) || t.addEventListener && t.addEventListener(p, s)), h.add && (h.add.call(t, u), u.handler.guid || (u.handler.guid = i.guid)), o ? d.splice(d.delegateCount++, 0, u) : d.push(u), nt.event.global[p] = !0)
        }, remove: function (t, e, i, n, o) {
            var r, s, a, l, c, u, h, d, p, f, g, v = Ct.hasData(t) && Ct.get(t);
            if (v && (l = v.events)) {
                for (c = (e = (e || "").match(mt) || [""]).length; c--;) if (a = zt.exec(e[c]) || [], p = g = a[1], f = (a[2] || "").split(".").sort(), p) {
                    for (h = nt.event.special[p] || {}, d = l[p = (n ? h.delegateType : h.bindType) || p] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = d.length; r--;) u = d[r], !o && g !== u.origType || i && i.guid !== u.guid || a && !a.test(u.namespace) || n && n !== u.selector && ("**" !== n || !u.selector) || (d.splice(r, 1), u.selector && d.delegateCount--, h.remove && h.remove.call(t, u));
                    s && !d.length && (h.teardown && !1 !== h.teardown.call(t, f, v.handle) || nt.removeEvent(t, p, v.handle), delete l[p])
                } else for (p in l) nt.event.remove(t, p + e[c], i, n, !0);
                nt.isEmptyObject(l) && Ct.remove(t, "handle events")
            }
        }, dispatch: function (t) {
            var e, i, n, o, r, s, a = nt.event.fix(t), l = new Array(arguments.length),
                c = (Ct.get(this, "events") || {})[a.type] || [], u = nt.event.special[a.type] || {};
            for (l[0] = a, e = 1; e < arguments.length; e++) l[e] = arguments[e];
            if (a.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, a)) {
                for (s = nt.event.handlers.call(this, a, c), e = 0; (o = s[e++]) && !a.isPropagationStopped();) for (a.currentTarget = o.elem, i = 0; (r = o.handlers[i++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r, a.data = r.data, void 0 !== (n = ((nt.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l)) && !1 === (a.result = n) && (a.preventDefault(), a.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, a), a.result
            }
        }, handlers: function (t, e) {
            var i, n, o, r, s, a = [], l = e.delegateCount, c = t.target;
            if (l && c.nodeType && !("click" === t.type && t.button >= 1)) for (; c !== this; c = c.parentNode || this) if (1 === c.nodeType && ("click" !== t.type || !0 !== c.disabled)) {
                for (r = [], s = {}, i = 0; i < l; i++) void 0 === s[o = (n = e[i]).selector + " "] && (s[o] = n.needsContext ? nt(o, this).index(c) > -1 : nt.find(o, this, null, [c]).length), s[o] && r.push(n);
                r.length && a.push({elem: c, handlers: r})
            }
            return c = this, l < e.length && a.push({elem: c, handlers: e.slice(l)}), a
        }, addProp: function (t, e) {
            Object.defineProperty(nt.Event.prototype, t, {
                enumerable: !0,
                configurable: !0,
                get: nt.isFunction(e) ? function () {
                    if (this.originalEvent) return e(this.originalEvent)
                } : function () {
                    if (this.originalEvent) return this.originalEvent[t]
                },
                set: function (e) {
                    Object.defineProperty(this, t, {enumerable: !0, configurable: !0, writable: !0, value: e})
                }
            })
        }, fix: function (t) {
            return t[nt.expando] ? t : new nt.Event(t)
        }, special: {
            load: {noBubble: !0}, focus: {
                trigger: function () {
                    if (this !== w() && this.focus) return this.focus(), !1
                }, delegateType: "focusin"
            }, blur: {
                trigger: function () {
                    if (this === w() && this.blur) return this.blur(), !1
                }, delegateType: "focusout"
            }, click: {
                trigger: function () {
                    if ("checkbox" === this.type && this.click && o(this, "input")) return this.click(), !1
                }, _default: function (t) {
                    return o(t.target, "a")
                }
            }, beforeunload: {
                postDispatch: function (t) {
                    void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
                }
            }
        }
    }, nt.removeEvent = function (t, e, i) {
        t.removeEventListener && t.removeEventListener(e, i)
    }, nt.Event = function (t, e) {
        if (!(this instanceof nt.Event)) return new nt.Event(t, e);
        t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? y : b, this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target, this.currentTarget = t.currentTarget, this.relatedTarget = t.relatedTarget) : this.type = t, e && nt.extend(this, e), this.timeStamp = t && t.timeStamp || nt.now(), this[nt.expando] = !0
    }, nt.Event.prototype = {
        constructor: nt.Event,
        isDefaultPrevented: b,
        isPropagationStopped: b,
        isImmediatePropagationStopped: b,
        isSimulated: !1,
        preventDefault: function () {
            var t = this.originalEvent;
            this.isDefaultPrevented = y, t && !this.isSimulated && t.preventDefault()
        },
        stopPropagation: function () {
            var t = this.originalEvent;
            this.isPropagationStopped = y, t && !this.isSimulated && t.stopPropagation()
        },
        stopImmediatePropagation: function () {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = y, t && !this.isSimulated && t.stopImmediatePropagation(), this.stopPropagation()
        }
    }, nt.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function (t) {
            var e = t.button;
            return null == t.which && Lt.test(t.type) ? null != t.charCode ? t.charCode : t.keyCode : !t.which && void 0 !== e && qt.test(t.type) ? 1 & e ? 1 : 2 & e ? 3 : 4 & e ? 2 : 0 : t.which
        }
    }, nt.event.addProp), nt.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (t, e) {
        nt.event.special[t] = {
            delegateType: e, bindType: e, handle: function (t) {
                var i, n = t.relatedTarget, o = t.handleObj;
                return n && (n === this || nt.contains(this, n)) || (t.type = o.origType, i = o.handler.apply(this, arguments), t.type = e), i
            }
        }
    }), nt.fn.extend({
        on: function (t, e, i, n) {
            return x(this, t, e, i, n)
        }, one: function (t, e, i, n) {
            return x(this, t, e, i, n, 1)
        }, off: function (t, e, i) {
            var n, o;
            if (t && t.preventDefault && t.handleObj) return n = t.handleObj, nt(t.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
            if ("object" == typeof t) {
                for (o in t) this.off(o, e, t[o]);
                return this
            }
            return !1 !== e && "function" != typeof e || (i = e, e = void 0), !1 === i && (i = b), this.each(function () {
                nt.event.remove(this, t, i, e)
            })
        }
    });
    var Rt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        Wt = /<script|<style|<link/i, Ft = /checked\s*(?:[^=]|=\s*.checked.)/i, Vt = /^true\/(.*)/,
        Qt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    nt.extend({
        htmlPrefilter: function (t) {
            return t.replace(Rt, "<$1></$2>")
        }, clone: function (t, e, i) {
            var n, o, r, s, a = t.cloneNode(!0), l = nt.contains(t.ownerDocument, t);
            if (!(it.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || nt.isXMLDoc(t))) for (s = g(a), n = 0, o = (r = g(t)).length; n < o; n++) !function (t, e) {
                var i = e.nodeName.toLowerCase();
                "input" === i && Ot.test(t.type) ? e.checked = t.checked : "input" !== i && "textarea" !== i || (e.defaultValue = t.defaultValue)
            }(r[n], s[n]);
            if (e) if (i) for (r = r || g(t), s = s || g(a), n = 0, o = r.length; n < o; n++) k(r[n], s[n]); else k(t, a);
            return (s = g(a, "script")).length > 0 && v(s, !l && g(t, "script")), a
        }, cleanData: function (t) {
            for (var e, i, n, o = nt.event.special, r = 0; void 0 !== (i = t[r]); r++) if (xt(i)) {
                if (e = i[Ct.expando]) {
                    if (e.events) for (n in e.events) o[n] ? nt.event.remove(i, n) : nt.removeEvent(i, n, e.handle);
                    i[Ct.expando] = void 0
                }
                i[kt.expando] && (i[kt.expando] = void 0)
            }
        }
    }), nt.fn.extend({
        detach: function (t) {
            return _(this, t, !0)
        }, remove: function (t) {
            return _(this, t)
        }, text: function (t) {
            return wt(this, function (t) {
                return void 0 === t ? nt.text(this) : this.empty().each(function () {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t)
                })
            }, null, t, arguments.length)
        }, append: function () {
            return T(this, arguments, function (t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    C(this, t).appendChild(t)
                }
            })
        }, prepend: function () {
            return T(this, arguments, function (t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = C(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        }, before: function () {
            return T(this, arguments, function (t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        }, after: function () {
            return T(this, arguments, function (t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        }, empty: function () {
            for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (nt.cleanData(g(t, !1)), t.textContent = "");
            return this
        }, clone: function (t, e) {
            return t = null != t && t, e = null == e ? t : e, this.map(function () {
                return nt.clone(this, t, e)
            })
        }, html: function (t) {
            return wt(this, function (t) {
                var e = this[0] || {}, i = 0, n = this.length;
                if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
                if ("string" == typeof t && !Wt.test(t) && !Nt[(Mt.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = nt.htmlPrefilter(t);
                    try {
                        for (; i < n; i++) 1 === (e = this[i] || {}).nodeType && (nt.cleanData(g(e, !1)), e.innerHTML = t);
                        e = 0
                    } catch (t) {
                    }
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        }, replaceWith: function () {
            var t = [];
            return T(this, arguments, function (e) {
                var i = this.parentNode;
                nt.inArray(this, t) < 0 && (nt.cleanData(g(this)), i && i.replaceChild(e, this))
            }, t)
        }
    }), nt.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (t, e) {
        nt.fn[t] = function (t) {
            for (var i, n = [], o = nt(t), r = o.length - 1, s = 0; s <= r; s++) i = s === r ? this : this.clone(!0), nt(o[s])[e](i), Y.apply(n, i.get());
            return this.pushStack(n)
        }
    });
    var Bt = /^margin/, Xt = new RegExp("^(" + St + ")(?!px)[a-z%]+$", "i"), Ut = function (e) {
        var i = e.ownerDocument.defaultView;
        return i && i.opener || (i = t), i.getComputedStyle(e)
    };
    !function () {
        function e() {
            if (a) {
                a.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", a.innerHTML = "", Ht.appendChild(s);
                var e = t.getComputedStyle(a);
                i = "1%" !== e.top, r = "2px" === e.marginLeft, n = "4px" === e.width, a.style.marginRight = "50%", o = "4px" === e.marginRight, Ht.removeChild(s), a = null
            }
        }

        var i, n, o, r, s = Q.createElement("div"), a = Q.createElement("div");
        a.style && (a.style.backgroundClip = "content-box", a.cloneNode(!0).style.backgroundClip = "", it.clearCloneStyle = "content-box" === a.style.backgroundClip, s.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", s.appendChild(a), nt.extend(it, {
            pixelPosition: function () {
                return e(), i
            }, boxSizingReliable: function () {
                return e(), n
            }, pixelMarginRight: function () {
                return e(), o
            }, reliableMarginLeft: function () {
                return e(), r
            }
        }))
    }();
    var Yt = /^(none|table(?!-c[ea]).+)/, Gt = /^--/,
        Zt = {position: "absolute", visibility: "hidden", display: "block"},
        Jt = {letterSpacing: "0", fontWeight: "400"}, Kt = ["Webkit", "Moz", "ms"], te = Q.createElement("div").style;
    nt.extend({
        cssHooks: {
            opacity: {
                get: function (t, e) {
                    if (e) {
                        var i = S(t, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {float: "cssFloat"},
        style: function (t, e, i, n) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var o, r, s, a = nt.camelCase(e), l = Gt.test(e), c = t.style;
                if (l || (e = A(a)), s = nt.cssHooks[e] || nt.cssHooks[a], void 0 === i) return s && "get" in s && void 0 !== (o = s.get(t, !1, n)) ? o : c[e];
                "string" == (r = typeof i) && (o = Et.exec(i)) && o[1] && (i = p(t, e, o), r = "number"), null != i && i == i && ("number" === r && (i += o && o[3] || (nt.cssNumber[a] ? "" : "px")), it.clearCloneStyle || "" !== i || 0 !== e.indexOf("background") || (c[e] = "inherit"), s && "set" in s && void 0 === (i = s.set(t, i, n)) || (l ? c.setProperty(e, i) : c[e] = i))
            }
        },
        css: function (t, e, i, n) {
            var o, r, s, a = nt.camelCase(e);
            return Gt.test(e) || (e = A(a)), (s = nt.cssHooks[e] || nt.cssHooks[a]) && "get" in s && (o = s.get(t, !0, i)), void 0 === o && (o = S(t, e, n)), "normal" === o && e in Jt && (o = Jt[e]), "" === i || i ? (r = parseFloat(o), !0 === i || isFinite(r) ? r || 0 : o) : o
        }
    }), nt.each(["height", "width"], function (t, e) {
        nt.cssHooks[e] = {
            get: function (t, i, n) {
                if (i) return !Yt.test(nt.css(t, "display")) || t.getClientRects().length && t.getBoundingClientRect().width ? D(t, e, n) : Pt(t, Zt, function () {
                    return D(t, e, n)
                })
            }, set: function (t, i, n) {
                var o, r = n && Ut(t), s = n && P(t, e, n, "border-box" === nt.css(t, "boxSizing", !1, r), r);
                return s && (o = Et.exec(i)) && "px" !== (o[3] || "px") && (t.style[e] = i, i = nt.css(t, e)), $(0, i, s)
            }
        }
    }), nt.cssHooks.marginLeft = E(it.reliableMarginLeft, function (t, e) {
        if (e) return (parseFloat(S(t, "marginLeft")) || t.getBoundingClientRect().left - Pt(t, {marginLeft: 0}, function () {
            return t.getBoundingClientRect().left
        })) + "px"
    }), nt.each({margin: "", padding: "", border: "Width"}, function (t, e) {
        nt.cssHooks[t + e] = {
            expand: function (i) {
                for (var n = 0, o = {}, r = "string" == typeof i ? i.split(" ") : [i]; n < 4; n++) o[t + At[n] + e] = r[n] || r[n - 2] || r[0];
                return o
            }
        }, Bt.test(t) || (nt.cssHooks[t + e].set = $)
    }), nt.fn.extend({
        css: function (t, e) {
            return wt(this, function (t, e, i) {
                var n, o, r = {}, s = 0;
                if (Array.isArray(e)) {
                    for (n = Ut(t), o = e.length; s < o; s++) r[e[s]] = nt.css(t, e[s], !1, n);
                    return r
                }
                return void 0 !== i ? nt.style(t, e, i) : nt.css(t, e)
            }, t, e, arguments.length > 1)
        }
    }), nt.Tween = O, (O.prototype = {
        constructor: O, init: function (t, e, i, n, o, r) {
            this.elem = t, this.prop = i, this.easing = o || nt.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = n, this.unit = r || (nt.cssNumber[i] ? "" : "px")
        }, cur: function () {
            var t = O.propHooks[this.prop];
            return t && t.get ? t.get(this) : O.propHooks._default.get(this)
        }, run: function (t) {
            var e, i = O.propHooks[this.prop];
            return this.options.duration ? this.pos = e = nt.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : O.propHooks._default.set(this), this
        }
    }).init.prototype = O.prototype, (O.propHooks = {
        _default: {
            get: function (t) {
                var e;
                return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = nt.css(t.elem, t.prop, "")) && "auto" !== e ? e : 0
            }, set: function (t) {
                nt.fx.step[t.prop] ? nt.fx.step[t.prop](t) : 1 !== t.elem.nodeType || null == t.elem.style[nt.cssProps[t.prop]] && !nt.cssHooks[t.prop] ? t.elem[t.prop] = t.now : nt.style(t.elem, t.prop, t.now + t.unit)
            }
        }
    }).scrollTop = O.propHooks.scrollLeft = {
        set: function (t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    }, nt.easing = {
        linear: function (t) {
            return t
        }, swing: function (t) {
            return .5 - Math.cos(t * Math.PI) / 2
        }, _default: "swing"
    }, nt.fx = O.prototype.init, nt.fx.step = {};
    var ee, ie, ne = /^(?:toggle|show|hide)$/, oe = /queueHooks$/;
    nt.Animation = nt.extend(H, {
        tweeners: {
            "*": [function (t, e) {
                var i = this.createTween(t, e);
                return p(i.elem, t, Et.exec(e), i), i
            }]
        }, tweener: function (t, e) {
            nt.isFunction(t) ? (e = t, t = ["*"]) : t = t.match(mt);
            for (var i, n = 0, o = t.length; n < o; n++) i = t[n], H.tweeners[i] = H.tweeners[i] || [], H.tweeners[i].unshift(e)
        }, prefilters: [function (t, e, i) {
            var n, o, r, s, a, l, c, u, h = "width" in e || "height" in e, d = this, p = {}, g = t.style,
                v = t.nodeType && $t(t), m = Ct.get(t, "fxshow");
            i.queue || (null == (s = nt._queueHooks(t, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function () {
                s.unqueued || a()
            }), s.unqueued++, d.always(function () {
                d.always(function () {
                    s.unqueued--, nt.queue(t, "fx").length || s.empty.fire()
                })
            }));
            for (n in e) if (o = e[n], ne.test(o)) {
                if (delete e[n], r = r || "toggle" === o, o === (v ? "hide" : "show")) {
                    if ("show" !== o || !m || void 0 === m[n]) continue;
                    v = !0
                }
                p[n] = m && m[n] || nt.style(t, n)
            }
            if ((l = !nt.isEmptyObject(e)) || !nt.isEmptyObject(p)) {
                h && 1 === t.nodeType && (i.overflow = [g.overflow, g.overflowX, g.overflowY], null == (c = m && m.display) && (c = Ct.get(t, "display")), "none" === (u = nt.css(t, "display")) && (c ? u = c : (f([t], !0), c = t.style.display || c, u = nt.css(t, "display"), f([t]))), ("inline" === u || "inline-block" === u && null != c) && "none" === nt.css(t, "float") && (l || (d.done(function () {
                    g.display = c
                }), null == c && (u = g.display, c = "none" === u ? "" : u)), g.display = "inline-block")), i.overflow && (g.overflow = "hidden", d.always(function () {
                    g.overflow = i.overflow[0], g.overflowX = i.overflow[1], g.overflowY = i.overflow[2]
                })), l = !1;
                for (n in p) l || (m ? "hidden" in m && (v = m.hidden) : m = Ct.access(t, "fxshow", {display: c}), r && (m.hidden = !v), v && f([t], !0), d.done(function () {
                    v || f([t]), Ct.remove(t, "fxshow");
                    for (n in p) nt.style(t, n, p[n])
                })), l = j(v ? m[n] : 0, n, d), n in m || (m[n] = l.start, v && (l.end = l.start, l.start = 0))
            }
        }], prefilter: function (t, e) {
            e ? H.prefilters.unshift(t) : H.prefilters.push(t)
        }
    }), nt.speed = function (t, e, i) {
        var n = t && "object" == typeof t ? nt.extend({}, t) : {
            complete: i || !i && e || nt.isFunction(t) && t,
            duration: t,
            easing: i && e || e && !nt.isFunction(e) && e
        };
        return nt.fx.off ? n.duration = 0 : "number" != typeof n.duration && (n.duration in nt.fx.speeds ? n.duration = nt.fx.speeds[n.duration] : n.duration = nt.fx.speeds._default), null != n.queue && !0 !== n.queue || (n.queue = "fx"), n.old = n.complete, n.complete = function () {
            nt.isFunction(n.old) && n.old.call(this), n.queue && nt.dequeue(this, n.queue)
        }, n
    }, nt.fn.extend({
        fadeTo: function (t, e, i, n) {
            return this.filter($t).css("opacity", 0).show().end().animate({opacity: e}, t, i, n)
        }, animate: function (t, e, i, n) {
            var o = nt.isEmptyObject(t), r = nt.speed(e, i, n), s = function () {
                var e = H(this, nt.extend({}, t), r);
                (o || Ct.get(this, "finish")) && e.stop(!0)
            };
            return s.finish = s, o || !1 === r.queue ? this.each(s) : this.queue(r.queue, s)
        }, stop: function (t, e, i) {
            var n = function (t) {
                var e = t.stop;
                delete t.stop, e(i)
            };
            return "string" != typeof t && (i = e, e = t, t = void 0), e && !1 !== t && this.queue(t || "fx", []), this.each(function () {
                var e = !0, o = null != t && t + "queueHooks", r = nt.timers, s = Ct.get(this);
                if (o) s[o] && s[o].stop && n(s[o]); else for (o in s) s[o] && s[o].stop && oe.test(o) && n(s[o]);
                for (o = r.length; o--;) r[o].elem !== this || null != t && r[o].queue !== t || (r[o].anim.stop(i), e = !1, r.splice(o, 1));
                !e && i || nt.dequeue(this, t)
            })
        }, finish: function (t) {
            return !1 !== t && (t = t || "fx"), this.each(function () {
                var e, i = Ct.get(this), n = i[t + "queue"], o = i[t + "queueHooks"], r = nt.timers,
                    s = n ? n.length : 0;
                for (i.finish = !0, nt.queue(this, t, []), o && o.stop && o.stop.call(this, !0), e = r.length; e--;) r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0), r.splice(e, 1));
                for (e = 0; e < s; e++) n[e] && n[e].finish && n[e].finish.call(this);
                delete i.finish
            })
        }
    }), nt.each(["toggle", "show", "hide"], function (t, e) {
        var i = nt.fn[e];
        nt.fn[e] = function (t, n, o) {
            return null == t || "boolean" == typeof t ? i.apply(this, arguments) : this.animate(N(e, !0), t, n, o)
        }
    }), nt.each({
        slideDown: N("show"),
        slideUp: N("hide"),
        slideToggle: N("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (t, e) {
        nt.fn[t] = function (t, i, n) {
            return this.animate(e, t, i, n)
        }
    }), nt.timers = [], nt.fx.tick = function () {
        var t, e = 0, i = nt.timers;
        for (ee = nt.now(); e < i.length; e++) (t = i[e])() || i[e] !== t || i.splice(e--, 1);
        i.length || nt.fx.stop(), ee = void 0
    }, nt.fx.timer = function (t) {
        nt.timers.push(t), nt.fx.start()
    }, nt.fx.interval = 13, nt.fx.start = function () {
        ie || (ie = !0, M())
    }, nt.fx.stop = function () {
        ie = null
    }, nt.fx.speeds = {slow: 600, fast: 200, _default: 400}, nt.fn.delay = function (e, i) {
        return e = nt.fx ? nt.fx.speeds[e] || e : e, i = i || "fx", this.queue(i, function (i, n) {
            var o = t.setTimeout(i, e);
            n.stop = function () {
                t.clearTimeout(o)
            }
        })
    }, function () {
        var t = Q.createElement("input"), e = Q.createElement("select").appendChild(Q.createElement("option"));
        t.type = "checkbox", it.checkOn = "" !== t.value, it.optSelected = e.selected, (t = Q.createElement("input")).value = "t", t.type = "radio", it.radioValue = "t" === t.value
    }();
    var re, se = nt.expr.attrHandle;
    nt.fn.extend({
        attr: function (t, e) {
            return wt(this, nt.attr, t, e, arguments.length > 1)
        }, removeAttr: function (t) {
            return this.each(function () {
                nt.removeAttr(this, t)
            })
        }
    }), nt.extend({
        attr: function (t, e, i) {
            var n, o, r = t.nodeType;
            if (3 !== r && 8 !== r && 2 !== r) return void 0 === t.getAttribute ? nt.prop(t, e, i) : (1 === r && nt.isXMLDoc(t) || (o = nt.attrHooks[e.toLowerCase()] || (nt.expr.match.bool.test(e) ? re : void 0)), void 0 !== i ? null === i ? void nt.removeAttr(t, e) : o && "set" in o && void 0 !== (n = o.set(t, i, e)) ? n : (t.setAttribute(e, i + ""), i) : o && "get" in o && null !== (n = o.get(t, e)) ? n : null == (n = nt.find.attr(t, e)) ? void 0 : n)
        }, attrHooks: {
            type: {
                set: function (t, e) {
                    if (!it.radioValue && "radio" === e && o(t, "input")) {
                        var i = t.value;
                        return t.setAttribute("type", e), i && (t.value = i), e
                    }
                }
            }
        }, removeAttr: function (t, e) {
            var i, n = 0, o = e && e.match(mt);
            if (o && 1 === t.nodeType) for (; i = o[n++];) t.removeAttribute(i)
        }
    }), re = {
        set: function (t, e, i) {
            return !1 === e ? nt.removeAttr(t, i) : t.setAttribute(i, i), i
        }
    }, nt.each(nt.expr.match.bool.source.match(/\w+/g), function (t, e) {
        var i = se[e] || nt.find.attr;
        se[e] = function (t, e, n) {
            var o, r, s = e.toLowerCase();
            return n || (r = se[s], se[s] = o, o = null != i(t, e, n) ? s : null, se[s] = r), o
        }
    });
    var ae = /^(?:input|select|textarea|button)$/i, le = /^(?:a|area)$/i;
    nt.fn.extend({
        prop: function (t, e) {
            return wt(this, nt.prop, t, e, arguments.length > 1)
        }, removeProp: function (t) {
            return this.each(function () {
                delete this[nt.propFix[t] || t]
            })
        }
    }), nt.extend({
        prop: function (t, e, i) {
            var n, o, r = t.nodeType;
            if (3 !== r && 8 !== r && 2 !== r) return 1 === r && nt.isXMLDoc(t) || (e = nt.propFix[e] || e, o = nt.propHooks[e]), void 0 !== i ? o && "set" in o && void 0 !== (n = o.set(t, i, e)) ? n : t[e] = i : o && "get" in o && null !== (n = o.get(t, e)) ? n : t[e]
        }, propHooks: {
            tabIndex: {
                get: function (t) {
                    var e = nt.find.attr(t, "tabindex");
                    return e ? parseInt(e, 10) : ae.test(t.nodeName) || le.test(t.nodeName) && t.href ? 0 : -1
                }
            }
        }, propFix: {for: "htmlFor", class: "className"}
    }), it.optSelected || (nt.propHooks.selected = {
        get: function (t) {
            var e = t.parentNode;
            return e && e.parentNode && e.parentNode.selectedIndex, null
        }, set: function (t) {
            var e = t.parentNode;
            e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex)
        }
    }), nt.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        nt.propFix[this.toLowerCase()] = this
    }), nt.fn.extend({
        addClass: function (t) {
            var e, i, n, o, r, s, a, l = 0;
            if (nt.isFunction(t)) return this.each(function (e) {
                nt(this).addClass(t.call(this, e, q(this)))
            });
            if ("string" == typeof t && t) for (e = t.match(mt) || []; i = this[l++];) if (o = q(i), n = 1 === i.nodeType && " " + L(o) + " ") {
                for (s = 0; r = e[s++];) n.indexOf(" " + r + " ") < 0 && (n += r + " ");
                o !== (a = L(n)) && i.setAttribute("class", a)
            }
            return this
        }, removeClass: function (t) {
            var e, i, n, o, r, s, a, l = 0;
            if (nt.isFunction(t)) return this.each(function (e) {
                nt(this).removeClass(t.call(this, e, q(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof t && t) for (e = t.match(mt) || []; i = this[l++];) if (o = q(i), n = 1 === i.nodeType && " " + L(o) + " ") {
                for (s = 0; r = e[s++];) for (; n.indexOf(" " + r + " ") > -1;) n = n.replace(" " + r + " ", " ");
                o !== (a = L(n)) && i.setAttribute("class", a)
            }
            return this
        }, toggleClass: function (t, e) {
            var i = typeof t;
            return "boolean" == typeof e && "string" === i ? e ? this.addClass(t) : this.removeClass(t) : nt.isFunction(t) ? this.each(function (i) {
                nt(this).toggleClass(t.call(this, i, q(this), e), e)
            }) : this.each(function () {
                var e, n, o, r;
                if ("string" === i) for (n = 0, o = nt(this), r = t.match(mt) || []; e = r[n++];) o.hasClass(e) ? o.removeClass(e) : o.addClass(e); else void 0 !== t && "boolean" !== i || ((e = q(this)) && Ct.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === t ? "" : Ct.get(this, "__className__") || ""))
            })
        }, hasClass: function (t) {
            var e, i, n = 0;
            for (e = " " + t + " "; i = this[n++];) if (1 === i.nodeType && (" " + L(q(i)) + " ").indexOf(e) > -1) return !0;
            return !1
        }
    });
    var ce = /\r/g;
    nt.fn.extend({
        val: function (t) {
            var e, i, n, o = this[0];
            {
                if (arguments.length) return n = nt.isFunction(t), this.each(function (i) {
                    var o;
                    1 === this.nodeType && (null == (o = n ? t.call(this, i, nt(this).val()) : t) ? o = "" : "number" == typeof o ? o += "" : Array.isArray(o) && (o = nt.map(o, function (t) {
                        return null == t ? "" : t + ""
                    })), (e = nt.valHooks[this.type] || nt.valHooks[this.nodeName.toLowerCase()]) && "set" in e && void 0 !== e.set(this, o, "value") || (this.value = o))
                });
                if (o) return (e = nt.valHooks[o.type] || nt.valHooks[o.nodeName.toLowerCase()]) && "get" in e && void 0 !== (i = e.get(o, "value")) ? i : "string" == typeof(i = o.value) ? i.replace(ce, "") : null == i ? "" : i
            }
        }
    }), nt.extend({
        valHooks: {
            option: {
                get: function (t) {
                    var e = nt.find.attr(t, "value");
                    return null != e ? e : L(nt.text(t))
                }
            }, select: {
                get: function (t) {
                    var e, i, n, r = t.options, s = t.selectedIndex, a = "select-one" === t.type, l = a ? null : [],
                        c = a ? s + 1 : r.length;
                    for (n = s < 0 ? c : a ? s : 0; n < c; n++) if (((i = r[n]).selected || n === s) && !i.disabled && (!i.parentNode.disabled || !o(i.parentNode, "optgroup"))) {
                        if (e = nt(i).val(), a) return e;
                        l.push(e)
                    }
                    return l
                }, set: function (t, e) {
                    for (var i, n, o = t.options, r = nt.makeArray(e), s = o.length; s--;) ((n = o[s]).selected = nt.inArray(nt.valHooks.option.get(n), r) > -1) && (i = !0);
                    return i || (t.selectedIndex = -1), r
                }
            }
        }
    }), nt.each(["radio", "checkbox"], function () {
        nt.valHooks[this] = {
            set: function (t, e) {
                if (Array.isArray(e)) return t.checked = nt.inArray(nt(t).val(), e) > -1
            }
        }, it.checkOn || (nt.valHooks[this].get = function (t) {
            return null === t.getAttribute("value") ? "on" : t.value
        })
    });
    var ue = /^(?:focusinfocus|focusoutblur)$/;
    nt.extend(nt.event, {
        trigger: function (e, i, n, o) {
            var r, s, a, l, c, u, h, d = [n || Q], p = K.call(e, "type") ? e.type : e,
                f = K.call(e, "namespace") ? e.namespace.split(".") : [];
            if (s = a = n = n || Q, 3 !== n.nodeType && 8 !== n.nodeType && !ue.test(p + nt.event.triggered) && (p.indexOf(".") > -1 && (p = (f = p.split(".")).shift(), f.sort()), c = p.indexOf(":") < 0 && "on" + p, e = e[nt.expando] ? e : new nt.Event(p, "object" == typeof e && e), e.isTrigger = o ? 2 : 3, e.namespace = f.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), i = null == i ? [e] : nt.makeArray(i, [e]), h = nt.event.special[p] || {}, o || !h.trigger || !1 !== h.trigger.apply(n, i))) {
                if (!o && !h.noBubble && !nt.isWindow(n)) {
                    for (l = h.delegateType || p, ue.test(l + p) || (s = s.parentNode); s; s = s.parentNode) d.push(s), a = s;
                    a === (n.ownerDocument || Q) && d.push(a.defaultView || a.parentWindow || t)
                }
                for (r = 0; (s = d[r++]) && !e.isPropagationStopped();) e.type = r > 1 ? l : h.bindType || p, (u = (Ct.get(s, "events") || {})[e.type] && Ct.get(s, "handle")) && u.apply(s, i), (u = c && s[c]) && u.apply && xt(s) && (e.result = u.apply(s, i), !1 === e.result && e.preventDefault());
                return e.type = p, o || e.isDefaultPrevented() || h._default && !1 !== h._default.apply(d.pop(), i) || !xt(n) || c && nt.isFunction(n[p]) && !nt.isWindow(n) && ((a = n[c]) && (n[c] = null), nt.event.triggered = p, n[p](), nt.event.triggered = void 0, a && (n[c] = a)), e.result
            }
        }, simulate: function (t, e, i) {
            var n = nt.extend(new nt.Event, i, {type: t, isSimulated: !0});
            nt.event.trigger(n, null, e)
        }
    }), nt.fn.extend({
        trigger: function (t, e) {
            return this.each(function () {
                nt.event.trigger(t, e, this)
            })
        }, triggerHandler: function (t, e) {
            var i = this[0];
            if (i) return nt.event.trigger(t, e, i, !0)
        }
    }), nt.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (t, e) {
        nt.fn[e] = function (t, i) {
            return arguments.length > 0 ? this.on(e, null, t, i) : this.trigger(e)
        }
    }), nt.fn.extend({
        hover: function (t, e) {
            return this.mouseenter(t).mouseleave(e || t)
        }
    }), it.focusin = "onfocusin" in t, it.focusin || nt.each({focus: "focusin", blur: "focusout"}, function (t, e) {
        var i = function (t) {
            nt.event.simulate(e, t.target, nt.event.fix(t))
        };
        nt.event.special[e] = {
            setup: function () {
                var n = this.ownerDocument || this, o = Ct.access(n, e);
                o || n.addEventListener(t, i, !0), Ct.access(n, e, (o || 0) + 1)
            }, teardown: function () {
                var n = this.ownerDocument || this, o = Ct.access(n, e) - 1;
                o ? Ct.access(n, e, o) : (n.removeEventListener(t, i, !0), Ct.remove(n, e))
            }
        }
    });
    var he = t.location, de = nt.now(), pe = /\?/;
    nt.parseXML = function (e) {
        var i;
        if (!e || "string" != typeof e) return null;
        try {
            i = (new t.DOMParser).parseFromString(e, "text/xml")
        } catch (t) {
            i = void 0
        }
        return i && !i.getElementsByTagName("parsererror").length || nt.error("Invalid XML: " + e), i
    };
    var fe = /\[\]$/, ge = /\r?\n/g, ve = /^(?:submit|button|image|reset|file)$/i,
        me = /^(?:input|select|textarea|keygen)/i;
    nt.param = function (t, e) {
        var i, n = [], o = function (t, e) {
            var i = nt.isFunction(e) ? e() : e;
            n[n.length] = encodeURIComponent(t) + "=" + encodeURIComponent(null == i ? "" : i)
        };
        if (Array.isArray(t) || t.jquery && !nt.isPlainObject(t)) nt.each(t, function () {
            o(this.name, this.value)
        }); else for (i in t) z(i, t[i], e, o);
        return n.join("&")
    }, nt.fn.extend({
        serialize: function () {
            return nt.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var t = nt.prop(this, "elements");
                return t ? nt.makeArray(t) : this
            }).filter(function () {
                var t = this.type;
                return this.name && !nt(this).is(":disabled") && me.test(this.nodeName) && !ve.test(t) && (this.checked || !Ot.test(t))
            }).map(function (t, e) {
                var i = nt(this).val();
                return null == i ? null : Array.isArray(i) ? nt.map(i, function (t) {
                    return {name: e.name, value: t.replace(ge, "\r\n")}
                }) : {name: e.name, value: i.replace(ge, "\r\n")}
            }).get()
        }
    });
    var ye = /%20/g, be = /#.*$/, we = /([?&])_=[^&]*/, xe = /^(.*?):[ \t]*([^\r\n]*)$/gm, Ce = /^(?:GET|HEAD)$/,
        ke = /^\/\//, Te = {}, _e = {}, Se = "*/".concat("*"), Ee = Q.createElement("a");
    Ee.href = he.href, nt.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: he.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(he.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Se,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": JSON.parse, "text xml": nt.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (t, e) {
            return e ? F(F(t, nt.ajaxSettings), e) : F(nt.ajaxSettings, t)
        },
        ajaxPrefilter: R(Te),
        ajaxTransport: R(_e),
        ajax: function (e, i) {
            function n(e, i, n, a) {
                var c, d, p, w, x, C = i;
                u || (u = !0, l && t.clearTimeout(l), o = void 0, s = a || "", k.readyState = e > 0 ? 4 : 0, c = e >= 200 && e < 300 || 304 === e, n && (w = function (t, e, i) {
                    for (var n, o, r, s, a = t.contents, l = t.dataTypes; "*" === l[0];) l.shift(), void 0 === n && (n = t.mimeType || e.getResponseHeader("Content-Type"));
                    if (n) for (o in a) if (a[o] && a[o].test(n)) {
                        l.unshift(o);
                        break
                    }
                    if (l[0] in i) r = l[0]; else {
                        for (o in i) {
                            if (!l[0] || t.converters[o + " " + l[0]]) {
                                r = o;
                                break
                            }
                            s || (s = o)
                        }
                        r = r || s
                    }
                    if (r) return r !== l[0] && l.unshift(r), i[r]
                }(f, k, n)), w = function (t, e, i, n) {
                    var o, r, s, a, l, c = {}, u = t.dataTypes.slice();
                    if (u[1]) for (s in t.converters) c[s.toLowerCase()] = t.converters[s];
                    for (r = u.shift(); r;) if (t.responseFields[r] && (i[t.responseFields[r]] = e), !l && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = r, r = u.shift()) if ("*" === r) r = l; else if ("*" !== l && l !== r) {
                        if (!(s = c[l + " " + r] || c["* " + r])) for (o in c) if ((a = o.split(" "))[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                            !0 === s ? s = c[o] : !0 !== c[o] && (r = a[0], u.unshift(a[1]));
                            break
                        }
                        if (!0 !== s) if (s && t.throws) e = s(e); else try {
                            e = s(e)
                        } catch (t) {
                            return {state: "parsererror", error: s ? t : "No conversion from " + l + " to " + r}
                        }
                    }
                    return {state: "success", data: e}
                }(f, w, k, c), c ? (f.ifModified && ((x = k.getResponseHeader("Last-Modified")) && (nt.lastModified[r] = x), (x = k.getResponseHeader("etag")) && (nt.etag[r] = x)), 204 === e || "HEAD" === f.type ? C = "nocontent" : 304 === e ? C = "notmodified" : (C = w.state, d = w.data, c = !(p = w.error))) : (p = C, !e && C || (C = "error", e < 0 && (e = 0))), k.status = e, k.statusText = (i || C) + "", c ? m.resolveWith(g, [d, C, k]) : m.rejectWith(g, [k, C, p]), k.statusCode(b), b = void 0, h && v.trigger(c ? "ajaxSuccess" : "ajaxError", [k, f, c ? d : p]), y.fireWith(g, [k, C]), h && (v.trigger("ajaxComplete", [k, f]), --nt.active || nt.event.trigger("ajaxStop")))
            }

            "object" == typeof e && (i = e, e = void 0), i = i || {};
            var o, r, s, a, l, c, u, h, d, p, f = nt.ajaxSetup({}, i), g = f.context || f,
                v = f.context && (g.nodeType || g.jquery) ? nt(g) : nt.event, m = nt.Deferred(),
                y = nt.Callbacks("once memory"), b = f.statusCode || {}, w = {}, x = {}, C = "canceled", k = {
                    readyState: 0, getResponseHeader: function (t) {
                        var e;
                        if (u) {
                            if (!a) for (a = {}; e = xe.exec(s);) a[e[1].toLowerCase()] = e[2];
                            e = a[t.toLowerCase()]
                        }
                        return null == e ? null : e
                    }, getAllResponseHeaders: function () {
                        return u ? s : null
                    }, setRequestHeader: function (t, e) {
                        return null == u && (t = x[t.toLowerCase()] = x[t.toLowerCase()] || t, w[t] = e), this
                    }, overrideMimeType: function (t) {
                        return null == u && (f.mimeType = t), this
                    }, statusCode: function (t) {
                        var e;
                        if (t) if (u) k.always(t[k.status]); else for (e in t) b[e] = [b[e], t[e]];
                        return this
                    }, abort: function (t) {
                        var e = t || C;
                        return o && o.abort(e), n(0, e), this
                    }
                };
            if (m.promise(k), f.url = ((e || f.url || he.href) + "").replace(ke, he.protocol + "//"), f.type = i.method || i.type || f.method || f.type, f.dataTypes = (f.dataType || "*").toLowerCase().match(mt) || [""], null == f.crossDomain) {
                c = Q.createElement("a");
                try {
                    c.href = f.url, c.href = c.href, f.crossDomain = Ee.protocol + "//" + Ee.host != c.protocol + "//" + c.host
                } catch (t) {
                    f.crossDomain = !0
                }
            }
            if (f.data && f.processData && "string" != typeof f.data && (f.data = nt.param(f.data, f.traditional)), W(Te, f, i, k), u) return k;
            (h = nt.event && f.global) && 0 == nt.active++ && nt.event.trigger("ajaxStart"), f.type = f.type.toUpperCase(), f.hasContent = !Ce.test(f.type), r = f.url.replace(be, ""), f.hasContent ? f.data && f.processData && 0 === (f.contentType || "").indexOf("application/x-www-form-urlencoded") && (f.data = f.data.replace(ye, "+")) : (p = f.url.slice(r.length), f.data && (r += (pe.test(r) ? "&" : "?") + f.data, delete f.data), !1 === f.cache && (r = r.replace(we, "$1"), p = (pe.test(r) ? "&" : "?") + "_=" + de++ + p), f.url = r + p), f.ifModified && (nt.lastModified[r] && k.setRequestHeader("If-Modified-Since", nt.lastModified[r]), nt.etag[r] && k.setRequestHeader("If-None-Match", nt.etag[r])), (f.data && f.hasContent && !1 !== f.contentType || i.contentType) && k.setRequestHeader("Content-Type", f.contentType), k.setRequestHeader("Accept", f.dataTypes[0] && f.accepts[f.dataTypes[0]] ? f.accepts[f.dataTypes[0]] + ("*" !== f.dataTypes[0] ? ", " + Se + "; q=0.01" : "") : f.accepts["*"]);
            for (d in f.headers) k.setRequestHeader(d, f.headers[d]);
            if (f.beforeSend && (!1 === f.beforeSend.call(g, k, f) || u)) return k.abort();
            if (C = "abort", y.add(f.complete), k.done(f.success), k.fail(f.error), o = W(_e, f, i, k)) {
                if (k.readyState = 1, h && v.trigger("ajaxSend", [k, f]), u) return k;
                f.async && f.timeout > 0 && (l = t.setTimeout(function () {
                    k.abort("timeout")
                }, f.timeout));
                try {
                    u = !1, o.send(w, n)
                } catch (t) {
                    if (u) throw t;
                    n(-1, t)
                }
            } else n(-1, "No Transport");
            return k
        },
        getJSON: function (t, e, i) {
            return nt.get(t, e, i, "json")
        },
        getScript: function (t, e) {
            return nt.get(t, void 0, e, "script")
        }
    }), nt.each(["get", "post"], function (t, e) {
        nt[e] = function (t, i, n, o) {
            return nt.isFunction(i) && (o = o || n, n = i, i = void 0), nt.ajax(nt.extend({
                url: t,
                type: e,
                dataType: o,
                data: i,
                success: n
            }, nt.isPlainObject(t) && t))
        }
    }), nt._evalUrl = function (t) {
        return nt.ajax({url: t, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, throws: !0})
    }, nt.fn.extend({
        wrapAll: function (t) {
            var e;
            return this[0] && (nt.isFunction(t) && (t = t.call(this[0])), e = nt(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
                for (var t = this; t.firstElementChild;) t = t.firstElementChild;
                return t
            }).append(this)), this
        }, wrapInner: function (t) {
            return nt.isFunction(t) ? this.each(function (e) {
                nt(this).wrapInner(t.call(this, e))
            }) : this.each(function () {
                var e = nt(this), i = e.contents();
                i.length ? i.wrapAll(t) : e.append(t)
            })
        }, wrap: function (t) {
            var e = nt.isFunction(t);
            return this.each(function (i) {
                nt(this).wrapAll(e ? t.call(this, i) : t)
            })
        }, unwrap: function (t) {
            return this.parent(t).not("body").each(function () {
                nt(this).replaceWith(this.childNodes)
            }), this
        }
    }), nt.expr.pseudos.hidden = function (t) {
        return !nt.expr.pseudos.visible(t)
    }, nt.expr.pseudos.visible = function (t) {
        return !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length)
    }, nt.ajaxSettings.xhr = function () {
        try {
            return new t.XMLHttpRequest
        } catch (t) {
        }
    };
    var Ae = {0: 200, 1223: 204}, $e = nt.ajaxSettings.xhr();
    it.cors = !!$e && "withCredentials" in $e, it.ajax = $e = !!$e, nt.ajaxTransport(function (e) {
        var i, n;
        if (it.cors || $e && !e.crossDomain) return {
            send: function (o, r) {
                var s, a = e.xhr();
                if (a.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields) for (s in e.xhrFields) a[s] = e.xhrFields[s];
                e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType), e.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
                for (s in o) a.setRequestHeader(s, o[s]);
                i = function (t) {
                    return function () {
                        i && (i = n = a.onload = a.onerror = a.onabort = a.onreadystatechange = null, "abort" === t ? a.abort() : "error" === t ? "number" != typeof a.status ? r(0, "error") : r(a.status, a.statusText) : r(Ae[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {binary: a.response} : {text: a.responseText}, a.getAllResponseHeaders()))
                    }
                }, a.onload = i(), n = a.onerror = i("error"), void 0 !== a.onabort ? a.onabort = n : a.onreadystatechange = function () {
                    4 === a.readyState && t.setTimeout(function () {
                        i && n()
                    })
                }, i = i("abort");
                try {
                    a.send(e.hasContent && e.data || null)
                } catch (t) {
                    if (i) throw t
                }
            }, abort: function () {
                i && i()
            }
        }
    }), nt.ajaxPrefilter(function (t) {
        t.crossDomain && (t.contents.script = !1)
    }), nt.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /\b(?:java|ecma)script\b/},
        converters: {
            "text script": function (t) {
                return nt.globalEval(t), t
            }
        }
    }), nt.ajaxPrefilter("script", function (t) {
        void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET")
    }), nt.ajaxTransport("script", function (t) {
        if (t.crossDomain) {
            var e, i;
            return {
                send: function (n, o) {
                    e = nt("<script>").prop({charset: t.scriptCharset, src: t.url}).on("load error", i = function (t) {
                        e.remove(), i = null, t && o("error" === t.type ? 404 : 200, t.type)
                    }), Q.head.appendChild(e[0])
                }, abort: function () {
                    i && i()
                }
            }
        }
    });
    var Pe = [], De = /(=)\?(?=&|$)|\?\?/;
    nt.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var t = Pe.pop() || nt.expando + "_" + de++;
            return this[t] = !0, t
        }
    }), nt.ajaxPrefilter("json jsonp", function (e, i, n) {
        var o, r, s,
            a = !1 !== e.jsonp && (De.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && De.test(e.data) && "data");
        if (a || "jsonp" === e.dataTypes[0]) return o = e.jsonpCallback = nt.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(De, "$1" + o) : !1 !== e.jsonp && (e.url += (pe.test(e.url) ? "&" : "?") + e.jsonp + "=" + o), e.converters["script json"] = function () {
            return s || nt.error(o + " was not called"), s[0]
        }, e.dataTypes[0] = "json", r = t[o], t[o] = function () {
            s = arguments
        }, n.always(function () {
            void 0 === r ? nt(t).removeProp(o) : t[o] = r, e[o] && (e.jsonpCallback = i.jsonpCallback, Pe.push(o)), s && nt.isFunction(r) && r(s[0]), s = r = void 0
        }), "script"
    }), it.createHTMLDocument = function () {
        var t = Q.implementation.createHTMLDocument("").body;
        return t.innerHTML = "<form></form><form></form>", 2 === t.childNodes.length
    }(), nt.parseHTML = function (t, e, i) {
        if ("string" != typeof t) return [];
        "boolean" == typeof e && (i = e, e = !1);
        var n, o, r;
        return e || (it.createHTMLDocument ? ((n = (e = Q.implementation.createHTMLDocument("")).createElement("base")).href = Q.location.href, e.head.appendChild(n)) : e = Q), o = ht.exec(t), r = !i && [], o ? [e.createElement(o[1])] : (o = m([t], e, r), r && r.length && nt(r).remove(), nt.merge([], o.childNodes))
    }, nt.fn.load = function (t, e, i) {
        var n, o, r, s = this, a = t.indexOf(" ");
        return a > -1 && (n = L(t.slice(a)), t = t.slice(0, a)), nt.isFunction(e) ? (i = e, e = void 0) : e && "object" == typeof e && (o = "POST"), s.length > 0 && nt.ajax({
            url: t,
            type: o || "GET",
            dataType: "html",
            data: e
        }).done(function (t) {
            r = arguments, s.html(n ? nt("<div>").append(nt.parseHTML(t)).find(n) : t)
        }).always(i && function (t, e) {
            s.each(function () {
                i.apply(this, r || [t.responseText, e, t])
            })
        }), this
    }, nt.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
        nt.fn[e] = function (t) {
            return this.on(e, t)
        }
    }), nt.expr.pseudos.animated = function (t) {
        return nt.grep(nt.timers, function (e) {
            return t === e.elem
        }).length
    }, nt.offset = {
        setOffset: function (t, e, i) {
            var n, o, r, s, a, l, c = nt.css(t, "position"), u = nt(t), h = {};
            "static" === c && (t.style.position = "relative"), a = u.offset(), r = nt.css(t, "top"), l = nt.css(t, "left"), ("absolute" === c || "fixed" === c) && (r + l).indexOf("auto") > -1 ? (s = (n = u.position()).top, o = n.left) : (s = parseFloat(r) || 0, o = parseFloat(l) || 0), nt.isFunction(e) && (e = e.call(t, i, nt.extend({}, a))), null != e.top && (h.top = e.top - a.top + s), null != e.left && (h.left = e.left - a.left + o), "using" in e ? e.using.call(t, h) : u.css(h)
        }
    }, nt.fn.extend({
        offset: function (t) {
            if (arguments.length) return void 0 === t ? this : this.each(function (e) {
                nt.offset.setOffset(this, t, e)
            });
            var e, i, n, o, r = this[0];
            if (r) return r.getClientRects().length ? (n = r.getBoundingClientRect(), e = r.ownerDocument, i = e.documentElement, o = e.defaultView, {
                top: n.top + o.pageYOffset - i.clientTop,
                left: n.left + o.pageXOffset - i.clientLeft
            }) : {top: 0, left: 0}
        }, position: function () {
            if (this[0]) {
                var t, e, i = this[0], n = {top: 0, left: 0};
                return "fixed" === nt.css(i, "position") ? e = i.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), o(t[0], "html") || (n = t.offset()), n = {
                    top: n.top + nt.css(t[0], "borderTopWidth", !0),
                    left: n.left + nt.css(t[0], "borderLeftWidth", !0)
                }), {
                    top: e.top - n.top - nt.css(i, "marginTop", !0),
                    left: e.left - n.left - nt.css(i, "marginLeft", !0)
                }
            }
        }, offsetParent: function () {
            return this.map(function () {
                for (var t = this.offsetParent; t && "static" === nt.css(t, "position");) t = t.offsetParent;
                return t || Ht
            })
        }
    }), nt.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (t, e) {
        var i = "pageYOffset" === e;
        nt.fn[t] = function (n) {
            return wt(this, function (t, n, o) {
                var r;
                if (nt.isWindow(t) ? r = t : 9 === t.nodeType && (r = t.defaultView), void 0 === o) return r ? r[e] : t[n];
                r ? r.scrollTo(i ? r.pageXOffset : o, i ? o : r.pageYOffset) : t[n] = o
            }, t, n, arguments.length)
        }
    }), nt.each(["top", "left"], function (t, e) {
        nt.cssHooks[e] = E(it.pixelPosition, function (t, i) {
            if (i) return i = S(t, e), Xt.test(i) ? nt(t).position()[e] + "px" : i
        })
    }), nt.each({Height: "height", Width: "width"}, function (t, e) {
        nt.each({padding: "inner" + t, content: e, "": "outer" + t}, function (i, n) {
            nt.fn[n] = function (o, r) {
                var s = arguments.length && (i || "boolean" != typeof o),
                    a = i || (!0 === o || !0 === r ? "margin" : "border");
                return wt(this, function (e, i, o) {
                    var r;
                    return nt.isWindow(e) ? 0 === n.indexOf("outer") ? e["inner" + t] : e.document.documentElement["client" + t] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + t], r["scroll" + t], e.body["offset" + t], r["offset" + t], r["client" + t])) : void 0 === o ? nt.css(e, i, a) : nt.style(e, i, o, a)
                }, e, s ? o : void 0, s)
            }
        })
    }), nt.fn.extend({
        bind: function (t, e, i) {
            return this.on(t, null, e, i)
        }, unbind: function (t, e) {
            return this.off(t, null, e)
        }, delegate: function (t, e, i, n) {
            return this.on(e, t, i, n)
        }, undelegate: function (t, e, i) {
            return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", i)
        }
    }), nt.holdReady = function (t) {
        t ? nt.readyWait++ : nt.ready(!0)
    }, nt.isArray = Array.isArray, nt.parseJSON = JSON.parse, nt.nodeName = o, "function" == typeof define && define.amd && define("jquery", [], function () {
        return nt
    });
    var Oe = t.jQuery, Me = t.$;
    return nt.noConflict = function (e) {
        return t.$ === nt && (t.$ = Me), e && t.jQuery === nt && (t.jQuery = Oe), nt
    }, e || (t.jQuery = t.$ = nt), nt
});
var _createClass = function () {
    function t(t, e) {
        for (var i = 0; i < e.length; i++) {
            var n = e[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n)
        }
    }

    return function (e, i, n) {
        return i && t(e.prototype, i), n && t(e, n), e
    }
}();
"undefined" == typeof jQuery && ("function" == typeof require ? jQuery = $ = require("jquery") : jQuery = $), function (t) {
    "function" == typeof define && define.amd ? define(["jquery"], function (e) {
        return t(e)
    }) : "object" == typeof module && "object" == typeof module.exports ? exports = t(require("jquery")) : t(jQuery)
}(function (t) {
    function e(t) {
        var e = 7.5625, i = 2.75;
        return t < 1 / i ? e * t * t : t < 2 / i ? e * (t -= 1.5 / i) * t + .75 : t < 2.5 / i ? e * (t -= 2.25 / i) * t + .9375 : e * (t -= 2.625 / i) * t + .984375
    }

    t.easing.jswing = t.easing.swing;
    var i = Math.pow, n = Math.sqrt, o = Math.sin, r = Math.cos, s = Math.PI, a = 1.70158, l = 1.525 * a, c = 2 * s / 3,
        u = 2 * s / 4.5;
    t.extend(t.easing, {
        def: "easeOutQuad", swing: function (e) {
            return t.easing[t.easing.def](e)
        }, easeInQuad: function (t) {
            return t * t
        }, easeOutQuad: function (t) {
            return 1 - (1 - t) * (1 - t)
        }, easeInOutQuad: function (t) {
            return t < .5 ? 2 * t * t : 1 - i(-2 * t + 2, 2) / 2
        }, easeInCubic: function (t) {
            return t * t * t
        }, easeOutCubic: function (t) {
            return 1 - i(1 - t, 3)
        }, easeInOutCubic: function (t) {
            return t < .5 ? 4 * t * t * t : 1 - i(-2 * t + 2, 3) / 2
        }, easeInQuart: function (t) {
            return t * t * t * t
        }, easeOutQuart: function (t) {
            return 1 - i(1 - t, 4)
        }, easeInOutQuart: function (t) {
            return t < .5 ? 8 * t * t * t * t : 1 - i(-2 * t + 2, 4) / 2
        }, easeInQuint: function (t) {
            return t * t * t * t * t
        }, easeOutQuint: function (t) {
            return 1 - i(1 - t, 5)
        }, easeInOutQuint: function (t) {
            return t < .5 ? 16 * t * t * t * t * t : 1 - i(-2 * t + 2, 5) / 2
        }, easeInSine: function (t) {
            return 1 - r(t * s / 2)
        }, easeOutSine: function (t) {
            return o(t * s / 2)
        }, easeInOutSine: function (t) {
            return -(r(s * t) - 1) / 2
        }, easeInExpo: function (t) {
            return 0 === t ? 0 : i(2, 10 * t - 10)
        }, easeOutExpo: function (t) {
            return 1 === t ? 1 : 1 - i(2, -10 * t)
        }, easeInOutExpo: function (t) {
            return 0 === t ? 0 : 1 === t ? 1 : t < .5 ? i(2, 20 * t - 10) / 2 : (2 - i(2, -20 * t + 10)) / 2
        }, easeInCirc: function (t) {
            return 1 - n(1 - i(t, 2))
        }, easeOutCirc: function (t) {
            return n(1 - i(t - 1, 2))
        }, easeInOutCirc: function (t) {
            return t < .5 ? (1 - n(1 - i(2 * t, 2))) / 2 : (n(1 - i(-2 * t + 2, 2)) + 1) / 2
        }, easeInElastic: function (t) {
            return 0 === t ? 0 : 1 === t ? 1 : -i(2, 10 * t - 10) * o((10 * t - 10.75) * c)
        }, easeOutElastic: function (t) {
            return 0 === t ? 0 : 1 === t ? 1 : i(2, -10 * t) * o((10 * t - .75) * c) + 1
        }, easeInOutElastic: function (t) {
            return 0 === t ? 0 : 1 === t ? 1 : t < .5 ? -i(2, 20 * t - 10) * o((20 * t - 11.125) * u) / 2 : i(2, -20 * t + 10) * o((20 * t - 11.125) * u) / 2 + 1
        }, easeInBack: function (t) {
            return 2.70158 * t * t * t - a * t * t
        }, easeOutBack: function (t) {
            return 1 + 2.70158 * i(t - 1, 3) + a * i(t - 1, 2)
        }, easeInOutBack: function (t) {
            return t < .5 ? i(2 * t, 2) * (7.189819 * t - l) / 2 : (i(2 * t - 2, 2) * ((l + 1) * (2 * t - 2) + l) + 2) / 2
        }, easeInBounce: function (t) {
            return 1 - e(1 - t)
        }, easeOutBounce: e, easeInOutBounce: function (t) {
            return t < .5 ? (1 - e(1 - 2 * t)) / 2 : (1 + e(2 * t - 1)) / 2
        }
    })
}), jQuery.extend(jQuery.easing, {
    easeInOutMaterial: function (t, e, i, n, o) {
        return (e /= o / 2) < 1 ? n / 2 * e * e + i : n / 4 * ((e -= 2) * e * e + 2) + i
    }
}), jQuery.Velocity ? console.log("Velocity is already loaded. You may be needlessly importing Velocity again; note that Materialize includes Velocity.") : (function (t) {
    function e(t) {
        var e = t.length, n = i.type(t);
        return "function" !== n && !i.isWindow(t) && (!(1 !== t.nodeType || !e) || ("array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t))
    }

    if (!t.jQuery) {
        var i = function (t, e) {
            return new i.fn.init(t, e)
        };
        i.isWindow = function (t) {
            return null != t && t == t.window
        }, i.type = function (t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? o[s.call(t)] || "object" : typeof t
        }, i.isArray = Array.isArray || function (t) {
            return "array" === i.type(t)
        }, i.isPlainObject = function (t) {
            var e;
            if (!t || "object" !== i.type(t) || t.nodeType || i.isWindow(t)) return !1;
            try {
                if (t.constructor && !r.call(t, "constructor") && !r.call(t.constructor.prototype, "isPrototypeOf")) return !1
            } catch (t) {
                return !1
            }
            for (e in t) ;
            return void 0 === e || r.call(t, e)
        }, i.each = function (t, i, n) {
            var o = 0, r = t.length, s = e(t);
            if (n) {
                if (s) for (; r > o && !1 !== i.apply(t[o], n); o++) ; else for (o in t) if (!1 === i.apply(t[o], n)) break
            } else if (s) for (; r > o && !1 !== i.call(t[o], o, t[o]); o++) ; else for (o in t) if (!1 === i.call(t[o], o, t[o])) break;
            return t
        }, i.data = function (t, e, o) {
            if (void 0 === o) {
                var r = (s = t[i.expando]) && n[s];
                if (void 0 === e) return r;
                if (r && e in r) return r[e]
            } else if (void 0 !== e) {
                var s = t[i.expando] || (t[i.expando] = ++i.uuid);
                return n[s] = n[s] || {}, n[s][e] = o, o
            }
        }, i.removeData = function (t, e) {
            var o = t[i.expando], r = o && n[o];
            r && i.each(e, function (t, e) {
                delete r[e]
            })
        }, i.extend = function () {
            var t, e, n, o, r, s, a = arguments[0] || {}, l = 1, c = arguments.length, u = !1;
            for ("boolean" == typeof a && (u = a, a = arguments[l] || {}, l++), "object" != typeof a && "function" !== i.type(a) && (a = {}), l === c && (a = this, l--); c > l; l++) if (null != (r = arguments[l])) for (o in r) t = a[o], a !== (n = r[o]) && (u && n && (i.isPlainObject(n) || (e = i.isArray(n))) ? (e ? (e = !1, s = t && i.isArray(t) ? t : []) : s = t && i.isPlainObject(t) ? t : {}, a[o] = i.extend(u, s, n)) : void 0 !== n && (a[o] = n));
            return a
        }, i.queue = function (t, n, o) {
            if (t) {
                n = (n || "fx") + "queue";
                var r = i.data(t, n);
                return o ? (!r || i.isArray(o) ? r = i.data(t, n, function (t, i) {
                    var n = i || [];
                    return null != t && (e(Object(t)) ? function (t, e) {
                        for (var i = +e.length, n = 0, o = t.length; i > n;) t[o++] = e[n++];
                        if (i != i) for (; void 0 !== e[n];) t[o++] = e[n++];
                        t.length = o
                    }(n, "string" == typeof t ? [t] : t) : [].push.call(n, t)), n
                }(o)) : r.push(o), r) : r || []
            }
        }, i.dequeue = function (t, e) {
            i.each(t.nodeType ? [t] : t, function (t, n) {
                e = e || "fx";
                var o = i.queue(n, e), r = o.shift();
                "inprogress" === r && (r = o.shift()), r && ("fx" === e && o.unshift("inprogress"), r.call(n, function () {
                    i.dequeue(n, e)
                }))
            })
        }, i.fn = i.prototype = {
            init: function (t) {
                if (t.nodeType) return this[0] = t, this;
                throw new Error("Not a DOM node.")
            }, offset: function () {
                var e = this[0].getBoundingClientRect ? this[0].getBoundingClientRect() : {top: 0, left: 0};
                return {
                    top: e.top + (t.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0),
                    left: e.left + (t.pageXOffset || document.scrollLeft || 0) - (document.clientLeft || 0)
                }
            }, position: function () {
                function t() {
                    for (var t = this.offsetParent || document; t && "html" === !t.nodeType.toLowerCase && "static" === t.style.position;) t = t.offsetParent;
                    return t || document
                }

                var e = this[0], t = t.apply(e), n = this.offset(),
                    o = /^(?:body|html)$/i.test(t.nodeName) ? {top: 0, left: 0} : i(t).offset();
                return n.top -= parseFloat(e.style.marginTop) || 0, n.left -= parseFloat(e.style.marginLeft) || 0, t.style && (o.top += parseFloat(t.style.borderTopWidth) || 0, o.left += parseFloat(t.style.borderLeftWidth) || 0), {
                    top: n.top - o.top,
                    left: n.left - o.left
                }
            }
        };
        var n = {};
        i.expando = "velocity" + (new Date).getTime(), i.uuid = 0;
        for (var o = {}, r = o.hasOwnProperty, s = o.toString, a = "Boolean Number String Function Array Date RegExp Object Error".split(" "), l = 0; l < a.length; l++) o["[object " + a[l] + "]"] = a[l].toLowerCase();
        i.fn.init.prototype = i.fn, t.Velocity = {Utilities: i}
    }
}(window), function (t) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : t()
}(function () {
    return function (t, e, i, n) {
        function o(t) {
            return p.isWrapped(t) ? t = [].slice.call(t) : p.isNode(t) && (t = [t]), t
        }

        function r(t) {
            var e = u.data(t, "velocity");
            return null === e ? n : e
        }

        function s(t, i, n, o) {
            function r(t, e) {
                return 1 - 3 * e + 3 * t
            }

            function s(t, e) {
                return 3 * e - 6 * t
            }

            function a(t) {
                return 3 * t
            }

            function l(t, e, i) {
                return ((r(e, i) * t + s(e, i)) * t + a(e)) * t
            }

            function c(t, e, i) {
                return 3 * r(e, i) * t * t + 2 * s(e, i) * t + a(e)
            }

            function u(e, i) {
                for (var o = 0; d > o; ++o) {
                    var r = c(i, t, n);
                    if (0 === r) return i;
                    i -= (l(i, t, n) - e) / r
                }
                return i
            }

            function h(e, i, o) {
                var r, s, a = 0;
                do {
                    (r = l(s = i + (o - i) / 2, t, n) - e) > 0 ? o = s : i = s
                } while (Math.abs(r) > f && ++a < g);
                return s
            }

            var d = 4, p = .001, f = 1e-7, g = 10, v = 11, m = 1 / (v - 1), y = "Float32Array" in e;
            if (4 !== arguments.length) return !1;
            for (var b = 0; 4 > b; ++b) if ("number" != typeof arguments[b] || isNaN(arguments[b]) || !isFinite(arguments[b])) return !1;
            t = Math.min(t, 1), n = Math.min(n, 1), t = Math.max(t, 0), n = Math.max(n, 0);
            var w = y ? new Float32Array(v) : new Array(v), x = !1, C = function (e) {
                return x || (x = !0, (t != i || n != o) && function () {
                    for (var e = 0; v > e; ++e) w[e] = l(e * m, t, n)
                }()), t === i && n === o ? e : 0 === e ? 0 : 1 === e ? 1 : l(function (e) {
                    for (var i = 0, o = 1, r = v - 1; o != r && w[o] <= e; ++o) i += m;
                    var s = i + (e - w[--o]) / (w[o + 1] - w[o]) * m, a = c(s, t, n);
                    return a >= p ? u(e, s) : 0 == a ? s : h(e, i, i + m)
                }(e), i, o)
            };
            C.getControlPoints = function () {
                return [{x: t, y: i}, {x: n, y: o}]
            };
            var k = "generateBezier(" + [t, i, n, o] + ")";
            return C.toString = function () {
                return k
            }, C
        }

        function a(t, e) {
            var i = t;
            return p.isString(t) ? m.Easings[t] || (i = !1) : i = p.isArray(t) && 1 === t.length ? function (t) {
                return function (e) {
                    return Math.round(e * t) * (1 / t)
                }
            }.apply(null, t) : p.isArray(t) && 2 === t.length ? y.apply(null, t.concat([e])) : !(!p.isArray(t) || 4 !== t.length) && s.apply(null, t), !1 === i && (i = m.Easings[m.defaults.easing] ? m.defaults.easing : v), i
        }

        function l(t) {
            if (t) {
                var e = (new Date).getTime(), i = m.State.calls.length;
                i > 1e4 && (m.State.calls = function (t) {
                    for (var e = -1, i = t ? t.length : 0, n = []; ++e < i;) {
                        var o = t[e];
                        o && n.push(o)
                    }
                    return n
                }(m.State.calls));
                for (var o = 0; i > o; o++) if (m.State.calls[o]) {
                    var s = m.State.calls[o], a = s[0], h = s[2], d = s[3], f = !!d, g = null;
                    d || (d = m.State.calls[o][3] = e - 16);
                    for (var v = Math.min((e - d) / h.duration, 1), y = 0, w = a.length; w > y; y++) {
                        var C = a[y], k = C.element;
                        if (r(k)) {
                            var T = !1;
                            if (h.display !== n && null !== h.display && "none" !== h.display) {
                                if ("flex" === h.display) {
                                    u.each(["-webkit-box", "-moz-box", "-ms-flexbox", "-webkit-flex"], function (t, e) {
                                        b.setPropertyValue(k, "display", e)
                                    })
                                }
                                b.setPropertyValue(k, "display", h.display)
                            }
                            h.visibility !== n && "hidden" !== h.visibility && b.setPropertyValue(k, "visibility", h.visibility);
                            for (var _ in C) if ("element" !== _) {
                                var S, E = C[_], A = p.isString(E.easing) ? m.Easings[E.easing] : E.easing;
                                if (1 === v) S = E.endValue; else {
                                    var $ = E.endValue - E.startValue;
                                    if (S = E.startValue + $ * A(v, h, $), !f && S === E.currentValue) continue
                                }
                                if (E.currentValue = S, "tween" === _) g = S; else {
                                    if (b.Hooks.registered[_]) {
                                        var P = b.Hooks.getRoot(_), D = r(k).rootPropertyValueCache[P];
                                        D && (E.rootPropertyValue = D)
                                    }
                                    var O = b.setPropertyValue(k, _, E.currentValue + (0 === parseFloat(S) ? "" : E.unitType), E.rootPropertyValue, E.scrollData);
                                    b.Hooks.registered[_] && (r(k).rootPropertyValueCache[P] = b.Normalizations.registered[P] ? b.Normalizations.registered[P]("extract", null, O[1]) : O[1]), "transform" === O[0] && (T = !0)
                                }
                            }
                            h.mobileHA && r(k).transformCache.translate3d === n && (r(k).transformCache.translate3d = "(0px, 0px, 0px)", T = !0), T && b.flushTransformCache(k)
                        }
                    }
                    h.display !== n && "none" !== h.display && (m.State.calls[o][2].display = !1), h.visibility !== n && "hidden" !== h.visibility && (m.State.calls[o][2].visibility = !1), h.progress && h.progress.call(s[1], s[1], v, Math.max(0, d + h.duration - e), d, g), 1 === v && c(o)
                }
            }
            m.State.isTicking && x(l)
        }

        function c(t, e) {
            if (!m.State.calls[t]) return !1;
            for (var i = m.State.calls[t][0], o = m.State.calls[t][1], s = m.State.calls[t][2], a = m.State.calls[t][4], l = !1, c = 0, h = i.length; h > c; c++) {
                var d = i[c].element;
                if (e || s.loop || ("none" === s.display && b.setPropertyValue(d, "display", s.display), "hidden" === s.visibility && b.setPropertyValue(d, "visibility", s.visibility)), !0 !== s.loop && (u.queue(d)[1] === n || !/\.velocityQueueEntryFlag/i.test(u.queue(d)[1])) && r(d)) {
                    r(d).isAnimating = !1, r(d).rootPropertyValueCache = {};
                    var p = !1;
                    u.each(b.Lists.transforms3D, function (t, e) {
                        var i = /^scale/.test(e) ? 1 : 0, o = r(d).transformCache[e];
                        r(d).transformCache[e] !== n && new RegExp("^\\(" + i + "[^.]").test(o) && (p = !0, delete r(d).transformCache[e])
                    }), s.mobileHA && (p = !0, delete r(d).transformCache.translate3d), p && b.flushTransformCache(d), b.Values.removeClass(d, "velocity-animating")
                }
                if (!e && s.complete && !s.loop && c === h - 1) try {
                    s.complete.call(o, o)
                } catch (t) {
                    setTimeout(function () {
                        throw t
                    }, 1)
                }
                a && !0 !== s.loop && a(o), r(d) && !0 === s.loop && !e && (u.each(r(d).tweensContainer, function (t, e) {
                    /^rotate/.test(t) && 360 === parseFloat(e.endValue) && (e.endValue = 0, e.startValue = 360), /^backgroundPosition/.test(t) && 100 === parseFloat(e.endValue) && "%" === e.unitType && (e.endValue = 0, e.startValue = 100)
                }), m(d, "reverse", {loop: !0, delay: s.delay})), !1 !== s.queue && u.dequeue(d, s.queue)
            }
            m.State.calls[t] = !1;
            for (var f = 0, g = m.State.calls.length; g > f; f++) if (!1 !== m.State.calls[f]) {
                l = !0;
                break
            }
            !1 === l && (m.State.isTicking = !1, delete m.State.calls, m.State.calls = [])
        }

        var u, h = function () {
            if (i.documentMode) return i.documentMode;
            for (var t = 7; t > 4; t--) {
                var e = i.createElement("div");
                if (e.innerHTML = "\x3c!--[if IE " + t + "]><span></span><![endif]--\x3e", e.getElementsByTagName("span").length) return e = null, t
            }
            return n
        }(), d = function () {
            var t = 0;
            return e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || function (e) {
                var i, n = (new Date).getTime();
                return i = Math.max(0, 16 - (n - t)), t = n + i, setTimeout(function () {
                    e(n + i)
                }, i)
            }
        }(), p = {
            isString: function (t) {
                return "string" == typeof t
            }, isArray: Array.isArray || function (t) {
                return "[object Array]" === Object.prototype.toString.call(t)
            }, isFunction: function (t) {
                return "[object Function]" === Object.prototype.toString.call(t)
            }, isNode: function (t) {
                return t && t.nodeType
            }, isNodeList: function (t) {
                return "object" == typeof t && /^\[object (HTMLCollection|NodeList|Object)\]$/.test(Object.prototype.toString.call(t)) && t.length !== n && (0 === t.length || "object" == typeof t[0] && t[0].nodeType > 0)
            }, isWrapped: function (t) {
                return t && (t.jquery || e.Zepto && e.Zepto.zepto.isZ(t))
            }, isSVG: function (t) {
                return e.SVGElement && t instanceof e.SVGElement
            }, isEmptyObject: function (t) {
                for (var e in t) return !1;
                return !0
            }
        }, f = !1;
        if (t.fn && t.fn.jquery ? (u = t, f = !0) : u = e.Velocity.Utilities, 8 >= h && !f) throw new Error("Velocity: IE8 and below require jQuery to be loaded before Velocity.");
        {
            if (!(7 >= h)) {
                var g = 400, v = "swing", m = {
                    State: {
                        isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
                        isAndroid: /Android/i.test(navigator.userAgent),
                        isGingerbread: /Android 2\.3\.[3-7]/i.test(navigator.userAgent),
                        isChrome: e.chrome,
                        isFirefox: /Firefox/i.test(navigator.userAgent),
                        prefixElement: i.createElement("div"),
                        prefixMatches: {},
                        scrollAnchor: null,
                        scrollPropertyLeft: null,
                        scrollPropertyTop: null,
                        isTicking: !1,
                        calls: []
                    },
                    CSS: {},
                    Utilities: u,
                    Redirects: {},
                    Easings: {},
                    Promise: e.Promise,
                    defaults: {
                        queue: "",
                        duration: g,
                        easing: v,
                        begin: n,
                        complete: n,
                        progress: n,
                        display: n,
                        visibility: n,
                        loop: !1,
                        delay: !1,
                        mobileHA: !0,
                        _cacheValues: !0
                    },
                    init: function (t) {
                        u.data(t, "velocity", {
                            isSVG: p.isSVG(t),
                            isAnimating: !1,
                            computedStyle: null,
                            tweensContainer: null,
                            rootPropertyValueCache: {},
                            transformCache: {}
                        })
                    },
                    hook: null,
                    mock: !1,
                    version: {major: 1, minor: 2, patch: 2},
                    debug: !1
                };
                e.pageYOffset !== n ? (m.State.scrollAnchor = e, m.State.scrollPropertyLeft = "pageXOffset", m.State.scrollPropertyTop = "pageYOffset") : (m.State.scrollAnchor = i.documentElement || i.body.parentNode || i.body, m.State.scrollPropertyLeft = "scrollLeft", m.State.scrollPropertyTop = "scrollTop");
                var y = function () {
                    function t(t) {
                        return -t.tension * t.x - t.friction * t.v
                    }

                    function e(e, i, n) {
                        var o = {x: e.x + n.dx * i, v: e.v + n.dv * i, tension: e.tension, friction: e.friction};
                        return {dx: o.v, dv: t(o)}
                    }

                    function i(i, n) {
                        var o = {dx: i.v, dv: t(i)}, r = e(i, .5 * n, o), s = e(i, .5 * n, r), a = e(i, n, s),
                            l = 1 / 6 * (o.dx + 2 * (r.dx + s.dx) + a.dx),
                            c = 1 / 6 * (o.dv + 2 * (r.dv + s.dv) + a.dv);
                        return i.x = i.x + l * n, i.v = i.v + c * n, i
                    }

                    return function t(e, n, o) {
                        var r, s, a, l = {x: -1, v: 0, tension: null, friction: null}, c = [0], u = 0;
                        for (e = parseFloat(e) || 500, n = parseFloat(n) || 20, o = o || null, l.tension = e, l.friction = n, (r = null !== o) ? (u = t(e, n), s = u / o * .016) : s = .016; a = i(a || l, s), c.push(1 + a.x), u += 16, Math.abs(a.x) > 1e-4 && Math.abs(a.v) > 1e-4;) ;
                        return r ? function (t) {
                            return c[t * (c.length - 1) | 0]
                        } : u
                    }
                }();
                m.Easings = {
                    linear: function (t) {
                        return t
                    }, swing: function (t) {
                        return .5 - Math.cos(t * Math.PI) / 2
                    }, spring: function (t) {
                        return 1 - Math.cos(4.5 * t * Math.PI) * Math.exp(6 * -t)
                    }
                }, u.each([["ease", [.25, .1, .25, 1]], ["ease-in", [.42, 0, 1, 1]], ["ease-out", [0, 0, .58, 1]], ["ease-in-out", [.42, 0, .58, 1]], ["easeInSine", [.47, 0, .745, .715]], ["easeOutSine", [.39, .575, .565, 1]], ["easeInOutSine", [.445, .05, .55, .95]], ["easeInQuad", [.55, .085, .68, .53]], ["easeOutQuad", [.25, .46, .45, .94]], ["easeInOutQuad", [.455, .03, .515, .955]], ["easeInCubic", [.55, .055, .675, .19]], ["easeOutCubic", [.215, .61, .355, 1]], ["easeInOutCubic", [.645, .045, .355, 1]], ["easeInQuart", [.895, .03, .685, .22]], ["easeOutQuart", [.165, .84, .44, 1]], ["easeInOutQuart", [.77, 0, .175, 1]], ["easeInQuint", [.755, .05, .855, .06]], ["easeOutQuint", [.23, 1, .32, 1]], ["easeInOutQuint", [.86, 0, .07, 1]], ["easeInExpo", [.95, .05, .795, .035]], ["easeOutExpo", [.19, 1, .22, 1]], ["easeInOutExpo", [1, 0, 0, 1]], ["easeInCirc", [.6, .04, .98, .335]], ["easeOutCirc", [.075, .82, .165, 1]], ["easeInOutCirc", [.785, .135, .15, .86]]], function (t, e) {
                    m.Easings[e[0]] = s.apply(null, e[1])
                });
                var b = m.CSS = {
                    RegEx: {
                        isHex: /^#([A-f\d]{3}){1,2}$/i,
                        valueUnwrap: /^[A-z]+\((.*)\)$/i,
                        wrappedValueAlreadyExtracted: /[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,
                        valueSplit: /([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi
                    },
                    Lists: {
                        colors: ["fill", "stroke", "stopColor", "color", "backgroundColor", "borderColor", "borderTopColor", "borderRightColor", "borderBottomColor", "borderLeftColor", "outlineColor"],
                        transformsBase: ["translateX", "translateY", "scale", "scaleX", "scaleY", "skewX", "skewY", "rotateZ"],
                        transforms3D: ["transformPerspective", "translateZ", "scaleZ", "rotateX", "rotateY"]
                    },
                    Hooks: {
                        templates: {
                            textShadow: ["Color X Y Blur", "black 0px 0px 0px"],
                            boxShadow: ["Color X Y Blur Spread", "black 0px 0px 0px 0px"],
                            clip: ["Top Right Bottom Left", "0px 0px 0px 0px"],
                            backgroundPosition: ["X Y", "0% 0%"],
                            transformOrigin: ["X Y Z", "50% 50% 0px"],
                            perspectiveOrigin: ["X Y", "50% 50%"]
                        }, registered: {}, register: function () {
                            for (r = 0; r < b.Lists.colors.length; r++) {
                                var t = "color" === b.Lists.colors[r] ? "0 0 0 1" : "255 255 255 1";
                                b.Hooks.templates[b.Lists.colors[r]] = ["Red Green Blue Alpha", t]
                            }
                            var e, i, n;
                            if (h) for (e in b.Hooks.templates) {
                                n = (i = b.Hooks.templates[e])[0].split(" ");
                                var o = i[1].match(b.RegEx.valueSplit);
                                "Color" === n[0] && (n.push(n.shift()), o.push(o.shift()), b.Hooks.templates[e] = [n.join(" "), o.join(" ")])
                            }
                            for (e in b.Hooks.templates) {
                                n = (i = b.Hooks.templates[e])[0].split(" ");
                                for (var r in n) {
                                    var s = e + n[r], a = r;
                                    b.Hooks.registered[s] = [e, a]
                                }
                            }
                        }, getRoot: function (t) {
                            var e = b.Hooks.registered[t];
                            return e ? e[0] : t
                        }, cleanRootPropertyValue: function (t, e) {
                            return b.RegEx.valueUnwrap.test(e) && (e = e.match(b.RegEx.valueUnwrap)[1]), b.Values.isCSSNullValue(e) && (e = b.Hooks.templates[t][1]), e
                        }, extractValue: function (t, e) {
                            var i = b.Hooks.registered[t];
                            if (i) {
                                var n = i[0], o = i[1];
                                return (e = b.Hooks.cleanRootPropertyValue(n, e)).toString().match(b.RegEx.valueSplit)[o]
                            }
                            return e
                        }, injectValue: function (t, e, i) {
                            var n = b.Hooks.registered[t];
                            if (n) {
                                var o, r = n[0], s = n[1];
                                return i = b.Hooks.cleanRootPropertyValue(r, i), o = i.toString().match(b.RegEx.valueSplit), o[s] = e, o.join(" ")
                            }
                            return i
                        }
                    },
                    Normalizations: {
                        registered: {
                            clip: function (t, e, i) {
                                switch (t) {
                                    case"name":
                                        return "clip";
                                    case"extract":
                                        var n;
                                        return b.RegEx.wrappedValueAlreadyExtracted.test(i) ? n = i : (n = i.toString().match(b.RegEx.valueUnwrap), n = n ? n[1].replace(/,(\s+)?/g, " ") : i), n;
                                    case"inject":
                                        return "rect(" + i + ")"
                                }
                            }, blur: function (t, e, i) {
                                switch (t) {
                                    case"name":
                                        return m.State.isFirefox ? "filter" : "-webkit-filter";
                                    case"extract":
                                        var n = parseFloat(i);
                                        if (!n && 0 !== n) {
                                            var o = i.toString().match(/blur\(([0-9]+[A-z]+)\)/i);
                                            n = o ? o[1] : 0
                                        }
                                        return n;
                                    case"inject":
                                        return parseFloat(i) ? "blur(" + i + ")" : "none"
                                }
                            }, opacity: function (t, e, i) {
                                if (8 >= h) switch (t) {
                                    case"name":
                                        return "filter";
                                    case"extract":
                                        var n = i.toString().match(/alpha\(opacity=(.*)\)/i);
                                        return i = n ? n[1] / 100 : 1;
                                    case"inject":
                                        return e.style.zoom = 1, parseFloat(i) >= 1 ? "" : "alpha(opacity=" + parseInt(100 * parseFloat(i), 10) + ")"
                                } else switch (t) {
                                    case"name":
                                        return "opacity";
                                    case"extract":
                                    case"inject":
                                        return i
                                }
                            }
                        }, register: function () {
                            9 >= h || m.State.isGingerbread || (b.Lists.transformsBase = b.Lists.transformsBase.concat(b.Lists.transforms3D));
                            for (t = 0; t < b.Lists.transformsBase.length; t++) !function () {
                                var e = b.Lists.transformsBase[t];
                                b.Normalizations.registered[e] = function (t, i, o) {
                                    switch (t) {
                                        case"name":
                                            return "transform";
                                        case"extract":
                                            return r(i) === n || r(i).transformCache[e] === n ? /^scale/i.test(e) ? 1 : 0 : r(i).transformCache[e].replace(/[()]/g, "");
                                        case"inject":
                                            var s = !1;
                                            switch (e.substr(0, e.length - 1)) {
                                                case"translate":
                                                    s = !/(%|px|em|rem|vw|vh|\d)$/i.test(o);
                                                    break;
                                                case"scal":
                                                case"scale":
                                                    m.State.isAndroid && r(i).transformCache[e] === n && 1 > o && (o = 1), s = !/(\d)$/i.test(o);
                                                    break;
                                                case"skew":
                                                    s = !/(deg|\d)$/i.test(o);
                                                    break;
                                                case"rotate":
                                                    s = !/(deg|\d)$/i.test(o)
                                            }
                                            return s || (r(i).transformCache[e] = "(" + o + ")"), r(i).transformCache[e]
                                    }
                                }
                            }();
                            for (var t = 0; t < b.Lists.colors.length; t++) !function () {
                                var e = b.Lists.colors[t];
                                b.Normalizations.registered[e] = function (t, i, o) {
                                    switch (t) {
                                        case"name":
                                            return e;
                                        case"extract":
                                            var r;
                                            if (b.RegEx.wrappedValueAlreadyExtracted.test(o)) r = o; else {
                                                var s, a = {
                                                    black: "rgb(0, 0, 0)",
                                                    blue: "rgb(0, 0, 255)",
                                                    gray: "rgb(128, 128, 128)",
                                                    green: "rgb(0, 128, 0)",
                                                    red: "rgb(255, 0, 0)",
                                                    white: "rgb(255, 255, 255)"
                                                };
                                                /^[A-z]+$/i.test(o) ? s = a[o] !== n ? a[o] : a.black : b.RegEx.isHex.test(o) ? s = "rgb(" + b.Values.hexToRgb(o).join(" ") + ")" : /^rgba?\(/i.test(o) || (s = a.black), r = (s || o).toString().match(b.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g, " ")
                                            }
                                            return 8 >= h || 3 !== r.split(" ").length || (r += " 1"), r;
                                        case"inject":
                                            return 8 >= h ? 4 === o.split(" ").length && (o = o.split(/\s+/).slice(0, 3).join(" ")) : 3 === o.split(" ").length && (o += " 1"), (8 >= h ? "rgb" : "rgba") + "(" + o.replace(/\s+/g, ",").replace(/\.(\d)+(?=,)/g, "") + ")"
                                    }
                                }
                            }()
                        }
                    },
                    Names: {
                        camelCase: function (t) {
                            return t.replace(/-(\w)/g, function (t, e) {
                                return e.toUpperCase()
                            })
                        }, SVGAttribute: function (t) {
                            var e = "width|height|x|y|cx|cy|r|rx|ry|x1|x2|y1|y2";
                            return (h || m.State.isAndroid && !m.State.isChrome) && (e += "|transform"), new RegExp("^(" + e + ")$", "i").test(t)
                        }, prefixCheck: function (t) {
                            if (m.State.prefixMatches[t]) return [m.State.prefixMatches[t], !0];
                            for (var e = ["", "Webkit", "Moz", "ms", "O"], i = 0, n = e.length; n > i; i++) {
                                var o;
                                if (o = 0 === i ? t : e[i] + t.replace(/^\w/, function (t) {
                                        return t.toUpperCase()
                                    }), p.isString(m.State.prefixElement.style[o])) return m.State.prefixMatches[t] = o, [o, !0]
                            }
                            return [t, !1]
                        }
                    },
                    Values: {
                        hexToRgb: function (t) {
                            var e;
                            return t = t.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function (t, e, i, n) {
                                return e + e + i + i + n + n
                            }), (e = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(t)) ? [parseInt(e[1], 16), parseInt(e[2], 16), parseInt(e[3], 16)] : [0, 0, 0]
                        }, isCSSNullValue: function (t) {
                            return 0 == t || /^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(t)
                        }, getUnitType: function (t) {
                            return /^(rotate|skew)/i.test(t) ? "deg" : /(^(scale|scaleX|scaleY|scaleZ|alpha|flexGrow|flexHeight|zIndex|fontWeight)$)|((opacity|red|green|blue|alpha)$)/i.test(t) ? "" : "px"
                        }, getDisplayType: function (t) {
                            var e = t && t.tagName.toString().toLowerCase();
                            return /^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(e) ? "inline" : /^(li)$/i.test(e) ? "list-item" : /^(tr)$/i.test(e) ? "table-row" : /^(table)$/i.test(e) ? "table" : /^(tbody)$/i.test(e) ? "table-row-group" : "block"
                        }, addClass: function (t, e) {
                            t.classList ? t.classList.add(e) : t.className += (t.className.length ? " " : "") + e
                        }, removeClass: function (t, e) {
                            t.classList ? t.classList.remove(e) : t.className = t.className.toString().replace(new RegExp("(^|\\s)" + e.split(" ").join("|") + "(\\s|$)", "gi"), " ")
                        }
                    },
                    getPropertyValue: function (t, i, o, s) {
                        function a(t, i) {
                            function o() {
                                c && b.setPropertyValue(t, "display", "none")
                            }

                            var l = 0;
                            if (8 >= h) l = u.css(t, i); else {
                                var c = !1;
                                if (/^(width|height)$/.test(i) && 0 === b.getPropertyValue(t, "display") && (c = !0, b.setPropertyValue(t, "display", b.Values.getDisplayType(t))), !s) {
                                    if ("height" === i && "border-box" !== b.getPropertyValue(t, "boxSizing").toString().toLowerCase()) {
                                        var d = t.offsetHeight - (parseFloat(b.getPropertyValue(t, "borderTopWidth")) || 0) - (parseFloat(b.getPropertyValue(t, "borderBottomWidth")) || 0) - (parseFloat(b.getPropertyValue(t, "paddingTop")) || 0) - (parseFloat(b.getPropertyValue(t, "paddingBottom")) || 0);
                                        return o(), d
                                    }
                                    if ("width" === i && "border-box" !== b.getPropertyValue(t, "boxSizing").toString().toLowerCase()) {
                                        var p = t.offsetWidth - (parseFloat(b.getPropertyValue(t, "borderLeftWidth")) || 0) - (parseFloat(b.getPropertyValue(t, "borderRightWidth")) || 0) - (parseFloat(b.getPropertyValue(t, "paddingLeft")) || 0) - (parseFloat(b.getPropertyValue(t, "paddingRight")) || 0);
                                        return o(), p
                                    }
                                }
                                var f;
                                f = r(t) === n ? e.getComputedStyle(t, null) : r(t).computedStyle ? r(t).computedStyle : r(t).computedStyle = e.getComputedStyle(t, null), "borderColor" === i && (i = "borderTopColor"), ("" === (l = 9 === h && "filter" === i ? f.getPropertyValue(i) : f[i]) || null === l) && (l = t.style[i]), o()
                            }
                            if ("auto" === l && /^(top|right|bottom|left)$/i.test(i)) {
                                var g = a(t, "position");
                                ("fixed" === g || "absolute" === g && /top|left/i.test(i)) && (l = u(t).position()[i] + "px")
                            }
                            return l
                        }

                        var l;
                        if (b.Hooks.registered[i]) {
                            var c = i, d = b.Hooks.getRoot(c);
                            o === n && (o = b.getPropertyValue(t, b.Names.prefixCheck(d)[0])), b.Normalizations.registered[d] && (o = b.Normalizations.registered[d]("extract", t, o)), l = b.Hooks.extractValue(c, o)
                        } else if (b.Normalizations.registered[i]) {
                            var p, f;
                            "transform" !== (p = b.Normalizations.registered[i]("name", t)) && (f = a(t, b.Names.prefixCheck(p)[0]), b.Values.isCSSNullValue(f) && b.Hooks.templates[i] && (f = b.Hooks.templates[i][1])), l = b.Normalizations.registered[i]("extract", t, f)
                        }
                        if (!/^[\d-]/.test(l)) if (r(t) && r(t).isSVG && b.Names.SVGAttribute(i)) if (/^(height|width)$/i.test(i)) try {
                            l = t.getBBox()[i]
                        } catch (t) {
                            l = 0
                        } else l = t.getAttribute(i); else l = a(t, b.Names.prefixCheck(i)[0]);
                        return b.Values.isCSSNullValue(l) && (l = 0), m.debug >= 2 && console.log("Get " + i + ": " + l), l
                    },
                    setPropertyValue: function (t, i, n, o, s) {
                        var a = i;
                        if ("scroll" === i) s.container ? s.container["scroll" + s.direction] = n : "Left" === s.direction ? e.scrollTo(n, s.alternateValue) : e.scrollTo(s.alternateValue, n); else if (b.Normalizations.registered[i] && "transform" === b.Normalizations.registered[i]("name", t)) b.Normalizations.registered[i]("inject", t, n), a = "transform", n = r(t).transformCache[i]; else {
                            if (b.Hooks.registered[i]) {
                                var l = i, c = b.Hooks.getRoot(i);
                                o = o || b.getPropertyValue(t, c), n = b.Hooks.injectValue(l, n, o), i = c
                            }
                            if (b.Normalizations.registered[i] && (n = b.Normalizations.registered[i]("inject", t, n), i = b.Normalizations.registered[i]("name", t)), a = b.Names.prefixCheck(i)[0], 8 >= h) try {
                                t.style[a] = n
                            } catch (t) {
                                m.debug && console.log("Browser does not support [" + n + "] for [" + a + "]")
                            } else r(t) && r(t).isSVG && b.Names.SVGAttribute(i) ? t.setAttribute(i, n) : t.style[a] = n;
                            m.debug >= 2 && console.log("Set " + i + " (" + a + "): " + n)
                        }
                        return [a, n]
                    },
                    flushTransformCache: function (t) {
                        function e(e) {
                            return parseFloat(b.getPropertyValue(t, e))
                        }

                        var i = "";
                        if ((h || m.State.isAndroid && !m.State.isChrome) && r(t).isSVG) {
                            var n = {
                                translate: [e("translateX"), e("translateY")],
                                skewX: [e("skewX")],
                                skewY: [e("skewY")],
                                scale: 1 !== e("scale") ? [e("scale"), e("scale")] : [e("scaleX"), e("scaleY")],
                                rotate: [e("rotateZ"), 0, 0]
                            };
                            u.each(r(t).transformCache, function (t) {
                                /^translate/i.test(t) ? t = "translate" : /^scale/i.test(t) ? t = "scale" : /^rotate/i.test(t) && (t = "rotate"), n[t] && (i += t + "(" + n[t].join(" ") + ") ", delete n[t])
                            })
                        } else {
                            var o, s;
                            u.each(r(t).transformCache, function (e) {
                                return o = r(t).transformCache[e], "transformPerspective" === e ? (s = o, !0) : (9 === h && "rotateZ" === e && (e = "rotate"), void(i += e + o + " "))
                            }), s && (i = "perspective" + s + " " + i)
                        }
                        b.setPropertyValue(t, "transform", i)
                    }
                };
                b.Hooks.register(), b.Normalizations.register(), m.hook = function (t, e, i) {
                    var s = n;
                    return t = o(t), u.each(t, function (t, o) {
                        if (r(o) === n && m.init(o), i === n) s === n && (s = m.CSS.getPropertyValue(o, e)); else {
                            var a = m.CSS.setPropertyValue(o, e, i);
                            "transform" === a[0] && m.CSS.flushTransformCache(o), s = a
                        }
                    }), s
                };
                var w = function () {
                    function t() {
                        return h ? S.promise || null : d
                    }

                    function s() {
                        function t(t) {
                            function d(t, e) {
                                var i = n, o = n, r = n;
                                return p.isArray(t) ? (i = t[0], !p.isArray(t[1]) && /^[\d-]/.test(t[1]) || p.isFunction(t[1]) || b.RegEx.isHex.test(t[1]) ? r = t[1] : (p.isString(t[1]) && !b.RegEx.isHex.test(t[1]) || p.isArray(t[1])) && (o = e ? t[1] : a(t[1], c.duration), t[2] !== n && (r = t[2]))) : i = t, e || (o = o || c.easing), p.isFunction(i) && (i = i.call(s, T, k)), p.isFunction(r) && (r = r.call(s, T, k)), [i || 0, o, r]
                            }

                            function f(t, e) {
                                var i, n;
                                return n = (e || "0").toString().toLowerCase().replace(/[%A-z]+$/, function (t) {
                                    return i = t, ""
                                }), i || (i = b.Values.getUnitType(t)), [n, i]
                            }

                            if (c.begin && 0 === T) try {
                                c.begin.call(v, v)
                            } catch (t) {
                                setTimeout(function () {
                                    throw t
                                }, 1)
                            }
                            if ("scroll" === E) {
                                var g, w, C, _ = /^x$/i.test(c.axis) ? "Left" : "Top", A = parseFloat(c.offset) || 0;
                                c.container ? p.isWrapped(c.container) || p.isNode(c.container) ? (c.container = c.container[0] || c.container, g = c.container["scroll" + _], C = g + u(s).position()[_.toLowerCase()] + A) : c.container = null : (g = m.State.scrollAnchor[m.State["scrollProperty" + _]], w = m.State.scrollAnchor[m.State["scrollProperty" + ("Left" === _ ? "Top" : "Left")]], C = u(s).offset()[_.toLowerCase()] + A), h = {
                                    scroll: {
                                        rootPropertyValue: !1,
                                        startValue: g,
                                        currentValue: g,
                                        endValue: C,
                                        unitType: "",
                                        easing: c.easing,
                                        scrollData: {container: c.container, direction: _, alternateValue: w}
                                    }, element: s
                                }, m.debug && console.log("tweensContainer (scroll): ", h.scroll, s)
                            } else if ("reverse" === E) {
                                if (!r(s).tweensContainer) return void u.dequeue(s, c.queue);
                                "none" === r(s).opts.display && (r(s).opts.display = "auto"), "hidden" === r(s).opts.visibility && (r(s).opts.visibility = "visible"), r(s).opts.loop = !1, r(s).opts.begin = null, r(s).opts.complete = null, x.easing || delete c.easing, x.duration || delete c.duration, c = u.extend({}, r(s).opts, c);
                                D = u.extend(!0, {}, r(s).tweensContainer);
                                for (var $ in D) if ("element" !== $) {
                                    var P = D[$].startValue;
                                    D[$].startValue = D[$].currentValue = D[$].endValue, D[$].endValue = P, p.isEmptyObject(x) || (D[$].easing = c.easing), m.debug && console.log("reverse tweensContainer (" + $ + "): " + JSON.stringify(D[$]), s)
                                }
                                h = D
                            } else if ("start" === E) {
                                var D;
                                r(s).tweensContainer && !0 === r(s).isAnimating && (D = r(s).tweensContainer), u.each(y, function (t, e) {
                                    if (RegExp("^" + b.Lists.colors.join("$|^") + "$").test(t)) {
                                        var i = d(e, !0), o = i[0], r = i[1], s = i[2];
                                        if (b.RegEx.isHex.test(o)) {
                                            for (var a = ["Red", "Green", "Blue"], l = b.Values.hexToRgb(o), c = s ? b.Values.hexToRgb(s) : n, u = 0; u < a.length; u++) {
                                                var h = [l[u]];
                                                r && h.push(r), c !== n && h.push(c[u]), y[t + a[u]] = h
                                            }
                                            delete y[t]
                                        }
                                    }
                                });
                                for (var I in y) {
                                    var N = d(y[I]), j = N[0], H = N[1], L = N[2];
                                    I = b.Names.camelCase(I);
                                    var q = b.Hooks.getRoot(I), z = !1;
                                    if (r(s).isSVG || "tween" === q || !1 !== b.Names.prefixCheck(q)[1] || b.Normalizations.registered[q] !== n) {
                                        (c.display !== n && null !== c.display && "none" !== c.display || c.visibility !== n && "hidden" !== c.visibility) && /opacity|filter/.test(I) && !L && 0 !== j && (L = 0), c._cacheValues && D && D[I] ? (L === n && (L = D[I].endValue + D[I].unitType), z = r(s).rootPropertyValueCache[q]) : b.Hooks.registered[I] ? L === n ? (z = b.getPropertyValue(s, q), L = b.getPropertyValue(s, I, z)) : z = b.Hooks.templates[q][1] : L === n && (L = b.getPropertyValue(s, I));
                                        var R, W, F, V = !1;
                                        if (R = f(I, L), L = R[0], F = R[1], R = f(I, j), j = R[0].replace(/^([+-\/*])=/, function (t, e) {
                                                return V = e, ""
                                            }), W = R[1], L = parseFloat(L) || 0, j = parseFloat(j) || 0, "%" === W && (/^(fontSize|lineHeight)$/.test(I) ? (j /= 100, W = "em") : /^scale/.test(I) ? (j /= 100, W = "") : /(Red|Green|Blue)$/i.test(I) && (j = j / 100 * 255, W = "")), /[\/*]/.test(V)) W = F; else if (F !== W && 0 !== L) if (0 === j) W = F; else {
                                            o = o || function () {
                                                var t = {
                                                        myParent: s.parentNode || i.body,
                                                        position: b.getPropertyValue(s, "position"),
                                                        fontSize: b.getPropertyValue(s, "fontSize")
                                                    }, n = t.position === O.lastPosition && t.myParent === O.lastParent,
                                                    o = t.fontSize === O.lastFontSize;
                                                O.lastParent = t.myParent, O.lastPosition = t.position, O.lastFontSize = t.fontSize;
                                                var a = 100, l = {};
                                                if (o && n) l.emToPx = O.lastEmToPx, l.percentToPxWidth = O.lastPercentToPxWidth, l.percentToPxHeight = O.lastPercentToPxHeight; else {
                                                    var c = r(s).isSVG ? i.createElementNS("http://www.w3.org/2000/svg", "rect") : i.createElement("div");
                                                    m.init(c), t.myParent.appendChild(c), u.each(["overflow", "overflowX", "overflowY"], function (t, e) {
                                                        m.CSS.setPropertyValue(c, e, "hidden")
                                                    }), m.CSS.setPropertyValue(c, "position", t.position), m.CSS.setPropertyValue(c, "fontSize", t.fontSize), m.CSS.setPropertyValue(c, "boxSizing", "content-box"), u.each(["minWidth", "maxWidth", "width", "minHeight", "maxHeight", "height"], function (t, e) {
                                                        m.CSS.setPropertyValue(c, e, a + "%")
                                                    }), m.CSS.setPropertyValue(c, "paddingLeft", a + "em"), l.percentToPxWidth = O.lastPercentToPxWidth = (parseFloat(b.getPropertyValue(c, "width", null, !0)) || 1) / a, l.percentToPxHeight = O.lastPercentToPxHeight = (parseFloat(b.getPropertyValue(c, "height", null, !0)) || 1) / a, l.emToPx = O.lastEmToPx = (parseFloat(b.getPropertyValue(c, "paddingLeft")) || 1) / a, t.myParent.removeChild(c)
                                                }
                                                return null === O.remToPx && (O.remToPx = parseFloat(b.getPropertyValue(i.body, "fontSize")) || 16), null === O.vwToPx && (O.vwToPx = parseFloat(e.innerWidth) / 100, O.vhToPx = parseFloat(e.innerHeight) / 100), l.remToPx = O.remToPx, l.vwToPx = O.vwToPx, l.vhToPx = O.vhToPx, m.debug >= 1 && console.log("Unit ratios: " + JSON.stringify(l), s), l
                                            }();
                                            var Q = /margin|padding|left|right|width|text|word|letter/i.test(I) || /X$/.test(I) || "x" === I ? "x" : "y";
                                            switch (F) {
                                                case"%":
                                                    L *= "x" === Q ? o.percentToPxWidth : o.percentToPxHeight;
                                                    break;
                                                case"px":
                                                    break;
                                                default:
                                                    L *= o[F + "ToPx"]
                                            }
                                            switch (W) {
                                                case"%":
                                                    L *= 1 / ("x" === Q ? o.percentToPxWidth : o.percentToPxHeight);
                                                    break;
                                                case"px":
                                                    break;
                                                default:
                                                    L *= 1 / o[W + "ToPx"]
                                            }
                                        }
                                        switch (V) {
                                            case"+":
                                                j = L + j;
                                                break;
                                            case"-":
                                                j = L - j;
                                                break;
                                            case"*":
                                                j *= L;
                                                break;
                                            case"/":
                                                j = L / j
                                        }
                                        h[I] = {
                                            rootPropertyValue: z,
                                            startValue: L,
                                            currentValue: L,
                                            endValue: j,
                                            unitType: W,
                                            easing: H
                                        }, m.debug && console.log("tweensContainer (" + I + "): " + JSON.stringify(h[I]), s)
                                    } else m.debug && console.log("Skipping [" + q + "] due to a lack of browser support.")
                                }
                                h.element = s
                            }
                            h.element && (b.Values.addClass(s, "velocity-animating"), M.push(h), "" === c.queue && (r(s).tweensContainer = h, r(s).opts = c), r(s).isAnimating = !0, T === k - 1 ? (m.State.calls.push([M, v, c, null, S.resolver]), !1 === m.State.isTicking && (m.State.isTicking = !0, l())) : T++)
                        }

                        var o, s = this, c = u.extend({}, m.defaults, x), h = {};
                        switch (r(s) === n && m.init(s), parseFloat(c.delay) && !1 !== c.queue && u.queue(s, c.queue, function (t) {
                            m.velocityQueueEntryFlag = !0, r(s).delayTimer = {
                                setTimeout: setTimeout(t, parseFloat(c.delay)),
                                next: t
                            }
                        }), c.duration.toString().toLowerCase()) {
                            case"fast":
                                c.duration = 200;
                                break;
                            case"normal":
                                c.duration = g;
                                break;
                            case"slow":
                                c.duration = 600;
                                break;
                            default:
                                c.duration = parseFloat(c.duration) || 1
                        }
                        !1 !== m.mock && (!0 === m.mock ? c.duration = c.delay = 1 : (c.duration *= parseFloat(m.mock) || 1, c.delay *= parseFloat(m.mock) || 1)), c.easing = a(c.easing, c.duration), c.begin && !p.isFunction(c.begin) && (c.begin = null), c.progress && !p.isFunction(c.progress) && (c.progress = null), c.complete && !p.isFunction(c.complete) && (c.complete = null), c.display !== n && null !== c.display && (c.display = c.display.toString().toLowerCase(), "auto" === c.display && (c.display = m.CSS.Values.getDisplayType(s))), c.visibility !== n && null !== c.visibility && (c.visibility = c.visibility.toString().toLowerCase()), c.mobileHA = c.mobileHA && m.State.isMobile && !m.State.isGingerbread, !1 === c.queue ? c.delay ? setTimeout(t, c.delay) : t() : u.queue(s, c.queue, function (e, i) {
                            return !0 === i ? (S.promise && S.resolver(v), !0) : (m.velocityQueueEntryFlag = !0, void t())
                        }), "" !== c.queue && "fx" !== c.queue || "inprogress" === u.queue(s)[0] || u.dequeue(s)
                    }

                    var h, d, f, v, y, x,
                        C = arguments[0] && (arguments[0].p || u.isPlainObject(arguments[0].properties) && !arguments[0].properties.names || p.isString(arguments[0].properties));
                    if (p.isWrapped(this) ? (h = !1, f = 0, v = this, d = this) : (h = !0, f = 1, v = C ? arguments[0].elements || arguments[0].e : arguments[0]), v = o(v)) {
                        C ? (y = arguments[0].properties || arguments[0].p, x = arguments[0].options || arguments[0].o) : (y = arguments[f], x = arguments[f + 1]);
                        var k = v.length, T = 0;
                        if (!/^(stop|finish)$/i.test(y) && !u.isPlainObject(x)) {
                            x = {};
                            for (var _ = f + 1; _ < arguments.length; _++) p.isArray(arguments[_]) || !/^(fast|normal|slow)$/i.test(arguments[_]) && !/^\d/.test(arguments[_]) ? p.isString(arguments[_]) || p.isArray(arguments[_]) ? x.easing = arguments[_] : p.isFunction(arguments[_]) && (x.complete = arguments[_]) : x.duration = arguments[_]
                        }
                        var S = {promise: null, resolver: null, rejecter: null};
                        h && m.Promise && (S.promise = new m.Promise(function (t, e) {
                            S.resolver = t, S.rejecter = e
                        }));
                        var E;
                        switch (y) {
                            case"scroll":
                                E = "scroll";
                                break;
                            case"reverse":
                                E = "reverse";
                                break;
                            case"finish":
                            case"stop":
                                u.each(v, function (t, e) {
                                    r(e) && r(e).delayTimer && (clearTimeout(r(e).delayTimer.setTimeout), r(e).delayTimer.next && r(e).delayTimer.next(), delete r(e).delayTimer)
                                });
                                var A = [];
                                return u.each(m.State.calls, function (t, e) {
                                    e && u.each(e[1], function (i, o) {
                                        var s = x === n ? "" : x;
                                        return !0 !== s && e[2].queue !== s && (x !== n || !1 !== e[2].queue) || void u.each(v, function (i, n) {
                                            n === o && ((!0 === x || p.isString(x)) && (u.each(u.queue(n, p.isString(x) ? x : ""), function (t, e) {
                                                p.isFunction(e) && e(null, !0)
                                            }), u.queue(n, p.isString(x) ? x : "", [])), "stop" === y ? (r(n) && r(n).tweensContainer && !1 !== s && u.each(r(n).tweensContainer, function (t, e) {
                                                e.endValue = e.currentValue
                                            }), A.push(t)) : "finish" === y && (e[2].duration = 1))
                                        })
                                    })
                                }), "stop" === y && (u.each(A, function (t, e) {
                                    c(e, !0)
                                }), S.promise && S.resolver(v)), t();
                            default:
                                if (!u.isPlainObject(y) || p.isEmptyObject(y)) {
                                    if (p.isString(y) && m.Redirects[y]) {
                                        var $ = (N = u.extend({}, x)).duration, P = N.delay || 0;
                                        return !0 === N.backwards && (v = u.extend(!0, [], v).reverse()), u.each(v, function (t, e) {
                                            parseFloat(N.stagger) ? N.delay = P + parseFloat(N.stagger) * t : p.isFunction(N.stagger) && (N.delay = P + N.stagger.call(e, t, k)), N.drag && (N.duration = parseFloat($) || (/^(callout|transition)/.test(y) ? 1e3 : g), N.duration = Math.max(N.duration * (N.backwards ? 1 - t / k : (t + 1) / k), .75 * N.duration, 200)), m.Redirects[y].call(e, e, N || {}, t, k, v, S.promise ? S : n)
                                        }), t()
                                    }
                                    var D = "Velocity: First argument (" + y + ") was not a property map, a known action, or a registered redirect. Aborting.";
                                    return S.promise ? S.rejecter(new Error(D)) : console.log(D), t()
                                }
                                E = "start"
                        }
                        var O = {
                            lastParent: null,
                            lastPosition: null,
                            lastFontSize: null,
                            lastPercentToPxWidth: null,
                            lastPercentToPxHeight: null,
                            lastEmToPx: null,
                            remToPx: null,
                            vwToPx: null,
                            vhToPx: null
                        }, M = [];
                        u.each(v, function (t, e) {
                            p.isNode(e) && s.call(e)
                        });
                        var I, N = u.extend({}, m.defaults, x);
                        if (N.loop = parseInt(N.loop), I = 2 * N.loop - 1, N.loop) for (var j = 0; I > j; j++) {
                            var H = {delay: N.delay, progress: N.progress};
                            j === I - 1 && (H.display = N.display, H.visibility = N.visibility, H.complete = N.complete), w(v, "reverse", H)
                        }
                        return t()
                    }
                };
                (m = u.extend(w, m)).animate = w;
                var x = e.requestAnimationFrame || d;
                return m.State.isMobile || i.hidden === n || i.addEventListener("visibilitychange", function () {
                    i.hidden ? (x = function (t) {
                        return setTimeout(function () {
                            t(!0)
                        }, 16)
                    }, l()) : x = e.requestAnimationFrame || d
                }), t.Velocity = m, t !== e && (t.fn.velocity = w, t.fn.velocity.defaults = m.defaults), u.each(["Down", "Up"], function (t, e) {
                    m.Redirects["slide" + e] = function (t, i, o, r, s, a) {
                        var l = u.extend({}, i), c = l.begin, h = l.complete,
                            d = {height: "", marginTop: "", marginBottom: "", paddingTop: "", paddingBottom: ""},
                            p = {};
                        l.display === n && (l.display = "Down" === e ? "inline" === m.CSS.Values.getDisplayType(t) ? "inline-block" : "block" : "none"), l.begin = function () {
                            c && c.call(s, s);
                            for (var i in d) {
                                p[i] = t.style[i];
                                var n = m.CSS.getPropertyValue(t, i);
                                d[i] = "Down" === e ? [n, 0] : [0, n]
                            }
                            p.overflow = t.style.overflow, t.style.overflow = "hidden"
                        }, l.complete = function () {
                            for (var e in p) t.style[e] = p[e];
                            h && h.call(s, s), a && a.resolver(s)
                        }, m(t, d, l)
                    }
                }), u.each(["In", "Out"], function (t, e) {
                    m.Redirects["fade" + e] = function (t, i, o, r, s, a) {
                        var l = u.extend({}, i), c = {opacity: "In" === e ? 1 : 0}, h = l.complete;
                        l.complete = o !== r - 1 ? l.begin = null : function () {
                            h && h.call(s, s), a && a.resolver(s)
                        }, l.display === n && (l.display = "In" === e ? "auto" : "none"), m(this, c, l)
                    }
                }), m
            }
            jQuery.fn.velocity = jQuery.fn.animate
        }
    }(window.jQuery || window.Zepto || window, window, document)
})), function (t, e, i, n) {
    "use strict";

    function o(t, e, i) {
        return setTimeout(u(t, i), e)
    }

    function r(t, e, i) {
        return !!Array.isArray(t) && (s(t, i[e], i), !0)
    }

    function s(t, e, i) {
        var o;
        if (t) if (t.forEach) t.forEach(e, i); else if (t.length !== n) for (o = 0; o < t.length;) e.call(i, t[o], o, t), o++; else for (o in t) t.hasOwnProperty(o) && e.call(i, t[o], o, t)
    }

    function a(t, e, i) {
        for (var o = Object.keys(e), r = 0; r < o.length;) (!i || i && t[o[r]] === n) && (t[o[r]] = e[o[r]]), r++;
        return t
    }

    function l(t, e) {
        return a(t, e, !0)
    }

    function c(t, e, i) {
        var n, o = e.prototype;
        (n = t.prototype = Object.create(o)).constructor = t, n._super = o, i && a(n, i)
    }

    function u(t, e) {
        return function () {
            return t.apply(e, arguments)
        }
    }

    function h(t, e) {
        return typeof t == J ? t.apply(e ? e[0] || n : n, e) : t
    }

    function d(t, e) {
        return t === n ? e : t
    }

    function p(t, e, i) {
        s(m(e), function (e) {
            t.addEventListener(e, i, !1)
        })
    }

    function f(t, e, i) {
        s(m(e), function (e) {
            t.removeEventListener(e, i, !1)
        })
    }

    function g(t, e) {
        for (; t;) {
            if (t == e) return !0;
            t = t.parentNode
        }
        return !1
    }

    function v(t, e) {
        return t.indexOf(e) > -1
    }

    function m(t) {
        return t.trim().split(/\s+/g)
    }

    function y(t, e, i) {
        if (t.indexOf && !i) return t.indexOf(e);
        for (var n = 0; n < t.length;) {
            if (i && t[n][i] == e || !i && t[n] === e) return n;
            n++
        }
        return -1
    }

    function b(t) {
        return Array.prototype.slice.call(t, 0)
    }

    function w(t, e, i) {
        for (var n = [], o = [], r = 0; r < t.length;) {
            var s = e ? t[r][e] : t[r];
            y(o, s) < 0 && n.push(t[r]), o[r] = s, r++
        }
        return i && (n = e ? n.sort(function (t, i) {
            return t[e] > i[e]
        }) : n.sort()), n
    }

    function x(t, e) {
        for (var i, o, r = e[0].toUpperCase() + e.slice(1), s = 0; s < G.length;) {
            if (i = G[s], (o = i ? i + r : e) in t) return o;
            s++
        }
        return n
    }

    function C(t) {
        var e = t.ownerDocument;
        return e.defaultView || e.parentWindow
    }

    function k(t, e) {
        var i = this;
        this.manager = t, this.callback = e, this.element = t.element, this.target = t.options.inputTarget, this.domHandler = function (e) {
            h(t.options.enable, [t]) && i.handler(e)
        }, this.init()
    }

    function T(t, e) {
        var i, o, r, s, a = t.lastInterval || e, l = e.timeStamp - a.timeStamp;
        if (e.eventType != dt && (l > lt || a.velocity === n)) {
            var c = a.deltaX - e.deltaX, u = a.deltaY - e.deltaY, h = function (t, e, i) {
                return {x: e / t || 0, y: i / t || 0}
            }(l, c, u);
            o = h.x, r = h.y, i = tt(h.x) > tt(h.y) ? h.x : h.y, s = E(c, u), t.lastInterval = e
        } else i = a.velocity, o = a.velocityX, r = a.velocityY, s = a.direction;
        e.velocity = i, e.velocityX = o, e.velocityY = r, e.direction = s
    }

    function _(t) {
        for (var e = [], i = 0; i < t.pointers.length;) e[i] = {
            clientX: K(t.pointers[i].clientX),
            clientY: K(t.pointers[i].clientY)
        }, i++;
        return {timeStamp: et(), pointers: e, center: S(e), deltaX: t.deltaX, deltaY: t.deltaY}
    }

    function S(t) {
        var e = t.length;
        if (1 === e) return {x: K(t[0].clientX), y: K(t[0].clientY)};
        for (var i = 0, n = 0, o = 0; e > o;) i += t[o].clientX, n += t[o].clientY, o++;
        return {x: K(i / e), y: K(n / e)}
    }

    function E(t, e) {
        return t === e ? pt : tt(t) >= tt(e) ? t > 0 ? ft : gt : e > 0 ? vt : mt
    }

    function A(t, e, i) {
        i || (i = xt);
        var n = e[i[0]] - t[i[0]], o = e[i[1]] - t[i[1]];
        return Math.sqrt(n * n + o * o)
    }

    function $(t, e, i) {
        i || (i = xt);
        var n = e[i[0]] - t[i[0]], o = e[i[1]] - t[i[1]];
        return 180 * Math.atan2(o, n) / Math.PI
    }

    function P() {
        this.evEl = Tt, this.evWin = _t, this.allow = !0, this.pressed = !1, k.apply(this, arguments)
    }

    function D() {
        this.evEl = At, this.evWin = $t, k.apply(this, arguments), this.store = this.manager.session.pointerEvents = []
    }

    function O() {
        this.evTarget = Dt, this.evWin = Ot, this.started = !1, k.apply(this, arguments)
    }

    function M() {
        this.evTarget = It, this.targetIds = {}, k.apply(this, arguments)
    }

    function I() {
        k.apply(this, arguments);
        var t = u(this.handler, this);
        this.touch = new M(this.manager, t), this.mouse = new P(this.manager, t)
    }

    function N(t, e) {
        this.manager = t, this.set(e)
    }

    function j(t) {
        this.id = it++, this.manager = null, this.options = l(t || {}, this.defaults), this.options.enable = d(this.options.enable, !0), this.state = Ft, this.simultaneous = {}, this.requireFail = []
    }

    function H(t) {
        return t == mt ? "down" : t == vt ? "up" : t == ft ? "left" : t == gt ? "right" : ""
    }

    function L(t, e) {
        var i = e.manager;
        return i ? i.get(t) : t
    }

    function q() {
        j.apply(this, arguments)
    }

    function z() {
        q.apply(this, arguments), this.pX = null, this.pY = null
    }

    function R() {
        q.apply(this, arguments)
    }

    function W() {
        j.apply(this, arguments), this._timer = null, this._input = null
    }

    function F() {
        q.apply(this, arguments)
    }

    function V() {
        q.apply(this, arguments)
    }

    function Q() {
        j.apply(this, arguments), this.pTime = !1, this.pCenter = !1, this._timer = null, this._input = null, this.count = 0
    }

    function B(t, e) {
        return e = e || {}, e.recognizers = d(e.recognizers, B.defaults.preset), new X(t, e)
    }

    function X(t, e) {
        e = e || {}, this.options = l(e, B.defaults), this.options.inputTarget = this.options.inputTarget || t, this.handlers = {}, this.session = {}, this.recognizers = [], this.element = t, this.input = function (t) {
            var e = t.options.inputClass;
            return new (e || (ot ? D : rt ? M : nt ? I : P))(t, function (t, e, i) {
                var n = i.pointers.length, o = i.changedPointers.length, r = e & ct && 0 == n - o,
                    s = e & (ht | dt) && 0 == n - o;
                i.isFirst = !!r, i.isFinal = !!s, r && (t.session = {}), i.eventType = e, function (t, e) {
                    var i = t.session, n = e.pointers, o = n.length;
                    i.firstInput || (i.firstInput = _(e)), o > 1 && !i.firstMultiple ? i.firstMultiple = _(e) : 1 === o && (i.firstMultiple = !1);
                    var r = i.firstInput, s = i.firstMultiple, a = s ? s.center : r.center, l = e.center = S(n);
                    e.timeStamp = et(), e.deltaTime = e.timeStamp - r.timeStamp, e.angle = $(a, l), e.distance = A(a, l), function (t, e) {
                        var i = e.center, n = t.offsetDelta || {}, o = t.prevDelta || {}, r = t.prevInput || {};
                        (e.eventType === ct || r.eventType === ht) && (o = t.prevDelta = {
                            x: r.deltaX || 0,
                            y: r.deltaY || 0
                        }, n = t.offsetDelta = {
                            x: i.x,
                            y: i.y
                        }), e.deltaX = o.x + (i.x - n.x), e.deltaY = o.y + (i.y - n.y)
                    }(i, e), e.offsetDirection = E(e.deltaX, e.deltaY), e.scale = s ? function (t, e) {
                        return A(e[0], e[1], Ct) / A(t[0], t[1], Ct)
                    }(s.pointers, n) : 1, e.rotation = s ? function (t, e) {
                        return $(e[1], e[0], Ct) - $(t[1], t[0], Ct)
                    }(s.pointers, n) : 0, T(i, e);
                    var c = t.element;
                    g(e.srcEvent.target, c) && (c = e.srcEvent.target), e.target = c
                }(t, i), t.emit("hammer.input", i), t.recognize(i), t.session.prevInput = i
            })
        }(this), this.touchAction = new N(this, this.options.touchAction), U(this, !0), s(e.recognizers, function (t) {
            var e = this.add(new t[0](t[1]));
            t[2] && e.recognizeWith(t[2]), t[3] && e.requireFailure(t[3])
        }, this)
    }

    function U(t, e) {
        var i = t.element;
        s(t.options.cssProps, function (t, n) {
            i.style[x(i.style, n)] = e ? t : ""
        })
    }

    function Y(t, i) {
        var n = e.createEvent("Event");
        n.initEvent(t, !0, !0), n.gesture = i, i.target.dispatchEvent(n)
    }

    var G = ["", "webkit", "moz", "MS", "ms", "o"], Z = e.createElement("div"), J = "function", K = Math.round,
        tt = Math.abs, et = Date.now, it = 1, nt = "ontouchstart" in t, ot = x(t, "PointerEvent") !== n,
        rt = nt && /mobile|tablet|ip(ad|hone|od)|android/i.test(navigator.userAgent), st = "touch", at = "mouse",
        lt = 25, ct = 1, ut = 2, ht = 4, dt = 8, pt = 1, ft = 2, gt = 4, vt = 8, mt = 16, yt = ft | gt, bt = vt | mt,
        wt = yt | bt, xt = ["x", "y"], Ct = ["clientX", "clientY"];
    k.prototype = {
        handler: function () {
        }, init: function () {
            this.evEl && p(this.element, this.evEl, this.domHandler), this.evTarget && p(this.target, this.evTarget, this.domHandler), this.evWin && p(C(this.element), this.evWin, this.domHandler)
        }, destroy: function () {
            this.evEl && f(this.element, this.evEl, this.domHandler), this.evTarget && f(this.target, this.evTarget, this.domHandler), this.evWin && f(C(this.element), this.evWin, this.domHandler)
        }
    };
    var kt = {mousedown: ct, mousemove: ut, mouseup: ht}, Tt = "mousedown", _t = "mousemove mouseup";
    c(P, k, {
        handler: function (t) {
            var e = kt[t.type];
            e & ct && 0 === t.button && (this.pressed = !0), e & ut && 1 !== t.which && (e = ht), this.pressed && this.allow && (e & ht && (this.pressed = !1), this.callback(this.manager, e, {
                pointers: [t],
                changedPointers: [t],
                pointerType: at,
                srcEvent: t
            }))
        }
    });
    var St = {pointerdown: ct, pointermove: ut, pointerup: ht, pointercancel: dt, pointerout: dt},
        Et = {2: st, 3: "pen", 4: at, 5: "kinect"}, At = "pointerdown", $t = "pointermove pointerup pointercancel";
    t.MSPointerEvent && (At = "MSPointerDown", $t = "MSPointerMove MSPointerUp MSPointerCancel"), c(D, k, {
        handler: function (t) {
            var e = this.store, i = !1, n = t.type.toLowerCase().replace("ms", ""), o = St[n],
                r = Et[t.pointerType] || t.pointerType, s = r == st, a = y(e, t.pointerId, "pointerId");
            o & ct && (0 === t.button || s) ? 0 > a && (e.push(t), a = e.length - 1) : o & (ht | dt) && (i = !0), 0 > a || (e[a] = t, this.callback(this.manager, o, {
                pointers: e,
                changedPointers: [t],
                pointerType: r,
                srcEvent: t
            }), i && e.splice(a, 1))
        }
    });
    var Pt = {touchstart: ct, touchmove: ut, touchend: ht, touchcancel: dt}, Dt = "touchstart",
        Ot = "touchstart touchmove touchend touchcancel";
    c(O, k, {
        handler: function (t) {
            var e = Pt[t.type];
            if (e === ct && (this.started = !0), this.started) {
                var i = function (t, e) {
                    var i = b(t.touches), n = b(t.changedTouches);
                    return e & (ht | dt) && (i = w(i.concat(n), "identifier", !0)), [i, n]
                }.call(this, t, e);
                e & (ht | dt) && 0 == i[0].length - i[1].length && (this.started = !1), this.callback(this.manager, e, {
                    pointers: i[0],
                    changedPointers: i[1],
                    pointerType: st,
                    srcEvent: t
                })
            }
        }
    });
    var Mt = {touchstart: ct, touchmove: ut, touchend: ht, touchcancel: dt},
        It = "touchstart touchmove touchend touchcancel";
    c(M, k, {
        handler: function (t) {
            var e = Mt[t.type], i = function (t, e) {
                var i = b(t.touches), n = this.targetIds;
                if (e & (ct | ut) && 1 === i.length) return n[i[0].identifier] = !0, [i, i];
                var o, r, s = b(t.changedTouches), a = [], l = this.target;
                if (r = i.filter(function (t) {
                        return g(t.target, l)
                    }), e === ct) for (o = 0; o < r.length;) n[r[o].identifier] = !0, o++;
                for (o = 0; o < s.length;) n[s[o].identifier] && a.push(s[o]), e & (ht | dt) && delete n[s[o].identifier], o++;
                return a.length ? [w(r.concat(a), "identifier", !0), a] : void 0
            }.call(this, t, e);
            i && this.callback(this.manager, e, {pointers: i[0], changedPointers: i[1], pointerType: st, srcEvent: t})
        }
    }), c(I, k, {
        handler: function (t, e, i) {
            var n = i.pointerType == st, o = i.pointerType == at;
            if (n) this.mouse.allow = !1; else if (o && !this.mouse.allow) return;
            e & (ht | dt) && (this.mouse.allow = !0), this.callback(t, e, i)
        }, destroy: function () {
            this.touch.destroy(), this.mouse.destroy()
        }
    });
    var Nt = x(Z.style, "touchAction"), jt = Nt !== n, Ht = "compute", Lt = "auto", qt = "manipulation", zt = "none",
        Rt = "pan-x", Wt = "pan-y";
    N.prototype = {
        set: function (t) {
            t == Ht && (t = this.compute()), jt && (this.manager.element.style[Nt] = t), this.actions = t.toLowerCase().trim()
        }, update: function () {
            this.set(this.manager.options.touchAction)
        }, compute: function () {
            var t = [];
            return s(this.manager.recognizers, function (e) {
                h(e.options.enable, [e]) && (t = t.concat(e.getTouchAction()))
            }), function (t) {
                if (v(t, zt)) return zt;
                var e = v(t, Rt), i = v(t, Wt);
                return e && i ? Rt + " " + Wt : e || i ? e ? Rt : Wt : v(t, qt) ? qt : Lt
            }(t.join(" "))
        }, preventDefaults: function (t) {
            if (!jt) {
                var e = t.srcEvent, i = t.offsetDirection;
                if (this.manager.session.prevented) return void e.preventDefault();
                var n = this.actions, o = v(n, zt), r = v(n, Wt), s = v(n, Rt);
                return o || r && i & yt || s && i & bt ? this.preventSrc(e) : void 0
            }
        }, preventSrc: function (t) {
            this.manager.session.prevented = !0, t.preventDefault()
        }
    };
    var Ft = 1, Vt = 2, Qt = 4, Bt = 8, Xt = Bt, Ut = 16;
    j.prototype = {
        defaults: {}, set: function (t) {
            return a(this.options, t), this.manager && this.manager.touchAction.update(), this
        }, recognizeWith: function (t) {
            if (r(t, "recognizeWith", this)) return this;
            var e = this.simultaneous;
            return t = L(t, this), e[t.id] || (e[t.id] = t, t.recognizeWith(this)), this
        }, dropRecognizeWith: function (t) {
            return r(t, "dropRecognizeWith", this) ? this : (t = L(t, this), delete this.simultaneous[t.id], this)
        }, requireFailure: function (t) {
            if (r(t, "requireFailure", this)) return this;
            var e = this.requireFail;
            return t = L(t, this), -1 === y(e, t) && (e.push(t), t.requireFailure(this)), this
        }, dropRequireFailure: function (t) {
            if (r(t, "dropRequireFailure", this)) return this;
            t = L(t, this);
            var e = y(this.requireFail, t);
            return e > -1 && this.requireFail.splice(e, 1), this
        }, hasRequireFailures: function () {
            return this.requireFail.length > 0
        }, canRecognizeWith: function (t) {
            return !!this.simultaneous[t.id]
        }, emit: function (t) {
            function e(e) {
                i.manager.emit(i.options.event + (e ? function (t) {
                    return t & Ut ? "cancel" : t & Bt ? "end" : t & Qt ? "move" : t & Vt ? "start" : ""
                }(n) : ""), t)
            }

            var i = this, n = this.state;
            Bt > n && e(!0), e(), n >= Bt && e(!0)
        }, tryEmit: function (t) {
            return this.canEmit() ? this.emit(t) : void(this.state = 32)
        }, canEmit: function () {
            for (var t = 0; t < this.requireFail.length;) {
                if (!(this.requireFail[t].state & (32 | Ft))) return !1;
                t++
            }
            return !0
        }, recognize: function (t) {
            var e = a({}, t);
            return h(this.options.enable, [this, e]) ? (this.state & (Xt | Ut | 32) && (this.state = Ft), this.state = this.process(e), void(this.state & (Vt | Qt | Bt | Ut) && this.tryEmit(e))) : (this.reset(), void(this.state = 32))
        }, process: function () {
        }, getTouchAction: function () {
        }, reset: function () {
        }
    }, c(q, j, {
        defaults: {pointers: 1}, attrTest: function (t) {
            var e = this.options.pointers;
            return 0 === e || t.pointers.length === e
        }, process: function (t) {
            var e = this.state, i = t.eventType, n = e & (Vt | Qt), o = this.attrTest(t);
            return n && (i & dt || !o) ? e | Ut : n || o ? i & ht ? e | Bt : e & Vt ? e | Qt : Vt : 32
        }
    }), c(z, q, {
        defaults: {event: "pan", threshold: 10, pointers: 1, direction: wt}, getTouchAction: function () {
            var t = this.options.direction, e = [];
            return t & yt && e.push(Wt), t & bt && e.push(Rt), e
        }, directionTest: function (t) {
            var e = this.options, i = !0, n = t.distance, o = t.direction, r = t.deltaX, s = t.deltaY;
            return o & e.direction || (e.direction & yt ? (o = 0 === r ? pt : 0 > r ? ft : gt, i = r != this.pX, n = Math.abs(t.deltaX)) : (o = 0 === s ? pt : 0 > s ? vt : mt, i = s != this.pY, n = Math.abs(t.deltaY))), t.direction = o, i && n > e.threshold && o & e.direction
        }, attrTest: function (t) {
            return q.prototype.attrTest.call(this, t) && (this.state & Vt || !(this.state & Vt) && this.directionTest(t))
        }, emit: function (t) {
            this.pX = t.deltaX, this.pY = t.deltaY;
            var e = H(t.direction);
            e && this.manager.emit(this.options.event + e, t), this._super.emit.call(this, t)
        }
    }), c(R, q, {
        defaults: {event: "pinch", threshold: 0, pointers: 2}, getTouchAction: function () {
            return [zt]
        }, attrTest: function (t) {
            return this._super.attrTest.call(this, t) && (Math.abs(t.scale - 1) > this.options.threshold || this.state & Vt)
        }, emit: function (t) {
            if (this._super.emit.call(this, t), 1 !== t.scale) {
                var e = t.scale < 1 ? "in" : "out";
                this.manager.emit(this.options.event + e, t)
            }
        }
    }), c(W, j, {
        defaults: {event: "press", pointers: 1, time: 500, threshold: 5}, getTouchAction: function () {
            return [Lt]
        }, process: function (t) {
            var e = this.options, i = t.pointers.length === e.pointers, n = t.distance < e.threshold,
                r = t.deltaTime > e.time;
            if (this._input = t, !n || !i || t.eventType & (ht | dt) && !r) this.reset(); else if (t.eventType & ct) this.reset(), this._timer = o(function () {
                this.state = Xt, this.tryEmit()
            }, e.time, this); else if (t.eventType & ht) return Xt;
            return 32
        }, reset: function () {
            clearTimeout(this._timer)
        }, emit: function (t) {
            this.state === Xt && (t && t.eventType & ht ? this.manager.emit(this.options.event + "up", t) : (this._input.timeStamp = et(), this.manager.emit(this.options.event, this._input)))
        }
    }), c(F, q, {
        defaults: {event: "rotate", threshold: 0, pointers: 2}, getTouchAction: function () {
            return [zt]
        }, attrTest: function (t) {
            return this._super.attrTest.call(this, t) && (Math.abs(t.rotation) > this.options.threshold || this.state & Vt)
        }
    }), c(V, q, {
        defaults: {event: "swipe", threshold: 10, velocity: .65, direction: yt | bt, pointers: 1},
        getTouchAction: function () {
            return z.prototype.getTouchAction.call(this)
        },
        attrTest: function (t) {
            var e, i = this.options.direction;
            return i & (yt | bt) ? e = t.velocity : i & yt ? e = t.velocityX : i & bt && (e = t.velocityY), this._super.attrTest.call(this, t) && i & t.direction && t.distance > this.options.threshold && tt(e) > this.options.velocity && t.eventType & ht
        },
        emit: function (t) {
            var e = H(t.direction);
            e && this.manager.emit(this.options.event + e, t), this.manager.emit(this.options.event, t)
        }
    }), c(Q, j, {
        defaults: {
            event: "tap",
            pointers: 1,
            taps: 1,
            interval: 300,
            time: 250,
            threshold: 2,
            posThreshold: 10
        }, getTouchAction: function () {
            return [qt]
        }, process: function (t) {
            var e = this.options, i = t.pointers.length === e.pointers, n = t.distance < e.threshold,
                r = t.deltaTime < e.time;
            if (this.reset(), t.eventType & ct && 0 === this.count) return this.failTimeout();
            if (n && r && i) {
                if (t.eventType != ht) return this.failTimeout();
                var s = !this.pTime || t.timeStamp - this.pTime < e.interval,
                    a = !this.pCenter || A(this.pCenter, t.center) < e.posThreshold;
                this.pTime = t.timeStamp, this.pCenter = t.center, a && s ? this.count += 1 : this.count = 1, this._input = t;
                if (0 === this.count % e.taps) return this.hasRequireFailures() ? (this._timer = o(function () {
                    this.state = Xt, this.tryEmit()
                }, e.interval, this), Vt) : Xt
            }
            return 32
        }, failTimeout: function () {
            return this._timer = o(function () {
                this.state = 32
            }, this.options.interval, this), 32
        }, reset: function () {
            clearTimeout(this._timer)
        }, emit: function () {
            this.state == Xt && (this._input.tapCount = this.count, this.manager.emit(this.options.event, this._input))
        }
    }), B.VERSION = "2.0.4", B.defaults = {
        domEvents: !1,
        touchAction: Ht,
        enable: !0,
        inputTarget: null,
        inputClass: null,
        preset: [[F, {enable: !1}], [R, {enable: !1}, ["rotate"]], [V, {direction: yt}], [z, {direction: yt}, ["swipe"]], [Q], [Q, {
            event: "doubletap",
            taps: 2
        }, ["tap"]], [W]],
        cssProps: {
            userSelect: "default",
            touchSelect: "none",
            touchCallout: "none",
            contentZooming: "none",
            userDrag: "none",
            tapHighlightColor: "rgba(0,0,0,0)"
        }
    };
    X.prototype = {
        set: function (t) {
            return a(this.options, t), t.touchAction && this.touchAction.update(), t.inputTarget && (this.input.destroy(), this.input.target = t.inputTarget, this.input.init()), this
        }, stop: function (t) {
            this.session.stopped = t ? 2 : 1
        }, recognize: function (t) {
            var e = this.session;
            if (!e.stopped) {
                this.touchAction.preventDefaults(t);
                var i, n = this.recognizers, o = e.curRecognizer;
                (!o || o && o.state & Xt) && (o = e.curRecognizer = null);
                for (var r = 0; r < n.length;) i = n[r], 2 === e.stopped || o && i != o && !i.canRecognizeWith(o) ? i.reset() : i.recognize(t), !o && i.state & (Vt | Qt | Bt) && (o = e.curRecognizer = i), r++
            }
        }, get: function (t) {
            if (t instanceof j) return t;
            for (var e = this.recognizers, i = 0; i < e.length; i++) if (e[i].options.event == t) return e[i];
            return null
        }, add: function (t) {
            if (r(t, "add", this)) return this;
            var e = this.get(t.options.event);
            return e && this.remove(e), this.recognizers.push(t), t.manager = this, this.touchAction.update(), t
        }, remove: function (t) {
            if (r(t, "remove", this)) return this;
            var e = this.recognizers;
            return t = this.get(t), e.splice(y(e, t), 1), this.touchAction.update(), this
        }, on: function (t, e) {
            var i = this.handlers;
            return s(m(t), function (t) {
                i[t] = i[t] || [], i[t].push(e)
            }), this
        }, off: function (t, e) {
            var i = this.handlers;
            return s(m(t), function (t) {
                e ? i[t].splice(y(i[t], e), 1) : delete i[t]
            }), this
        }, emit: function (t, e) {
            this.options.domEvents && Y(t, e);
            var i = this.handlers[t] && this.handlers[t].slice();
            if (i && i.length) {
                e.type = t, e.preventDefault = function () {
                    e.srcEvent.preventDefault()
                };
                for (var n = 0; n < i.length;) i[n](e), n++
            }
        }, destroy: function () {
            this.element && U(this, !1), this.handlers = {}, this.session = {}, this.input.destroy(), this.element = null
        }
    }, a(B, {
        INPUT_START: ct,
        INPUT_MOVE: ut,
        INPUT_END: ht,
        INPUT_CANCEL: dt,
        STATE_POSSIBLE: Ft,
        STATE_BEGAN: Vt,
        STATE_CHANGED: Qt,
        STATE_ENDED: Bt,
        STATE_RECOGNIZED: Xt,
        STATE_CANCELLED: Ut,
        STATE_FAILED: 32,
        DIRECTION_NONE: pt,
        DIRECTION_LEFT: ft,
        DIRECTION_RIGHT: gt,
        DIRECTION_UP: vt,
        DIRECTION_DOWN: mt,
        DIRECTION_HORIZONTAL: yt,
        DIRECTION_VERTICAL: bt,
        DIRECTION_ALL: wt,
        Manager: X,
        Input: k,
        TouchAction: N,
        TouchInput: M,
        MouseInput: P,
        PointerEventInput: D,
        TouchMouseInput: I,
        SingleTouchInput: O,
        Recognizer: j,
        AttrRecognizer: q,
        Tap: Q,
        Pan: z,
        Swipe: V,
        Pinch: R,
        Rotate: F,
        Press: W,
        on: p,
        off: f,
        each: s,
        merge: l,
        extend: a,
        inherit: c,
        bindFn: u,
        prefixed: x
    }), typeof define == J && define.amd ? define(function () {
        return B
    }) : "undefined" != typeof module && module.exports ? module.exports = B : t.Hammer = B
}(window, document), function (t) {
    "function" == typeof define && define.amd ? define(["jquery", "hammerjs"], t) : "object" == typeof exports ? t(require("jquery"), require("hammerjs")) : t(jQuery, Hammer)
}(function (t, e) {
    t.fn.hammer = function (i) {
        return this.each(function () {
            !function (i, n) {
                var o = t(i);
                o.data("hammer") || o.data("hammer", new e(o[0], n))
            }(this, i)
        })
    }, e.Manager.prototype.emit = function (e) {
        return function (i, n) {
            e.call(this, i, n), t(this.element).trigger({type: i, gesture: n})
        }
    }(e.Manager.prototype.emit)
}), function (t) {
    t.Package ? Materialize = {} : t.Materialize = {}
}(window), "undefined" == typeof exports || exports.nodeType || ("undefined" != typeof module && !module.nodeType && module.exports && (exports = module.exports = Materialize), exports.default = Materialize), function (t) {
    for (var e = 0, i = ["webkit", "moz"], n = t.requestAnimationFrame, o = t.cancelAnimationFrame, r = i.length; --r >= 0 && !n;) n = t[i[r] + "RequestAnimationFrame"], o = t[i[r] + "CancelRequestAnimationFrame"];
    n && o || (n = function (t) {
        var i = +Date.now(), n = Math.max(e + 16, i);
        return setTimeout(function () {
            t(e = n)
        }, n - i)
    }, o = clearTimeout), t.requestAnimationFrame = n, t.cancelAnimationFrame = o
}(window), Materialize.objectSelectorString = function (t) {
    return ((t.prop("tagName") || "") + (t.attr("id") || "") + (t.attr("class") || "")).replace(/\s/g, "")
}, Materialize.guid = function () {
    function t() {
        return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
    }

    return function () {
        return t() + t() + "-" + t() + "-" + t() + "-" + t() + "-" + t() + t() + t()
    }
}(), Materialize.escapeHash = function (t) {
    return t.replace(/(:|\.|\[|\]|,|=)/g, "\\$1")
}, Materialize.elementOrParentIsFixed = function (t) {
    var e = $(t), i = !1;
    return e.add(e.parents()).each(function () {
        if ("fixed" === $(this).css("position")) return i = !0, !1
    }), i
};
var getTime = Date.now || function () {
    return (new Date).getTime()
};
Materialize.throttle = function (t, e, i) {
    var n, o, r, s = null, a = 0;
    i || (i = {});
    return function () {
        var l = getTime();
        a || !1 !== i.leading || (a = l);
        var c = e - (l - a);
        return n = this, o = arguments, c <= 0 ? (clearTimeout(s), s = null, a = l, r = t.apply(n, o), n = o = null) : s || !1 === i.trailing || (s = setTimeout(function () {
            a = !1 === i.leading ? 0 : getTime(), s = null, r = t.apply(n, o), n = o = null
        }, c)), r
    }
};
var Vel;
if (Vel = jQuery ? jQuery.Velocity : $ ? $.Velocity : Velocity, Materialize.Vel = Vel || Velocity, function (t) {
        t.fn.collapsible = function (e, i) {
            var n = {accordion: void 0, onOpen: void 0, onClose: void 0}, o = e;
            return e = t.extend(n, e), this.each(function () {
                function n(i, n) {
                    n || i.toggleClass("active"), e.accordion || "accordion" === u || void 0 === u ? function (e) {
                        c = l.find("> li > .collapsible-header"), e.hasClass("active") ? e.parent().addClass("active") : e.parent().removeClass("active"), e.parent().hasClass("active") ? e.siblings(".collapsible-body").stop(!0, !1).slideDown({
                            duration: 350,
                            easing: "easeOutQuart",
                            queue: !1,
                            complete: function () {
                                t(this).css("height", "")
                            }
                        }) : e.siblings(".collapsible-body").stop(!0, !1).slideUp({
                            duration: 350,
                            easing: "easeOutQuart",
                            queue: !1,
                            complete: function () {
                                t(this).css("height", "")
                            }
                        }), c.not(e).removeClass("active").parent().removeClass("active"), c.not(e).parent().children(".collapsible-body").stop(!0, !1).each(function () {
                            t(this).is(":visible") && t(this).slideUp({
                                duration: 350,
                                easing: "easeOutQuart",
                                queue: !1,
                                complete: function () {
                                    t(this).css("height", ""), r(t(this).siblings(".collapsible-header"))
                                }
                            })
                        })
                    }(i) : function (e) {
                        e.hasClass("active") ? e.parent().addClass("active") : e.parent().removeClass("active"), e.parent().hasClass("active") ? e.siblings(".collapsible-body").stop(!0, !1).slideDown({
                            duration: 350,
                            easing: "easeOutQuart",
                            queue: !1,
                            complete: function () {
                                t(this).css("height", "")
                            }
                        }) : e.siblings(".collapsible-body").stop(!0, !1).slideUp({
                            duration: 350,
                            easing: "easeOutQuart",
                            queue: !1,
                            complete: function () {
                                t(this).css("height", "")
                            }
                        })
                    }(i), r(i)
                }

                function r(t) {
                    t.hasClass("active") ? "function" == typeof e.onOpen && e.onOpen.call(this, t.parent()) : "function" == typeof e.onClose && e.onClose.call(this, t.parent())
                }

                function s(t) {
                    return t.closest("li > .collapsible-header")
                }

                function a() {
                    l.off("click.collapse", "> li > .collapsible-header")
                }

                var l = t(this), c = t(this).find("> li > .collapsible-header"), u = l.data("collapsible");
                if ("destroy" !== o) if (i >= 0 && i < c.length) {
                    var h = c.eq(i);
                    h.length && ("open" === o || "close" === o && h.hasClass("active")) && n(h)
                } else a(), l.on("click.collapse", "> li > .collapsible-header", function (e) {
                    var i = t(e.target);
                    (function (t) {
                        return s(t).length > 0
                    })(i) && (i = s(i)), n(i)
                }), e.accordion || "accordion" === u || void 0 === u ? n(c.filter(".active").first(), !0) : c.filter(".active").each(function () {
                    n(t(this), !0)
                }); else a()
            })
        }, t(document).ready(function () {
            t(".collapsible").collapsible()
        })
    }(jQuery), function (t) {
        t.fn.scrollTo = function (e) {
            return t(this).scrollTop(t(this).scrollTop() - t(this).offset().top + t(e).offset().top), this
        }, t.fn.dropdown = function (e) {
            var i = {
                inDuration: 300,
                outDuration: 225,
                constrainWidth: !0,
                hover: !1,
                gutter: 0,
                belowOrigin: !1,
                alignment: "left",
                stopPropagation: !1
            };
            return "open" === e ? (this.each(function () {
                t(this).trigger("open")
            }), !1) : "close" === e ? (this.each(function () {
                t(this).trigger("close")
            }), !1) : void this.each(function () {
                function n() {
                    void 0 !== s.data("induration") && (a.inDuration = s.data("induration")), void 0 !== s.data("outduration") && (a.outDuration = s.data("outduration")), void 0 !== s.data("constrainwidth") && (a.constrainWidth = s.data("constrainwidth")), void 0 !== s.data("hover") && (a.hover = s.data("hover")), void 0 !== s.data("gutter") && (a.gutter = s.data("gutter")), void 0 !== s.data("beloworigin") && (a.belowOrigin = s.data("beloworigin")), void 0 !== s.data("alignment") && (a.alignment = s.data("alignment")), void 0 !== s.data("stoppropagation") && (a.stopPropagation = s.data("stoppropagation"))
                }

                function o(e) {
                    "focus" === e && (l = !0), n(), c.addClass("active"), s.addClass("active");
                    var i = s[0].getBoundingClientRect().width;
                    !0 === a.constrainWidth ? c.css("width", i) : c.css("white-space", "nowrap");
                    var o = window.innerHeight, u = s.innerHeight(), h = s.offset().left,
                        d = s.offset().top - t(window).scrollTop(), p = a.alignment, f = 0, g = 0, v = 0;
                    !0 === a.belowOrigin && (v = u);
                    var m = 0, y = 0, b = s.parent();
                    if (b.is("body") || (b[0].scrollHeight > b[0].clientHeight && (m = b[0].scrollTop), b[0].scrollWidth > b[0].clientWidth && (y = b[0].scrollLeft)), h + c.innerWidth() > t(window).width() ? p = "right" : h - c.innerWidth() + s.innerWidth() < 0 && (p = "left"), d + c.innerHeight() > o) if (d + u - c.innerHeight() < 0) {
                        var w = o - d - v;
                        c.css("max-height", w)
                    } else v || (v += u), v -= c.innerHeight();
                    if ("left" === p) f = a.gutter, g = s.position().left + f; else if ("right" === p) {
                        c.stop(!0, !0).css({opacity: 0, left: 0});
                        g = s.position().left + i - c.width() + (f = -a.gutter)
                    }
                    c.css({position: "absolute", top: s.position().top + v + m, left: g + y}), c.slideDown({
                        queue: !1,
                        duration: a.inDuration,
                        easing: "easeOutCubic",
                        complete: function () {
                            t(this).css("height", "")
                        }
                    }).animate({opacity: 1}, {
                        queue: !1,
                        duration: a.inDuration,
                        easing: "easeOutSine"
                    }), setTimeout(function () {
                        t(document).on("click." + c.attr("id"), function (e) {
                            r(), t(document).off("click." + c.attr("id"))
                        })
                    }, 0)
                }

                function r() {
                    l = !1, c.fadeOut(a.outDuration), c.removeClass("active"), s.removeClass("active"), t(document).off("click." + c.attr("id")), setTimeout(function () {
                        c.css("max-height", "")
                    }, a.outDuration)
                }

                var s = t(this), a = t.extend({}, i, e), l = !1, c = t("#" + s.attr("data-activates"));
                if (n(), s.after(c), a.hover) {
                    var u = !1;
                    s.off("click." + s.attr("id")), s.on("mouseenter", function (t) {
                        !1 === u && (o(), u = !0)
                    }), s.on("mouseleave", function (e) {
                        var i = e.toElement || e.relatedTarget;
                        t(i).closest(".dropdown-content").is(c) || (c.stop(!0, !0), r(), u = !1)
                    }), c.on("mouseleave", function (e) {
                        var i = e.toElement || e.relatedTarget;
                        t(i).closest(".dropdown-button").is(s) || (c.stop(!0, !0), r(), u = !1)
                    })
                } else s.off("click." + s.attr("id")), s.on("click." + s.attr("id"), function (e) {
                    l || (s[0] != e.currentTarget || s.hasClass("active") || 0 !== t(e.target).closest(".dropdown-content").length ? s.hasClass("active") && (r(), t(document).off("click." + c.attr("id"))) : (e.preventDefault(), a.stopPropagation && e.stopPropagation(), o("click")))
                });
                s.on("open", function (t, e) {
                    o(e)
                }), s.on("close", r)
            })
        }, t(document).ready(function () {
            t(".dropdown-button").dropdown()
        })
    }(jQuery), function (t, e) {
        "use strict";
        var i = {
            opacity: .5,
            inDuration: 250,
            outDuration: 250,
            ready: void 0,
            complete: void 0,
            dismissible: !0,
            startingTop: "4%",
            endingTop: "10%"
        }, n = function () {
            function n(e, i) {
                _classCallCheck(this, n), e[0].M_Modal && e[0].M_Modal.destroy(), this.$el = e, this.options = t.extend({}, n.defaults, i), this.isOpen = !1, this.$el[0].M_Modal = this, this.id = e.attr("id"), this.openingTrigger = void 0, this.$overlay = t('<div class="modal-overlay"></div>'), n._increment++, n._count++, this.$overlay[0].style.zIndex = 1e3 + 2 * n._increment, this.$el[0].style.zIndex = 1e3 + 2 * n._increment + 1, this.setupEventHandlers()
            }

            return _createClass(n, [{
                key: "getInstance", value: function () {
                    return this
                }
            }, {
                key: "destroy", value: function () {
                    this.removeEventHandlers(), this.$el[0].removeAttribute("style"), this.$overlay[0].parentNode && this.$overlay[0].parentNode.removeChild(this.$overlay[0]), this.$el[0].M_Modal = void 0, n._count--
                }
            }, {
                key: "setupEventHandlers", value: function () {
                    this.handleOverlayClickBound = this.handleOverlayClick.bind(this), this.handleModalCloseClickBound = this.handleModalCloseClick.bind(this), 1 === n._count && document.body.addEventListener("click", this.handleTriggerClick), this.$overlay[0].addEventListener("click", this.handleOverlayClickBound), this.$el[0].addEventListener("click", this.handleModalCloseClickBound)
                }
            }, {
                key: "removeEventHandlers", value: function () {
                    0 === n._count && document.body.removeEventListener("click", this.handleTriggerClick), this.$overlay[0].removeEventListener("click", this.handleOverlayClickBound), this.$el[0].removeEventListener("click", this.handleModalCloseClickBound)
                }
            }, {
                key: "handleTriggerClick", value: function (e) {
                    var i = t(e.target).closest(".modal-trigger");
                    if (e.target && i.length) {
                        var n = i[0].getAttribute("href");
                        n = n ? n.slice(1) : i[0].getAttribute("data-target");
                        var o = document.getElementById(n).M_Modal;
                        o && o.open(i), e.preventDefault()
                    }
                }
            }, {
                key: "handleOverlayClick", value: function () {
                    this.options.dismissible && this.close()
                }
            }, {
                key: "handleModalCloseClick", value: function (e) {
                    var i = t(e.target).closest(".modal-close");
                    e.target && i.length && this.close()
                }
            }, {
                key: "handleKeydown", value: function (t) {
                    27 === t.keyCode && this.options.dismissible && this.close()
                }
            }, {
                key: "animateIn", value: function () {
                    var i = this;
                    t.extend(this.$el[0].style, {
                        display: "block",
                        opacity: 0
                    }), t.extend(this.$overlay[0].style, {
                        display: "block",
                        opacity: 0
                    }), e(this.$overlay[0], {opacity: this.options.opacity}, {
                        duration: this.options.inDuration,
                        queue: !1,
                        ease: "easeOutCubic"
                    });
                    var n = {
                        duration: this.options.inDuration, queue: !1, ease: "easeOutCubic", complete: function () {
                            "function" == typeof i.options.ready && i.options.ready.call(i, i.$el, i.openingTrigger)
                        }
                    };
                    this.$el[0].classList.contains("bottom-sheet") ? e(this.$el[0], {
                        bottom: 0,
                        opacity: 1
                    }, n) : (e.hook(this.$el[0], "scaleX", .7), this.$el[0].style.top = this.options.startingTop, e(this.$el[0], {
                        top: this.options.endingTop,
                        opacity: 1,
                        scaleX: 1
                    }, n))
                }
            }, {
                key: "animateOut", value: function () {
                    var t = this;
                    e(this.$overlay[0], {opacity: 0}, {
                        duration: this.options.outDuration,
                        queue: !1,
                        ease: "easeOutQuart"
                    });
                    var i = {
                        duration: this.options.outDuration,
                        queue: !1,
                        ease: "easeOutCubic",
                        complete: function () {
                            t.$el[0].style.display = "none", "function" == typeof t.options.complete && t.options.complete.call(t, t.$el), t.$overlay[0].parentNode.removeChild(t.$overlay[0])
                        }
                    };
                    this.$el[0].classList.contains("bottom-sheet") ? e(this.$el[0], {
                        bottom: "-100%",
                        opacity: 0
                    }, i) : e(this.$el[0], {top: this.options.startingTop, opacity: 0, scaleX: .7}, i)
                }
            }, {
                key: "open", value: function (t) {
                    if (!this.isOpen) {
                        this.isOpen = !0;
                        var e = document.body;
                        return e.style.overflow = "hidden", this.$el[0].classList.add("open"), e.appendChild(this.$overlay[0]), this.openingTrigger = t || void 0, this.options.dismissible && (this.handleKeydownBound = this.handleKeydown.bind(this), document.addEventListener("keydown", this.handleKeydownBound)), this.animateIn(), this
                    }
                }
            }, {
                key: "close", value: function () {
                    if (this.isOpen) return this.isOpen = !1, this.$el[0].classList.remove("open"), document.body.style.overflow = "", this.options.dismissible && document.removeEventListener("keydown", this.handleKeydownBound), this.animateOut(), this
                }
            }], [{
                key: "init", value: function (e, i) {
                    var o = [];
                    return e.each(function () {
                        o.push(new n(t(this), i))
                    }), o
                }
            }, {
                key: "defaults", get: function () {
                    return i
                }
            }]), n
        }();
        n._increment = 0, n._count = 0, Materialize.Modal = n, t.fn.modal = function (e) {
            return n.prototype[e] ? "get" === e.slice(0, 3) ? this.first()[0].M_Modal[e]() : this.each(function () {
                this.M_Modal[e]()
            }) : "object" != typeof e && e ? void t.error("Method " + e + " does not exist on jQuery.modal") : (n.init(this, arguments[0]), this)
        }
    }(jQuery, Materialize.Vel), function (t) {
        t.fn.materialbox = function () {
            return this.each(function () {
                function e() {
                    r = !1;
                    var e = a.parent(".material-placeholder"),
                        n = (window.innerWidth, window.innerHeight, a.data("width")), l = a.data("height");
                    a.velocity("stop", !0), t("#materialbox-overlay").velocity("stop", !0), t(".materialbox-caption").velocity("stop", !0), t(window).off("scroll.materialbox"), t(document).off("keyup.materialbox"), t(window).off("resize.materialbox"), t("#materialbox-overlay").velocity({opacity: 0}, {
                        duration: s,
                        queue: !1,
                        easing: "easeOutQuad",
                        complete: function () {
                            o = !1, t(this).remove()
                        }
                    }), a.velocity({width: n, height: l, left: 0, top: 0}, {
                        duration: s,
                        queue: !1,
                        easing: "easeOutQuad",
                        complete: function () {
                            e.css({
                                height: "",
                                width: "",
                                position: "",
                                top: "",
                                left: ""
                            }), a.removeAttr("style"), a.attr("style", c), a.removeClass("active"), r = !0, i && i.css("overflow", "")
                        }
                    }), t(".materialbox-caption").velocity({opacity: 0}, {
                        duration: s,
                        queue: !1,
                        easing: "easeOutQuad",
                        complete: function () {
                            t(this).remove()
                        }
                    })
                }

                if (!t(this).hasClass("initialized")) {
                    t(this).addClass("initialized");
                    var i, n, o = !1, r = !0, s = 200, a = t(this),
                        l = t("<div></div>").addClass("material-placeholder"), c = a.attr("style");
                    a.wrap(l), a.on("click", function () {
                        var s = a.parent(".material-placeholder"), l = window.innerWidth, c = window.innerHeight,
                            u = a.width(), h = a.height();
                        if (!1 === r) return e(), !1;
                        if (o && !0 === r) return e(), !1;
                        r = !1, a.addClass("active"), o = !0, s.css({
                            width: s[0].getBoundingClientRect().width,
                            height: s[0].getBoundingClientRect().height,
                            position: "relative",
                            top: 0,
                            left: 0
                        }), i = void 0, n = s[0].parentNode;
                        for (; null !== n && !t(n).is(document);) {
                            var d = t(n);
                            "visible" !== d.css("overflow") && (d.css("overflow", "visible"), i = void 0 === i ? d : i.add(d)), n = n.parentNode
                        }
                        a.css({
                            position: "absolute",
                            "z-index": 1e3,
                            "will-change": "left, top, width, height"
                        }).data("width", u).data("height", h);
                        var p = t('<div id="materialbox-overlay"></div>').css({opacity: 0}).click(function () {
                            !0 === r && e()
                        });
                        a.before(p);
                        var f = p[0].getBoundingClientRect();
                        if (p.css({
                                width: l,
                                height: c,
                                left: -1 * f.left,
                                top: -1 * f.top
                            }), p.velocity({opacity: 1}, {
                                duration: 275,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), "" !== a.data("caption")) {
                            var g = t('<div class="materialbox-caption"></div>');
                            g.text(a.data("caption")), t("body").append(g), g.css({display: "inline"}), g.velocity({opacity: 1}, {
                                duration: 275,
                                queue: !1,
                                easing: "easeOutQuad"
                            })
                        }
                        var v = 0, m = 0;
                        u / l > h / c ? (v = .9 * l, m = .9 * l * (h / u)) : (v = .9 * c * (u / h), m = .9 * c), a.hasClass("responsive-img") ? a.velocity({
                            "max-width": v,
                            width: u
                        }, {
                            duration: 0, queue: !1, complete: function () {
                                a.css({left: 0, top: 0}).velocity({
                                    height: m,
                                    width: v,
                                    left: t(document).scrollLeft() + l / 2 - a.parent(".material-placeholder").offset().left - v / 2,
                                    top: t(document).scrollTop() + c / 2 - a.parent(".material-placeholder").offset().top - m / 2
                                }, {
                                    duration: 275, queue: !1, easing: "easeOutQuad", complete: function () {
                                        r = !0
                                    }
                                })
                            }
                        }) : a.css("left", 0).css("top", 0).velocity({
                            height: m,
                            width: v,
                            left: t(document).scrollLeft() + l / 2 - a.parent(".material-placeholder").offset().left - v / 2,
                            top: t(document).scrollTop() + c / 2 - a.parent(".material-placeholder").offset().top - m / 2
                        }, {
                            duration: 275, queue: !1, easing: "easeOutQuad", complete: function () {
                                r = !0
                            }
                        }), t(window).on("scroll.materialbox", function () {
                            o && e()
                        }), t(window).on("resize.materialbox", function () {
                            o && e()
                        }), t(document).on("keyup.materialbox", function (t) {
                            27 === t.keyCode && !0 === r && o && e()
                        })
                    })
                }
            })
        }, t(document).ready(function () {
            t(".materialboxed").materialbox()
        })
    }(jQuery), function (t) {
        t.fn.parallax = function () {
            var e = t(window).width();
            return this.each(function (i) {
                function n(i) {
                    var n;
                    n = e < 601 ? o.height() > 0 ? o.height() : o.children("img").height() : o.height() > 0 ? o.height() : 500;
                    var r = o.children("img").first(), s = r.height() - n, a = o.offset().top + n, l = o.offset().top,
                        c = t(window).scrollTop(), u = window.innerHeight, h = (c + u - l) / (n + u),
                        d = Math.round(s * h);
                    i && r.css("display", "block"), a > c && l < c + u && r.css("transform", "translate3D(-50%," + d + "px, 0)")
                }

                var o = t(this);
                o.addClass("parallax"), o.children("img").one("load", function () {
                    n(!0)
                }).each(function () {
                    this.complete && t(this).trigger("load")
                }), t(window).scroll(function () {
                    e = t(window).width(), n(!1)
                }), t(window).resize(function () {
                    e = t(window).width(), n(!1)
                })
            })
        }
    }(jQuery), function (t) {
        var e = {
            init: function (e) {
                var i = {onShow: null, swipeable: !1, responsiveThreshold: 1 / 0};
                e = t.extend(i, e);
                var n = Materialize.objectSelectorString(t(this));
                return this.each(function (i) {
                    var o, r, s, a, l, c = n + i, u = t(this), h = t(window).width(), d = u.find("li.tab a"),
                        p = u.width(), f = t(), g = Math.max(p, u[0].scrollWidth) / d.length, v = 0, m = 0, y = !1,
                        b = function (t) {
                            return Math.ceil(p - t.position().left - t[0].getBoundingClientRect().width - u.scrollLeft())
                        }, w = function (t) {
                            return Math.floor(t.position().left + u.scrollLeft())
                        }, x = function (t) {
                            v - t >= 0 ? (a.velocity({right: b(o)}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), a.velocity({left: w(o)}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad",
                                delay: 90
                            })) : (a.velocity({left: w(o)}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), a.velocity({right: b(o)}, {duration: 300, queue: !1, easing: "easeOutQuad", delay: 90}))
                        };
                    e.swipeable && h > e.responsiveThreshold && (e.swipeable = !1), 0 === (o = t(d.filter('[href="' + location.hash + '"]'))).length && (o = t(this).find("li.tab a.active").first()), 0 === o.length && (o = t(this).find("li.tab a").first()), o.addClass("active"), (v = d.index(o)) < 0 && (v = 0), void 0 !== o[0] && (r = t(o[0].hash)).addClass("active"), u.find(".indicator").length || u.append('<li class="indicator"></li>'), a = u.find(".indicator"), u.append(a), u.is(":visible") && setTimeout(function () {
                        a.css({right: b(o)}), a.css({left: w(o)})
                    }, 0), t(window).off("resize.tabs-" + c).on("resize.tabs-" + c, function () {
                        p = u.width(), g = Math.max(p, u[0].scrollWidth) / d.length, v < 0 && (v = 0), 0 !== g && 0 !== p && (a.css({right: b(o)}), a.css({left: w(o)}))
                    }), e.swipeable ? (d.each(function () {
                        var e = t(Materialize.escapeHash(this.hash));
                        e.addClass("carousel-item"), f = f.add(e)
                    }), s = f.wrapAll('<div class="tabs-content carousel"></div>'), f.css("display", ""), t(".tabs-content.carousel").carousel({
                        fullWidth: !0,
                        noWrap: !0,
                        onCycleTo: function (t) {
                            if (!y) {
                                var i = v;
                                v = s.index(t), o.removeClass("active"), (o = d.eq(v)).addClass("active"), x(i), "function" == typeof e.onShow && e.onShow.call(u[0], r)
                            }
                        }
                    })) : d.not(o).each(function () {
                        t(Materialize.escapeHash(this.hash)).hide()
                    }), u.off("click.tabs").on("click.tabs", "a", function (i) {
                        if (t(this).parent().hasClass("disabled")) i.preventDefault(); else if (!t(this).attr("target")) {
                            y = !0, p = u.width(), g = Math.max(p, u[0].scrollWidth) / d.length, o.removeClass("active");
                            var n = r;
                            o = t(this), r = t(Materialize.escapeHash(this.hash)), d = u.find("li.tab a");
                            o.position();
                            o.addClass("active"), m = v, (v = d.index(t(this))) < 0 && (v = 0), e.swipeable ? f.length && f.carousel("set", v, function () {
                                "function" == typeof e.onShow && e.onShow.call(u[0], r)
                            }) : (void 0 !== r && (r.show(), r.addClass("active"), "function" == typeof e.onShow && e.onShow.call(this, r)), void 0 === n || n.is(r) || (n.hide(), n.removeClass("active"))), l = setTimeout(function () {
                                y = !1
                            }, 300), x(m), i.preventDefault()
                        }
                    })
                })
            }, select_tab: function (t) {
                this.find('a[href="#' + t + '"]').trigger("click")
            }
        };
        t.fn.tabs = function (i) {
            return e[i] ? e[i].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof i && i ? void t.error("Method " + i + " does not exist on jQuery.tabs") : e.init.apply(this, arguments)
        }, t(document).ready(function () {
            t("ul.tabs").tabs()
        })
    }(jQuery), function (t) {
        t.fn.tooltip = function (i) {
            return "remove" === i ? (this.each(function () {
                t("#" + t(this).attr("data-tooltip-id")).remove(), t(this).removeAttr("data-tooltip-id"), t(this).off("mouseenter.tooltip mouseleave.tooltip")
            }), !1) : (i = t.extend({delay: 350, tooltip: "", position: "bottom", html: !1}, i), this.each(function () {
                var n = Materialize.guid(), o = t(this);
                o.attr("data-tooltip-id") && t("#" + o.attr("data-tooltip-id")).remove(), o.attr("data-tooltip-id", n);
                var r, s, a, l, c, u, h = function () {
                    r = o.attr("data-html") ? "true" === o.attr("data-html") : i.html, s = o.attr("data-delay"), s = void 0 === s || "" === s ? i.delay : s, a = o.attr("data-position"), a = void 0 === a || "" === a ? i.position : a, l = o.attr("data-tooltip"), l = void 0 === l || "" === l ? i.tooltip : l
                };
                h();
                c = function () {
                    var e = t('<div class="material-tooltip"></div>');
                    return l = r ? t("<span></span>").html(l) : t("<span></span>").text(l), e.append(l).appendTo(t("body")).attr("id", n), (u = t('<div class="backdrop"></div>')).appendTo(e), e
                }(), o.off("mouseenter.tooltip mouseleave.tooltip");
                var d, p = !1;
                o.on({
                    "mouseenter.tooltip": function (t) {
                        d = setTimeout(function () {
                            h(), p = !0, c.velocity("stop"), u.velocity("stop"), c.css({
                                visibility: "visible",
                                left: "0px",
                                top: "0px"
                            });
                            var t, i, n, r = o.outerWidth(), s = o.outerHeight(), l = c.outerHeight(),
                                d = c.outerWidth(), f = "0px", g = "0px", v = u[0].offsetWidth, m = u[0].offsetHeight,
                                y = 8, b = 8, w = 0;
                            "top" === a ? (t = o.offset().top - l - 5, i = o.offset().left + r / 2 - d / 2, n = e(i, t, d, l), f = "-10px", u.css({
                                bottom: 0,
                                left: 0,
                                borderRadius: "14px 14px 0 0",
                                transformOrigin: "50% 100%",
                                marginTop: l,
                                marginLeft: d / 2 - v / 2
                            })) : "left" === a ? (t = o.offset().top + s / 2 - l / 2, i = o.offset().left - d - 5, n = e(i, t, d, l), g = "-10px", u.css({
                                top: "-7px",
                                right: 0,
                                width: "14px",
                                height: "14px",
                                borderRadius: "14px 0 0 14px",
                                transformOrigin: "95% 50%",
                                marginTop: l / 2,
                                marginLeft: d
                            })) : "right" === a ? (t = o.offset().top + s / 2 - l / 2, i = o.offset().left + r + 5, n = e(i, t, d, l), g = "+10px", u.css({
                                top: "-7px",
                                left: 0,
                                width: "14px",
                                height: "14px",
                                borderRadius: "0 14px 14px 0",
                                transformOrigin: "5% 50%",
                                marginTop: l / 2,
                                marginLeft: "0px"
                            })) : (t = o.offset().top + o.outerHeight() + 5, i = o.offset().left + r / 2 - d / 2, n = e(i, t, d, l), f = "+10px", u.css({
                                top: 0,
                                left: 0,
                                marginLeft: d / 2 - v / 2
                            })), c.css({
                                top: n.y,
                                left: n.x
                            }), y = Math.SQRT2 * d / parseInt(v), b = Math.SQRT2 * l / parseInt(m), w = Math.max(y, b), c.velocity({
                                translateY: f,
                                translateX: g
                            }, {duration: 350, queue: !1}).velocity({opacity: 1}, {
                                duration: 300,
                                delay: 50,
                                queue: !1
                            }), u.css({visibility: "visible"}).velocity({opacity: 1}, {
                                duration: 55,
                                delay: 0,
                                queue: !1
                            }).velocity({scaleX: w, scaleY: w}, {
                                duration: 300,
                                delay: 0,
                                queue: !1,
                                easing: "easeInOutQuad"
                            })
                        }, s)
                    }, "mouseleave.tooltip": function () {
                        p = !1, clearTimeout(d), setTimeout(function () {
                            !0 !== p && (c.velocity({opacity: 0, translateY: 0, translateX: 0}, {
                                duration: 225,
                                queue: !1
                            }), u.velocity({opacity: 0, scaleX: 1, scaleY: 1}, {
                                duration: 225,
                                queue: !1,
                                complete: function () {
                                    u.css({visibility: "hidden"}), c.css({visibility: "hidden"}), p = !1
                                }
                            }))
                        }, 225)
                    }
                })
            }))
        };
        var e = function (e, i, n, o) {
            var r = e, s = i;
            return r < 0 ? r = 4 : r + n > window.innerWidth && (r -= r + n - window.innerWidth), s < 0 ? s = 4 : s + o > window.innerHeight + t(window).scrollTop && (s -= s + o - window.innerHeight), {
                x: r,
                y: s
            }
        };
        t(document).ready(function () {
            t(".tooltipped").tooltip()
        })
    }(jQuery), function (t) {
        "use strict";

        function e(t) {
            var e = "";
            for (var i in t) t.hasOwnProperty(i) && (e += i + ":" + t[i] + ";");
            return e
        }

        function i(e) {
            var i = function (t) {
                if (!1 === s.allowEvent(t)) return null;
                for (var e = null, i = t.target || t.srcElement; null !== i.parentNode;) {
                    if (!(i instanceof SVGElement) && -1 !== i.className.indexOf("waves-effect")) {
                        e = i;
                        break
                    }
                    i = i.parentNode
                }
                return e
            }(e);
            null !== i && (r.show(e, i), "ontouchstart" in t && (i.addEventListener("touchend", r.hide, !1), i.addEventListener("touchcancel", r.hide, !1)), i.addEventListener("mouseup", r.hide, !1), i.addEventListener("mouseleave", r.hide, !1), i.addEventListener("dragend", r.hide, !1))
        }

        var n = n || {}, o = document.querySelectorAll.bind(document), r = {
            duration: 750, show: function (t, i) {
                if (2 === t.button) return !1;
                var n = i || this, o = document.createElement("div");
                o.className = "waves-ripple", n.appendChild(o);
                var s = function (t) {
                    var e, i, n = {top: 0, left: 0}, o = t && t.ownerDocument;
                    return e = o.documentElement, void 0 !== t.getBoundingClientRect && (n = t.getBoundingClientRect()), i = function (t) {
                        return function (t) {
                            return null !== t && t === t.window
                        }(t) ? t : 9 === t.nodeType && t.defaultView
                    }(o), {top: n.top + i.pageYOffset - e.clientTop, left: n.left + i.pageXOffset - e.clientLeft}
                }(n), a = t.pageY - s.top, l = t.pageX - s.left, c = "scale(" + n.clientWidth / 100 * 10 + ")";
                "touches" in t && (a = t.touches[0].pageY - s.top, l = t.touches[0].pageX - s.left), o.setAttribute("data-hold", Date.now()), o.setAttribute("data-scale", c), o.setAttribute("data-x", l), o.setAttribute("data-y", a);
                var u = {top: a + "px", left: l + "px"};
                o.className = o.className + " waves-notransition", o.setAttribute("style", e(u)), o.className = o.className.replace("waves-notransition", ""), u["-webkit-transform"] = c, u["-moz-transform"] = c, u["-ms-transform"] = c, u["-o-transform"] = c, u.transform = c, u.opacity = "1", u["-webkit-transition-duration"] = r.duration + "ms", u["-moz-transition-duration"] = r.duration + "ms", u["-o-transition-duration"] = r.duration + "ms", u["transition-duration"] = r.duration + "ms", u["-webkit-transition-timing-function"] = "cubic-bezier(0.250, 0.460, 0.450, 0.940)", u["-moz-transition-timing-function"] = "cubic-bezier(0.250, 0.460, 0.450, 0.940)", u["-o-transition-timing-function"] = "cubic-bezier(0.250, 0.460, 0.450, 0.940)", u["transition-timing-function"] = "cubic-bezier(0.250, 0.460, 0.450, 0.940)", o.setAttribute("style", e(u))
            }, hide: function (t) {
                s.touchup(t);
                var i = this, n = (i.clientWidth, null), o = i.getElementsByClassName("waves-ripple");
                if (!(o.length > 0)) return !1;
                var a = (n = o[o.length - 1]).getAttribute("data-x"), l = n.getAttribute("data-y"),
                    c = n.getAttribute("data-scale"), u = 350 - (Date.now() - Number(n.getAttribute("data-hold")));
                u < 0 && (u = 0), setTimeout(function () {
                    var t = {
                        top: l + "px",
                        left: a + "px",
                        opacity: "0",
                        "-webkit-transition-duration": r.duration + "ms",
                        "-moz-transition-duration": r.duration + "ms",
                        "-o-transition-duration": r.duration + "ms",
                        "transition-duration": r.duration + "ms",
                        "-webkit-transform": c,
                        "-moz-transform": c,
                        "-ms-transform": c,
                        "-o-transform": c,
                        transform: c
                    };
                    n.setAttribute("style", e(t)), setTimeout(function () {
                        try {
                            i.removeChild(n)
                        } catch (t) {
                            return !1
                        }
                    }, r.duration)
                }, u)
            }, wrapInput: function (t) {
                for (var e = 0; e < t.length; e++) {
                    var i = t[e];
                    if ("input" === i.tagName.toLowerCase()) {
                        var n = i.parentNode;
                        if ("i" === n.tagName.toLowerCase() && -1 !== n.className.indexOf("waves-effect")) continue;
                        var o = document.createElement("i");
                        o.className = i.className + " waves-input-wrapper";
                        var r = i.getAttribute("style");
                        r || (r = ""), o.setAttribute("style", r), i.className = "waves-button-input", i.removeAttribute("style"), n.replaceChild(o, i), o.appendChild(i)
                    }
                }
            }
        }, s = {
            touches: 0, allowEvent: function (t) {
                var e = !0;
                return "touchstart" === t.type ? s.touches += 1 : "touchend" === t.type || "touchcancel" === t.type ? setTimeout(function () {
                    s.touches > 0 && (s.touches -= 1)
                }, 500) : "mousedown" === t.type && s.touches > 0 && (e = !1), e
            }, touchup: function (t) {
                s.allowEvent(t)
            }
        };
        n.displayEffect = function (e) {
            "duration" in (e = e || {}) && (r.duration = e.duration), r.wrapInput(o(".waves-effect")), "ontouchstart" in t && document.body.addEventListener("touchstart", i, !1), document.body.addEventListener("mousedown", i, !1)
        }, n.attach = function (e) {
            "input" === e.tagName.toLowerCase() && (r.wrapInput([e]), e = e.parentNode), "ontouchstart" in t && e.addEventListener("touchstart", i, !1), e.addEventListener("mousedown", i, !1)
        }, t.Waves = n, document.addEventListener("DOMContentLoaded", function () {
            n.displayEffect()
        }, !1)
    }(window), function (t, e) {
        "use strict";
        var i = {
            displayLength: 1 / 0,
            inDuration: 300,
            outDuration: 375,
            className: void 0,
            completeCallback: void 0,
            activationPercent: .8
        }, n = function () {
            function n(e, i, o, r) {
                if (_classCallCheck(this, n), e) {
                    this.options = {
                        displayLength: i,
                        className: o,
                        completeCallback: r
                    }, this.options = t.extend({}, n.defaults, this.options), this.message = e, this.panning = !1, this.timeRemaining = this.options.displayLength, 0 === n._toasts.length && n._createContainer(), n._toasts.push(this);
                    var s = this.createToast();
                    s.M_Toast = this, this.el = s, this._animateIn(), this.setTimer()
                }
            }

            return _createClass(n, [{
                key: "createToast", value: function () {
                    var e = document.createElement("div");
                    if (e.classList.add("toast"), this.options.className) {
                        var i = this.options.className.split(" "), o = void 0, r = void 0;
                        for (o = 0, r = i.length; o < r; o++) e.classList.add(i[o])
                    }
                    return ("object" == typeof HTMLElement ? this.message instanceof HTMLElement : this.message && "object" == typeof this.message && null !== this.message && 1 === this.message.nodeType && "string" == typeof this.message.nodeName) ? e.appendChild(this.message) : this.message instanceof jQuery ? t(e).append(this.message) : e.innerHTML = this.message, n._container.appendChild(e), e
                }
            }, {
                key: "_animateIn", value: function () {
                    e(this.el, {top: 0, opacity: 1}, {duration: 300, easing: "easeOutCubic", queue: !1})
                }
            }, {
                key: "setTimer", value: function () {
                    var t = this;
                    this.timeRemaining !== 1 / 0 && (this.counterInterval = setInterval(function () {
                        t.panning || (t.timeRemaining -= 20), t.timeRemaining <= 0 && t.remove()
                    }, 20))
                }
            }, {
                key: "remove", value: function () {
                    var t = this;
                    window.clearInterval(this.counterInterval);
                    var i = this.el.offsetWidth * this.options.activationPercent;
                    this.wasSwiped && (this.el.style.transition = "transform .05s, opacity .05s", this.el.style.transform = "translateX(" + i + "px)", this.el.style.opacity = 0), e(this.el, {
                        opacity: 0,
                        marginTop: "-40px"
                    }, {
                        duration: this.options.outDuration, easing: "easeOutExpo", queue: !1, complete: function () {
                            "function" == typeof t.options.completeCallback && t.options.completeCallback(), t.el.parentNode.removeChild(t.el), n._toasts.splice(n._toasts.indexOf(t), 1), 0 === n._toasts.length && n._removeContainer()
                        }
                    })
                }
            }], [{
                key: "_createContainer", value: function () {
                    var t = document.createElement("div");
                    t.setAttribute("id", "toast-container"), t.addEventListener("touchstart", n._onDragStart), t.addEventListener("touchmove", n._onDragMove), t.addEventListener("touchend", n._onDragEnd), t.addEventListener("mousedown", n._onDragStart), document.addEventListener("mousemove", n._onDragMove), document.addEventListener("mouseup", n._onDragEnd), document.body.appendChild(t), n._container = t
                }
            }, {
                key: "_removeContainer", value: function () {
                    document.removeEventListener("mousemove", n._onDragMove), document.removeEventListener("mouseup", n._onDragEnd), n._container.parentNode.removeChild(n._container), n._container = null
                }
            }, {
                key: "_onDragStart", value: function (e) {
                    if (e.target && t(e.target).closest(".toast").length) {
                        var i = t(e.target).closest(".toast")[0].M_Toast;
                        i.panning = !0, n._draggedToast = i, i.el.classList.add("panning"), i.el.style.transition = "", i.startingXPos = n._xPos(e), i.time = Date.now(), i.xPos = n._xPos(e)
                    }
                }
            }, {
                key: "_onDragMove", value: function (t) {
                    if (n._draggedToast) {
                        t.preventDefault();
                        var e = n._draggedToast;
                        e.deltaX = Math.abs(e.xPos - n._xPos(t)), e.xPos = n._xPos(t), e.velocityX = e.deltaX / (Date.now() - e.time), e.time = Date.now();
                        var i = e.xPos - e.startingXPos, o = e.el.offsetWidth * e.options.activationPercent;
                        e.el.style.transform = "translateX(" + i + "px)", e.el.style.opacity = 1 - Math.abs(i / o)
                    }
                }
            }, {
                key: "_onDragEnd", value: function (t) {
                    if (n._draggedToast) {
                        var e = n._draggedToast;
                        e.panning = !1, e.el.classList.remove("panning");
                        var i = e.xPos - e.startingXPos, o = e.el.offsetWidth * e.options.activationPercent;
                        Math.abs(i) > o || e.velocityX > 1 ? (e.wasSwiped = !0, e.remove()) : (e.el.style.transition = "transform .2s, opacity .2s", e.el.style.transform = "", e.el.style.opacity = ""), n._draggedToast = null
                    }
                }
            }, {
                key: "_xPos", value: function (t) {
                    return t.targetTouches && t.targetTouches.length >= 1 ? t.targetTouches[0].clientX : t.clientX
                }
            }, {
                key: "removeAll", value: function () {
                    for (var t in n._toasts) n._toasts[t].remove()
                }
            }, {
                key: "defaults", get: function () {
                    return i
                }
            }]), n
        }();
        n._toasts = [], n._container = null, n._draggedToast = null, Materialize.Toast = n, Materialize.toast = function (t, e, i, o) {
            return new n(t, e, i, o)
        }
    }(jQuery, Materialize.Vel), function (t) {
        var e = {
            init: function (e) {
                e = t.extend({
                    menuWidth: 300,
                    edge: "left",
                    closeOnClick: !1,
                    draggable: !0,
                    onOpen: null,
                    onClose: null
                }, e), t(this).each(function () {
                    var i = t(this), n = i.attr("data-activates"), o = t("#" + n);
                    300 != e.menuWidth && o.css("width", e.menuWidth);
                    var r = t('.drag-target[data-sidenav="' + n + '"]');
                    e.draggable ? (r.length && r.remove(), r = t('<div class="drag-target"></div>').attr("data-sidenav", n), t("body").append(r)) : r = t(), "left" == e.edge ? (o.css("transform", "translateX(-100%)"), r.css({left: 0})) : (o.addClass("right-aligned").css("transform", "translateX(100%)"), r.css({right: 0})), o.hasClass("fixed") && window.innerWidth > 992 && o.css("transform", "translateX(0)"), o.hasClass("fixed") && t(window).resize(function () {
                        window.innerWidth > 992 ? 0 !== t("#sidenav-overlay").length && l ? s(!0) : o.css("transform", "translateX(0%)") : !1 === l && ("left" === e.edge ? o.css("transform", "translateX(-100%)") : o.css("transform", "translateX(100%)"))
                    }), !0 === e.closeOnClick && o.on("click.itemclick", "a:not(.collapsible-header)", function () {
                        window.innerWidth > 992 && o.hasClass("fixed") || s()
                    });
                    var s = function (i) {
                        a = !1, l = !1, t("body").css({
                            overflow: "",
                            width: ""
                        }), t("#sidenav-overlay").velocity({opacity: 0}, {
                            duration: 200,
                            queue: !1,
                            easing: "easeOutQuad",
                            complete: function () {
                                t(this).remove()
                            }
                        }), "left" === e.edge ? (r.css({
                            width: "",
                            right: "",
                            left: "0"
                        }), o.velocity({translateX: "-100%"}, {
                            duration: 200,
                            queue: !1,
                            easing: "easeOutCubic",
                            complete: function () {
                                !0 === i && (o.removeAttr("style"), o.css("width", e.menuWidth))
                            }
                        })) : (r.css({
                            width: "",
                            right: "0",
                            left: ""
                        }), o.velocity({translateX: "100%"}, {
                            duration: 200,
                            queue: !1,
                            easing: "easeOutCubic",
                            complete: function () {
                                !0 === i && (o.removeAttr("style"), o.css("width", e.menuWidth))
                            }
                        })), "function" == typeof e.onClose && e.onClose.call(this, o)
                    }, a = !1, l = !1;
                    e.draggable && (r.on("click", function () {
                        l && s()
                    }), r.hammer({prevent_default: !1}).on("pan", function (i) {
                        if ("touch" == i.gesture.pointerType) {
                            i.gesture.direction;
                            var n = i.gesture.center.x, r = i.gesture.center.y;
                            i.gesture.velocityX;
                            if (0 === n && 0 === r) return;
                            var a = t("body"), c = t("#sidenav-overlay"), u = a.innerWidth();
                            if (a.css("overflow", "hidden"), a.width(u), 0 === c.length && ((c = t('<div id="sidenav-overlay"></div>')).css("opacity", 0).click(function () {
                                    s()
                                }), "function" == typeof e.onOpen && e.onOpen.call(this, o), t("body").append(c)), "left" === e.edge && (n > e.menuWidth ? n = e.menuWidth : n < 0 && (n = 0)), "left" === e.edge) n < e.menuWidth / 2 ? l = !1 : n >= e.menuWidth / 2 && (l = !0), o.css("transform", "translateX(" + (n - e.menuWidth) + "px)"); else {
                                n < window.innerWidth - e.menuWidth / 2 ? l = !0 : n >= window.innerWidth - e.menuWidth / 2 && (l = !1);
                                var h = n - e.menuWidth / 2;
                                h < 0 && (h = 0), o.css("transform", "translateX(" + h + "px)")
                            }
                            var d;
                            "left" === e.edge ? (d = n / e.menuWidth, c.velocity({opacity: d}, {
                                duration: 10,
                                queue: !1,
                                easing: "easeOutQuad"
                            })) : (d = Math.abs((n - window.innerWidth) / e.menuWidth), c.velocity({opacity: d}, {
                                duration: 10,
                                queue: !1,
                                easing: "easeOutQuad"
                            }))
                        }
                    }).on("panend", function (i) {
                        if ("touch" == i.gesture.pointerType) {
                            var n = t("#sidenav-overlay"), s = i.gesture.velocityX, c = i.gesture.center.x,
                                u = c - e.menuWidth, h = c - e.menuWidth / 2;
                            u > 0 && (u = 0), h < 0 && (h = 0), a = !1, "left" === e.edge ? l && s <= .3 || s < -.5 ? (0 !== u && o.velocity({translateX: [0, u]}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), n.velocity({opacity: 1}, {
                                duration: 50,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), r.css({
                                width: "50%",
                                right: 0,
                                left: ""
                            }), l = !0) : (!l || s > .3) && (t("body").css({
                                overflow: "",
                                width: ""
                            }), o.velocity({translateX: [-1 * e.menuWidth - 10, u]}, {
                                duration: 200,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), n.velocity({opacity: 0}, {
                                duration: 200,
                                queue: !1,
                                easing: "easeOutQuad",
                                complete: function () {
                                    "function" == typeof e.onClose && e.onClose.call(this, o), t(this).remove()
                                }
                            }), r.css({
                                width: "10px",
                                right: "",
                                left: 0
                            })) : l && s >= -.3 || s > .5 ? (0 !== h && o.velocity({translateX: [0, h]}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), n.velocity({opacity: 1}, {
                                duration: 50,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), r.css({
                                width: "50%",
                                right: "",
                                left: 0
                            }), l = !0) : (!l || s < -.3) && (t("body").css({
                                overflow: "",
                                width: ""
                            }), o.velocity({translateX: [e.menuWidth + 10, h]}, {
                                duration: 200,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), n.velocity({opacity: 0}, {
                                duration: 200,
                                queue: !1,
                                easing: "easeOutQuad",
                                complete: function () {
                                    "function" == typeof e.onClose && e.onClose.call(this, o), t(this).remove()
                                }
                            }), r.css({width: "10px", right: 0, left: ""}))
                        }
                    })), i.off("click.sidenav").on("click.sidenav", function () {
                        if (!0 === l) l = !1, a = !1, s(); else {
                            var i = t("body"), n = t('<div id="sidenav-overlay"></div>'), c = i.innerWidth();
                            i.css("overflow", "hidden"), i.width(c), t("body").append(r), "left" === e.edge ? (r.css({
                                width: "50%",
                                right: 0,
                                left: ""
                            }), o.velocity({translateX: [0, -1 * e.menuWidth]}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad"
                            })) : (r.css({
                                width: "50%",
                                right: "",
                                left: 0
                            }), o.velocity({translateX: [0, e.menuWidth]}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad"
                            })), n.css("opacity", 0).click(function () {
                                l = !1, a = !1, s(), n.velocity({opacity: 0}, {
                                    duration: 300,
                                    queue: !1,
                                    easing: "easeOutQuad",
                                    complete: function () {
                                        t(this).remove()
                                    }
                                })
                            }), t("body").append(n), n.velocity({opacity: 1}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad",
                                complete: function () {
                                    l = !0, a = !1
                                }
                            }), "function" == typeof e.onOpen && e.onOpen.call(this, o)
                        }
                        return !1
                    })
                })
            }, destroy: function () {
                var e = t("#sidenav-overlay"),
                    i = t('.drag-target[data-sidenav="' + t(this).attr("data-activates") + '"]');
                e.trigger("click"), i.remove(), t(this).off("click"), e.remove()
            }, show: function () {
                this.trigger("click")
            }, hide: function () {
                t("#sidenav-overlay").trigger("click")
            }
        };
        t.fn.sideNav = function (i) {
            return e[i] ? e[i].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof i && i ? void t.error("Method " + i + " does not exist on jQuery.sideNav") : e.init.apply(this, arguments)
        }
    }(jQuery), function (t) {
        var e = t(window), i = [], n = [], o = !1, r = 0, s = {top: 0, right: 0, bottom: 0, left: 0};
        t.scrollSpy = function (a, l) {
            l = t.extend({
                throttle: 100, scrollOffset: 200, activeClass: "active", getActiveElement: function (t) {
                    return 'a[href="#' + t + '"]'
                }
            }, l);
            var c = [];
            var u = Materialize.throttle(function () {
                !function (o) {
                    ++r;
                    var a = e.scrollTop(), l = e.scrollLeft(), c = l + e.width(), u = a + e.height(),
                        h = function (e, n, o, r) {
                            var s = t();
                            return t.each(i, function (t, i) {
                                if (i.height() > 0) {
                                    var a = i.offset().top, l = i.offset().left, c = l + i.width(), u = a + i.height();
                                    !(l > n || c < r || a > o || u < e) && s.push(i)
                                }
                            }), s
                        }(a + s.top + o || 200, c + s.right, u + s.bottom, l + s.left);
                    t.each(h, function (t, e) {
                        "number" != typeof e.data("scrollSpy:ticks") && e.triggerHandler("scrollSpy:enter"), e.data("scrollSpy:ticks", r)
                    }), t.each(n, function (t, e) {
                        var i = e.data("scrollSpy:ticks");
                        "number" == typeof i && i !== r && (e.triggerHandler("scrollSpy:exit"), e.data("scrollSpy:ticks", null))
                    }), n = h
                }(l.scrollOffset)
            }, l.throttle || 100), h = function () {
                t(document).ready(u)
            };
            return o || (e.on("scroll", h), e.on("resize", h), o = !0), setTimeout(h, 0), a.on("scrollSpy:enter", function () {
                c = t.grep(c, function (t) {
                    return 0 != t.height()
                });
                var e = t(this);
                c[0] ? (t(l.getActiveElement(c[0].attr("id"))).removeClass(l.activeClass), e.data("scrollSpy:id") < c[0].data("scrollSpy:id") ? c.unshift(t(this)) : c.push(t(this))) : c.push(t(this)), t(l.getActiveElement(c[0].attr("id"))).addClass(l.activeClass)
            }), a.on("scrollSpy:exit", function () {
                if ((c = t.grep(c, function (t) {
                        return 0 != t.height()
                    }))[0]) {
                    t(l.getActiveElement(c[0].attr("id"))).removeClass(l.activeClass);
                    var e = t(this);
                    (c = t.grep(c, function (t) {
                        return t.attr("id") != e.attr("id")
                    }))[0] && t(l.getActiveElement(c[0].attr("id"))).addClass(l.activeClass)
                }
            }), a
        }, t.winSizeSpy = function (i) {
            return t.winSizeSpy = function () {
                return e
            }, i = i || {throttle: 100}, e.on("resize", Materialize.throttle(function () {
                e.trigger("scrollSpy:winSize")
            }, i.throttle || 100))
        }, t.fn.scrollSpy = function (e) {
            return t.scrollSpy(t(this), e)
        }
    }(jQuery), function (t) {
        t(document).ready(function () {
            Materialize.updateTextFields = function () {
                t("input[type=text], input[type=password], input[type=email], input[type=url], input[type=tel], input[type=number], input[type=search], textarea").each(function (e, i) {
                    var n = t(this);
                    t(i).val().length > 0 || t(i).is(":focus") || i.autofocus || void 0 !== n.attr("placeholder") ? n.siblings("label").addClass("active") : t(i)[0].validity ? n.siblings("label").toggleClass("active", !0 === t(i)[0].validity.badInput) : n.siblings("label").removeClass("active")
                })
            };
            var e = "input[type=text], input[type=password], input[type=email], input[type=url], input[type=tel], input[type=number], input[type=search], textarea";
            t(document).on("change", e, function () {
                0 === t(this).val().length && void 0 === t(this).attr("placeholder") || t(this).siblings("label").addClass("active"), validate_field(t(this))
            }), t(document).ready(function () {
                Materialize.updateTextFields()
            }), t(document).on("reset", function (i) {
                var n = t(i.target);
                n.is("form") && (n.find(e).removeClass("valid").removeClass("invalid"), n.find(e).each(function () {
                    "" === t(this).attr("value") && t(this).siblings("label").removeClass("active")
                }), n.find("select.initialized").each(function () {
                    var t = n.find("option[selected]").text();
                    n.siblings("input.select-dropdown").val(t)
                }))
            }), t(document).on("focus", e, function () {
                t(this).siblings("label, .prefix").addClass("active")
            }), t(document).on("blur", e, function () {
                var e = t(this), i = ".prefix";
                0 === e.val().length && !0 !== e[0].validity.badInput && void 0 === e.attr("placeholder") && (i += ", label"), e.siblings(i).removeClass("active"), validate_field(e)
            }), window.validate_field = function (t) {
                var e = void 0 !== t.attr("data-length"), i = parseInt(t.attr("data-length")), n = t.val().length;
                0 !== t.val().length || !1 !== t[0].validity.badInput || t.is(":required") ? t.hasClass("validate") && (t.is(":valid") && e && n <= i || t.is(":valid") && !e ? (t.removeClass("invalid"), t.addClass("valid")) : (t.removeClass("valid"), t.addClass("invalid"))) : t.hasClass("validate") && (t.removeClass("valid"), t.removeClass("invalid"))
            };
            t(document).on("keyup.radio", "input[type=radio], input[type=checkbox]", function (e) {
                if (9 !== e.which) ; else {
                    t(this).addClass("tabbed");
                    t(this).one("blur", function (e) {
                        t(this).removeClass("tabbed")
                    })
                }
            });
            var i = t(".hiddendiv").first();
            i.length || (i = t('<div class="hiddendiv common"></div>'), t("body").append(i));
            t(".materialize-textarea").each(function () {
                var e = t(this);
                e.data("original-height", e.height()), e.data("previous-length", e.val().length)
            }), t("body").on("keyup keydown autoresize", ".materialize-textarea", function () {
                !function (e) {
                    var n = e.css("font-family"), o = e.css("font-size"), r = e.css("line-height"),
                        s = e.css("padding");
                    o && i.css("font-size", o), n && i.css("font-family", n), r && i.css("line-height", r), s && i.css("padding", s), e.data("original-height") || e.data("original-height", e.height()), "off" === e.attr("wrap") && i.css("overflow-wrap", "normal").css("white-space", "pre"), i.text(e.val() + "\n");
                    var a = i.html().replace(/\n/g, "<br>");
                    i.html(a), e.is(":visible") ? i.css("width", e.width()) : i.css("width", t(window).width() / 2), e.data("original-height") <= i.height() ? e.css("height", i.height()) : e.val().length < e.data("previous-length") && e.css("height", e.data("original-height")), e.data("previous-length", e.val().length)
                }(t(this))
            }), t(document).on("change", '.file-field input[type="file"]', function () {
                for (var e = t(this).closest(".file-field").find("input.file-path"), i = t(this)[0].files, n = [], o = 0; o < i.length; o++) n.push(i[o].name);
                e.val(n.join(", ")), e.trigger("change")
            });
            var n = "input[type=range]", o = !1;
            t(n).each(function () {
                var e = t('<span class="thumb"><span class="value"></span></span>');
                t(this).after(e)
            });
            var r = function (t) {
                var e = -7 + parseInt(t.parent().css("padding-left")) + "px";
                t.velocity({height: "30px", width: "30px", top: "-30px", marginLeft: e}, {
                    duration: 300,
                    easing: "easeOutExpo"
                })
            }, s = function (t) {
                var e = t.width() - 15, i = parseFloat(t.attr("max")), n = parseFloat(t.attr("min"));
                return (parseFloat(t.val()) - n) / (i - n) * e
            };
            t(document).on("change", n, function (e) {
                var i = t(this).siblings(".thumb");
                i.find(".value").html(t(this).val()), i.hasClass("active") || r(i);
                var n = s(t(this));
                i.addClass("active").css("left", n)
            }), t(document).on("mousedown touchstart", n, function (e) {
                var i = t(this).siblings(".thumb");
                if (i.length <= 0 && (i = t('<span class="thumb"><span class="value"></span></span>'), t(this).after(i)), i.find(".value").html(t(this).val()), o = !0, t(this).addClass("active"), i.hasClass("active") || r(i), "input" !== e.type) {
                    var n = s(t(this));
                    i.addClass("active").css("left", n)
                }
            }), t(document).on("mouseup touchend", ".range-field", function () {
                o = !1, t(this).removeClass("active")
            }), t(document).on("input mousemove touchmove", ".range-field", function (e) {
                var i = t(this).children(".thumb"), a = t(this).find(n);
                if (o) {
                    i.hasClass("active") || r(i);
                    var l = s(a);
                    i.addClass("active").css("left", l), i.find(".value").html(i.siblings(n).val())
                }
            }), t(document).on("mouseout touchleave", ".range-field", function () {
                if (!o) {
                    var e = t(this).children(".thumb"), i = 7 + parseInt(t(this).css("padding-left")) + "px";
                    e.hasClass("active") && e.velocity({
                        height: "0",
                        width: "0",
                        top: "10px",
                        marginLeft: i
                    }, {duration: 100}), e.removeClass("active")
                }
            }), t.fn.autocomplete = function (e) {
                var i = {data: {}, limit: 1 / 0, onAutocomplete: null, minLength: 1};
                return e = t.extend(i, e), this.each(function () {
                    var i, n = t(this), o = e.data, r = 0, s = -1, a = n.closest(".input-field");
                    if (t.isEmptyObject(o)) n.off("keyup.autocomplete focus.autocomplete"); else {
                        var l, c = t('<ul class="autocomplete-content dropdown-content"></ul>');
                        a.length ? (l = a.children(".autocomplete-content.dropdown-content").first()).length || a.append(c) : (l = n.next(".autocomplete-content.dropdown-content")).length || n.after(c), l.length && (c = l);
                        var u = function () {
                            c.empty(), s = -1, c.find(".active").removeClass("active"), i = void 0
                        };
                        n.off("blur.autocomplete").on("blur.autocomplete", function () {
                            u()
                        }), n.off("keyup.autocomplete focus.autocomplete").on("keyup.autocomplete focus.autocomplete", function (s) {
                            r = 0;
                            var a = n.val().toLowerCase();
                            if (13 !== s.which && 38 !== s.which && 40 !== s.which) {
                                if (i !== a && (u(), a.length >= e.minLength)) for (var l in o) if (o.hasOwnProperty(l) && -1 !== l.toLowerCase().indexOf(a)) {
                                    if (r >= e.limit) break;
                                    var h = t("<li></li>");
                                    o[l] ? h.append('<img src="' + o[l] + '" class="right circle"><span>' + l + "</span>") : h.append("<span>" + l + "</span>"), c.append(h), function (t, e) {
                                        var i = e.find("img"), n = e.text().toLowerCase().indexOf("" + t.toLowerCase()),
                                            o = n + t.length - 1, r = e.text().slice(0, n),
                                            s = e.text().slice(n, o + 1), a = e.text().slice(o + 1);
                                        e.html("<span>" + r + "<span class='highlight'>" + s + "</span>" + a + "</span>"), i.length && e.prepend(i)
                                    }(a, h), r++
                                }
                                i = a
                            }
                        }), n.off("keydown.autocomplete").on("keydown.autocomplete", function (t) {
                            var e, i = t.which, n = c.children("li").length, o = c.children(".active").first();
                            13 === i && s >= 0 ? (e = c.children("li").eq(s)).length && (e.trigger("mousedown.autocomplete"), t.preventDefault()) : 38 !== i && 40 !== i || (t.preventDefault(), 38 === i && s > 0 && s--, 40 === i && s < n - 1 && s++, o.removeClass("active"), s >= 0 && c.children("li").eq(s).addClass("active"))
                        }), c.off("mousedown.autocomplete touchstart.autocomplete").on("mousedown.autocomplete touchstart.autocomplete", "li", function () {
                            var i = t(this).text().trim();
                            n.val(i), n.trigger("change"), u(), "function" == typeof e.onAutocomplete && e.onAutocomplete.call(this, i)
                        })
                    }
                })
            }
        }), t.fn.material_select = function (e) {
            function i(t, e, i) {
                var n = t.indexOf(e), o = -1 === n;
                return o ? t.push(e) : t.splice(n, 1), i.siblings("ul.dropdown-content").find("li:not(.optgroup)").eq(e).toggleClass("active"), i.find("option").eq(e).prop("selected", o), function (t, e) {
                    for (var i = "", n = 0, o = t.length; n < o; n++) {
                        var r = e.find("option").eq(t[n]).text();
                        i += 0 === n ? r : ", " + r
                    }
                    "" === i && (i = e.find("option:disabled").eq(0).text());
                    e.siblings("input.select-dropdown").val(i)
                }(t, i), o
            }

            t(this).each(function () {
                var n = t(this);
                if (!n.hasClass("browser-default")) {
                    var o = !!n.attr("multiple"), r = n.attr("data-select-id");
                    if (r && (n.parent().find("span.caret").remove(), n.parent().find("input").remove(), n.unwrap(), t("ul#select-options-" + r).remove()), "destroy" === e) return n.removeAttr("data-select-id").removeClass("initialized"), void t(window).off("click.select");
                    var s = Materialize.guid();
                    n.attr("data-select-id", s);
                    var a = t('<div class="select-wrapper"></div>');
                    a.addClass(n.attr("class")), n.is(":disabled") && a.addClass("disabled");
                    var l = t('<ul id="select-options-' + s + '" class="dropdown-content select-dropdown ' + (o ? "multiple-select-dropdown" : "") + '"></ul>'),
                        c = n.children("option, optgroup"), u = [], h = !1,
                        d = n.find("option:selected").html() || n.find("option:first").html() || "",
                        p = function (e, i, n) {
                            var r = i.is(":disabled") ? "disabled " : "",
                                s = "optgroup-option" === n ? "optgroup-option " : "",
                                a = o ? '<input type="checkbox"' + r + "/><label></label>" : "", c = i.data("icon"),
                                u = i.attr("class");
                            if (c) {
                                var h = "";
                                return u && (h = ' class="' + u + '"'), l.append(t('<li class="' + r + s + '"><img alt="" src="' + c + '"' + h + "><span>" + a + i.html() + "</span></li>")), !0
                            }
                            l.append(t('<li class="' + r + s + '"><span>' + a + i.html() + "</span></li>"))
                        };
                    c.length && c.each(function () {
                        if (t(this).is("option")) o ? p(0, t(this), "multiple") : p(0, t(this)); else if (t(this).is("optgroup")) {
                            var e = t(this).children("option");
                            l.append(t('<li class="optgroup"><span>' + t(this).attr("label") + "</span></li>")), e.each(function () {
                                p(0, t(this), "optgroup-option")
                            })
                        }
                    }), l.find("li:not(.optgroup)").each(function (r) {
                        t(this).click(function (s) {
                            if (!t(this).hasClass("disabled") && !t(this).hasClass("optgroup")) {
                                var a = !0;
                                o ? (t('input[type="checkbox"]', this).prop("checked", function (t, e) {
                                    return !e
                                }), a = i(u, r, n), v.trigger("focus")) : (l.find("li").removeClass("active"), t(this).toggleClass("active"), v.val(t(this).text())), m(l, t(this)), n.find("option").eq(r).prop("selected", a), n.trigger("change"), void 0 !== e && e()
                            }
                            s.stopPropagation()
                        })
                    }), n.wrap(a);
                    var f = t('<span class="caret">&#9660;</span>'), g = d.replace(/"/g, "&quot;"),
                        v = t('<input type="text" class="select-dropdown" readonly="true" ' + (n.is(":disabled") ? "disabled" : "") + ' data-activates="select-options-' + s + '" value="' + g + '"/>');
                    n.before(v), v.before(f), v.after(l), n.is(":disabled") || v.dropdown({hover: !1}), n.attr("tabindex") && t(v[0]).attr("tabindex", n.attr("tabindex")), n.addClass("initialized"), v.on({
                        focus: function () {
                            if (t("ul.select-dropdown").not(l[0]).is(":visible") && (t("input.select-dropdown").trigger("close"), t(window).off("click.select")), !l.is(":visible")) {
                                t(this).trigger("open", ["focus"]);
                                var e = t(this).val();
                                o && e.indexOf(",") >= 0 && (e = e.split(",")[0]);
                                var i = l.find("li").filter(function () {
                                    return t(this).text().toLowerCase() === e.toLowerCase()
                                })[0];
                                m(l, i, !0), t(window).off("click.select").on("click.select", function () {
                                    o && (h || v.trigger("close")), t(window).off("click.select")
                                })
                            }
                        }, click: function (t) {
                            t.stopPropagation()
                        }
                    }), v.on("blur", function () {
                        o || (t(this).trigger("close"), t(window).off("click.select")), l.find("li.selected").removeClass("selected")
                    }), l.hover(function () {
                        h = !0
                    }, function () {
                        h = !1
                    }), o && n.find("option:selected:not(:disabled)").each(function () {
                        var t = this.index;
                        i(u, t, n), l.find("li:not(.optgroup)").eq(t).find(":checkbox").prop("checked", !0)
                    });
                    var m = function (e, i, n) {
                        if (i) {
                            e.find("li.selected").removeClass("selected");
                            var r = t(i);
                            r.addClass("selected"), o && !n || l.scrollTo(r)
                        }
                    }, y = [];
                    v.on("keydown", function (e) {
                        if (9 != e.which) if (40 != e.which || l.is(":visible")) {
                            if (13 != e.which || l.is(":visible")) {
                                e.preventDefault();
                                var i = String.fromCharCode(e.which).toLowerCase();
                                if (i && -1 === [9, 13, 27, 38, 40].indexOf(e.which)) {
                                    y.push(i);
                                    var n = y.join(""), r = l.find("li").filter(function () {
                                        return 0 === t(this).text().toLowerCase().indexOf(n)
                                    })[0];
                                    r && m(l, r)
                                }
                                if (13 == e.which) {
                                    var s = l.find("li.selected:not(.disabled)")[0];
                                    s && (t(s).trigger("click"), o || v.trigger("close"))
                                }
                                40 == e.which && (r = l.find("li.selected").length ? l.find("li.selected").next("li:not(.disabled)")[0] : l.find("li:not(.disabled)")[0], m(l, r)), 27 == e.which && v.trigger("close"), 38 == e.which && (r = l.find("li.selected").prev("li:not(.disabled)")[0]) && m(l, r), setTimeout(function () {
                                    y = []
                                }, 1e3)
                            }
                        } else v.trigger("open"); else v.trigger("close")
                    })
                }
            })
        }
    }(jQuery), function (t) {
        var e = {
            init: function (e) {
                return e = t.extend({
                    indicators: !0,
                    height: 400,
                    transition: 500,
                    interval: 6e3
                }, e), this.each(function () {
                    function i(t, e) {
                        t.hasClass("center-align") ? t.velocity({opacity: 0, translateY: -100}, {
                            duration: e,
                            queue: !1
                        }) : t.hasClass("right-align") ? t.velocity({opacity: 0, translateX: 100}, {
                            duration: e,
                            queue: !1
                        }) : t.hasClass("left-align") && t.velocity({opacity: 0, translateX: -100}, {
                            duration: e,
                            queue: !1
                        })
                    }

                    function n(t) {
                        t >= c.length ? t = 0 : t < 0 && (t = c.length - 1), (u = l.find(".active").index()) != t && (o = c.eq(u), $caption = o.find(".caption"), o.removeClass("active"), o.velocity({opacity: 0}, {
                            duration: e.transition,
                            queue: !1,
                            easing: "easeOutQuad",
                            complete: function () {
                                c.not(".active").velocity({opacity: 0, translateX: 0, translateY: 0}, {
                                    duration: 0,
                                    queue: !1
                                })
                            }
                        }), i($caption, e.transition), e.indicators && r.eq(u).removeClass("active"), c.eq(t).velocity({opacity: 1}, {
                            duration: e.transition,
                            queue: !1,
                            easing: "easeOutQuad"
                        }), c.eq(t).find(".caption").velocity({
                            opacity: 1,
                            translateX: 0,
                            translateY: 0
                        }, {
                            duration: e.transition,
                            delay: e.transition,
                            queue: !1,
                            easing: "easeOutQuad"
                        }), c.eq(t).addClass("active"), e.indicators && r.eq(t).addClass("active"))
                    }

                    var o, r, s, a = t(this), l = a.find("ul.slides").first(), c = l.find("> li"),
                        u = l.find(".active").index();
                    -1 != u && (o = c.eq(u)), a.hasClass("fullscreen") || (e.indicators ? a.height(e.height + 40) : a.height(e.height), l.height(e.height)), c.find(".caption").each(function () {
                        i(t(this), 0)
                    }), c.find("img").each(function () {
                        var e = "data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==";
                        t(this).attr("src") !== e && (t(this).css("background-image", 'url("' + t(this).attr("src") + '")'), t(this).attr("src", e))
                    }), e.indicators && (r = t('<ul class="indicators"></ul>'), c.each(function (i) {
                        var o = t('<li class="indicator-item"></li>');
                        o.click(function () {
                            n(l.parent().find(t(this)).index()), clearInterval(s), s = setInterval(function () {
                                u = l.find(".active").index(), c.length == u + 1 ? u = 0 : u += 1, n(u)
                            }, e.transition + e.interval)
                        }), r.append(o)
                    }), a.append(r), r = a.find("ul.indicators").find("li.indicator-item")), o ? o.show() : (c.first().addClass("active").velocity({opacity: 1}, {
                        duration: e.transition,
                        queue: !1,
                        easing: "easeOutQuad"
                    }), u = 0, o = c.eq(u), e.indicators && r.eq(u).addClass("active")), o.find("img").each(function () {
                        o.find(".caption").velocity({opacity: 1, translateX: 0, translateY: 0}, {
                            duration: e.transition,
                            queue: !1,
                            easing: "easeOutQuad"
                        })
                    }), s = setInterval(function () {
                        n((u = l.find(".active").index()) + 1)
                    }, e.transition + e.interval);
                    var h = !1, d = !1, p = !1;
                    a.hammer({prevent_default: !1}).on("pan", function (t) {
                        if ("touch" === t.gesture.pointerType) {
                            clearInterval(s);
                            var e = t.gesture.direction, i = t.gesture.deltaX, n = t.gesture.velocityX,
                                o = t.gesture.velocityY;
                            $curr_slide = l.find(".active"), Math.abs(n) > Math.abs(o) && $curr_slide.velocity({translateX: i}, {
                                duration: 50,
                                queue: !1,
                                easing: "easeOutQuad"
                            }), 4 === e && (i > a.innerWidth() / 2 || n < -.65) ? p = !0 : 2 === e && (i < -1 * a.innerWidth() / 2 || n > .65) && (d = !0);
                            var r;
                            d && (0 === (r = $curr_slide.next()).length && (r = c.first()), r.velocity({opacity: 1}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad"
                            })), p && (0 === (r = $curr_slide.prev()).length && (r = c.last()), r.velocity({opacity: 1}, {
                                duration: 300,
                                queue: !1,
                                easing: "easeOutQuad"
                            }))
                        }
                    }).on("panend", function (t) {
                        "touch" === t.gesture.pointerType && ($curr_slide = l.find(".active"), h = !1, curr_index = l.find(".active").index(), !p && !d || c.length <= 1 ? $curr_slide.velocity({translateX: 0}, {
                            duration: 300,
                            queue: !1,
                            easing: "easeOutQuad"
                        }) : d ? (n(curr_index + 1), $curr_slide.velocity({translateX: -1 * a.innerWidth()}, {
                            duration: 300,
                            queue: !1,
                            easing: "easeOutQuad",
                            complete: function () {
                                $curr_slide.velocity({opacity: 0, translateX: 0}, {duration: 0, queue: !1})
                            }
                        })) : p && (n(curr_index - 1), $curr_slide.velocity({translateX: a.innerWidth()}, {
                            duration: 300,
                            queue: !1,
                            easing: "easeOutQuad",
                            complete: function () {
                                $curr_slide.velocity({opacity: 0, translateX: 0}, {duration: 0, queue: !1})
                            }
                        })), d = !1, p = !1, clearInterval(s), s = setInterval(function () {
                            u = l.find(".active").index(), c.length == u + 1 ? u = 0 : u += 1, n(u)
                        }, e.transition + e.interval))
                    }), a.on("sliderPause", function () {
                        clearInterval(s)
                    }), a.on("sliderStart", function () {
                        clearInterval(s), s = setInterval(function () {
                            u = l.find(".active").index(), c.length == u + 1 ? u = 0 : u += 1, n(u)
                        }, e.transition + e.interval)
                    }), a.on("sliderNext", function () {
                        n((u = l.find(".active").index()) + 1)
                    }), a.on("sliderPrev", function () {
                        n((u = l.find(".active").index()) - 1)
                    })
                })
            }, pause: function () {
                t(this).trigger("sliderPause")
            }, start: function () {
                t(this).trigger("sliderStart")
            }, next: function () {
                t(this).trigger("sliderNext")
            }, prev: function () {
                t(this).trigger("sliderPrev")
            }
        };
        t.fn.slider = function (i) {
            return e[i] ? e[i].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof i && i ? void t.error("Method " + i + " does not exist on jQuery.tooltip") : e.init.apply(this, arguments)
        }
    }(jQuery), function (t) {
        t(document).ready(function () {
            t(document).on("click.card", ".card", function (e) {
                if (t(this).find("> .card-reveal").length) {
                    var i = t(e.target).closest(".card");
                    void 0 === i.data("initialOverflow") && i.data("initialOverflow", void 0 === i.css("overflow") ? "" : i.css("overflow")), t(e.target).is(t(".card-reveal .card-title")) || t(e.target).is(t(".card-reveal .card-title i")) ? t(this).find(".card-reveal").velocity({translateY: 0}, {
                        duration: 225,
                        queue: !1,
                        easing: "easeInOutQuad",
                        complete: function () {
                            t(this).css({display: "none"}), i.css("overflow", i.data("initialOverflow"))
                        }
                    }) : (t(e.target).is(t(".card .activator")) || t(e.target).is(t(".card .activator i"))) && (i.css("overflow", "hidden"), t(this).find(".card-reveal").css({display: "block"}).velocity("stop", !1).velocity({translateY: "-100%"}, {
                        duration: 300,
                        queue: !1,
                        easing: "easeInOutQuad"
                    }))
                }
            })
        })
    }(jQuery), function (t) {
        var e = {data: [], placeholder: "", secondaryPlaceholder: "", autocompleteOptions: {}};
        t(document).ready(function () {
            t(document).on("click", ".chip .close", function (e) {
                t(this).closest(".chips").attr("data-initialized") || t(this).closest(".chip").remove()
            })
        }), t.fn.material_chip = function (i) {
            var n = this;
            if (this.$el = t(this), this.$document = t(document), this.SELS = {
                    CHIPS: ".chips",
                    CHIP: ".chip",
                    INPUT: "input",
                    DELETE: ".material-icons",
                    SELECTED_CHIP: ".selected"
                }, "data" === i) return this.$el.data("chips");
            var o = t.extend({}, e, i);
            n.hasAutocomplete = !t.isEmptyObject(o.autocompleteOptions.data), this.init = function () {
                var e = 0;
                n.$el.each(function () {
                    var i = t(this), r = Materialize.guid();
                    n.chipId = r, o.data && o.data instanceof Array || (o.data = []), i.data("chips", o.data), i.attr("data-index", e), i.attr("data-initialized", !0), i.hasClass(n.SELS.CHIPS) || i.addClass("chips"), n.chips(i, r), e++
                })
            }, this.handleEvents = function () {
                var e = n.SELS;
                n.$document.off("click.chips-focus", e.CHIPS).on("click.chips-focus", e.CHIPS, function (i) {
                    t(i.target).find(e.INPUT).focus()
                }), n.$document.off("click.chips-select", e.CHIP).on("click.chips-select", e.CHIP, function (i) {
                    var o = t(i.target);
                    if (o.length) {
                        var r = o.hasClass("selected"), s = o.closest(e.CHIPS);
                        t(e.CHIP).removeClass("selected"), r || n.selectChip(o.index(), s)
                    }
                }), n.$document.off("keydown.chips").on("keydown.chips", function (i) {
                    if (!t(i.target).is("input, textarea")) {
                        var o, r = n.$document.find(e.CHIP + e.SELECTED_CHIP), s = r.closest(e.CHIPS),
                            a = r.siblings(e.CHIP).length;
                        if (r.length) if (8 === i.which || 46 === i.which) {
                            i.preventDefault(), o = r.index(), n.deleteChip(o, s);
                            var l = null;
                            o + 1 < a ? l = o : o !== a && o + 1 !== a || (l = a - 1), l < 0 && (l = null), null !== l && n.selectChip(l, s), a || s.find("input").focus()
                        } else if (37 === i.which) {
                            if ((o = r.index() - 1) < 0) return;
                            t(e.CHIP).removeClass("selected"), n.selectChip(o, s)
                        } else if (39 === i.which) {
                            if (o = r.index() + 1, t(e.CHIP).removeClass("selected"), o > a) return void s.find("input").focus();
                            n.selectChip(o, s)
                        }
                    }
                }), n.$document.off("focusin.chips", e.CHIPS + " " + e.INPUT).on("focusin.chips", e.CHIPS + " " + e.INPUT, function (i) {
                    var n = t(i.target).closest(e.CHIPS);
                    n.addClass("focus"), n.siblings("label, .prefix").addClass("active"), t(e.CHIP).removeClass("selected")
                }), n.$document.off("focusout.chips", e.CHIPS + " " + e.INPUT).on("focusout.chips", e.CHIPS + " " + e.INPUT, function (i) {
                    var n = t(i.target).closest(e.CHIPS);
                    n.removeClass("focus"), void 0 !== n.data("chips") && n.data("chips").length || n.siblings("label").removeClass("active"), n.siblings(".prefix").removeClass("active")
                }), n.$document.off("keydown.chips-add", e.CHIPS + " " + e.INPUT).on("keydown.chips-add", e.CHIPS + " " + e.INPUT, function (i) {
                    var o = t(i.target), r = o.closest(e.CHIPS), s = r.children(e.CHIP).length;
                    if (13 === i.which) {
                        if (n.hasAutocomplete && r.find(".autocomplete-content.dropdown-content").length && r.find(".autocomplete-content.dropdown-content").children().length) return;
                        return i.preventDefault(), n.addChip({tag: o.val()}, r), void o.val("")
                    }
                    if ((8 === i.keyCode || 37 === i.keyCode) && "" === o.val() && s) return i.preventDefault(), n.selectChip(s - 1, r), void o.blur()
                }), n.$document.off("click.chips-delete", e.CHIPS + " " + e.DELETE).on("click.chips-delete", e.CHIPS + " " + e.DELETE, function (i) {
                    var o = t(i.target), r = o.closest(e.CHIPS), s = o.closest(e.CHIP);
                    i.stopPropagation(), n.deleteChip(s.index(), r), r.find("input").focus()
                })
            }, this.chips = function (e, i) {
                e.empty(), e.data("chips").forEach(function (t) {
                    e.append(n.renderChip(t))
                }), e.append(t('<input id="' + i + '" class="input" placeholder="">')), n.setPlaceholder(e);
                var r = e.next("label");
                r.length && (r.attr("for", i), void 0 !== e.data("chips") && e.data("chips").length && r.addClass("active"));
                var s = t("#" + i);
                n.hasAutocomplete && (o.autocompleteOptions.onAutocomplete = function (t) {
                    n.addChip({tag: t}, e), s.val(""), s.focus()
                }, s.autocomplete(o.autocompleteOptions))
            }, this.renderChip = function (e) {
                if (e.tag) {
                    var i = t('<div class="chip"></div>');
                    return i.text(e.tag), e.image && i.prepend(t("<img />").attr("src", e.image)), i.append(t('<i class="material-icons close">close</i>')), i
                }
            }, this.setPlaceholder = function (t) {
                void 0 !== t.data("chips") && !t.data("chips").length && o.placeholder ? t.find("input").prop("placeholder", o.placeholder) : (void 0 === t.data("chips") || t.data("chips").length) && o.secondaryPlaceholder && t.find("input").prop("placeholder", o.secondaryPlaceholder)
            }, this.isValid = function (t, e) {
                for (var i = t.data("chips"), n = !1, o = 0; o < i.length; o++) if (i[o].tag === e.tag) return void(n = !0);
                return "" !== e.tag && !n
            }, this.addChip = function (t, e) {
                if (n.isValid(e, t)) {
                    for (var i = n.renderChip(t), o = [], r = e.data("chips"), s = 0; s < r.length; s++) o.push(r[s]);
                    o.push(t), e.data("chips", o), i.insertBefore(e.find("input")), e.trigger("chip.add", t), n.setPlaceholder(e)
                }
            }, this.deleteChip = function (t, e) {
                var i = e.data("chips")[t];
                e.find(".chip").eq(t).remove();
                for (var o = [], r = e.data("chips"), s = 0; s < r.length; s++) s !== t && o.push(r[s]);
                e.data("chips", o), e.trigger("chip.delete", i), n.setPlaceholder(e)
            }, this.selectChip = function (t, e) {
                var i = e.find(".chip").eq(t);
                i && !1 === i.hasClass("selected") && (i.addClass("selected"), e.trigger("chip.select", e.data("chips")[t]))
            }, this.getChipsElement = function (t, e) {
                return e.eq(t)
            }, this.init(), this.handleEvents()
        }
    }(jQuery), function (t) {
        t.fn.pushpin = function (e) {
            var i = {top: 0, bottom: 1 / 0, offset: 0};
            return "remove" === e ? (this.each(function () {
                (id = t(this).data("pushpin-id")) && (t(window).off("scroll." + id), t(this).removeData("pushpin-id").removeClass("pin-top pinned pin-bottom").removeAttr("style"))
            }), !1) : (e = t.extend(i, e), $index = 0, this.each(function () {
                function i(t) {
                    t.removeClass("pin-top"), t.removeClass("pinned"), t.removeClass("pin-bottom")
                }

                function n(n, o) {
                    n.each(function () {
                        e.top <= o && e.bottom >= o && !t(this).hasClass("pinned") && (i(t(this)), t(this).css("top", e.offset), t(this).addClass("pinned")), o < e.top && !t(this).hasClass("pin-top") && (i(t(this)), t(this).css("top", 0), t(this).addClass("pin-top")), o > e.bottom && !t(this).hasClass("pin-bottom") && (i(t(this)), t(this).addClass("pin-bottom"), t(this).css("top", e.bottom - s))
                    })
                }

                var o = Materialize.guid(), r = t(this), s = t(this).offset().top;
                t(this).data("pushpin-id", o), n(r, t(window).scrollTop()), t(window).on("scroll." + o, function () {
                    var i = t(window).scrollTop() + e.offset;
                    n(r, i)
                })
            }))
        }
    }(jQuery), function (t) {
        t(document).ready(function () {
            t.fn.reverse = [].reverse, t(document).on("mouseenter.fixedActionBtn", ".fixed-action-btn:not(.click-to-toggle):not(.toolbar)", function (i) {
                var n = t(this);
                e(n)
            }), t(document).on("mouseleave.fixedActionBtn", ".fixed-action-btn:not(.click-to-toggle):not(.toolbar)", function (e) {
                var n = t(this);
                i(n)
            }), t(document).on("click.fabClickToggle", ".fixed-action-btn.click-to-toggle > a", function (n) {
                var o = t(this).parent();
                o.hasClass("active") ? i(o) : e(o)
            }), t(document).on("click.fabToolbar", ".fixed-action-btn.toolbar > a", function (e) {
                var i = t(this).parent();
                n(i)
            })
        }), t.fn.extend({
            openFAB: function () {
                e(t(this))
            }, closeFAB: function () {
                i(t(this))
            }, openToolbar: function () {
                n(t(this))
            }, closeToolbar: function () {
                o(t(this))
            }
        });
        var e = function (e) {
            var i = e;
            if (!1 === i.hasClass("active")) {
                var n, o;
                !0 === i.hasClass("horizontal") ? o = 40 : n = 40, i.addClass("active"), i.find("ul .btn-floating").velocity({
                    scaleY: ".4",
                    scaleX: ".4",
                    translateY: n + "px",
                    translateX: o + "px"
                }, {duration: 0});
                var r = 0;
                i.find("ul .btn-floating").reverse().each(function () {
                    t(this).velocity({
                        opacity: "1",
                        scaleX: "1",
                        scaleY: "1",
                        translateY: "0",
                        translateX: "0"
                    }, {duration: 80, delay: r}), r += 40
                })
            }
        }, i = function (t) {
            var e, i, n = t;
            !0 === n.hasClass("horizontal") ? i = 40 : e = 40, n.removeClass("active");
            n.find("ul .btn-floating").velocity("stop", !0), n.find("ul .btn-floating").velocity({
                opacity: "0",
                scaleX: ".4",
                scaleY: ".4",
                translateY: e + "px",
                translateX: i + "px"
            }, {duration: 80})
        }, n = function (e) {
            if ("true" !== e.attr("data-open")) {
                var i, n, r, s = window.innerWidth, a = window.innerHeight, l = e[0].getBoundingClientRect(),
                    c = e.find("> a").first(), u = e.find("> ul").first(), h = t('<div class="fab-backdrop"></div>'),
                    d = c.css("background-color");
                c.append(h), i = l.left - s / 2 + l.width / 2, n = a - l.bottom, r = s / h.width(), e.attr("data-origin-bottom", l.bottom), e.attr("data-origin-left", l.left), e.attr("data-origin-width", l.width), e.addClass("active"), e.attr("data-open", !0), e.css({
                    "text-align": "center",
                    width: "100%",
                    bottom: 0,
                    left: 0,
                    transform: "translateX(" + i + "px)",
                    transition: "none"
                }), c.css({
                    transform: "translateY(" + -n + "px)",
                    transition: "none"
                }), h.css({"background-color": d}), setTimeout(function () {
                    e.css({
                        transform: "",
                        transition: "transform .2s cubic-bezier(0.550, 0.085, 0.680, 0.530), background-color 0s linear .2s"
                    }), c.css({
                        overflow: "visible",
                        transform: "",
                        transition: "transform .2s"
                    }), setTimeout(function () {
                        e.css({overflow: "hidden", "background-color": d}), h.css({
                            transform: "scale(" + r + ")",
                            transition: "transform .2s cubic-bezier(0.550, 0.055, 0.675, 0.190)"
                        }), u.find("> li > a").css({opacity: 1}), t(window).on("scroll.fabToolbarClose", function () {
                            o(e), t(window).off("scroll.fabToolbarClose"), t(document).off("click.fabToolbarClose")
                        }), t(document).on("click.fabToolbarClose", function (i) {
                            t(i.target).closest(u).length || (o(e), t(window).off("scroll.fabToolbarClose"), t(document).off("click.fabToolbarClose"))
                        })
                    }, 100)
                }, 0)
            }
        }, o = function (t) {
            if ("true" === t.attr("data-open")) {
                var e, i, n = window.innerWidth, o = window.innerHeight, r = t.attr("data-origin-width"),
                    s = t.attr("data-origin-bottom"), a = t.attr("data-origin-left"),
                    l = t.find("> .btn-floating").first(), c = t.find("> ul").first(), u = t.find(".fab-backdrop"),
                    h = l.css("background-color");
                e = a - n / 2 + r / 2, i = o - s, n / u.width(), t.removeClass("active"), t.attr("data-open", !1), t.css({
                    "background-color": "transparent",
                    transition: "none"
                }), l.css({transition: "none"}), u.css({
                    transform: "scale(0)",
                    "background-color": h
                }), c.find("> li > a").css({opacity: ""}), setTimeout(function () {
                    u.remove(), t.css({
                        "text-align": "",
                        width: "",
                        bottom: "",
                        left: "",
                        overflow: "",
                        "background-color": "",
                        transform: "translate3d(" + -e + "px,0,0)"
                    }), l.css({overflow: "", transform: "translate3d(0," + i + "px,0)"}), setTimeout(function () {
                        t.css({
                            transform: "translate3d(0,0,0)",
                            transition: "transform .2s"
                        }), l.css({
                            transform: "translate3d(0,0,0)",
                            transition: "transform .2s cubic-bezier(0.550, 0.055, 0.675, 0.190)"
                        })
                    }, 20)
                }, 200)
            }
        }
    }(jQuery), function (t) {
        Materialize.fadeInImage = function (e) {
            var i;
            if ("string" == typeof e) i = t(e); else {
                if ("object" != typeof e) return;
                i = e
            }
            i.css({opacity: 0}), t(i).velocity({opacity: 1}, {
                duration: 650,
                queue: !1,
                easing: "easeOutSine"
            }), t(i).velocity({opacity: 1}, {
                duration: 1300, queue: !1, easing: "swing", step: function (e, i) {
                    i.start = 100;
                    var n = e / 100, o = 150 - (100 - e) / 1.75;
                    o < 100 && (o = 100), e >= 0 && t(this).css({
                        "-webkit-filter": "grayscale(" + n + ")brightness(" + o + "%)",
                        filter: "grayscale(" + n + ")brightness(" + o + "%)"
                    })
                }
            })
        }, Materialize.showStaggeredList = function (e) {
            var i;
            if ("string" == typeof e) i = t(e); else {
                if ("object" != typeof e) return;
                i = e
            }
            var n = 0;
            i.find("li").velocity({translateX: "-100px"}, {duration: 0}), i.find("li").each(function () {
                t(this).velocity({opacity: "1", translateX: "0"}, {duration: 800, delay: n, easing: [60, 10]}), n += 120
            })
        }, t(document).ready(function () {
            var e = !1, i = !1;
            t(".dismissable").each(function () {
                t(this).hammer({prevent_default: !1}).on("pan", function (n) {
                    if ("touch" === n.gesture.pointerType) {
                        var o = t(this), r = n.gesture.direction, s = n.gesture.deltaX, a = n.gesture.velocityX;
                        o.velocity({translateX: s}, {
                            duration: 50,
                            queue: !1,
                            easing: "easeOutQuad"
                        }), 4 === r && (s > o.innerWidth() / 2 || a < -.75) && (e = !0), 2 === r && (s < -1 * o.innerWidth() / 2 || a > .75) && (i = !0)
                    }
                }).on("panend", function (n) {
                    if (Math.abs(n.gesture.deltaX) < t(this).innerWidth() / 2 && (i = !1, e = !1), "touch" === n.gesture.pointerType) {
                        var o = t(this);
                        if (e || i) {
                            var r;
                            r = e ? o.innerWidth() : -1 * o.innerWidth(), o.velocity({translateX: r}, {
                                duration: 100,
                                queue: !1,
                                easing: "easeOutQuad",
                                complete: function () {
                                    o.css("border", "none"), o.velocity({height: 0, padding: 0}, {
                                        duration: 200,
                                        queue: !1,
                                        easing: "easeOutQuad",
                                        complete: function () {
                                            o.remove()
                                        }
                                    })
                                }
                            })
                        } else o.velocity({translateX: 0}, {duration: 100, queue: !1, easing: "easeOutQuad"});
                        e = !1, i = !1
                    }
                })
            })
        })
    }(jQuery), function (t) {
        var e = !1;
        Materialize.scrollFire = function (t) {
            var i = Materialize.throttle(function () {
                !function () {
                    for (var e = window.pageYOffset + window.innerHeight, i = 0; i < t.length; i++) {
                        var n = t[i], o = n.selector, r = n.offset, s = n.callback, a = document.querySelector(o);
                        null !== a && e > a.getBoundingClientRect().top + window.pageYOffset + r && !0 !== n.done && ("function" == typeof s ? s.call(this, a) : "string" == typeof s && new Function(s)(a), n.done = !0)
                    }
                }()
            }, t.throttle || 100);
            e || (window.addEventListener("scroll", i), window.addEventListener("resize", i), e = !0), setTimeout(i, 0)
        }
    }(jQuery), Materialize.Picker = function (t) {
        function e(o, s, c, u) {
            function h() {
                return e._.node("div", e._.node("div", e._.node("div", e._.node("div", w.component.nodes(g.open), m.box), m.wrap), m.frame), m.holder)
            }

            function d(t) {
                var e = t.keyCode, i = /^(8|46)$/.test(e);
                if (27 == e) return w.close(), !1;
                (32 == e || i || !g.open && w.component.key[e]) && (t.preventDefault(), t.stopPropagation(), i ? w.clear().close() : w.open())
            }

            function p(t) {
                t.stopPropagation(), "focus" == t.type && w.$root.addClass(m.focused), w.open()
            }

            if (!o) return e;
            var f = !1, g = {id: o.id || "P" + Math.abs(~~(Math.random() * new Date))},
                v = c ? t.extend(!0, {}, c.defaults, u) : u || {}, m = t.extend({}, e.klasses(), v.klass), y = t(o),
                b = function () {
                    return this.start()
                }, w = b.prototype = {
                    constructor: b, $node: y, start: function () {
                        return g && g.start ? w : (g.methods = {}, g.start = !0, g.open = !1, g.type = o.type, o.autofocus = o == r(), o.readOnly = !v.editable, o.id = o.id || g.id, "text" != o.type && (o.type = "text"), w.component = new c(w, v), w.$root = t(e._.node("div", h(), m.picker, 'id="' + o.id + '_root" tabindex="0"')), w.$root.on({
                            keydown: d,
                            focusin: function (t) {
                                w.$root.removeClass(m.focused), t.stopPropagation()
                            },
                            "mousedown click": function (e) {
                                var i = e.target;
                                i != w.$root.children()[0] && (e.stopPropagation(), "mousedown" != e.type || t(i).is("input, select, textarea, button, option") || (e.preventDefault(), w.$root.eq(0).focus()))
                            }
                        }).on({
                            focus: function () {
                                y.addClass(m.target)
                            }, blur: function () {
                                y.removeClass(m.target)
                            }
                        }).on("focus.toOpen", p).on("click", "[data-pick], [data-nav], [data-clear], [data-close]", function () {
                            var e = t(this), i = e.data(), n = e.hasClass(m.navDisabled) || e.hasClass(m.disabled), o = r();
                            o = o && (o.type || o.href) && o, (n || o && !t.contains(w.$root[0], o)) && w.$root.eq(0).focus(), !n && i.nav ? w.set("highlight", w.component.item.highlight, {nav: i.nav}) : !n && "pick" in i ? (w.set("select", i.pick), v.closeOnSelect && w.close(!0)) : i.clear ? (w.clear(), v.closeOnSelect && w.close(!0)) : i.close && w.close(!0)
                        }), n(w.$root[0], "hidden", !0), v.formatSubmit && function () {
                            var e;
                            !0 === v.hiddenName ? (e = o.name, o.name = "") : (e = ["string" == typeof v.hiddenPrefix ? v.hiddenPrefix : "", "string" == typeof v.hiddenSuffix ? v.hiddenSuffix : "_submit"], e = e[0] + o.name + e[1]);
                            w._hidden = t('<input type=hidden name="' + e + '"' + (y.data("value") || o.value ? ' value="' + w.get("select", v.formatSubmit) + '"' : "") + ">")[0], y.on("change." + g.id, function () {
                                w._hidden.value = o.value ? w.get("select", v.formatSubmit) : ""
                            }), v.container ? t(v.container).append(w._hidden) : y.before(w._hidden)
                        }(), function () {
                            y.data(s, w).addClass(m.input).attr("tabindex", -1).val(y.data("value") ? w.get("select", v.format) : o.value), v.editable || y.on("focus." + g.id + " click." + g.id, function (t) {
                                t.preventDefault(), w.$root.eq(0).focus()
                            }).on("keydown." + g.id, d);
                            n(o, {haspopup: !0, expanded: !1, readonly: !1, owns: o.id + "_root"})
                        }(), v.container ? t(v.container).append(w.$root) : y.before(w.$root), w.on({
                            start: w.component.onStart,
                            render: w.component.onRender,
                            stop: w.component.onStop,
                            open: w.component.onOpen,
                            close: w.component.onClose,
                            set: w.component.onSet
                        }).on({
                            start: v.onStart,
                            render: v.onRender,
                            stop: v.onStop,
                            open: v.onOpen,
                            close: v.onClose,
                            set: v.onSet
                        }), f = function (t) {
                            var e;
                            t.currentStyle ? e = t.currentStyle.position : window.getComputedStyle && (e = getComputedStyle(t).position);
                            return "fixed" == e
                        }(w.$root.children()[0]), o.autofocus && w.open(), w.trigger("start").trigger("render"))
                    }, render: function (t) {
                        return t ? w.$root.html(h()) : w.$root.find("." + m.box).html(w.component.nodes(g.open)), w.trigger("render")
                    }, stop: function () {
                        return g.start ? (w.close(), w._hidden && w._hidden.parentNode.removeChild(w._hidden), w.$root.remove(), y.removeClass(m.input).removeData(s), setTimeout(function () {
                            y.off("." + g.id)
                        }, 0), o.type = g.type, o.readOnly = !1, w.trigger("stop"), g.methods = {}, g.start = !1, w) : w
                    }, open: function (r) {
                        return g.open ? w : (y.addClass(m.active), n(o, "expanded", !0), setTimeout(function () {
                            w.$root.addClass(m.opened), n(w.$root[0], "hidden", !1)
                        }, 0), !1 !== r && (g.open = !0, f && l.css("overflow", "hidden").css("padding-right", "+=" + i()), w.$root.eq(0).focus(), a.on("click." + g.id + " focusin." + g.id, function (t) {
                            var e = t.target;
                            e != o && e != document && 3 != t.which && w.close(e === w.$root.children()[0])
                        }).on("keydown." + g.id, function (i) {
                            var n = i.keyCode, o = w.component.key[n], r = i.target;
                            27 == n ? w.close(!0) : r != w.$root[0] || !o && 13 != n ? t.contains(w.$root[0], r) && 13 == n && (i.preventDefault(), r.click()) : (i.preventDefault(), o ? e._.trigger(w.component.key.go, w, [e._.trigger(o)]) : w.$root.find("." + m.highlighted).hasClass(m.disabled) || (w.set("select", w.component.item.highlight), v.closeOnSelect && w.close(!0)))
                        })), w.trigger("open"))
                    }, close: function (t) {
                        return t && (w.$root.off("focus.toOpen").eq(0).focus(), setTimeout(function () {
                            w.$root.on("focus.toOpen", p)
                        }, 0)), y.removeClass(m.active), n(o, "expanded", !1), setTimeout(function () {
                            w.$root.removeClass(m.opened + " " + m.focused), n(w.$root[0], "hidden", !0)
                        }, 0), g.open ? (g.open = !1, f && l.css("overflow", "").css("padding-right", "-=" + i()), a.off("." + g.id), w.trigger("close")) : w
                    }, clear: function (t) {
                        return w.set("clear", null, t)
                    }, set: function (e, i, n) {
                        var o, r, s = t.isPlainObject(e), a = s ? e : {};
                        if (n = s && t.isPlainObject(i) ? i : n || {}, e) {
                            s || (a[e] = i);
                            for (o in a) r = a[o], o in w.component.item && (void 0 === r && (r = null), w.component.set(o, r, n)), "select" != o && "clear" != o || y.val("clear" == o ? "" : w.get(o, v.format)).trigger("change");
                            w.render()
                        }
                        return n.muted ? w : w.trigger("set", a)
                    }, get: function (t, i) {
                        if (t = t || "value", null != g[t]) return g[t];
                        if ("valueSubmit" == t) {
                            if (w._hidden) return w._hidden.value;
                            t = "value"
                        }
                        if ("value" == t) return o.value;
                        if (t in w.component.item) {
                            if ("string" == typeof i) {
                                var n = w.component.get(t);
                                return n ? e._.trigger(w.component.formats.toString, w.component, [i, n]) : ""
                            }
                            return w.component.get(t)
                        }
                    }, on: function (e, i, n) {
                        var o, r, s = t.isPlainObject(e), a = s ? e : {};
                        if (e) {
                            s || (a[e] = i);
                            for (o in a) r = a[o], n && (o = "_" + o), g.methods[o] = g.methods[o] || [], g.methods[o].push(r)
                        }
                        return w
                    }, off: function () {
                        var t, e, i = arguments;
                        for (t = 0, namesCount = i.length; t < namesCount; t += 1) (e = i[t]) in g.methods && delete g.methods[e];
                        return w
                    }, trigger: function (t, i) {
                        var n = function (t) {
                            var n = g.methods[t];
                            n && n.map(function (t) {
                                e._.trigger(t, w, [i])
                            })
                        };
                        return n("_" + t), n(t), w
                    }
                };
            return new b
        }

        function i() {
            if (l.height() <= s.height()) return 0;
            var e = t('<div style="visibility:hidden;width:100px" />').appendTo("body"), i = e[0].offsetWidth;
            e.css("overflow", "scroll");
            var n = t('<div style="width:100%" />').appendTo(e)[0].offsetWidth;
            return e.remove(), i - n
        }

        function n(e, i, n) {
            if (t.isPlainObject(i)) for (var r in i) o(e, r, i[r]); else o(e, i, n)
        }

        function o(t, e, i) {
            t.setAttribute(("role" == e ? "" : "aria-") + e, i)
        }

        function r() {
            try {
                return document.activeElement
            } catch (t) {
            }
        }

        var s = t(window), a = t(document), l = t(document.documentElement);
        e.klasses = function (t) {
            return t = t || "picker", {
                picker: t,
                opened: t + "--opened",
                focused: t + "--focused",
                input: t + "__input",
                active: t + "__input--active",
                target: t + "__input--target",
                holder: t + "__holder",
                frame: t + "__frame",
                wrap: t + "__wrap",
                box: t + "__box"
            }
        };
        e._ = {
            group: function (t) {
                for (var i, n = "", o = e._.trigger(t.min, t); o <= e._.trigger(t.max, t, [o]); o += t.i) i = e._.trigger(t.item, t, [o]), n += e._.node(t.node, i[0], i[1], i[2]);
                return n
            }, node: function (e, i, n, o) {
                return i ? (i = t.isArray(i) ? i.join("") : i, n = n ? ' class="' + n + '"' : "", o = o ? " " + o : "", "<" + e + n + o + ">" + i + "</" + e + ">") : ""
            }, lead: function (t) {
                return (t < 10 ? "0" : "") + t
            }, trigger: function (t, e, i) {
                return "function" == typeof t ? t.apply(e, i || []) : t
            }, digits: function (t) {
                return /\d/.test(t[1]) ? 2 : 1
            }, isDate: function (t) {
                return {}.toString.call(t).indexOf("Date") > -1 && this.isInteger(t.getDate())
            }, isInteger: function (t) {
                return {}.toString.call(t).indexOf("Number") > -1 && t % 1 == 0
            }, ariaAttr: function (e, i) {
                t.isPlainObject(e) || (e = {attribute: i});
                i = "";
                for (var n in e) {
                    var o = ("role" == n ? "" : "aria-") + n, r = e[n];
                    i += null == r ? "" : o + '="' + e[n] + '"'
                }
                return i
            }
        }, e.extend = function (i, n) {
            t.fn[i] = function (o, r) {
                var s = this.data(i);
                return "picker" == o ? s : s && "string" == typeof o ? e._.trigger(s[o], s, [r]) : this.each(function () {
                    t(this).data(i) || new e(this, i, n, o)
                })
            }, t.fn[i].defaults = n.defaults
        };
        return e
    }(jQuery), function (t, e) {
        function i(t, e) {
            var i = this, n = t.$node[0], o = n.value, r = t.$node.data("value"), s = r || o,
                a = r ? e.formatSubmit : e.format, l = function () {
                    return n.currentStyle ? "rtl" == n.currentStyle.direction : "rtl" == getComputedStyle(t.$root[0]).direction
                };
            i.settings = e, i.$node = t.$node, i.queue = {
                min: "measure create",
                max: "measure create",
                now: "now create",
                select: "parse create validate",
                highlight: "parse navigate create validate",
                view: "parse create validate viewset",
                disable: "deactivate",
                enable: "activate"
            }, i.item = {}, i.item.clear = null, i.item.disable = (e.disable || []).slice(0), i.item.enable = -function (t) {
                return !0 === t[0] ? t.shift() : -1
            }(i.item.disable), i.set("min", e.min).set("max", e.max).set("now"), s ? i.set("select", s, {format: a}) : i.set("select", null).set("highlight", i.item.now), i.key = {
                40: 7,
                38: -7,
                39: function () {
                    return l() ? -1 : 1
                },
                37: function () {
                    return l() ? 1 : -1
                },
                go: function (t) {
                    var e = i.item.highlight, n = new Date(e.year, e.month, e.date + t);
                    i.set("highlight", n, {interval: t}), this.render()
                }
            }, t.on("render", function () {
                t.$root.find("." + e.klass.selectMonth).on("change", function () {
                    var i = this.value;
                    i && (t.set("highlight", [t.get("view").year, i, t.get("highlight").date]), t.$root.find("." + e.klass.selectMonth).trigger("focus"))
                }), t.$root.find("." + e.klass.selectYear).on("change", function () {
                    var i = this.value;
                    i && (t.set("highlight", [i, t.get("view").month, t.get("highlight").date]), t.$root.find("." + e.klass.selectYear).trigger("focus"))
                })
            }, 1).on("open", function () {
                var n = "";
                i.disabled(i.get("now")) && (n = ":not(." + e.klass.buttonToday + ")"), t.$root.find("button" + n + ", select").attr("disabled", !1)
            }, 1).on("close", function () {
                t.$root.find("button, select").attr("disabled", !0)
            }, 1)
        }

        var n = t._;
        i.prototype.set = function (t, e, i) {
            var n = this, o = n.item;
            return null === e ? ("clear" == t && (t = "select"), o[t] = e, n) : (o["enable" == t ? "disable" : "flip" == t ? "enable" : t] = n.queue[t].split(" ").map(function (o) {
                return e = n[o](t, e, i)
            }).pop(), "select" == t ? n.set("highlight", o.select, i) : "highlight" == t ? n.set("view", o.highlight, i) : t.match(/^(flip|min|max|disable|enable)$/) && (o.select && n.disabled(o.select) && n.set("select", o.select, i), o.highlight && n.disabled(o.highlight) && n.set("highlight", o.highlight, i)), n)
        }, i.prototype.get = function (t) {
            return this.item[t]
        }, i.prototype.create = function (t, i, o) {
            var r;
            return (i = void 0 === i ? t : i) == -1 / 0 || i == 1 / 0 ? r = i : e.isPlainObject(i) && n.isInteger(i.pick) ? i = i.obj : e.isArray(i) ? (i = new Date(i[0], i[1], i[2]), i = n.isDate(i) ? i : this.create().obj) : i = n.isInteger(i) || n.isDate(i) ? this.normalize(new Date(i), o) : this.now(t, i, o), {
                year: r || i.getFullYear(),
                month: r || i.getMonth(),
                date: r || i.getDate(),
                day: r || i.getDay(),
                obj: r || i,
                pick: r || i.getTime()
            }
        }, i.prototype.createRange = function (t, i) {
            var o = this, r = function (t) {
                return !0 === t || e.isArray(t) || n.isDate(t) ? o.create(t) : t
            };
            return n.isInteger(t) || (t = r(t)), n.isInteger(i) || (i = r(i)), n.isInteger(t) && e.isPlainObject(i) ? t = [i.year, i.month, i.date + t] : n.isInteger(i) && e.isPlainObject(t) && (i = [t.year, t.month, t.date + i]), {
                from: r(t),
                to: r(i)
            }
        }, i.prototype.withinRange = function (t, e) {
            return t = this.createRange(t.from, t.to), e.pick >= t.from.pick && e.pick <= t.to.pick
        }, i.prototype.overlapRanges = function (t, e) {
            return t = this.createRange(t.from, t.to), e = this.createRange(e.from, e.to), this.withinRange(t, e.from) || this.withinRange(t, e.to) || this.withinRange(e, t.from) || this.withinRange(e, t.to)
        }, i.prototype.now = function (t, e, i) {
            return e = new Date, i && i.rel && e.setDate(e.getDate() + i.rel), this.normalize(e, i)
        }, i.prototype.navigate = function (t, i, n) {
            var o, r, s, a, l = e.isArray(i), c = e.isPlainObject(i), u = this.item.view;
            if (l || c) {
                for (c ? (r = i.year, s = i.month, a = i.date) : (r = +i[0], s = +i[1], a = +i[2]), n && n.nav && u && u.month !== s && (r = u.year, s = u.month), o = new Date(r, s + (n && n.nav ? n.nav : 0), 1), r = o.getFullYear(), s = o.getMonth(); new Date(r, s, a).getMonth() !== s;) a -= 1;
                i = [r, s, a]
            }
            return i
        }, i.prototype.normalize = function (t) {
            return t.setHours(0, 0, 0, 0), t
        }, i.prototype.measure = function (t, e) {
            return e ? "string" == typeof e ? e = this.parse(t, e) : n.isInteger(e) && (e = this.now(t, e, {rel: e})) : e = "min" == t ? -1 / 0 : 1 / 0, e
        }, i.prototype.viewset = function (t, e) {
            return this.create([e.year, e.month, 1])
        }, i.prototype.validate = function (t, i, o) {
            var r, s, a, l, c = this, u = i, h = o && o.interval ? o.interval : 1, d = -1 === c.item.enable,
                p = c.item.min, f = c.item.max, g = d && c.item.disable.filter(function (t) {
                    if (e.isArray(t)) {
                        var o = c.create(t).pick;
                        o < i.pick ? r = !0 : o > i.pick && (s = !0)
                    }
                    return n.isInteger(t)
                }).length;
            if ((!o || !o.nav) && (!d && c.disabled(i) || d && c.disabled(i) && (g || r || s) || !d && (i.pick <= p.pick || i.pick >= f.pick))) for (d && !g && (!s && h > 0 || !r && h < 0) && (h *= -1); c.disabled(i) && (Math.abs(h) > 1 && (i.month < u.month || i.month > u.month) && (i = u, h = h > 0 ? 1 : -1), i.pick <= p.pick ? (a = !0, h = 1, i = c.create([p.year, p.month, p.date + (i.pick === p.pick ? 0 : -1)])) : i.pick >= f.pick && (l = !0, h = -1, i = c.create([f.year, f.month, f.date + (i.pick === f.pick ? 0 : 1)])), !a || !l);) i = c.create([i.year, i.month, i.date + h]);
            return i
        }, i.prototype.disabled = function (t) {
            var i = this, o = i.item.disable.filter(function (o) {
                return n.isInteger(o) ? t.day === (i.settings.firstDay ? o : o - 1) % 7 : e.isArray(o) || n.isDate(o) ? t.pick === i.create(o).pick : e.isPlainObject(o) ? i.withinRange(o, t) : void 0
            });
            return o = o.length && !o.filter(function (t) {
                return e.isArray(t) && "inverted" == t[3] || e.isPlainObject(t) && t.inverted
            }).length, -1 === i.item.enable ? !o : o || t.pick < i.item.min.pick || t.pick > i.item.max.pick
        }, i.prototype.parse = function (t, e, i) {
            var o = this, r = {};
            return e && "string" == typeof e ? (i && i.format || ((i = i || {}).format = o.settings.format), o.formats.toArray(i.format).map(function (t) {
                var i = o.formats[t], s = i ? n.trigger(i, o, [e, r]) : t.replace(/^!/, "").length;
                i && (r[t] = e.substr(0, s)), e = e.substr(s)
            }), [r.yyyy || r.yy, +(r.mm || r.m) - 1, r.dd || r.d]) : e
        }, i.prototype.formats = function () {
            function t(t, e, i) {
                var n = t.match(/\w+/)[0];
                return i.mm || i.m || (i.m = e.indexOf(n) + 1), n.length
            }

            function e(t) {
                return t.match(/\w+/)[0].length
            }

            return {
                d: function (t, e) {
                    return t ? n.digits(t) : e.date
                }, dd: function (t, e) {
                    return t ? 2 : n.lead(e.date)
                }, ddd: function (t, i) {
                    return t ? e(t) : this.settings.weekdaysShort[i.day]
                }, dddd: function (t, i) {
                    return t ? e(t) : this.settings.weekdaysFull[i.day]
                }, m: function (t, e) {
                    return t ? n.digits(t) : e.month + 1
                }, mm: function (t, e) {
                    return t ? 2 : n.lead(e.month + 1)
                }, mmm: function (e, i) {
                    var n = this.settings.monthsShort;
                    return e ? t(e, n, i) : n[i.month]
                }, mmmm: function (e, i) {
                    var n = this.settings.monthsFull;
                    return e ? t(e, n, i) : n[i.month]
                }, yy: function (t, e) {
                    return t ? 2 : ("" + e.year).slice(2)
                }, yyyy: function (t, e) {
                    return t ? 4 : e.year
                }, toArray: function (t) {
                    return t.split(/(d{1,4}|m{1,4}|y{4}|yy|!.)/g)
                }, toString: function (t, e) {
                    var i = this;
                    return i.formats.toArray(t).map(function (t) {
                        return n.trigger(i.formats[t], i, [0, e]) || t.replace(/^!/, "")
                    }).join("")
                }
            }
        }(), i.prototype.isDateExact = function (t, i) {
            return n.isInteger(t) && n.isInteger(i) || "boolean" == typeof t && "boolean" == typeof i ? t === i : (n.isDate(t) || e.isArray(t)) && (n.isDate(i) || e.isArray(i)) ? this.create(t).pick === this.create(i).pick : !(!e.isPlainObject(t) || !e.isPlainObject(i)) && (this.isDateExact(t.from, i.from) && this.isDateExact(t.to, i.to))
        }, i.prototype.isDateOverlap = function (t, i) {
            var o = this.settings.firstDay ? 1 : 0;
            return n.isInteger(t) && (n.isDate(i) || e.isArray(i)) ? (t = t % 7 + o) === this.create(i).day + 1 : n.isInteger(i) && (n.isDate(t) || e.isArray(t)) ? (i = i % 7 + o) === this.create(t).day + 1 : !(!e.isPlainObject(t) || !e.isPlainObject(i)) && this.overlapRanges(t, i)
        }, i.prototype.flipEnable = function (t) {
            var e = this.item;
            e.enable = t || (-1 == e.enable ? 1 : -1)
        }, i.prototype.deactivate = function (t, i) {
            var o = this, r = o.item.disable.slice(0);
            return "flip" == i ? o.flipEnable() : !1 === i ? (o.flipEnable(1), r = []) : !0 === i ? (o.flipEnable(-1), r = []) : i.map(function (t) {
                for (var i, s = 0; s < r.length; s += 1) if (o.isDateExact(t, r[s])) {
                    i = !0;
                    break
                }
                i || (n.isInteger(t) || n.isDate(t) || e.isArray(t) || e.isPlainObject(t) && t.from && t.to) && r.push(t)
            }), r
        }, i.prototype.activate = function (t, i) {
            var o = this, r = o.item.disable, s = r.length;
            return "flip" == i ? o.flipEnable() : !0 === i ? (o.flipEnable(1), r = []) : !1 === i ? (o.flipEnable(-1), r = []) : i.map(function (t) {
                var i, a, l, c;
                for (l = 0; l < s; l += 1) {
                    if (a = r[l], o.isDateExact(a, t)) {
                        i = r[l] = null, c = !0;
                        break
                    }
                    if (o.isDateOverlap(a, t)) {
                        e.isPlainObject(t) ? (t.inverted = !0, i = t) : e.isArray(t) ? (i = t)[3] || i.push("inverted") : n.isDate(t) && (i = [t.getFullYear(), t.getMonth(), t.getDate(), "inverted"]);
                        break
                    }
                }
                if (i) for (l = 0; l < s; l += 1) if (o.isDateExact(r[l], t)) {
                    r[l] = null;
                    break
                }
                if (c) for (l = 0; l < s; l += 1) if (o.isDateOverlap(r[l], t)) {
                    r[l] = null;
                    break
                }
                i && r.push(i)
            }), r.filter(function (t) {
                return null != t
            })
        }, i.prototype.nodes = function (t) {
            var e = this, i = e.settings, o = e.item, r = o.now, s = o.select, a = o.highlight, l = o.view,
                c = o.disable, u = o.min, h = o.max, d = function (t, e) {
                    return i.firstDay && (t.push(t.shift()), e.push(e.shift())), n.node("thead", n.node("tr", n.group({
                        min: 0,
                        max: 6,
                        i: 1,
                        node: "th",
                        item: function (n) {
                            return [t[n], i.klass.weekdays, 'scope=col title="' + e[n] + '"']
                        }
                    })))
                }((i.showWeekdaysFull ? i.weekdaysFull : i.weekdaysLetter).slice(0), i.weekdaysFull.slice(0)),
                p = function (t) {
                    return n.node("div", " ", i.klass["nav" + (t ? "Next" : "Prev")] + (t && l.year >= h.year && l.month >= h.month || !t && l.year <= u.year && l.month <= u.month ? " " + i.klass.navDisabled : ""), "data-nav=" + (t || -1) + " " + n.ariaAttr({
                        role: "button",
                        controls: e.$node[0].id + "_table"
                    }) + ' title="' + (t ? i.labelMonthNext : i.labelMonthPrev) + '"')
                }, f = function (o) {
                    var r = i.showMonthsShort ? i.monthsShort : i.monthsFull;
                    return "short_months" == o && (r = i.monthsShort), i.selectMonths && void 0 == o ? n.node("select", n.group({
                        min: 0,
                        max: 11,
                        i: 1,
                        node: "option",
                        item: function (t) {
                            return [r[t], 0, "value=" + t + (l.month == t ? " selected" : "") + (l.year == u.year && t < u.month || l.year == h.year && t > h.month ? " disabled" : "")]
                        }
                    }), i.klass.selectMonth + " browser-default", (t ? "" : "disabled") + " " + n.ariaAttr({controls: e.$node[0].id + "_table"}) + ' title="' + i.labelMonthSelect + '"') : "short_months" == o ? null != s ? r[s.month] : r[l.month] : n.node("div", r[l.month], i.klass.month)
                }, g = function (o) {
                    var r = l.year, a = !0 === i.selectYears ? 5 : ~~(i.selectYears / 2);
                    if (a) {
                        var c = u.year, d = h.year, p = r - a, f = r + a;
                        if (c > p && (f += c - p, p = c), d < f) {
                            var g = p - c, v = f - d;
                            p -= g > v ? v : g, f = d
                        }
                        if (i.selectYears && void 0 == o) return n.node("select", n.group({
                            min: p,
                            max: f,
                            i: 1,
                            node: "option",
                            item: function (t) {
                                return [t, 0, "value=" + t + (r == t ? " selected" : "")]
                            }
                        }), i.klass.selectYear + " browser-default", (t ? "" : "disabled") + " " + n.ariaAttr({controls: e.$node[0].id + "_table"}) + ' title="' + i.labelYearSelect + '"')
                    }
                    return "raw" === o && null != s ? n.node("div", s.year) : n.node("div", r, i.klass.year)
                };
            return createDayLabel = function () {
                return null != s ? s.date : r.date
            }, createWeekdayLabel = function () {
                var t;
                t = null != s ? s.day : r.day;
                return i.weekdaysShort[t]
            }, n.node("div", n.node("div", g("raw"), i.klass.year_display) + n.node("span", createWeekdayLabel() + ", ", "picker__weekday-display") + n.node("span", f("short_months") + " ", i.klass.month_display) + n.node("span", createDayLabel(), i.klass.day_display), i.klass.date_display) + n.node("div", n.node("div", n.node("div", (i.selectYears, f() + g() + p() + p(1)), i.klass.header) + n.node("table", d + n.node("tbody", n.group({
                min: 0,
                max: 5,
                i: 1,
                node: "tr",
                item: function (t) {
                    var o = i.firstDay && 0 === e.create([l.year, l.month, 1]).day ? -7 : 0;
                    return [n.group({
                        min: 7 * t - l.day + o + 1, max: function () {
                            return this.min + 7 - 1
                        }, i: 1, node: "td", item: function (t) {
                            t = e.create([l.year, l.month, t + (i.firstDay ? 1 : 0)]);
                            var o = s && s.pick == t.pick, d = a && a.pick == t.pick,
                                p = c && e.disabled(t) || t.pick < u.pick || t.pick > h.pick,
                                f = n.trigger(e.formats.toString, e, [i.format, t]);
                            return [n.node("div", t.date, function (e) {
                                return e.push(l.month == t.month ? i.klass.infocus : i.klass.outfocus), r.pick == t.pick && e.push(i.klass.now), o && e.push(i.klass.selected), d && e.push(i.klass.highlighted), p && e.push(i.klass.disabled), e.join(" ")
                            }([i.klass.day]), "data-pick=" + t.pick + " " + n.ariaAttr({
                                role: "gridcell",
                                label: f,
                                selected: !(!o || e.$node.val() !== f) || null,
                                activedescendant: !!d || null,
                                disabled: !!p || null
                            }) + " " + (p ? "" : 'tabindex="0"')), "", n.ariaAttr({role: "presentation"})]
                        }
                    })]
                }
            })), i.klass.table, 'id="' + e.$node[0].id + '_table" ' + n.ariaAttr({
                role: "grid",
                controls: e.$node[0].id,
                readonly: !0
            })), i.klass.calendar_container) + n.node("div", n.node("button", i.today, "btn-flat picker__today waves-effect", "type=button data-pick=" + r.pick + (t && !e.disabled(r) ? "" : " disabled") + " " + n.ariaAttr({controls: e.$node[0].id})) + n.node("button", i.clear, "btn-flat picker__clear waves-effect", "type=button data-clear=1" + (t ? "" : " disabled") + " " + n.ariaAttr({controls: e.$node[0].id})) + n.node("button", i.close, "btn-flat picker__close waves-effect", "type=button data-close=true " + (t ? "" : " disabled") + " " + n.ariaAttr({controls: e.$node[0].id})), i.klass.footer), "picker__container__wrapper")
        }, i.defaults = function (t) {
            return {
                labelMonthNext: "Next month",
                labelMonthPrev: "Previous month",
                labelMonthSelect: "Select a month",
                labelYearSelect: "Select a year",
                monthsFull: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                weekdaysFull: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                weekdaysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                weekdaysLetter: ["S", "M", "T", "W", "T", "F", "S"],
                today: "Today",
                clear: "Clear",
                close: "Ok",
                closeOnSelect: !1,
                format: "d mmmm, yyyy",
                klass: {
                    table: t + "table",
                    header: t + "header",
                    date_display: t + "date-display",
                    day_display: t + "day-display",
                    month_display: t + "month-display",
                    year_display: t + "year-display",
                    calendar_container: t + "calendar-container",
                    navPrev: t + "nav--prev",
                    navNext: t + "nav--next",
                    navDisabled: t + "nav--disabled",
                    month: t + "month",
                    year: t + "year",
                    selectMonth: t + "select--month",
                    selectYear: t + "select--year",
                    weekdays: t + "weekday",
                    day: t + "day",
                    disabled: t + "day--disabled",
                    selected: t + "day--selected",
                    highlighted: t + "day--highlighted",
                    now: t + "day--today",
                    infocus: t + "day--infocus",
                    outfocus: t + "day--outfocus",
                    footer: t + "footer",
                    buttonClear: t + "button--clear",
                    buttonToday: t + "button--today",
                    buttonClose: t + "button--close"
                }
            }
        }(t.klasses().picker + "__"), t.extend("pickadate", i)
    }(Materialize.Picker, jQuery), function (t) {
        function e(t) {
            return document.createElementNS(a, t)
        }

        function i(t) {
            return (t < 10 ? "0" : "") + t
        }

        function n(n, r) {
            function a(t, e) {
                var i = u.offset(), n = /^touch/.test(t.type), o = i.left + v, a = i.top + v,
                    l = (n ? t.originalEvent.touches[0] : t).pageX - o,
                    c = (n ? t.originalEvent.touches[0] : t).pageY - a, h = Math.sqrt(l * l + c * c), f = !1;
                if (!e || !(h < m - b || h > m + b)) {
                    t.preventDefault();
                    var g = setTimeout(function () {
                        $.popover.addClass("clockpicker-moving")
                    }, 200);
                    $.setHand(l, c, !e, !0), s.off(d).on(d, function (t) {
                        t.preventDefault();
                        var e = /^touch/.test(t.type), i = (e ? t.originalEvent.touches[0] : t).pageX - o,
                            n = (e ? t.originalEvent.touches[0] : t).pageY - a;
                        (f || i !== l || n !== c) && (f = !0, $.setHand(i, n, !1, !0))
                    }), s.off(p).on(p, function (t) {
                        s.off(p), t.preventDefault();
                        var i = /^touch/.test(t.type), n = (i ? t.originalEvent.changedTouches[0] : t).pageX - o,
                            h = (i ? t.originalEvent.changedTouches[0] : t).pageY - a;
                        (e || f) && n === l && h === c && $.setHand(n, h), "hours" === $.currentView ? $.toggleView("minutes", x / 2) : r.autoclose && ($.minutesView.addClass("clockpicker-dial-out"), setTimeout(function () {
                            $.done()
                        }, x / 2)), u.prepend(N), clearTimeout(g), $.popover.removeClass("clockpicker-moving"), s.off(d)
                    })
                }
            }

            var c = t(C), u = c.find(".clockpicker-plate"), f = c.find(".picker__holder"),
                k = c.find(".clockpicker-hours"), T = c.find(".clockpicker-minutes"),
                _ = c.find(".clockpicker-am-pm-block"), S = "INPUT" === n.prop("tagName"), E = S ? n : n.find("input"),
                A = t("label[for=" + E.attr("id") + "]"), $ = this;
            this.id = function (t) {
                var e = ++g + "";
                return t ? t + e : e
            }("cp"), this.element = n, this.holder = f, this.options = r, this.isAppended = !1, this.isShown = !1, this.currentView = "hours", this.isInput = S, this.input = E, this.label = A, this.popover = c, this.plate = u, this.hoursView = k, this.minutesView = T, this.amPmBlock = _, this.spanHours = c.find(".clockpicker-span-hours"), this.spanMinutes = c.find(".clockpicker-span-minutes"), this.spanAmPm = c.find(".clockpicker-span-am-pm"), this.footer = c.find(".picker__footer"), this.amOrPm = "PM", r.twelvehour && (r.ampmclickable ? (this.spanAmPm.empty(), t('<div id="click-am">AM</div>').on("click", function () {
                $.spanAmPm.children("#click-am").addClass("text-primary"), $.spanAmPm.children("#click-pm").removeClass("text-primary"), $.amOrPm = "AM"
            }).appendTo(this.spanAmPm), t('<div id="click-pm">PM</div>').on("click", function () {
                $.spanAmPm.children("#click-pm").addClass("text-primary"), $.spanAmPm.children("#click-am").removeClass("text-primary"), $.amOrPm = "PM"
            }).appendTo(this.spanAmPm)) : (this.spanAmPm.empty(), t('<div id="click-am">AM</div>').appendTo(this.spanAmPm), t('<div id="click-pm">PM</div>').appendTo(this.spanAmPm))), t('<button type="button" class="btn-flat picker__clear" tabindex="' + (r.twelvehour ? "3" : "1") + '">' + r.cleartext + "</button>").click(t.proxy(this.clear, this)).appendTo(this.footer), t('<button type="button" class="btn-flat picker__close" tabindex="' + (r.twelvehour ? "3" : "1") + '">' + r.canceltext + "</button>").click(t.proxy(this.hide, this)).appendTo(this.footer), t('<button type="button" class="btn-flat picker__close" tabindex="' + (r.twelvehour ? "3" : "1") + '">' + r.donetext + "</button>").click(t.proxy(this.done, this)).appendTo(this.footer), this.spanHours.click(t.proxy(this.toggleView, this, "hours")), this.spanMinutes.click(t.proxy(this.toggleView, this, "minutes")), E.on("focus.clockpicker click.clockpicker", t.proxy(this.show, this));
            var P, D, O, M, I = t('<div class="clockpicker-tick"></div>');
            if (r.twelvehour) for (P = 1; P < 13; P += 1) D = I.clone(), O = P / 6 * Math.PI, M = m, D.css({
                left: v + Math.sin(O) * M - b,
                top: v - Math.cos(O) * M - b
            }), D.html(0 === P ? "00" : P), k.append(D), D.on(h, a); else for (P = 0; P < 24; P += 1) {
                D = I.clone(), O = P / 6 * Math.PI;
                M = P > 0 && P < 13 ? y : m, D.css({
                    left: v + Math.sin(O) * M - b,
                    top: v - Math.cos(O) * M - b
                }), D.html(0 === P ? "00" : P), k.append(D), D.on(h, a)
            }
            for (P = 0; P < 60; P += 5) D = I.clone(), O = P / 30 * Math.PI, D.css({
                left: v + Math.sin(O) * m - b,
                top: v - Math.cos(O) * m - b
            }), D.html(i(P)), T.append(D), D.on(h, a);
            if (u.on(h, function (e) {
                    0 === t(e.target).closest(".clockpicker-tick").length && a(e, !0)
                }), l) {
                var N = c.find(".clockpicker-canvas"), j = e("svg");
                j.setAttribute("class", "clockpicker-svg"), j.setAttribute("width", w), j.setAttribute("height", w);
                var H = e("g");
                H.setAttribute("transform", "translate(" + v + "," + v + ")");
                var L = e("circle");
                L.setAttribute("class", "clockpicker-canvas-bearing"), L.setAttribute("cx", 0), L.setAttribute("cy", 0), L.setAttribute("r", 4);
                var q = e("line");
                q.setAttribute("x1", 0), q.setAttribute("y1", 0);
                var z = e("circle");
                z.setAttribute("class", "clockpicker-canvas-bg"), z.setAttribute("r", b), H.appendChild(q), H.appendChild(z), H.appendChild(L), j.appendChild(H), N.append(j), this.hand = q, this.bg = z, this.bearing = L, this.g = H, this.canvas = N
            }
            o(this.options.init)
        }

        function o(t) {
            t && "function" == typeof t && t()
        }

        var r = t(window), s = t(document), a = "http://www.w3.org/2000/svg", l = "SVGAngle" in window && function () {
                var t, e = document.createElement("div");
                return e.innerHTML = "<svg/>", t = (e.firstChild && e.firstChild.namespaceURI) == a, e.innerHTML = "", t
            }(), c = function () {
                var t = document.createElement("div").style;
                return "transition" in t || "WebkitTransition" in t || "MozTransition" in t || "msTransition" in t || "OTransition" in t
            }(), u = "ontouchstart" in window, h = "mousedown" + (u ? " touchstart" : ""),
            d = "mousemove.clockpicker" + (u ? " touchmove.clockpicker" : ""),
            p = "mouseup.clockpicker" + (u ? " touchend.clockpicker" : ""),
            f = navigator.vibrate ? "vibrate" : navigator.webkitVibrate ? "webkitVibrate" : null, g = 0, v = 135,
            m = 105, y = 70, b = 20, w = 2 * v, x = c ? 350 : 1,
            C = ['<div class="clockpicker picker">', '<div class="picker__holder">', '<div class="picker__frame">', '<div class="picker__wrap">', '<div class="picker__box">', '<div class="picker__date-display">', '<div class="clockpicker-display">', '<div class="clockpicker-display-column">', '<span class="clockpicker-span-hours text-primary"></span>', ":", '<span class="clockpicker-span-minutes"></span>', "</div>", '<div class="clockpicker-display-column clockpicker-display-am-pm">', '<div class="clockpicker-span-am-pm"></div>', "</div>", "</div>", "</div>", '<div class="picker__container__wrapper">', '<div class="picker__calendar-container">', '<div class="clockpicker-plate">', '<div class="clockpicker-canvas"></div>', '<div class="clockpicker-dial clockpicker-hours"></div>', '<div class="clockpicker-dial clockpicker-minutes clockpicker-dial-out"></div>', "</div>", '<div class="clockpicker-am-pm-block">', "</div>", "</div>", '<div class="picker__footer">', "</div>", "</div>", "</div>", "</div>", "</div>", "</div>", "</div>"].join("");
        n.DEFAULTS = {
            default: "",
            fromnow: 0,
            donetext: "Ok",
            cleartext: "Clear",
            canceltext: "Cancel",
            autoclose: !1,
            ampmclickable: !0,
            darktheme: !1,
            twelvehour: !0,
            vibrate: !0
        }, n.prototype.toggle = function () {
            this[this.isShown ? "hide" : "show"]()
        }, n.prototype.locate = function () {
            var t = this.element, e = this.popover;
            t.offset(), t.outerWidth(), t.outerHeight(), this.options.align;
            e.show()
        }, n.prototype.show = function (e) {
            if (!this.isShown) {
                o(this.options.beforeShow), t(":input").each(function () {
                    t(this).attr("tabindex", -1)
                });
                var n = this;
                this.input.blur(), this.popover.addClass("picker--opened"), this.input.addClass("picker__input picker__input--active"), t(document.body).css("overflow", "hidden");
                var a = ((this.input.prop("value") || this.options.default || "") + "").split(":");
                if (this.options.twelvehour && void 0 !== a[1] && (a[1].indexOf("AM") > 0 ? this.amOrPm = "AM" : this.amOrPm = "PM", a[1] = a[1].replace("AM", "").replace("PM", "")), "now" === a[0]) {
                    var l = new Date(+new Date + this.options.fromnow);
                    a = [l.getHours(), l.getMinutes()], this.options.twelvehour && (this.amOrPm = a[0] >= 12 && a[0] < 24 ? "PM" : "AM")
                }
                if (this.hours = +a[0] || 0, this.minutes = +a[1] || 0, this.spanHours.html(this.hours), this.spanMinutes.html(i(this.minutes)), !this.isAppended) {
                    var c = document.querySelector(this.options.container);
                    this.options.container && c ? c.appendChild(this.popover[0]) : this.popover.insertAfter(this.input), this.options.twelvehour && ("PM" === this.amOrPm ? (this.spanAmPm.children("#click-pm").addClass("text-primary"), this.spanAmPm.children("#click-am").removeClass("text-primary")) : (this.spanAmPm.children("#click-am").addClass("text-primary"), this.spanAmPm.children("#click-pm").removeClass("text-primary"))), r.on("resize.clockpicker" + this.id, function () {
                        n.isShown && n.locate()
                    }), this.isAppended = !0
                }
                this.toggleView("hours"), this.locate(), this.isShown = !0, s.on("click.clockpicker." + this.id + " focusin.clockpicker." + this.id, function (e) {
                    var i = t(e.target);
                    0 === i.closest(n.popover.find(".picker__wrap")).length && 0 === i.closest(n.input).length && n.hide()
                }), s.on("keyup.clockpicker." + this.id, function (t) {
                    27 === t.keyCode && n.hide()
                }), o(this.options.afterShow)
            }
        }, n.prototype.hide = function () {
            o(this.options.beforeHide), this.input.removeClass("picker__input picker__input--active"), this.popover.removeClass("picker--opened"), t(document.body).css("overflow", "visible"), this.isShown = !1, t(":input").each(function (e) {
                t(this).attr("tabindex", e + 1)
            }), s.off("click.clockpicker." + this.id + " focusin.clockpicker." + this.id), s.off("keyup.clockpicker." + this.id), this.popover.hide(), o(this.options.afterHide)
        }, n.prototype.toggleView = function (e, i) {
            var n = !1;
            "minutes" === e && "visible" === t(this.hoursView).css("visibility") && (o(this.options.beforeHourSelect), n = !0);
            var r = "hours" === e, s = r ? this.hoursView : this.minutesView, a = r ? this.minutesView : this.hoursView;
            this.currentView = e, this.spanHours.toggleClass("text-primary", r), this.spanMinutes.toggleClass("text-primary", !r), a.addClass("clockpicker-dial-out"), s.css("visibility", "visible").removeClass("clockpicker-dial-out"), this.resetClock(i), clearTimeout(this.toggleViewTimer), this.toggleViewTimer = setTimeout(function () {
                a.css("visibility", "hidden")
            }, x), n && o(this.options.afterHourSelect)
        }, n.prototype.resetClock = function (t) {
            var e = this.currentView, i = this[e], n = "hours" === e, o = i * (Math.PI / (n ? 6 : 30)),
                r = n && i > 0 && i < 13 ? y : m, s = Math.sin(o) * r, a = -Math.cos(o) * r, c = this;
            l && t ? (c.canvas.addClass("clockpicker-canvas-out"), setTimeout(function () {
                c.canvas.removeClass("clockpicker-canvas-out"), c.setHand(s, a)
            }, t)) : this.setHand(s, a)
        }, n.prototype.setHand = function (e, n, o, r) {
            var s, a = Math.atan2(e, -n), c = "hours" === this.currentView, u = Math.PI / (c || o ? 6 : 30),
                h = Math.sqrt(e * e + n * n), d = this.options, p = c && h < (m + y) / 2, g = p ? y : m;
            if (d.twelvehour && (g = m), a < 0 && (a = 2 * Math.PI + a), s = Math.round(a / u), a = s * u, d.twelvehour ? c ? 0 === s && (s = 12) : (o && (s *= 5), 60 === s && (s = 0)) : c ? (12 === s && (s = 0), s = p ? 0 === s ? 12 : s : 0 === s ? 0 : s + 12) : (o && (s *= 5), 60 === s && (s = 0)), this[this.currentView] !== s && f && this.options.vibrate && (this.vibrateTimer || (navigator[f](10), this.vibrateTimer = setTimeout(t.proxy(function () {
                    this.vibrateTimer = null
                }, this), 100))), this[this.currentView] = s, c ? this.spanHours.html(s) : this.spanMinutes.html(i(s)), l) {
                var v = Math.sin(a) * (g - b), w = -Math.cos(a) * (g - b), x = Math.sin(a) * g, C = -Math.cos(a) * g;
                this.hand.setAttribute("x2", v), this.hand.setAttribute("y2", w), this.bg.setAttribute("cx", x), this.bg.setAttribute("cy", C)
            } else this[c ? "hoursView" : "minutesView"].find(".clockpicker-tick").each(function () {
                var e = t(this);
                e.toggleClass("active", s === +e.html())
            })
        }, n.prototype.done = function () {
            o(this.options.beforeDone), this.hide(), this.label.addClass("active");
            var t = this.input.prop("value"), e = i(this.hours) + ":" + i(this.minutes);
            this.options.twelvehour && (e += this.amOrPm), this.input.prop("value", e), e !== t && (this.input.triggerHandler("change"), this.isInput || this.element.trigger("change")), this.options.autoclose && this.input.trigger("blur"), o(this.options.afterDone)
        }, n.prototype.clear = function () {
            this.hide(), this.label.removeClass("active");
            var t = this.input.prop("value");
            this.input.prop("value", ""), "" !== t && (this.input.triggerHandler("change"), this.isInput || this.element.trigger("change")), this.options.autoclose && this.input.trigger("blur")
        }, n.prototype.remove = function () {
            this.element.removeData("clockpicker"), this.input.off("focus.clockpicker click.clockpicker"), this.isShown && this.hide(), this.isAppended && (r.off("resize.clockpicker" + this.id), this.popover.remove())
        }, t.fn.pickatime = function (e) {
            var i = Array.prototype.slice.call(arguments, 1);
            return this.each(function () {
                var o = t(this), r = o.data("clockpicker");
                if (r) "function" == typeof r[e] && r[e].apply(r, i); else {
                    var s = t.extend({}, n.DEFAULTS, o.data(), "object" == typeof e && e);
                    o.data("clockpicker", new n(o, s))
                }
            })
        }
    }(jQuery), function (t) {
        function e() {
            var e = +t(this).attr("data-length"), i = +t(this).val().length, n = i <= e;
            t(this).parent().find('span[class="character-counter"]').html(i + "/" + e), function (t, e) {
                var i = e.hasClass("invalid");
                t && i ? e.removeClass("invalid") : t || i || (e.removeClass("valid"), e.addClass("invalid"))
            }(n, t(this))
        }

        t.fn.characterCounter = function () {
            return this.each(function () {
                var i = t(this);
                if (!i.parent().find('span[class="character-counter"]').length) {
                    void 0 !== i.attr("data-length") && (i.on("input", e), i.on("focus", e), i.on("blur", function () {
                        t(this).parent().find('span[class="character-counter"]').html("")
                    }), function (e) {
                        var i = e.parent().find('span[class="character-counter"]');
                        i.length || (i = t("<span/>").addClass("character-counter").css("float", "right").css("font-size", "12px").css("height", 1), e.parent().append(i))
                    }(i))
                }
            })
        }, t(document).ready(function () {
            t("input, textarea").characterCounter()
        })
    }(jQuery), function (t) {
        var e = {
            init: function (e) {
                e = t.extend({
                    duration: 200,
                    dist: -100,
                    shift: 0,
                    padding: 0,
                    fullWidth: !1,
                    indicators: !1,
                    noWrap: !1,
                    onCycleTo: null
                }, e);
                var i = Materialize.objectSelectorString(t(this));
                return this.each(function (n) {
                    function o(t) {
                        return t.targetTouches && t.targetTouches.length >= 1 ? t.targetTouches[0].clientX : t.clientX
                    }

                    function r(t) {
                        return t.targetTouches && t.targetTouches.length >= 1 ? t.targetTouches[0].clientY : t.clientY
                    }

                    function s(t) {
                        return t >= x ? t % x : t < 0 ? s(x + t % x) : t
                    }

                    function a(i) {
                        E = !0, H.hasClass("scrolling") || H.addClass("scrolling"), null != N && window.clearTimeout(N), N = window.setTimeout(function () {
                            E = !1, H.removeClass("scrolling")
                        }, e.duration);
                        var n, o, r, a, l, c, u, h = m;
                        if (v = "number" == typeof i ? i : v, m = Math.floor((v + w / 2) / w), r = v - m * w, a = r < 0 ? 1 : -1, l = -a * r * 2 / w, o = x >> 1, e.fullWidth ? u = "translateX(0)" : (u = "translateX(" + (H[0].clientWidth - f) / 2 + "px) ", u += "translateY(" + (H[0].clientHeight - g) / 2 + "px)"), q) {
                            var d = m % x, y = I.find(".indicator-item.active");
                            y.index() !== d && (y.removeClass("active"), I.find(".indicator-item").eq(d).addClass("active"))
                        }
                        for ((!z || m >= 0 && m < x) && (c = p[s(m)], t(c).hasClass("active") || (H.find(".carousel-item").removeClass("active"), t(c).addClass("active")), c.style[A] = u + " translateX(" + -r / 2 + "px) translateX(" + a * e.shift * l * n + "px) translateZ(" + e.dist * l + "px)", c.style.zIndex = 0, e.fullWidth ? tweenedOpacity = 1 : tweenedOpacity = 1 - .2 * l, c.style.opacity = tweenedOpacity, c.style.display = "block"), n = 1; n <= o; ++n) e.fullWidth ? (zTranslation = e.dist, tweenedOpacity = n === o && r < 0 ? 1 - l : 1) : (zTranslation = e.dist * (2 * n + l * a), tweenedOpacity = 1 - .2 * (2 * n + l * a)), (!z || m + n < x) && ((c = p[s(m + n)]).style[A] = u + " translateX(" + (e.shift + (w * n - r) / 2) + "px) translateZ(" + zTranslation + "px)", c.style.zIndex = -n, c.style.opacity = tweenedOpacity, c.style.display = "block"), e.fullWidth ? (zTranslation = e.dist, tweenedOpacity = n === o && r > 0 ? 1 - l : 1) : (zTranslation = e.dist * (2 * n - l * a), tweenedOpacity = 1 - .2 * (2 * n - l * a)), (!z || m - n >= 0) && ((c = p[s(m - n)]).style[A] = u + " translateX(" + (-e.shift + (-w * n - r) / 2) + "px) translateZ(" + zTranslation + "px)", c.style.zIndex = -n, c.style.opacity = tweenedOpacity, c.style.display = "block");
                        if ((!z || m >= 0 && m < x) && ((c = p[s(m)]).style[A] = u + " translateX(" + -r / 2 + "px) translateX(" + a * e.shift * l + "px) translateZ(" + e.dist * l + "px)", c.style.zIndex = 0, e.fullWidth ? tweenedOpacity = 1 : tweenedOpacity = 1 - .2 * l, c.style.opacity = tweenedOpacity, c.style.display = "block"), h !== m && "function" == typeof e.onCycleTo) {
                            var b = H.find(".carousel-item").eq(s(m));
                            e.onCycleTo.call(this, b, O)
                        }
                        "function" == typeof j && (j.call(this, b, O), j = null)
                    }

                    function l() {
                        var t, i;
                        T && (t = Date.now() - P, (i = T * Math.exp(-t / e.duration)) > 2 || i < -2 ? (a(_ - i), requestAnimationFrame(l)) : a(_))
                    }

                    function c(t) {
                        var e = m % x - t;
                        z || (e < 0 ? Math.abs(e + x) < Math.abs(e) && (e += x) : e > 0 && Math.abs(e - x) < e && (e -= x)), e < 0 ? H.trigger("carouselNext", [Math.abs(e)]) : e > 0 && H.trigger("carouselPrev", [e])
                    }

                    function u(e) {
                        "mousedown" === e.type && t(e.target).is("img") && e.preventDefault(), b = !0, O = !1, M = !1, C = o(e), k = r(e), S = T = 0, $ = v, P = Date.now(), clearInterval(D), D = setInterval(function () {
                            var t, e, i;
                            e = (t = Date.now()) - P, P = t, i = v - $, $ = v, S = 1e3 * i / (1 + e) * .8 + .2 * S
                        }, 100)
                    }

                    function h(t) {
                        var e, i;
                        if (b) if (e = o(t), y = r(t), i = C - e, Math.abs(k - y) < 30 && !M) (i > 2 || i < -2) && (O = !0, C = e, a(v + i)); else {
                            if (O) return t.preventDefault(), t.stopPropagation(), !1;
                            M = !0
                        }
                        if (O) return t.preventDefault(), t.stopPropagation(), !1
                    }

                    function d(t) {
                        if (b) return b = !1, clearInterval(D), _ = v, (S > 10 || S < -10) && (_ = v + (T = .9 * S)), _ = Math.round(_ / w) * w, z && (_ >= w * (x - 1) ? _ = w * (x - 1) : _ < 0 && (_ = 0)), T = _ - v, P = Date.now(), requestAnimationFrame(l), O && (t.preventDefault(), t.stopPropagation()), !1
                    }

                    var p, f, g, v, m, b, w, x, C, k, T, _, S, E, A, $, P, D, O, M,
                        I = t('<ul class="indicators"></ul>'), N = null, j = null, H = t(this),
                        L = H.find(".carousel-item").length > 1, q = (H.attr("data-indicators") || e.indicators) && L,
                        z = H.attr("data-no-wrap") || e.noWrap || !L, R = H.attr("data-namespace") || i + n;
                    H.attr("data-namespace", R);
                    var W = function (e) {
                        var i = H.find(".carousel-item.active").length ? H.find(".carousel-item.active").first() : H.find(".carousel-item").first(),
                            n = i.find("img").first();
                        if (n.length) if (n[0].complete) {
                            if (n.height() > 0) H.css("height", n.height()); else {
                                var o = n[0].naturalWidth, r = n[0].naturalHeight, s = H.width() / o * r;
                                H.css("height", s)
                            }
                        } else n.on("load", function () {
                            H.css("height", t(this).height())
                        }); else if (!e) {
                            var a = i.height();
                            H.css("height", a)
                        }
                    };
                    if (e.fullWidth && (e.dist = 0, W(), q && H.find(".carousel-fixed-item").addClass("with-indicators")), H.hasClass("initialized")) return t(window).trigger("resize"), H.trigger("carouselNext", [1e-6]), !0;
                    H.addClass("initialized"), b = !1, v = _ = 0, p = [], f = H.find(".carousel-item").first().innerWidth(), g = H.find(".carousel-item").first().innerHeight(), w = 2 * f + e.padding, H.find(".carousel-item").each(function (e) {
                        if (p.push(t(this)[0]), q) {
                            var i = t('<li class="indicator-item"></li>');
                            0 === e && i.addClass("active"), i.click(function (e) {
                                e.stopPropagation();
                                c(t(this).index())
                            }), I.append(i)
                        }
                    }), q && H.append(I), x = p.length, A = "transform", ["webkit", "Moz", "O", "ms"].every(function (t) {
                        var e = t + "Transform";
                        return void 0 === document.body.style[e] || (A = e, !1)
                    });
                    var F = Materialize.throttle(function () {
                        if (e.fullWidth) {
                            f = H.find(".carousel-item").first().innerWidth();
                            H.find(".carousel-item.active").height();
                            w = 2 * f + e.padding, _ = v = 2 * m * f, W(!0)
                        } else a()
                    }, 200);
                    t(window).off("resize.carousel-" + R).on("resize.carousel-" + R, F), void 0 !== window.ontouchstart && (H.on("touchstart.carousel", u), H.on("touchmove.carousel", h), H.on("touchend.carousel", d)), H.on("mousedown.carousel", u), H.on("mousemove.carousel", h), H.on("mouseup.carousel", d), H.on("mouseleave.carousel", d), H.on("click.carousel", function (i) {
                        if (O) return i.preventDefault(), i.stopPropagation(), !1;
                        if (!e.fullWidth) {
                            var n = t(i.target).closest(".carousel-item").index();
                            0 != s(m) - n && (i.preventDefault(), i.stopPropagation()), c(n)
                        }
                    }), a(v), t(this).on("carouselNext", function (t, e, i) {
                        void 0 === e && (e = 1), "function" == typeof i && (j = i), _ = w * Math.round(v / w) + w * e, v !== _ && (T = _ - v, P = Date.now(), requestAnimationFrame(l))
                    }), t(this).on("carouselPrev", function (t, e, i) {
                        void 0 === e && (e = 1), "function" == typeof i && (j = i), _ = w * Math.round(v / w) - w * e, v !== _ && (T = _ - v, P = Date.now(), requestAnimationFrame(l))
                    }), t(this).on("carouselSet", function (t, e, i) {
                        void 0 === e && (e = 0), "function" == typeof i && (j = i), c(e)
                    })
                })
            }, next: function (e, i) {
                t(this).trigger("carouselNext", [e, i])
            }, prev: function (e, i) {
                t(this).trigger("carouselPrev", [e, i])
            }, set: function (e, i) {
                t(this).trigger("carouselSet", [e, i])
            }, destroy: function () {
                var e = t(this).attr("data-namespace");
                t(this).removeAttr("data-namespace"), t(this).removeClass("initialized"), t(this).find(".indicators").remove(), t(this).off("carouselNext carouselPrev carouselSet"), t(window).off("resize.carousel-" + e), void 0 !== window.ontouchstart && t(this).off("touchstart.carousel touchmove.carousel touchend.carousel"), t(this).off("mousedown.carousel mousemove.carousel mouseup.carousel mouseleave.carousel click.carousel")
            }
        };
        t.fn.carousel = function (i) {
            return e[i] ? e[i].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof i && i ? void t.error("Method " + i + " does not exist on jQuery.carousel") : e.init.apply(this, arguments)
        }
    }(jQuery), function (t) {
        var e = {
            init: function (e) {
                return this.each(function () {
                    var i = t("#" + t(this).attr("data-activates")), n = (t("body"), t(this)),
                        o = n.parent(".tap-target-wrapper"), r = o.find(".tap-target-wave"),
                        s = o.find(".tap-target-origin"), a = n.find(".tap-target-content");
                    o.length || (o = n.wrap(t('<div class="tap-target-wrapper"></div>')).parent()), a.length || (a = t('<div class="tap-target-content"></div>'), n.append(a)), r.length || (r = t('<div class="tap-target-wave"></div>'), s.length || ((s = i.clone(!0, !0)).addClass("tap-target-origin"), s.removeAttr("id"), s.removeAttr("style"), r.append(s)), o.append(r));
                    var l = function () {
                        o.is(".open") && (o.removeClass("open"), s.off("click.tapTarget"), t(document).off("click.tapTarget"), t(window).off("resize.tapTarget"))
                    }, c = function () {
                        var e = "fixed" === i.css("position");
                        if (!e) for (var s = i.parents(), l = 0; l < s.length && !(e = "fixed" == t(s[l]).css("position")); l++) ;
                        var c = i.outerWidth(), u = i.outerHeight(),
                            h = e ? i.offset().top - t(document).scrollTop() : i.offset().top,
                            d = e ? i.offset().left - t(document).scrollLeft() : i.offset().left, p = t(window).width(),
                            f = t(window).height(), g = p / 2, v = f / 2, m = d <= g, y = d > g, b = h <= v, w = h > v,
                            x = d >= .25 * p && d <= .75 * p, C = n.outerWidth(), k = n.outerHeight(),
                            T = h + u / 2 - k / 2, _ = d + c / 2 - C / 2, S = e ? "fixed" : "absolute",
                            E = x ? C : C / 2 + c, A = k / 2, $ = b ? k / 2 : 0, P = m && !x ? C / 2 - c : 0, D = c,
                            O = w ? "bottom" : "top", M = 2 * c, I = M, N = k / 2 - I / 2, j = C / 2 - M / 2, H = {};
                        H.top = b ? T : "", H.right = y ? p - _ - C : "", H.bottom = w ? f - T - k : "", H.left = m ? _ : "", H.position = S, o.css(H), a.css({
                            width: E,
                            height: A,
                            top: $,
                            right: 0,
                            bottom: 0,
                            left: P,
                            padding: D,
                            verticalAlign: O
                        }), r.css({top: N, left: j, width: M, height: I})
                    };
                    "open" == e && (c(), o.is(".open") || (o.addClass("open"), setTimeout(function () {
                        s.off("click.tapTarget").on("click.tapTarget", function (t) {
                            l(), s.off("click.tapTarget")
                        }), t(document).off("click.tapTarget").on("click.tapTarget", function (e) {
                            l(), t(document).off("click.tapTarget")
                        });
                        var e = Materialize.throttle(function () {
                            c()
                        }, 200);
                        t(window).off("resize.tapTarget").on("resize.tapTarget", e)
                    }, 0))), "close" == e && l()
                })
            }, open: function () {
            }, close: function () {
            }
        };
        t.fn.tapTarget = function (i) {
            if (e[i] || "object" == typeof i) return e.init.apply(this, arguments);
            t.error("Method " + i + " does not exist on jQuery.tap-target")
        }
    }(jQuery), "undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery");
+function (t) {
    "use strict";
    var e = jQuery.fn.jquery.split(" ")[0].split(".");
    if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1 || e[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4")
}(), function (t) {
    "use strict";
    t.fn.emulateTransitionEnd = function (e) {
        var i = !1, n = this;
        t(this).one("bsTransitionEnd", function () {
            i = !0
        });
        return setTimeout(function () {
            i || t(n).trigger(t.support.transition.end)
        }, e), this
    }, t(function () {
        t.support.transition = function () {
            var t = document.createElement("bootstrap"), e = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            };
            for (var i in e) if (void 0 !== t.style[i]) return {end: e[i]};
            return !1
        }(), t.support.transition && (t.event.special.bsTransitionEnd = {
            bindType: t.support.transition.end,
            delegateType: t.support.transition.end,
            handle: function (e) {
                if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
            }
        })
    })
}(jQuery), function (t) {
    "use strict";
    var e = '[data-dismiss="alert"]', i = function (i) {
        t(i).on("click", e, this.close)
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 150, i.prototype.close = function (e) {
        function n() {
            s.detach().trigger("closed.bs.alert").remove()
        }

        var o = t(this), r = o.attr("data-target");
        r || (r = (r = o.attr("href")) && r.replace(/.*(?=#[^\s]*$)/, ""));
        var s = t("#" === r ? [] : r);
        e && e.preventDefault(), s.length || (s = o.closest(".alert")), s.trigger(e = t.Event("close.bs.alert")), e.isDefaultPrevented() || (s.removeClass("in"), t.support.transition && s.hasClass("fade") ? s.one("bsTransitionEnd", n).emulateTransitionEnd(i.TRANSITION_DURATION) : n())
    };
    var n = t.fn.alert;
    t.fn.alert = function (e) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.alert");
            o || n.data("bs.alert", o = new i(this)), "string" == typeof e && o[e].call(n)
        })
    }, t.fn.alert.Constructor = i, t.fn.alert.noConflict = function () {
        return t.fn.alert = n, this
    }, t(document).on("click.bs.alert.data-api", e, i.prototype.close)
}(jQuery), function (t) {
    "use strict";

    function e(e) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.button"), r = "object" == typeof e && e;
            o || n.data("bs.button", o = new i(this, r)), "toggle" == e ? o.toggle() : e && o.setState(e)
        })
    }

    var i = function (e, n) {
        this.$element = t(e), this.options = t.extend({}, i.DEFAULTS, n), this.isLoading = !1
    };
    i.VERSION = "3.3.7", i.DEFAULTS = {loadingText: "loading..."}, i.prototype.setState = function (e) {
        var i = "disabled", n = this.$element, o = n.is("input") ? "val" : "html", r = n.data();
        e += "Text", null == r.resetText && n.data("resetText", n[o]()), setTimeout(t.proxy(function () {
            n[o](null == r[e] ? this.options[e] : r[e]), "loadingText" == e ? (this.isLoading = !0, n.addClass(i).attr(i, i).prop(i, !0)) : this.isLoading && (this.isLoading = !1, n.removeClass(i).removeAttr(i).prop(i, !1))
        }, this), 0)
    }, i.prototype.toggle = function () {
        var t = !0, e = this.$element.closest('[data-toggle="buttons"]');
        if (e.length) {
            var i = this.$element.find("input");
            "radio" == i.prop("type") ? (i.prop("checked") && (t = !1), e.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == i.prop("type") && (i.prop("checked") !== this.$element.hasClass("active") && (t = !1), this.$element.toggleClass("active")), i.prop("checked", this.$element.hasClass("active")), t && i.trigger("change")
        } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active")
    };
    var n = t.fn.button;
    t.fn.button = e, t.fn.button.Constructor = i, t.fn.button.noConflict = function () {
        return t.fn.button = n, this
    }, t(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function (i) {
        var n = t(i.target).closest(".btn");
        e.call(n, "toggle"), t(i.target).is('input[type="radio"], input[type="checkbox"]') || (i.preventDefault(), n.is("input,button") ? n.trigger("focus") : n.find("input:visible,button:visible").first().trigger("focus"))
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function (e) {
        t(e.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(e.type))
    })
}(jQuery), function (t) {
    "use strict";

    function e(e) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.carousel"),
                r = t.extend({}, i.DEFAULTS, n.data(), "object" == typeof e && e),
                s = "string" == typeof e ? e : r.slide;
            o || n.data("bs.carousel", o = new i(this, r)), "number" == typeof e ? o.to(e) : s ? o[s]() : r.interval && o.pause().cycle()
        })
    }

    var i = function (e, i) {
        this.$element = t(e), this.$indicators = this.$element.find(".carousel-indicators"), this.options = i, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", t.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", t.proxy(this.pause, this)).on("mouseleave.bs.carousel", t.proxy(this.cycle, this))
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 600, i.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    }, i.prototype.keydown = function (t) {
        if (!/input|textarea/i.test(t.target.tagName)) {
            switch (t.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            t.preventDefault()
        }
    }, i.prototype.cycle = function (e) {
        return e || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(t.proxy(this.next, this), this.options.interval)), this
    }, i.prototype.getItemIndex = function (t) {
        return this.$items = t.parent().children(".item"), this.$items.index(t || this.$active)
    }, i.prototype.getItemForDirection = function (t, e) {
        var i = this.getItemIndex(e);
        if (("prev" == t && 0 === i || "next" == t && i == this.$items.length - 1) && !this.options.wrap) return e;
        var n = (i + ("prev" == t ? -1 : 1)) % this.$items.length;
        return this.$items.eq(n)
    }, i.prototype.to = function (t) {
        var e = this, i = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        if (!(t > this.$items.length - 1 || t < 0)) return this.sliding ? this.$element.one("slid.bs.carousel", function () {
            e.to(t)
        }) : i == t ? this.pause().cycle() : this.slide(t > i ? "next" : "prev", this.$items.eq(t))
    }, i.prototype.pause = function (e) {
        return e || (this.paused = !0), this.$element.find(".next, .prev").length && t.support.transition && (this.$element.trigger(t.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, i.prototype.next = function () {
        if (!this.sliding) return this.slide("next")
    }, i.prototype.prev = function () {
        if (!this.sliding) return this.slide("prev")
    }, i.prototype.slide = function (e, n) {
        var o = this.$element.find(".item.active"), r = n || this.getItemForDirection(e, o), s = this.interval,
            a = "next" == e ? "left" : "right", l = this;
        if (r.hasClass("active")) return this.sliding = !1;
        var c = r[0], u = t.Event("slide.bs.carousel", {relatedTarget: c, direction: a});
        if (this.$element.trigger(u), !u.isDefaultPrevented()) {
            if (this.sliding = !0, s && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var h = t(this.$indicators.children()[this.getItemIndex(r)]);
                h && h.addClass("active")
            }
            var d = t.Event("slid.bs.carousel", {relatedTarget: c, direction: a});
            return t.support.transition && this.$element.hasClass("slide") ? (r.addClass(e), r[0].offsetWidth, o.addClass(a), r.addClass(a), o.one("bsTransitionEnd", function () {
                r.removeClass([e, a].join(" ")).addClass("active"), o.removeClass(["active", a].join(" ")), l.sliding = !1, setTimeout(function () {
                    l.$element.trigger(d)
                }, 0)
            }).emulateTransitionEnd(i.TRANSITION_DURATION)) : (o.removeClass("active"), r.addClass("active"), this.sliding = !1, this.$element.trigger(d)), s && this.cycle(), this
        }
    };
    var n = t.fn.carousel;
    t.fn.carousel = e, t.fn.carousel.Constructor = i, t.fn.carousel.noConflict = function () {
        return t.fn.carousel = n, this
    };
    var o = function (i) {
        var n, o = t(this), r = t(o.attr("data-target") || (n = o.attr("href")) && n.replace(/.*(?=#[^\s]+$)/, ""));
        if (r.hasClass("carousel")) {
            var s = t.extend({}, r.data(), o.data()), a = o.attr("data-slide-to");
            a && (s.interval = !1), e.call(r, s), a && r.data("bs.carousel").to(a), i.preventDefault()
        }
    };
    t(document).on("click.bs.carousel.data-api", "[data-slide]", o).on("click.bs.carousel.data-api", "[data-slide-to]", o), t(window).on("load", function () {
        t('[data-ride="carousel"]').each(function () {
            var i = t(this);
            e.call(i, i.data())
        })
    })
}(jQuery), function (t) {
    "use strict";

    function e(e) {
        var i, n = e.attr("data-target") || (i = e.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, "");
        return t(n)
    }

    function i(e) {
        return this.each(function () {
            var i = t(this), o = i.data("bs.collapse"),
                r = t.extend({}, n.DEFAULTS, i.data(), "object" == typeof e && e);
            !o && r.toggle && /show|hide/.test(e) && (r.toggle = !1), o || i.data("bs.collapse", o = new n(this, r)), "string" == typeof e && o[e]()
        })
    }

    var n = function (e, i) {
        this.$element = t(e), this.options = t.extend({}, n.DEFAULTS, i), this.$trigger = t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
    };
    n.VERSION = "3.3.7", n.TRANSITION_DURATION = 350, n.DEFAULTS = {toggle: !0}, n.prototype.dimension = function () {
        return this.$element.hasClass("width") ? "width" : "height"
    }, n.prototype.show = function () {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var e, o = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(o && o.length && (e = o.data("bs.collapse")) && e.transitioning)) {
                var r = t.Event("show.bs.collapse");
                if (this.$element.trigger(r), !r.isDefaultPrevented()) {
                    o && o.length && (i.call(o, "hide"), e || o.data("bs.collapse", null));
                    var s = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[s](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var a = function () {
                        this.$element.removeClass("collapsing").addClass("collapse in")[s](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                    };
                    if (!t.support.transition) return a.call(this);
                    var l = t.camelCase(["scroll", s].join("-"));
                    this.$element.one("bsTransitionEnd", t.proxy(a, this)).emulateTransitionEnd(n.TRANSITION_DURATION)[s](this.$element[0][l])
                }
            }
        }
    }, n.prototype.hide = function () {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var e = t.Event("hide.bs.collapse");
            if (this.$element.trigger(e), !e.isDefaultPrevented()) {
                var i = this.dimension();
                this.$element[i](this.$element[i]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var o = function () {
                    this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                if (!t.support.transition) return o.call(this);
                this.$element[i](0).one("bsTransitionEnd", t.proxy(o, this)).emulateTransitionEnd(n.TRANSITION_DURATION)
            }
        }
    }, n.prototype.toggle = function () {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }, n.prototype.getParent = function () {
        return t(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(t.proxy(function (i, n) {
            var o = t(n);
            this.addAriaAndCollapsedClass(e(o), o)
        }, this)).end()
    }, n.prototype.addAriaAndCollapsedClass = function (t, e) {
        var i = t.hasClass("in");
        t.attr("aria-expanded", i), e.toggleClass("collapsed", !i).attr("aria-expanded", i)
    };
    var o = t.fn.collapse;
    t.fn.collapse = i, t.fn.collapse.Constructor = n, t.fn.collapse.noConflict = function () {
        return t.fn.collapse = o, this
    }, t(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function (n) {
        var o = t(this);
        o.attr("data-target") || n.preventDefault();
        var r = e(o), s = r.data("bs.collapse") ? "toggle" : o.data();
        i.call(r, s)
    })
}(jQuery), function (t) {
    "use strict";

    function e(e) {
        var i = e.attr("data-target");
        i || (i = (i = e.attr("href")) && /#[A-Za-z]/.test(i) && i.replace(/.*(?=#[^\s]*$)/, ""));
        var n = i && t(i);
        return n && n.length ? n : e.parent()
    }

    function i(i) {
        i && 3 === i.which || (t(n).remove(), t(o).each(function () {
            var n = t(this), o = e(n), r = {relatedTarget: this};
            o.hasClass("open") && (i && "click" == i.type && /input|textarea/i.test(i.target.tagName) && t.contains(o[0], i.target) || (o.trigger(i = t.Event("hide.bs.dropdown", r)), i.isDefaultPrevented() || (n.attr("aria-expanded", "false"), o.removeClass("open").trigger(t.Event("hidden.bs.dropdown", r)))))
        }))
    }

    var n = ".dropdown-backdrop", o = '[data-toggle="dropdown"]', r = function (e) {
        t(e).on("click.bs.dropdown", this.toggle)
    };
    r.VERSION = "3.3.7", r.prototype.toggle = function (n) {
        var o = t(this);
        if (!o.is(".disabled, :disabled")) {
            var r = e(o), s = r.hasClass("open");
            if (i(), !s) {
                "ontouchstart" in document.documentElement && !r.closest(".navbar-nav").length && t(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(t(this)).on("click", i);
                var a = {relatedTarget: this};
                if (r.trigger(n = t.Event("show.bs.dropdown", a)), n.isDefaultPrevented()) return;
                o.trigger("focus").attr("aria-expanded", "true"), r.toggleClass("open").trigger(t.Event("shown.bs.dropdown", a))
            }
            return !1
        }
    }, r.prototype.keydown = function (i) {
        if (/(38|40|27|32)/.test(i.which) && !/input|textarea/i.test(i.target.tagName)) {
            var n = t(this);
            if (i.preventDefault(), i.stopPropagation(), !n.is(".disabled, :disabled")) {
                var r = e(n), s = r.hasClass("open");
                if (!s && 27 != i.which || s && 27 == i.which) return 27 == i.which && r.find(o).trigger("focus"), n.trigger("click");
                var a = r.find(".dropdown-menu li:not(.disabled):visible a");
                if (a.length) {
                    var l = a.index(i.target);
                    38 == i.which && l > 0 && l--, 40 == i.which && l < a.length - 1 && l++, ~l || (l = 0), a.eq(l).trigger("focus")
                }
            }
        }
    };
    var s = t.fn.dropdown;
    t.fn.dropdown = function (e) {
        return this.each(function () {
            var i = t(this), n = i.data("bs.dropdown");
            n || i.data("bs.dropdown", n = new r(this)), "string" == typeof e && n[e].call(i)
        })
    }, t.fn.dropdown.Constructor = r, t.fn.dropdown.noConflict = function () {
        return t.fn.dropdown = s, this
    }, t(document).on("click.bs.dropdown.data-api", i).on("click.bs.dropdown.data-api", ".dropdown form", function (t) {
        t.stopPropagation()
    }).on("click.bs.dropdown.data-api", o, r.prototype.toggle).on("keydown.bs.dropdown.data-api", o, r.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", r.prototype.keydown)
}(jQuery), function (t) {
    "use strict";

    function e(e, n) {
        return this.each(function () {
            var o = t(this), r = o.data("bs.modal"), s = t.extend({}, i.DEFAULTS, o.data(), "object" == typeof e && e);
            r || o.data("bs.modal", r = new i(this, s)), "string" == typeof e ? r[e](n) : s.show && r.show(n)
        })
    }

    var i = function (e, i) {
        this.options = i, this.$body = t(document.body), this.$element = t(e), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, t.proxy(function () {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 300, i.BACKDROP_TRANSITION_DURATION = 150, i.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, i.prototype.toggle = function (t) {
        return this.isShown ? this.hide() : this.show(t)
    }, i.prototype.show = function (e) {
        var n = this, o = t.Event("show.bs.modal", {relatedTarget: e});
        this.$element.trigger(o), this.isShown || o.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', t.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function () {
            n.$element.one("mouseup.dismiss.bs.modal", function (e) {
                t(e.target).is(n.$element) && (n.ignoreBackdropClick = !0)
            })
        }), this.backdrop(function () {
            var o = t.support.transition && n.$element.hasClass("fade");
            n.$element.parent().length || n.$element.appendTo(n.$body), n.$element.show().scrollTop(0), n.adjustDialog(), o && n.$element[0].offsetWidth, n.$element.addClass("in"), n.enforceFocus();
            var r = t.Event("shown.bs.modal", {relatedTarget: e});
            o ? n.$dialog.one("bsTransitionEnd", function () {
                n.$element.trigger("focus").trigger(r)
            }).emulateTransitionEnd(i.TRANSITION_DURATION) : n.$element.trigger("focus").trigger(r)
        }))
    }, i.prototype.hide = function (e) {
        e && e.preventDefault(), e = t.Event("hide.bs.modal"), this.$element.trigger(e), this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), t(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), t.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", t.proxy(this.hideModal, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : this.hideModal())
    }, i.prototype.enforceFocus = function () {
        t(document).off("focusin.bs.modal").on("focusin.bs.modal", t.proxy(function (t) {
            document === t.target || this.$element[0] === t.target || this.$element.has(t.target).length || this.$element.trigger("focus")
        }, this))
    }, i.prototype.escape = function () {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", t.proxy(function (t) {
            27 == t.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }, i.prototype.resize = function () {
        this.isShown ? t(window).on("resize.bs.modal", t.proxy(this.handleUpdate, this)) : t(window).off("resize.bs.modal")
    }, i.prototype.hideModal = function () {
        var t = this;
        this.$element.hide(), this.backdrop(function () {
            t.$body.removeClass("modal-open"), t.resetAdjustments(), t.resetScrollbar(), t.$element.trigger("hidden.bs.modal")
        })
    }, i.prototype.removeBackdrop = function () {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, i.prototype.backdrop = function (e) {
        var n = this, o = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var r = t.support.transition && o;
            if (this.$backdrop = t(document.createElement("div")).addClass("modal-backdrop " + o).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", t.proxy(function (t) {
                    this.ignoreBackdropClick ? this.ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide())
                }, this)), r && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !e) return;
            r ? this.$backdrop.one("bsTransitionEnd", e).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : e()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var s = function () {
                n.removeBackdrop(), e && e()
            };
            t.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", s).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : s()
        } else e && e()
    }, i.prototype.handleUpdate = function () {
        this.adjustDialog()
    }, i.prototype.adjustDialog = function () {
        var t = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ""
        })
    }, i.prototype.resetAdjustments = function () {
        this.$element.css({paddingLeft: "", paddingRight: ""})
    }, i.prototype.checkScrollbar = function () {
        var t = window.innerWidth;
        if (!t) {
            var e = document.documentElement.getBoundingClientRect();
            t = e.right - Math.abs(e.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < t, this.scrollbarWidth = this.measureScrollbar()
    }, i.prototype.setScrollbar = function () {
        var t = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", t + this.scrollbarWidth)
    }, i.prototype.resetScrollbar = function () {
        this.$body.css("padding-right", this.originalBodyPad)
    }, i.prototype.measureScrollbar = function () {
        var t = document.createElement("div");
        t.className = "modal-scrollbar-measure", this.$body.append(t);
        var e = t.offsetWidth - t.clientWidth;
        return this.$body[0].removeChild(t), e
    };
    var n = t.fn.modal;
    t.fn.modal = e, t.fn.modal.Constructor = i, t.fn.modal.noConflict = function () {
        return t.fn.modal = n, this
    }, t(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (i) {
        var n = t(this), o = n.attr("href"), r = t(n.attr("data-target") || o && o.replace(/.*(?=#[^\s]+$)/, "")),
            s = r.data("bs.modal") ? "toggle" : t.extend({remote: !/#/.test(o) && o}, r.data(), n.data());
        n.is("a") && i.preventDefault(), r.one("show.bs.modal", function (t) {
            t.isDefaultPrevented() || r.one("hidden.bs.modal", function () {
                n.is(":visible") && n.trigger("focus")
            })
        }), e.call(r, s, this)
    })
}(jQuery), function (t) {
    "use strict";
    var e = function (t, e) {
        this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", t, e)
    };
    e.VERSION = "3.3.7", e.TRANSITION_DURATION = 150, e.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {selector: "body", padding: 0}
    }, e.prototype.init = function (e, i, n) {
        if (this.enabled = !0, this.type = e, this.$element = t(i), this.options = this.getOptions(n), this.$viewport = this.options.viewport && t(t.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = {
                click: !1,
                hover: !1,
                focus: !1
            }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var o = this.options.trigger.split(" "), r = o.length; r--;) {
            var s = o[r];
            if ("click" == s) this.$element.on("click." + this.type, this.options.selector, t.proxy(this.toggle, this)); else if ("manual" != s) {
                var a = "hover" == s ? "mouseenter" : "focusin", l = "hover" == s ? "mouseleave" : "focusout";
                this.$element.on(a + "." + this.type, this.options.selector, t.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, t.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = t.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, e.prototype.getDefaults = function () {
        return e.DEFAULTS
    }, e.prototype.getOptions = function (e) {
        return (e = t.extend({}, this.getDefaults(), this.$element.data(), e)).delay && "number" == typeof e.delay && (e.delay = {
            show: e.delay,
            hide: e.delay
        }), e
    }, e.prototype.getDelegateOptions = function () {
        var e = {}, i = this.getDefaults();
        return this._options && t.each(this._options, function (t, n) {
            i[t] != n && (e[t] = n)
        }), e
    }, e.prototype.enter = function (e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        if (i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusin" == e.type ? "focus" : "hover"] = !0), i.tip().hasClass("in") || "in" == i.hoverState) i.hoverState = "in"; else {
            if (clearTimeout(i.timeout), i.hoverState = "in", !i.options.delay || !i.options.delay.show) return i.show();
            i.timeout = setTimeout(function () {
                "in" == i.hoverState && i.show()
            }, i.options.delay.show)
        }
    }, e.prototype.isInStateTrue = function () {
        for (var t in this.inState) if (this.inState[t]) return !0;
        return !1
    }, e.prototype.leave = function (e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        if (i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusout" == e.type ? "focus" : "hover"] = !1), !i.isInStateTrue()) {
            if (clearTimeout(i.timeout), i.hoverState = "out", !i.options.delay || !i.options.delay.hide) return i.hide();
            i.timeout = setTimeout(function () {
                "out" == i.hoverState && i.hide()
            }, i.options.delay.hide)
        }
    }, e.prototype.show = function () {
        var i = t.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(i);
            var n = t.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (i.isDefaultPrevented() || !n) return;
            var o = this, r = this.tip(), s = this.getUID(this.type);
            this.setContent(), r.attr("id", s), this.$element.attr("aria-describedby", s), this.options.animation && r.addClass("fade");
            var a = "function" == typeof this.options.placement ? this.options.placement.call(this, r[0], this.$element[0]) : this.options.placement,
                l = /\s?auto?\s?/i, c = l.test(a);
            c && (a = a.replace(l, "") || "top"), r.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(a).data("bs." + this.type, this), this.options.container ? r.appendTo(this.options.container) : r.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);
            var u = this.getPosition(), h = r[0].offsetWidth, d = r[0].offsetHeight;
            if (c) {
                var p = a, f = this.getPosition(this.$viewport);
                a = "bottom" == a && u.bottom + d > f.bottom ? "top" : "top" == a && u.top - d < f.top ? "bottom" : "right" == a && u.right + h > f.width ? "left" : "left" == a && u.left - h < f.left ? "right" : a, r.removeClass(p).addClass(a)
            }
            var g = this.getCalculatedOffset(a, u, h, d);
            this.applyPlacement(g, a);
            var v = function () {
                var t = o.hoverState;
                o.$element.trigger("shown.bs." + o.type), o.hoverState = null, "out" == t && o.leave(o)
            };
            t.support.transition && this.$tip.hasClass("fade") ? r.one("bsTransitionEnd", v).emulateTransitionEnd(e.TRANSITION_DURATION) : v()
        }
    }, e.prototype.applyPlacement = function (e, i) {
        var n = this.tip(), o = n[0].offsetWidth, r = n[0].offsetHeight, s = parseInt(n.css("margin-top"), 10),
            a = parseInt(n.css("margin-left"), 10);
        isNaN(s) && (s = 0), isNaN(a) && (a = 0), e.top += s, e.left += a, t.offset.setOffset(n[0], t.extend({
            using: function (t) {
                n.css({top: Math.round(t.top), left: Math.round(t.left)})
            }
        }, e), 0), n.addClass("in");
        var l = n[0].offsetWidth, c = n[0].offsetHeight;
        "top" == i && c != r && (e.top = e.top + r - c);
        var u = this.getViewportAdjustedDelta(i, e, l, c);
        u.left ? e.left += u.left : e.top += u.top;
        var h = /top|bottom/.test(i), d = h ? 2 * u.left - o + l : 2 * u.top - r + c,
            p = h ? "offsetWidth" : "offsetHeight";
        n.offset(e), this.replaceArrow(d, n[0][p], h)
    }, e.prototype.replaceArrow = function (t, e, i) {
        this.arrow().css(i ? "left" : "top", 50 * (1 - t / e) + "%").css(i ? "top" : "left", "")
    }, e.prototype.setContent = function () {
        var t = this.tip(), e = this.getTitle();
        t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e), t.removeClass("fade in top bottom left right")
    }, e.prototype.hide = function (i) {
        function n() {
            "in" != o.hoverState && r.detach(), o.$element && o.$element.removeAttr("aria-describedby").trigger("hidden.bs." + o.type), i && i()
        }

        var o = this, r = t(this.$tip), s = t.Event("hide.bs." + this.type);
        if (this.$element.trigger(s), !s.isDefaultPrevented()) return r.removeClass("in"), t.support.transition && r.hasClass("fade") ? r.one("bsTransitionEnd", n).emulateTransitionEnd(e.TRANSITION_DURATION) : n(), this.hoverState = null, this
    }, e.prototype.fixTitle = function () {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("data-original-title")) && t.attr("data-original-title", t.attr("title") || "").attr("title", "")
    }, e.prototype.hasContent = function () {
        return this.getTitle()
    }, e.prototype.getPosition = function (e) {
        var i = (e = e || this.$element)[0], n = "BODY" == i.tagName, o = i.getBoundingClientRect();
        null == o.width && (o = t.extend({}, o, {width: o.right - o.left, height: o.bottom - o.top}));
        var r = window.SVGElement && i instanceof window.SVGElement, s = n ? {top: 0, left: 0} : r ? null : e.offset(),
            a = {scroll: n ? document.documentElement.scrollTop || document.body.scrollTop : e.scrollTop()},
            l = n ? {width: t(window).width(), height: t(window).height()} : null;
        return t.extend({}, o, a, l, s)
    }, e.prototype.getCalculatedOffset = function (t, e, i, n) {
        return "bottom" == t ? {
            top: e.top + e.height,
            left: e.left + e.width / 2 - i / 2
        } : "top" == t ? {
            top: e.top - n,
            left: e.left + e.width / 2 - i / 2
        } : "left" == t ? {top: e.top + e.height / 2 - n / 2, left: e.left - i} : {
            top: e.top + e.height / 2 - n / 2,
            left: e.left + e.width
        }
    }, e.prototype.getViewportAdjustedDelta = function (t, e, i, n) {
        var o = {top: 0, left: 0};
        if (!this.$viewport) return o;
        var r = this.options.viewport && this.options.viewport.padding || 0, s = this.getPosition(this.$viewport);
        if (/right|left/.test(t)) {
            var a = e.top - r - s.scroll, l = e.top + r - s.scroll + n;
            a < s.top ? o.top = s.top - a : l > s.top + s.height && (o.top = s.top + s.height - l)
        } else {
            var c = e.left - r, u = e.left + r + i;
            c < s.left ? o.left = s.left - c : u > s.right && (o.left = s.left + s.width - u)
        }
        return o
    }, e.prototype.getTitle = function () {
        var t = this.$element, e = this.options;
        return t.attr("data-original-title") || ("function" == typeof e.title ? e.title.call(t[0]) : e.title)
    }, e.prototype.getUID = function (t) {
        do {
            t += ~~(1e6 * Math.random())
        } while (document.getElementById(t));
        return t
    }, e.prototype.tip = function () {
        if (!this.$tip && (this.$tip = t(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
        return this.$tip
    }, e.prototype.arrow = function () {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, e.prototype.enable = function () {
        this.enabled = !0
    }, e.prototype.disable = function () {
        this.enabled = !1
    }, e.prototype.toggleEnabled = function () {
        this.enabled = !this.enabled
    }, e.prototype.toggle = function (e) {
        var i = this;
        e && ((i = t(e.currentTarget).data("bs." + this.type)) || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i))), e ? (i.inState.click = !i.inState.click, i.isInStateTrue() ? i.enter(i) : i.leave(i)) : i.tip().hasClass("in") ? i.leave(i) : i.enter(i)
    }, e.prototype.destroy = function () {
        var t = this;
        clearTimeout(this.timeout), this.hide(function () {
            t.$element.off("." + t.type).removeData("bs." + t.type), t.$tip && t.$tip.detach(), t.$tip = null, t.$arrow = null, t.$viewport = null, t.$element = null
        })
    };
    var i = t.fn.tooltip;
    t.fn.tooltip = function (i) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.tooltip"), r = "object" == typeof i && i;
            !o && /destroy|hide/.test(i) || (o || n.data("bs.tooltip", o = new e(this, r)), "string" == typeof i && o[i]())
        })
    }, t.fn.tooltip.Constructor = e, t.fn.tooltip.noConflict = function () {
        return t.fn.tooltip = i, this
    }
}(jQuery), function (t) {
    "use strict";
    var e = function (t, e) {
        this.init("popover", t, e)
    };
    if (!t.fn.tooltip) throw new Error("Popover requires tooltip.js");
    e.VERSION = "3.3.7", e.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), e.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype), e.prototype.constructor = e, e.prototype.getDefaults = function () {
        return e.DEFAULTS
    }, e.prototype.setContent = function () {
        var t = this.tip(), e = this.getTitle(), i = this.getContent();
        t.find(".popover-title")[this.options.html ? "html" : "text"](e), t.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof i ? "html" : "append" : "text"](i), t.removeClass("fade top bottom left right in"), t.find(".popover-title").html() || t.find(".popover-title").hide()
    }, e.prototype.hasContent = function () {
        return this.getTitle() || this.getContent()
    }, e.prototype.getContent = function () {
        var t = this.$element, e = this.options;
        return t.attr("data-content") || ("function" == typeof e.content ? e.content.call(t[0]) : e.content)
    }, e.prototype.arrow = function () {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    };
    var i = t.fn.popover;
    t.fn.popover = function (i) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.popover"), r = "object" == typeof i && i;
            !o && /destroy|hide/.test(i) || (o || n.data("bs.popover", o = new e(this, r)), "string" == typeof i && o[i]())
        })
    }, t.fn.popover.Constructor = e, t.fn.popover.noConflict = function () {
        return t.fn.popover = i, this
    }
}(jQuery), function (t) {
    "use strict";

    function e(i, n) {
        this.$body = t(document.body), this.$scrollElement = t(t(i).is(document.body) ? window : i), this.options = t.extend({}, e.DEFAULTS, n), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", t.proxy(this.process, this)), this.refresh(), this.process()
    }

    function i(i) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.scrollspy"), r = "object" == typeof i && i;
            o || n.data("bs.scrollspy", o = new e(this, r)), "string" == typeof i && o[i]()
        })
    }

    e.VERSION = "3.3.7", e.DEFAULTS = {offset: 10}, e.prototype.getScrollHeight = function () {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, e.prototype.refresh = function () {
        var e = this, i = "offset", n = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), t.isWindow(this.$scrollElement[0]) || (i = "position", n = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function () {
            var e = t(this), o = e.data("target") || e.attr("href"), r = /^#./.test(o) && t(o);
            return r && r.length && r.is(":visible") && [[r[i]().top + n, o]] || null
        }).sort(function (t, e) {
            return t[0] - e[0]
        }).each(function () {
            e.offsets.push(this[0]), e.targets.push(this[1])
        })
    }, e.prototype.process = function () {
        var t, e = this.$scrollElement.scrollTop() + this.options.offset, i = this.getScrollHeight(),
            n = this.options.offset + i - this.$scrollElement.height(), o = this.offsets, r = this.targets,
            s = this.activeTarget;
        if (this.scrollHeight != i && this.refresh(), e >= n) return s != (t = r[r.length - 1]) && this.activate(t);
        if (s && e < o[0]) return this.activeTarget = null, this.clear();
        for (t = o.length; t--;) s != r[t] && e >= o[t] && (void 0 === o[t + 1] || e < o[t + 1]) && this.activate(r[t])
    }, e.prototype.activate = function (e) {
        this.activeTarget = e, this.clear();
        var i = this.selector + '[data-target="' + e + '"],' + this.selector + '[href="' + e + '"]',
            n = t(i).parents("li").addClass("active");
        n.parent(".dropdown-menu").length && (n = n.closest("li.dropdown").addClass("active")), n.trigger("activate.bs.scrollspy")
    }, e.prototype.clear = function () {
        t(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    };
    var n = t.fn.scrollspy;
    t.fn.scrollspy = i, t.fn.scrollspy.Constructor = e, t.fn.scrollspy.noConflict = function () {
        return t.fn.scrollspy = n, this
    }, t(window).on("load.bs.scrollspy.data-api", function () {
        t('[data-spy="scroll"]').each(function () {
            var e = t(this);
            i.call(e, e.data())
        })
    })
}(jQuery), function (t) {
    "use strict";

    function e(e) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.tab");
            o || n.data("bs.tab", o = new i(this)), "string" == typeof e && o[e]()
        })
    }

    var i = function (e) {
        this.element = t(e)
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 150, i.prototype.show = function () {
        var e = this.element, i = e.closest("ul:not(.dropdown-menu)"), n = e.data("target");
        if (n || (n = (n = e.attr("href")) && n.replace(/.*(?=#[^\s]*$)/, "")), !e.parent("li").hasClass("active")) {
            var o = i.find(".active:last a"), r = t.Event("hide.bs.tab", {relatedTarget: e[0]}),
                s = t.Event("show.bs.tab", {relatedTarget: o[0]});
            if (o.trigger(r), e.trigger(s), !s.isDefaultPrevented() && !r.isDefaultPrevented()) {
                var a = t(n);
                this.activate(e.closest("li"), i), this.activate(a, a.parent(), function () {
                    o.trigger({type: "hidden.bs.tab", relatedTarget: e[0]}), e.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: o[0]
                    })
                })
            }
        }
    }, i.prototype.activate = function (e, n, o) {
        function r() {
            s.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), a ? (e[0].offsetWidth, e.addClass("in")) : e.removeClass("fade"), e.parent(".dropdown-menu").length && e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), o && o()
        }

        var s = n.find("> .active"),
            a = o && t.support.transition && (s.length && s.hasClass("fade") || !!n.find("> .fade").length);
        s.length && a ? s.one("bsTransitionEnd", r).emulateTransitionEnd(i.TRANSITION_DURATION) : r(), s.removeClass("in")
    };
    var n = t.fn.tab;
    t.fn.tab = e, t.fn.tab.Constructor = i, t.fn.tab.noConflict = function () {
        return t.fn.tab = n, this
    };
    var o = function (i) {
        i.preventDefault(), e.call(t(this), "show")
    };
    t(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', o).on("click.bs.tab.data-api", '[data-toggle="pill"]', o)
}(jQuery), function (t) {
    "use strict";

    function e(e) {
        return this.each(function () {
            var n = t(this), o = n.data("bs.affix"), r = "object" == typeof e && e;
            o || n.data("bs.affix", o = new i(this, r)), "string" == typeof e && o[e]()
        })
    }

    var i = function (e, n) {
        this.options = t.extend({}, i.DEFAULTS, n), this.$target = t(this.options.target).on("scroll.bs.affix.data-api", t.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", t.proxy(this.checkPositionWithEventLoop, this)), this.$element = t(e), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
    };
    i.VERSION = "3.3.7", i.RESET = "affix affix-top affix-bottom", i.DEFAULTS = {
        offset: 0,
        target: window
    }, i.prototype.getState = function (t, e, i, n) {
        var o = this.$target.scrollTop(), r = this.$element.offset(), s = this.$target.height();
        if (null != i && "top" == this.affixed) return o < i && "top";
        if ("bottom" == this.affixed) return null != i ? !(o + this.unpin <= r.top) && "bottom" : !(o + s <= t - n) && "bottom";
        var a = null == this.affixed, l = a ? o : r.top;
        return null != i && o <= i ? "top" : null != n && l + (a ? s : e) >= t - n && "bottom"
    }, i.prototype.getPinnedOffset = function () {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(i.RESET).addClass("affix");
        var t = this.$target.scrollTop(), e = this.$element.offset();
        return this.pinnedOffset = e.top - t
    }, i.prototype.checkPositionWithEventLoop = function () {
        setTimeout(t.proxy(this.checkPosition, this), 1)
    }, i.prototype.checkPosition = function () {
        if (this.$element.is(":visible")) {
            var e = this.$element.height(), n = this.options.offset, o = n.top, r = n.bottom,
                s = Math.max(t(document).height(), t(document.body).height());
            "object" != typeof n && (r = o = n), "function" == typeof o && (o = n.top(this.$element)), "function" == typeof r && (r = n.bottom(this.$element));
            var a = this.getState(s, e, o, r);
            if (this.affixed != a) {
                null != this.unpin && this.$element.css("top", "");
                var l = "affix" + (a ? "-" + a : ""), c = t.Event(l + ".bs.affix");
                if (this.$element.trigger(c), c.isDefaultPrevented()) return;
                this.affixed = a, this.unpin = "bottom" == a ? this.getPinnedOffset() : null, this.$element.removeClass(i.RESET).addClass(l).trigger(l.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == a && this.$element.offset({top: s - e - r})
        }
    };
    var n = t.fn.affix;
    t.fn.affix = e, t.fn.affix.Constructor = i, t.fn.affix.noConflict = function () {
        return t.fn.affix = n, this
    }, t(window).on("load", function () {
        t('[data-spy="affix"]').each(function () {
            var i = t(this), n = i.data();
            n.offset = n.offset || {}, null != n.offsetBottom && (n.offset.bottom = n.offsetBottom), null != n.offsetTop && (n.offset.top = n.offsetTop), e.call(i, n)
        })
    })
}(jQuery), function () {
    var t, e, i, n, o, r = function (t, e) {
        return function () {
            return t.apply(e, arguments)
        }
    }, s = [].indexOf || function (t) {
        for (var e = 0, i = this.length; i > e; e++) if (e in this && this[e] === t) return e;
        return -1
    };
    e = function () {
        function t() {
        }

        return t.prototype.extend = function (t, e) {
            var i, n;
            for (i in e) n = e[i], null == t[i] && (t[i] = n);
            return t
        }, t.prototype.isMobile = function (t) {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(t)
        }, t.prototype.createEvent = function (t, e, i, n) {
            var o;
            return null == e && (e = !1), null == i && (i = !1), null == n && (n = null), null != document.createEvent ? (o = document.createEvent("CustomEvent")).initCustomEvent(t, e, i, n) : null != document.createEventObject ? (o = document.createEventObject(), o.eventType = t) : o.eventName = t, o
        }, t.prototype.emitEvent = function (t, e) {
            return null != t.dispatchEvent ? t.dispatchEvent(e) : e in (null != t) ? t[e]() : "on" + e in (null != t) ? t["on" + e]() : void 0
        }, t.prototype.addEvent = function (t, e, i) {
            return null != t.addEventListener ? t.addEventListener(e, i, !1) : null != t.attachEvent ? t.attachEvent("on" + e, i) : t[e] = i
        }, t.prototype.removeEvent = function (t, e, i) {
            return null != t.removeEventListener ? t.removeEventListener(e, i, !1) : null != t.detachEvent ? t.detachEvent("on" + e, i) : delete t[e]
        }, t.prototype.innerHeight = function () {
            return "innerHeight" in window ? window.innerHeight : document.documentElement.clientHeight
        }, t
    }(), i = this.WeakMap || this.MozWeakMap || (i = function () {
        function t() {
            this.keys = [], this.values = []
        }

        return t.prototype.get = function (t) {
            var e, i, n, o;
            for (e = i = 0, n = (o = this.keys).length; n > i; e = ++i) if (o[e] === t) return this.values[e]
        }, t.prototype.set = function (t, e) {
            var i, n, o, r;
            for (i = n = 0, o = (r = this.keys).length; o > n; i = ++n) if (r[i] === t) return void(this.values[i] = e);
            return this.keys.push(t), this.values.push(e)
        }, t
    }()), t = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (t = function () {
        function t() {
            "undefined" != typeof console && null !== console && console.warn("MutationObserver is not supported by your browser."), "undefined" != typeof console && null !== console && console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")
        }

        return t.notSupported = !0, t.prototype.observe = function () {
        }, t
    }()), n = this.getComputedStyle || function (t) {
        return this.getPropertyValue = function (e) {
            var i;
            return "float" === e && (e = "styleFloat"), o.test(e) && e.replace(o, function (t, e) {
                return e.toUpperCase()
            }), (null != (i = t.currentStyle) ? i[e] : void 0) || null
        }, this
    }, o = /(\-([a-z]){1})/g, this.WOW = function () {
        function o(t) {
            null == t && (t = {}), this.scrollCallback = r(this.scrollCallback, this), this.scrollHandler = r(this.scrollHandler, this), this.resetAnimation = r(this.resetAnimation, this), this.start = r(this.start, this), this.scrolled = !0, this.config = this.util().extend(t, this.defaults), this.animationNameCache = new i, this.wowEvent = this.util().createEvent(this.config.boxClass)
        }

        return o.prototype.defaults = {
            boxClass: "wow",
            animateClass: "animated",
            offset: 0,
            mobile: !0,
            live: !0,
            callback: null
        }, o.prototype.init = function () {
            var t;
            return this.element = window.document.documentElement, "interactive" === (t = document.readyState) || "complete" === t ? this.start() : this.util().addEvent(document, "DOMContentLoaded", this.start), this.finished = []
        }, o.prototype.start = function () {
            var e, i, n, o;
            if (this.stopped = !1, this.boxes = function () {
                    var t, i, n, o;
                    for (o = [], t = 0, i = (n = this.element.querySelectorAll("." + this.config.boxClass)).length; i > t; t++) e = n[t], o.push(e);
                    return o
                }.call(this), this.all = function () {
                    var t, i, n, o;
                    for (o = [], t = 0, i = (n = this.boxes).length; i > t; t++) e = n[t], o.push(e);
                    return o
                }.call(this), this.boxes.length) if (this.disabled()) this.resetStyle(); else for (o = this.boxes, i = 0, n = o.length; n > i; i++) e = o[i], this.applyStyle(e, !0);
            return this.disabled() || (this.util().addEvent(window, "scroll", this.scrollHandler), this.util().addEvent(window, "resize", this.scrollHandler), this.interval = setInterval(this.scrollCallback, 50)), this.config.live ? new t(function (t) {
                return function (e) {
                    var i, n, o, r, s;
                    for (s = [], i = 0, n = e.length; n > i; i++) r = e[i], s.push(function () {
                        var t, e, i, n;
                        for (n = [], t = 0, e = (i = r.addedNodes || []).length; e > t; t++) o = i[t], n.push(this.doSync(o));
                        return n
                    }.call(t));
                    return s
                }
            }(this)).observe(document.body, {childList: !0, subtree: !0}) : void 0
        }, o.prototype.stop = function () {
            return this.stopped = !0, this.util().removeEvent(window, "scroll", this.scrollHandler), this.util().removeEvent(window, "resize", this.scrollHandler), null != this.interval ? clearInterval(this.interval) : void 0
        }, o.prototype.sync = function () {
            return t.notSupported ? this.doSync(this.element) : void 0
        }, o.prototype.doSync = function (t) {
            var e, i, n, o, r;
            if (null == t && (t = this.element), 1 === t.nodeType) {
                for (r = [], i = 0, n = (o = (t = t.parentNode || t).querySelectorAll("." + this.config.boxClass)).length; n > i; i++) e = o[i], s.call(this.all, e) < 0 ? (this.boxes.push(e), this.all.push(e), this.stopped || this.disabled() ? this.resetStyle() : this.applyStyle(e, !0), r.push(this.scrolled = !0)) : r.push(void 0);
                return r
            }
        }, o.prototype.show = function (t) {
            return this.applyStyle(t), t.className = t.className + " " + this.config.animateClass, null != this.config.callback && this.config.callback(t), this.util().emitEvent(t, this.wowEvent), this.util().addEvent(t, "animationend", this.resetAnimation), this.util().addEvent(t, "oanimationend", this.resetAnimation), this.util().addEvent(t, "webkitAnimationEnd", this.resetAnimation), this.util().addEvent(t, "MSAnimationEnd", this.resetAnimation), t
        }, o.prototype.applyStyle = function (t, e) {
            var i, n, o;
            return n = t.getAttribute("data-wow-duration"), i = t.getAttribute("data-wow-delay"), o = t.getAttribute("data-wow-iteration"), this.animate(function (r) {
                return function () {
                    return r.customStyle(t, e, n, i, o)
                }
            }(this))
        }, o.prototype.animate = "requestAnimationFrame" in window ? function (t) {
            return window.requestAnimationFrame(t)
        } : function (t) {
            return t()
        }, o.prototype.resetStyle = function () {
            var t, e, i, n, o;
            for (o = [], e = 0, i = (n = this.boxes).length; i > e; e++) t = n[e], o.push(t.style.visibility = "visible");
            return o
        }, o.prototype.resetAnimation = function (t) {
            var e;
            return t.type.toLowerCase().indexOf("animationend") >= 0 ? (e = t.target || t.srcElement, e.className = e.className.replace(this.config.animateClass, "").trim()) : void 0
        }, o.prototype.customStyle = function (t, e, i, n, o) {
            return e && this.cacheAnimationName(t), t.style.visibility = e ? "hidden" : "visible", i && this.vendorSet(t.style, {animationDuration: i}), n && this.vendorSet(t.style, {animationDelay: n}), o && this.vendorSet(t.style, {animationIterationCount: o}), this.vendorSet(t.style, {animationName: e ? "none" : this.cachedAnimationName(t)}), t
        }, o.prototype.vendors = ["moz", "webkit"], o.prototype.vendorSet = function (t, e) {
            var i, n, o, r;
            n = [];
            for (i in e) o = e[i], t["" + i] = o, n.push(function () {
                var e, n, s, a;
                for (a = [], e = 0, n = (s = this.vendors).length; n > e; e++) r = s[e], a.push(t["" + r + i.charAt(0).toUpperCase() + i.substr(1)] = o);
                return a
            }.call(this));
            return n
        }, o.prototype.vendorCSS = function (t, e) {
            var i, o, r, s, a, l;
            for (s = (a = n(t)).getPropertyCSSValue(e), i = 0, o = (r = this.vendors).length; o > i; i++) l = r[i], s = s || a.getPropertyCSSValue("-" + l + "-" + e);
            return s
        }, o.prototype.animationName = function (t) {
            var e;
            try {
                e = this.vendorCSS(t, "animation-name").cssText
            } catch (i) {
                e = n(t).getPropertyValue("animation-name")
            }
            return "none" === e ? "" : e
        }, o.prototype.cacheAnimationName = function (t) {
            return this.animationNameCache.set(t, this.animationName(t))
        }, o.prototype.cachedAnimationName = function (t) {
            return this.animationNameCache.get(t)
        }, o.prototype.scrollHandler = function () {
            return this.scrolled = !0
        }, o.prototype.scrollCallback = function () {
            var t;
            return !this.scrolled || (this.scrolled = !1, this.boxes = function () {
                var e, i, n, o;
                for (o = [], e = 0, i = (n = this.boxes).length; i > e; e++) (t = n[e]) && (this.isVisible(t) ? this.show(t) : o.push(t));
                return o
            }.call(this), this.boxes.length || this.config.live) ? void 0 : this.stop()
        }, o.prototype.offsetTop = function (t) {
            for (var e; void 0 === t.offsetTop;) t = t.parentNode;
            for (e = t.offsetTop; t = t.offsetParent;) e += t.offsetTop;
            return e
        }, o.prototype.isVisible = function (t) {
            var e, i, n, o, r;
            return i = t.getAttribute("data-wow-offset") || this.config.offset, r = window.pageYOffset, o = r + Math.min(this.element.clientHeight, this.util().innerHeight()) - i, n = this.offsetTop(t), e = n + t.clientHeight, o >= n && e >= r
        }, o.prototype.util = function () {
            return null != this._util ? this._util : this._util = new e
        }, o.prototype.disabled = function () {
            return !this.config.mobile && this.util().isMobile(navigator.userAgent)
        }, o
    }()
}.call(this), function (t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], function (i) {
        return e(i, t, t.document)
    }) : "object" == typeof module && module.exports ? module.exports = e(require("jquery"), t, t.document) : e(jQuery, t, t.document)
}("undefined" != typeof window ? window : this, function (t, e, i, n) {
    "use strict";

    function o(i, n, o, r) {
        if (m === i && (o = !1), !0 === _) return !0;
        if (p[i]) {
            if (C = !1, o && O.before(i, f), y = 1, P = d[i], !1 === A && m > i && !1 === r && g[i] && (y = parseInt(f[i].outerHeight() / w.height()), P = parseInt(d[i]) + (f[i].outerHeight() - w.height())), O.updateHash && O.sectionName && (!0 !== A || 0 !== i)) if (history.pushState) try {
                history.replaceState(null, null, p[i])
            } catch (t) {
                e.console && console.warn("Scrollify warning: Page must be hosted to manipulate the hash value.")
            } else e.location.hash = p[i];
            if (A && (O.afterRender(), A = !1), m = i, n) t(O.target).stop().scrollTop(P), o && O.after(i, f); else {
                if (k = !0, t().velocity ? t(O.target).stop().velocity("scroll", {
                        duration: O.scrollSpeed,
                        easing: O.easing,
                        offset: P,
                        mobileHA: !1
                    }) : t(O.target).stop().animate({scrollTop: P}, O.scrollSpeed, O.easing), e.location.hash.length && O.sectionName && e.console) try {
                    t(e.location.hash).length && console.warn("Scrollify warning: ID matches hash value - this will cause the page to anchor.")
                } catch (t) {
                }
                t(O.target).promise().done(function () {
                    k = !1, A = !1, o && O.after(i, f)
                })
            }
        }
    }

    function r(t) {
        function e(e) {
            for (var i = 0, n = t.slice(Math.max(t.length - e, 1)), o = 0; o < n.length; o++) i += n[o];
            return Math.ceil(i / e)
        }

        return e(10) >= e(70)
    }

    function s(t, e) {
        for (var i = p.length; i >= 0; i--) "string" == typeof t ? p[i] === t && (v = i, o(i, e, !0, !0)) : i === t && (v = i, o(i, e, !0, !0))
    }

    var a, l, c, u, h, d = [], p = [], f = [], g = [], v = 0, m = 0, y = 1, b = !1, w = t(e), x = w.scrollTop(), C = !1,
        k = !1, T = !1, _ = !1, S = [], E = (new Date).getTime(), A = !0, $ = !1, P = 0,
        D = "onwheel" in i ? "wheel" : i.onmousewheel !== n ? "mousewheel" : "DOMMouseScroll", O = {
            section: ".section",
            sectionName: "section-name",
            interstitialSection: "",
            easing: "easeOutExpo",
            scrollSpeed: 1100,
            offset: 0,
            scrollbars: !0,
            target: "html,body",
            standardScrollElements: !1,
            setHeights: !0,
            overflowScroll: !0,
            updateHash: !0,
            touchScroll: !0,
            before: function () {
            },
            after: function () {
            },
            afterResize: function () {
            },
            afterRender: function () {
            }
        }, M = function (n) {
            function s(e) {
                t().velocity ? t(O.target).stop().velocity("scroll", {
                    duration: O.scrollSpeed,
                    easing: O.easing,
                    offset: e,
                    mobileHA: !1
                }) : t(O.target).stop().animate({scrollTop: e}, O.scrollSpeed, O.easing)
            }

            function m(e) {
                e && (x = w.scrollTop());
                var i = O.section;
                g = [], O.interstitialSection.length && (i += "," + O.interstitialSection), !1 === O.scrollbars && (O.overflowScroll = !1), t(i).each(function (e) {
                    var i = t(this);
                    O.setHeights ? i.is(O.interstitialSection) ? g[e] = !1 : i.css("height", "auto").outerHeight() < w.height() || "hidden" === i.css("overflow") ? (i.css({height: w.height()}), g[e] = !1) : (i.css({height: i.height()}), O.overflowScroll ? g[e] = !0 : g[e] = !1) : i.outerHeight() < w.height() || !1 === O.overflowScroll ? g[e] = !1 : g[e] = !0
                }), e && w.scrollTop(x)
            }

            function A(i, n) {
                var r = O.section;
                O.interstitialSection.length && (r += "," + O.interstitialSection), d = [], p = [], f = [], t(r).each(function (i) {
                    var n = t(this);
                    d[i] = i > 0 ? parseInt(n.offset().top) + O.offset : parseInt(n.offset().top), O.sectionName && n.data(O.sectionName) ? p[i] = "#" + n.data(O.sectionName).toString().replace(/ /g, "-") : !1 === n.is(O.interstitialSection) ? p[i] = "#" + (i + 1) : (p[i] = "#", i === t(r).length - 1 && i > 1 && (d[i] = d[i - 1] + (parseInt(t(t(r)[i - 1]).outerHeight()) - parseInt(t(e).height())) + parseInt(n.outerHeight()))), f[i] = n;
                    try {
                        t(p[i]).length && e.console && console.warn("Scrollify warning: Section names can't match IDs - this will cause the browser to anchor.")
                    } catch (t) {
                    }
                    e.location.hash === p[i] && (v = i, b = !0)
                }), !0 === i && o(v, !1, !1, !1)
            }

            function P() {
                return !g[v] || !((x = w.scrollTop()) > parseInt(d[v]))
            }

            function M() {
                return !g[v] || !((x = w.scrollTop()) < parseInt(d[v]) + (f[v].outerHeight() - w.height()) - 28)
            }

            $ = !0, t.easing.easeOutExpo = function (t, e, i, n, o) {
                return e == o ? i + n : n * (1 - Math.pow(2, -10 * e / o)) + i
            }, c = {
                handleMousedown: function () {
                    if (!0 === _) return !0;
                    C = !1, T = !1
                }, handleMouseup: function () {
                    if (!0 === _) return !0;
                    C = !0, T && c.calculateNearest(!1, !0)
                }, handleScroll: function () {
                    if (!0 === _) return !0;
                    a && clearTimeout(a), a = setTimeout(function () {
                        if (T = !0, !1 === C) return !1;
                        C = !1, c.calculateNearest(!1, !0)
                    }, 200)
                }, calculateNearest: function (t, e) {
                    x = w.scrollTop();
                    for (var i, n = 1, r = d.length, s = 0, a = Math.abs(d[0] - x); n < r; n++) (i = Math.abs(d[n] - x)) < a && (a = i, s = n);
                    (M() && s > v || P()) && (v = s, o(s, t, e, !1))
                }, wheelHandler: function (i) {
                    if (!0 === _) return !0;
                    if (O.standardScrollElements && (t(i.target).is(O.standardScrollElements) || t(i.target).closest(O.standardScrollElements).length)) return !0;
                    g[v] || i.preventDefault();
                    var n = (new Date).getTime(),
                        s = (i = i || e.event).originalEvent.wheelDelta || -i.originalEvent.deltaY || -i.originalEvent.detail,
                        a = Math.max(-1, Math.min(1, s));
                    if (S.length > 149 && S.shift(), S.push(Math.abs(s)), n - E > 200 && (S = []), E = n, k) return !1;
                    if (a < 0) {
                        if (v < d.length - 1 && M()) {
                            if (!r(S)) return !1;
                            i.preventDefault(), k = !0, o(++v, !1, !0, !1)
                        }
                    } else if (a > 0 && v > 0 && P()) {
                        if (!r(S)) return !1;
                        i.preventDefault(), k = !0, o(--v, !1, !0, !1)
                    }
                }, keyHandler: function (t) {
                    return !0 === _ || !1 === i.activeElement.readOnly || !0 !== k && void(38 == t.keyCode || 33 == t.keyCode ? v > 0 && P() && (t.preventDefault(), o(--v, !1, !0, !1)) : 40 != t.keyCode && 34 != t.keyCode || v < d.length - 1 && M() && (t.preventDefault(), o(++v, !1, !0, !1)))
                }, init: function () {
                    O.scrollbars ? (w.on("mousedown", c.handleMousedown), w.on("mouseup", c.handleMouseup), w.on("scroll", c.handleScroll)) : t("body").css({overflow: "hidden"}), w.on(D, c.wheelHandler), w.on("keydown", c.keyHandler)
                }
            }, u = {
                touches: {
                    touchstart: {y: -1, x: -1},
                    touchmove: {y: -1, x: -1},
                    touchend: !1,
                    direction: "undetermined"
                }, options: {distance: 30, timeGap: 800, timeStamp: (new Date).getTime()}, touchHandler: function (e) {
                    if (!0 === _) return !0;
                    if (O.standardScrollElements && (t(e.target).is(O.standardScrollElements) || t(e.target).closest(O.standardScrollElements).length)) return !0;
                    var i;
                    if (void 0 !== e && void 0 !== e.touches) switch (i = e.touches[0], e.type) {
                        case"touchstart":
                            u.touches.touchstart.y = i.pageY, u.touches.touchmove.y = -1, u.touches.touchstart.x = i.pageX, u.touches.touchmove.x = -1, u.options.timeStamp = (new Date).getTime(), u.touches.touchend = !1;
                        case"touchmove":
                            u.touches.touchmove.y = i.pageY, u.touches.touchmove.x = i.pageX, u.touches.touchstart.y !== u.touches.touchmove.y && Math.abs(u.touches.touchstart.y - u.touches.touchmove.y) > Math.abs(u.touches.touchstart.x - u.touches.touchmove.x) && (e.preventDefault(), u.touches.direction = "y", u.options.timeStamp + u.options.timeGap < (new Date).getTime() && 0 == u.touches.touchend && (u.touches.touchend = !0, u.touches.touchstart.y > -1 && Math.abs(u.touches.touchmove.y - u.touches.touchstart.y) > u.options.distance && (u.touches.touchstart.y < u.touches.touchmove.y ? u.up() : u.down())));
                            break;
                        case"touchend":
                            !1 === u.touches[e.type] && (u.touches[e.type] = !0, u.touches.touchstart.y > -1 && u.touches.touchmove.y > -1 && "y" === u.touches.direction && (Math.abs(u.touches.touchmove.y - u.touches.touchstart.y) > u.options.distance && (u.touches.touchstart.y < u.touches.touchmove.y ? u.up() : u.down()), u.touches.touchstart.y = -1, u.touches.touchstart.x = -1, u.touches.direction = "undetermined"))
                    }
                }, down: function () {
                    v < d.length && (M() && v < d.length - 1 ? o(++v, !1, !0, !1) : Math.floor(f[v].height() / w.height()) > y ? (s(parseInt(d[v]) + w.height() * y), y += 1) : s(parseInt(d[v]) + (f[v].outerHeight() - w.height())))
                }, up: function () {
                    v >= 0 && (P() && v > 0 ? o(--v, !1, !0, !1) : y > 2 ? (y -= 1, s(parseInt(d[v]) + w.height() * y)) : (y = 1, s(parseInt(d[v]))))
                }, init: function () {
                    i.addEventListener && O.touchScroll && (i.addEventListener("touchstart", u.touchHandler, !1), i.addEventListener("touchmove", u.touchHandler, !1), i.addEventListener("touchend", u.touchHandler, !1))
                }
            }, h = {
                refresh: function (t, e) {
                    clearTimeout(l), l = setTimeout(function () {
                        m(!0), A(e), t && O.afterResize()
                    }, 400)
                }, handleUpdate: function () {
                    h.refresh(!1, !1)
                }, handleResize: function () {
                    h.refresh(!0, !1)
                }, handleOrientation: function () {
                    h.refresh(!0, !0)
                }
            }, O = t.extend(O, n), m(!1), A(!1), !0 === b ? o(v, !1, !0, !0) : setTimeout(function () {
                c.calculateNearest(!0, !1)
            }, 200), d.length && (c.init(), u.init(), w.on("resize", h.handleResize), i.addEventListener && e.addEventListener("orientationchange", h.handleOrientation, !1))
        };
    return M.move = function (e) {
        if (e === n) return !1;
        e.originalEvent && (e = t(this).attr("href")), s(e, !1)
    }, M.instantMove = function (t) {
        if (t === n) return !1;
        s(t, !0)
    }, M.next = function () {
        v < p.length && o(v += 1, !1, !0, !0)
    }, M.previous = function () {
        v > 0 && o(v -= 1, !1, !0, !0)
    }, M.instantNext = function () {
        v < p.length && o(v += 1, !0, !0, !0)
    }, M.instantPrevious = function () {
        v > 0 && o(v -= 1, !0, !0, !0)
    }, M.destroy = function () {
        if (!$) return !1;
        O.setHeights && t(O.section).each(function () {
            t(this).css("height", "auto")
        }), w.off("resize", h.handleResize), O.scrollbars && (w.off("mousedown", c.handleMousedown), w.off("mouseup", c.handleMouseup), w.off("scroll", c.handleScroll)), w.off(D, c.wheelHandler), w.off("keydown", c.keyHandler), i.addEventListener && O.touchScroll && (i.removeEventListener("touchstart", u.touchHandler, !1), i.removeEventListener("touchmove", u.touchHandler, !1), i.removeEventListener("touchend", u.touchHandler, !1)), d = [], p = [], f = [], g = []
    }, M.update = function () {
        if (!$) return !1;
        h.handleUpdate()
    }, M.current = function () {
        return f[v]
    }, M.currentIndex = function () {
        return v
    }, M.disable = function () {
        _ = !0
    }, M.enable = function () {
        _ = !1, $ && c.calculateNearest(!1, !1)
    }, M.isDisabled = function () {
        return _
    }, M.setOptions = function (i) {
        if (!$) return !1;
        "object" == typeof i ? (O = t.extend(O, i), h.handleUpdate()) : e.console && console.warn("Scrollify warning: setOptions expects an object.")
    }, t.scrollify = M, M
}), function (t, e, i) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(e || i)
}(function (t) {
    var e = function (e, i, n) {
        var o = {
            invalid: [], getCaret: function () {
                try {
                    var t, i = 0, n = e.get(0), r = document.selection, s = n.selectionStart;
                    return r && -1 === navigator.appVersion.indexOf("MSIE 10") ? ((t = r.createRange()).moveStart("character", -o.val().length), i = t.text.length) : (s || "0" === s) && (i = s), i
                } catch (t) {
                }
            }, setCaret: function (t) {
                try {
                    if (e.is(":focus")) {
                        var i, n = e.get(0);
                        n.setSelectionRange ? n.setSelectionRange(t, t) : ((i = n.createTextRange()).collapse(!0), i.moveEnd("character", t), i.moveStart("character", t), i.select())
                    }
                } catch (t) {
                }
            }, events: function () {
                e.on("keydown.mask", function (t) {
                    e.data("mask-keycode", t.keyCode || t.which), e.data("mask-previus-value", e.val()), e.data("mask-previus-caret-pos", o.getCaret()), o.maskDigitPosMapOld = o.maskDigitPosMap
                }).on(t.jMaskGlobals.useInput ? "input.mask" : "keyup.mask", o.behaviour).on("paste.mask drop.mask", function () {
                    setTimeout(function () {
                        e.keydown().keyup()
                    }, 100)
                }).on("change.mask", function () {
                    e.data("changed", !0)
                }).on("blur.mask", function () {
                    a === o.val() || e.data("changed") || e.trigger("change"), e.data("changed", !1)
                }).on("blur.mask", function () {
                    a = o.val()
                }).on("focus.mask", function (e) {
                    !0 === n.selectOnFocus && t(e.target).select()
                }).on("focusout.mask", function () {
                    n.clearIfNotMatch && !r.test(o.val()) && o.val("")
                })
            }, getRegexMask: function () {
                for (var t, e, n, o, r, a, l = [], c = 0; c < i.length; c++) (t = s.translation[i.charAt(c)]) ? (e = t.pattern.toString().replace(/.{1}$|^.{1}/g, ""), n = t.optional, (o = t.recursive) ? (l.push(i.charAt(c)), r = {
                    digit: i.charAt(c),
                    pattern: e
                }) : l.push(n || o ? e + "?" : e)) : l.push(i.charAt(c).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&"));
                return a = l.join(""), r && (a = a.replace(new RegExp("(" + r.digit + "(.*" + r.digit + ")?)"), "($1)?").replace(new RegExp(r.digit, "g"), r.pattern)), new RegExp(a)
            }, destroyEvents: function () {
                e.off(["input", "keydown", "keyup", "paste", "drop", "blur", "focusout", ""].join(".mask "))
            }, val: function (t) {
                var i, n = e.is("input") ? "val" : "text";
                return arguments.length > 0 ? (e[n]() !== t && e[n](t), i = e) : i = e[n](), i
            }, calculateCaretPosition: function () {
                var t = e.data("mask-previus-value") || "", i = o.getMasked(), n = o.getCaret();
                if (t !== i) {
                    var r = e.data("mask-previus-caret-pos") || 0, s = i.length, a = t.length, l = 0, c = 0, u = 0,
                        h = 0, d = 0;
                    for (d = n; d < s && o.maskDigitPosMap[d]; d++) c++;
                    for (d = n - 1; d >= 0 && o.maskDigitPosMap[d]; d--) l++;
                    for (d = n - 1; d >= 0; d--) o.maskDigitPosMap[d] && u++;
                    for (d = r - 1; d >= 0; d--) o.maskDigitPosMapOld[d] && h++;
                    if (n > a) n = 10 * s; else if (r >= n && r !== a) {
                        if (!o.maskDigitPosMapOld[n]) {
                            var p = n;
                            n -= h - u, n -= l, o.maskDigitPosMap[n] && (n = p)
                        }
                    } else n > r && (n += u - h, n += c)
                }
                return n
            }, behaviour: function (i) {
                i = i || window.event, o.invalid = [];
                var n = e.data("mask-keycode");
                if (-1 === t.inArray(n, s.byPassKeys)) {
                    var r = o.getMasked(), a = o.getCaret();
                    return setTimeout(function () {
                        o.setCaret(o.calculateCaretPosition())
                    }, 10), o.val(r), o.setCaret(a), o.callbacks(i)
                }
            }, getMasked: function (t, e) {
                var r, a, l = [], c = void 0 === e ? o.val() : e + "", u = 0, h = i.length, d = 0, p = c.length, f = 1,
                    g = "push", v = -1, m = 0, y = [];
                n.reverse ? (g = "unshift", f = -1, r = 0, u = h - 1, d = p - 1, a = function () {
                    return u > -1 && d > -1
                }) : (r = h - 1, a = function () {
                    return u < h && d < p
                });
                for (var b; a();) {
                    var w = i.charAt(u), x = c.charAt(d), C = s.translation[w];
                    C ? (x.match(C.pattern) ? (l[g](x), C.recursive && (-1 === v ? v = u : u === r && (u = v - f), r === v && (u -= f)), u += f) : x === b ? (m--, b = void 0) : C.optional ? (u += f, d -= f) : C.fallback ? (l[g](C.fallback), u += f, d -= f) : o.invalid.push({
                        p: d,
                        v: x,
                        e: C.pattern
                    }), d += f) : (t || l[g](w), x === w ? (y.push(d), d += f) : (b = w, y.push(d + m), m++), u += f)
                }
                var k = i.charAt(r);
                h !== p + 1 || s.translation[k] || l.push(k);
                var T = l.join("");
                return o.mapMaskdigitPositions(T, y, p), T
            }, mapMaskdigitPositions: function (t, e, i) {
                var r = n.reverse ? t.length - i : 0;
                o.maskDigitPosMap = {};
                for (var s = 0; s < e.length; s++) o.maskDigitPosMap[e[s] + r] = 1
            }, callbacks: function (t) {
                var r = o.val(), s = r !== a, l = [r, t, e, n], c = function (t, e, i) {
                    "function" == typeof n[t] && e && n[t].apply(this, i)
                };
                c("onChange", !0 === s, l), c("onKeyPress", !0 === s, l), c("onComplete", r.length === i.length, l), c("onInvalid", o.invalid.length > 0, [r, t, e, o.invalid, n])
            }
        };
        e = t(e);
        var r, s = this, a = o.val();
        i = "function" == typeof i ? i(o.val(), void 0, e, n) : i, s.mask = i, s.options = n, s.remove = function () {
            var t = o.getCaret();
            return o.destroyEvents(), o.val(s.getCleanVal()), o.setCaret(t), e
        }, s.getCleanVal = function () {
            return o.getMasked(!0)
        }, s.getMaskedVal = function (t) {
            return o.getMasked(!1, t)
        }, s.init = function (a) {
            if (a = a || !1, n = n || {}, s.clearIfNotMatch = t.jMaskGlobals.clearIfNotMatch, s.byPassKeys = t.jMaskGlobals.byPassKeys, s.translation = t.extend({}, t.jMaskGlobals.translation, n.translation), s = t.extend(!0, {}, s, n), r = o.getRegexMask(), a) o.events(), o.val(o.getMasked()); else {
                n.placeholder && e.attr("placeholder", n.placeholder), e.data("mask") && e.attr("autocomplete", "off");
                for (var l = 0, c = !0; l < i.length; l++) {
                    var u = s.translation[i.charAt(l)];
                    if (u && u.recursive) {
                        c = !1;
                        break
                    }
                }
                c && e.attr("maxlength", i.length), o.destroyEvents(), o.events();
                var h = o.getCaret();
                o.val(o.getMasked()), o.setCaret(h)
            }
        }, s.init(!e.is("input"))
    };
    t.maskWatchers = {};
    var i = function (e, i, n) {
        n = n || {};
        var o = t(e).data("mask"), r = JSON.stringify, s = t(e).val() || t(e).text();
        try {
            return "function" == typeof i && (i = i(s)), "object" != typeof o || r(o.options) !== r(n) || o.mask !== i
        } catch (t) {
        }
    };
    t.fn.mask = function (n, o) {
        o = o || {};
        var r = this.selector, s = t.jMaskGlobals, a = s.watchInterval, l = o.watchInputs || s.watchInputs,
            c = function () {
                if (i(this, n, o)) return t(this).data("mask", new e(this, n, o))
            };
        return t(this).each(c), r && "" !== r && l && (clearInterval(t.maskWatchers[r]), t.maskWatchers[r] = setInterval(function () {
            t(document).find(r).each(c)
        }, a)), this
    }, t.fn.masked = function (t) {
        return this.data("mask").getMaskedVal(t)
    }, t.fn.unmask = function () {
        return clearInterval(t.maskWatchers[this.selector]), delete t.maskWatchers[this.selector], this.each(function () {
            var e = t(this).data("mask");
            e && e.remove().removeData("mask")
        })
    }, t.fn.cleanVal = function () {
        return this.data("mask").getCleanVal()
    }, t.applyDataMask = function (n) {
        ((n = n || t.jMaskGlobals.maskElements) instanceof t ? n : t(n)).filter(t.jMaskGlobals.dataMaskAttr).each(function () {
            var n = t(this), o = {}, r = n.attr("data-mask");
            if (n.attr("data-mask-reverse") && (o.reverse = !0), n.attr("data-mask-clearifnotmatch") && (o.clearIfNotMatch = !0), "true" === n.attr("data-mask-selectonfocus") && (o.selectOnFocus = !0), i(n, r, o)) return n.data("mask", new e(this, r, o))
        })
    };
    var n = {
        maskElements: "input,td,span,div",
        dataMaskAttr: "*[data-mask]",
        dataMask: !0,
        watchInterval: 300,
        watchInputs: !0,
        useInput: !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) && function (t) {
            var e, i = document.createElement("div");
            return t = "on" + t, (e = t in i) || (i.setAttribute(t, "return;"), e = "function" == typeof i[t]), i = null, e
        }("input"),
        watchDataMask: !1,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: {
            0: {pattern: /\d/},
            9: {pattern: /\d/, optional: !0},
            "#": {pattern: /\d/, recursive: !0},
            A: {pattern: /[a-zA-Z0-9]/},
            S: {pattern: /[a-zA-Z]/}
        }
    };
    t.jMaskGlobals = t.jMaskGlobals || {}, (n = t.jMaskGlobals = t.extend(!0, {}, n, t.jMaskGlobals)).dataMask && t.applyDataMask(), setInterval(function () {
        t.jMaskGlobals.watchDataMask && t.applyDataMask()
    }, n.watchInterval)
}, window.jQuery, window.Zepto), function (t, e, i, n) {
    function o(e, i) {
        this.settings = null, this.options = t.extend({}, o.Defaults, i), this.$element = t(e), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
            time: null,
            target: null,
            pointer: null,
            stage: {start: null, current: null},
            direction: null
        }, this._states = {
            current: {},
            tags: {initializing: ["busy"], animating: ["busy"], dragging: ["interacting"]}
        }, t.each(["onResize", "onThrottledResize"], t.proxy(function (e, i) {
            this._handlers[i] = t.proxy(this[i], this)
        }, this)), t.each(o.Plugins, t.proxy(function (t, e) {
            this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this)
        }, this)), t.each(o.Workers, t.proxy(function (e, i) {
            this._pipe.push({filter: i.filter, run: t.proxy(i.run, this)})
        }, this)), this.setup(), this.initialize()
    }

    o.Defaults = {
        items: 3,
        loop: !1,
        center: !1,
        rewind: !1,
        mouseDrag: !0,
        touchDrag: !0,
        pullDrag: !0,
        freeDrag: !1,
        margin: 0,
        stagePadding: 0,
        merge: !1,
        mergeFit: !0,
        autoWidth: !1,
        startPosition: 0,
        rtl: !1,
        smartSpeed: 250,
        fluidSpeed: !1,
        dragEndSpeed: !1,
        responsive: {},
        responsiveRefreshRate: 200,
        responsiveBaseElement: e,
        fallbackEasing: "swing",
        info: !1,
        nestedItemSelector: !1,
        itemElement: "div",
        stageElement: "div",
        refreshClass: "owl-refresh",
        loadedClass: "owl-loaded",
        loadingClass: "owl-loading",
        rtlClass: "owl-rtl",
        responsiveClass: "owl-responsive",
        dragClass: "owl-drag",
        itemClass: "owl-item",
        stageClass: "owl-stage",
        stageOuterClass: "owl-stage-outer",
        grabClass: "owl-grab"
    }, o.Width = {Default: "default", Inner: "inner", Outer: "outer"}, o.Type = {
        Event: "event",
        State: "state"
    }, o.Plugins = {}, o.Workers = [{
        filter: ["width", "settings"], run: function () {
            this._width = this.$element.width()
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            t.current = this._items && this._items[this.relative(this._current)]
        }
    }, {
        filter: ["items", "settings"], run: function () {
            this.$stage.children(".cloned").remove()
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            var e = this.settings.margin || "", i = !this.settings.autoWidth, n = this.settings.rtl,
                o = {width: "auto", "margin-left": n ? e : "", "margin-right": n ? "" : e};
            !i && this.$stage.children().css(o), t.css = o
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin, i = null,
                n = this._items.length, o = !this.settings.autoWidth, r = [];
            for (t.items = {
                merge: !1,
                width: e
            }; n--;) i = this._mergers[n], i = this.settings.mergeFit && Math.min(i, this.settings.items) || i, t.items.merge = i > 1 || t.items.merge, r[n] = o ? e * i : this._items[n].width();
            this._widths = r
        }
    }, {
        filter: ["items", "settings"], run: function () {
            var e = [], i = this._items, n = this.settings, o = Math.max(2 * n.items, 4),
                r = 2 * Math.ceil(i.length / 2), s = n.loop && i.length ? n.rewind ? o : Math.max(o, r) : 0, a = "",
                l = "";
            for (s /= 2; s--;) e.push(this.normalize(e.length / 2, !0)), a += i[e[e.length - 1]][0].outerHTML, e.push(this.normalize(i.length - 1 - (e.length - 1) / 2, !0)), l = i[e[e.length - 1]][0].outerHTML + l;
            this._clones = e, t(a).addClass("cloned").appendTo(this.$stage), t(l).addClass("cloned").prependTo(this.$stage)
        }
    }, {
        filter: ["width", "items", "settings"], run: function () {
            for (var t = this.settings.rtl ? 1 : -1, e = this._clones.length + this._items.length, i = -1, n = 0, o = 0, r = []; ++i < e;) n = r[i - 1] || 0, o = this._widths[this.relative(i)] + this.settings.margin, r.push(n + o * t);
            this._coordinates = r
        }
    }, {
        filter: ["width", "items", "settings"], run: function () {
            var t = this.settings.stagePadding, e = this._coordinates, i = {
                width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
                "padding-left": t || "",
                "padding-right": t || ""
            };
            this.$stage.css(i)
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            var e = this._coordinates.length, i = !this.settings.autoWidth, n = this.$stage.children();
            if (i && t.items.merge) for (; e--;) t.css.width = this._widths[this.relative(e)], n.eq(e).css(t.css); else i && (t.css.width = t.items.width, n.css(t.css))
        }
    }, {
        filter: ["items"], run: function () {
            this._coordinates.length < 1 && this.$stage.removeAttr("style")
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            t.current = t.current ? this.$stage.children().index(t.current) : 0, t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current)), this.reset(t.current)
        }
    }, {
        filter: ["position"], run: function () {
            this.animate(this.coordinates(this._current))
        }
    }, {
        filter: ["width", "position", "items", "settings"], run: function () {
            var t, e, i, n, o = this.settings.rtl ? 1 : -1, r = 2 * this.settings.stagePadding,
                s = this.coordinates(this.current()) + r, a = s + this.width() * o, l = [];
            for (i = 0, n = this._coordinates.length; i < n; i++) t = this._coordinates[i - 1] || 0, e = Math.abs(this._coordinates[i]) + r * o, (this.op(t, "<=", s) && this.op(t, ">", a) || this.op(e, "<", s) && this.op(e, ">", a)) && l.push(i);
            this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + l.join("), :eq(") + ")").addClass("active"), this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center"))
        }
    }], o.prototype.initialize = function () {
        if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
            var e, i, o;
            e = this.$element.find("img"), i = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : n, o = this.$element.children(i).width(), e.length && o <= 0 && this.preloadAutoWidthImages(e)
        }
        this.$element.addClass(this.options.loadingClass), this.$stage = t("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
    }, o.prototype.setup = function () {
        var e = this.viewport(), i = this.options.responsive, n = -1, o = null;
        i ? (t.each(i, function (t) {
            t <= e && t > n && (n = Number(t))
        }), "function" == typeof(o = t.extend({}, this.options, i[n])).stagePadding && (o.stagePadding = o.stagePadding()), delete o.responsive, o.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + n))) : o = t.extend({}, this.options), this.trigger("change", {
            property: {
                name: "settings",
                value: o
            }
        }), this._breakpoint = n, this.settings = o, this.invalidate("settings"), this.trigger("changed", {
            property: {
                name: "settings",
                value: this.settings
            }
        })
    }, o.prototype.optionsLogic = function () {
        this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
    }, o.prototype.prepare = function (e) {
        var i = this.trigger("prepare", {content: e});
        return i.data || (i.data = t("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(e)), this.trigger("prepared", {content: i.data}), i.data
    }, o.prototype.update = function () {
        for (var e = 0, i = this._pipe.length, n = t.proxy(function (t) {
            return this[t]
        }, this._invalidated), o = {}; e < i;) (this._invalidated.all || t.grep(this._pipe[e].filter, n).length > 0) && this._pipe[e].run(o), e++;
        this._invalidated = {}, !this.is("valid") && this.enter("valid")
    }, o.prototype.width = function (t) {
        switch (t = t || o.Width.Default) {
            case o.Width.Inner:
            case o.Width.Outer:
                return this._width;
            default:
                return this._width - 2 * this.settings.stagePadding + this.settings.margin
        }
    }, o.prototype.refresh = function () {
        this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
    }, o.prototype.onThrottledResize = function () {
        e.clearTimeout(this.resizeTimer), this.resizeTimer = e.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
    }, o.prototype.onResize = function () {
        return !!this._items.length && this._width !== this.$element.width() && !!this.$element.is(":visible") && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))
    }, o.prototype.registerEventHandlers = function () {
        t.support.transition && this.$stage.on(t.support.transition.end + ".owl.core", t.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(e, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
            return !1
        })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", t.proxy(this.onDragEnd, this)))
    }, o.prototype.onDragStart = function (e) {
        var n = null;
        3 !== e.which && (t.support.transform ? (n = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), n = {
            x: n[16 === n.length ? 12 : 4],
            y: n[16 === n.length ? 13 : 5]
        }) : (n = this.$stage.position(), n = {
            x: this.settings.rtl ? n.left + this.$stage.width() - this.width() + this.settings.margin : n.left,
            y: n.top
        }), this.is("animating") && (t.support.transform ? this.animate(n.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === e.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = t(e.target), this._drag.stage.start = n, this._drag.stage.current = n, this._drag.pointer = this.pointer(e), t(i).on("mouseup.owl.core touchend.owl.core", t.proxy(this.onDragEnd, this)), t(i).one("mousemove.owl.core touchmove.owl.core", t.proxy(function (e) {
            var n = this.difference(this._drag.pointer, this.pointer(e));
            t(i).on("mousemove.owl.core touchmove.owl.core", t.proxy(this.onDragMove, this)), Math.abs(n.x) < Math.abs(n.y) && this.is("valid") || (e.preventDefault(), this.enter("dragging"), this.trigger("drag"))
        }, this)))
    }, o.prototype.onDragMove = function (t) {
        var e = null, i = null, n = null, o = this.difference(this._drag.pointer, this.pointer(t)),
            r = this.difference(this._drag.stage.start, o);
        this.is("dragging") && (t.preventDefault(), this.settings.loop ? (e = this.coordinates(this.minimum()), i = this.coordinates(this.maximum() + 1) - e, r.x = ((r.x - e) % i + i) % i + e) : (e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), i = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), n = this.settings.pullDrag ? -1 * o.x / 5 : 0, r.x = Math.max(Math.min(r.x, e + n), i + n)), this._drag.stage.current = r, this.animate(r.x))
    }, o.prototype.onDragEnd = function (e) {
        var n = this.difference(this._drag.pointer, this.pointer(e)), o = this._drag.stage.current,
            r = n.x > 0 ^ this.settings.rtl ? "left" : "right";
        t(i).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== n.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(o.x, 0 !== n.x ? r : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = r, (Math.abs(n.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () {
            return !1
        })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
    }, o.prototype.closest = function (e, i) {
        var n = -1, o = this.width(), r = this.coordinates();
        return this.settings.freeDrag || t.each(r, t.proxy(function (t, s) {
            return "left" === i && e > s - 30 && e < s + 30 ? n = t : "right" === i && e > s - o - 30 && e < s - o + 30 ? n = t + 1 : this.op(e, "<", s) && this.op(e, ">", r[t + 1] || s - o) && (n = "left" === i ? t + 1 : t), -1 === n
        }, this)), this.settings.loop || (this.op(e, ">", r[this.minimum()]) ? n = e = this.minimum() : this.op(e, "<", r[this.maximum()]) && (n = e = this.maximum())), n
    }, o.prototype.animate = function (e) {
        var i = this.speed() > 0;
        this.is("animating") && this.onTransitionEnd(), i && (this.enter("animating"), this.trigger("translate")), t.support.transform3d && t.support.transition ? this.$stage.css({
            transform: "translate3d(" + e + "px,0px,0px)",
            transition: this.speed() / 1e3 + "s"
        }) : i ? this.$stage.animate({left: e + "px"}, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this)) : this.$stage.css({left: e + "px"})
    }, o.prototype.is = function (t) {
        return this._states.current[t] && this._states.current[t] > 0
    }, o.prototype.current = function (t) {
        if (t === n) return this._current;
        if (0 === this._items.length) return n;
        if (t = this.normalize(t), this._current !== t) {
            var e = this.trigger("change", {property: {name: "position", value: t}});
            e.data !== n && (t = this.normalize(e.data)), this._current = t, this.invalidate("position"), this.trigger("changed", {
                property: {
                    name: "position",
                    value: this._current
                }
            })
        }
        return this._current
    }, o.prototype.invalidate = function (e) {
        return "string" === t.type(e) && (this._invalidated[e] = !0, this.is("valid") && this.leave("valid")), t.map(this._invalidated, function (t, e) {
            return e
        })
    }, o.prototype.reset = function (t) {
        (t = this.normalize(t)) !== n && (this._speed = 0, this._current = t, this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]))
    }, o.prototype.normalize = function (t, e) {
        var i = this._items.length, o = e ? 0 : this._clones.length;
        return !this.isNumeric(t) || i < 1 ? t = n : (t < 0 || t >= i + o) && (t = ((t - o / 2) % i + i) % i + o / 2), t
    }, o.prototype.relative = function (t) {
        return t -= this._clones.length / 2, this.normalize(t, !0)
    }, o.prototype.maximum = function (t) {
        var e, i, n, o = this.settings, r = this._coordinates.length;
        if (o.loop) r = this._clones.length / 2 + this._items.length - 1; else if (o.autoWidth || o.merge) {
            for (e = this._items.length, i = this._items[--e].width(), n = this.$element.width(); e-- && !((i += this._items[e].width() + this.settings.margin) > n);) ;
            r = e + 1
        } else r = o.center ? this._items.length - 1 : this._items.length - o.items;
        return t && (r -= this._clones.length / 2), Math.max(r, 0)
    }, o.prototype.minimum = function (t) {
        return t ? 0 : this._clones.length / 2
    }, o.prototype.items = function (t) {
        return t === n ? this._items.slice() : (t = this.normalize(t, !0), this._items[t])
    }, o.prototype.mergers = function (t) {
        return t === n ? this._mergers.slice() : (t = this.normalize(t, !0), this._mergers[t])
    }, o.prototype.clones = function (e) {
        var i = this._clones.length / 2, o = i + this._items.length, r = function (t) {
            return t % 2 == 0 ? o + t / 2 : i - (t + 1) / 2
        };
        return e === n ? t.map(this._clones, function (t, e) {
            return r(e)
        }) : t.map(this._clones, function (t, i) {
            return t === e ? r(i) : null
        })
    }, o.prototype.speed = function (t) {
        return t !== n && (this._speed = t), this._speed
    }, o.prototype.coordinates = function (e) {
        var i, o = 1, r = e - 1;
        return e === n ? t.map(this._coordinates, t.proxy(function (t, e) {
            return this.coordinates(e)
        }, this)) : (this.settings.center ? (this.settings.rtl && (o = -1, r = e + 1), i = this._coordinates[e], i += (this.width() - i + (this._coordinates[r] || 0)) / 2 * o) : i = this._coordinates[r] || 0, i = Math.ceil(i))
    }, o.prototype.duration = function (t, e, i) {
        return 0 === i ? 0 : Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(i || this.settings.smartSpeed)
    }, o.prototype.to = function (t, e) {
        var i = this.current(), n = null, o = t - this.relative(i), r = (o > 0) - (o < 0), s = this._items.length,
            a = this.minimum(), l = this.maximum();
        this.settings.loop ? (!this.settings.rewind && Math.abs(o) > s / 2 && (o += -1 * r * s), t = i + o, (n = ((t - a) % s + s) % s + a) !== t && n - o <= l && n - o > 0 && (i = n - o, t = n, this.reset(i))) : this.settings.rewind ? (l += 1, t = (t % l + l) % l) : t = Math.max(a, Math.min(l, t)), this.speed(this.duration(i, t, e)), this.current(t), this.$element.is(":visible") && this.update()
    }, o.prototype.next = function (t) {
        t = t || !1, this.to(this.relative(this.current()) + 1, t)
    }, o.prototype.prev = function (t) {
        t = t || !1, this.to(this.relative(this.current()) - 1, t)
    }, o.prototype.onTransitionEnd = function (t) {
        if (t !== n && (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) !== this.$stage.get(0))) return !1;
        this.leave("animating"), this.trigger("translated")
    }, o.prototype.viewport = function () {
        var n;
        return this.options.responsiveBaseElement !== e ? n = t(this.options.responsiveBaseElement).width() : e.innerWidth ? n = e.innerWidth : i.documentElement && i.documentElement.clientWidth ? n = i.documentElement.clientWidth : console.warn("Can not detect viewport width."), n
    }, o.prototype.replace = function (e) {
        this.$stage.empty(), this._items = [], e && (e = e instanceof jQuery ? e : t(e)), this.settings.nestedItemSelector && (e = e.find("." + this.settings.nestedItemSelector)), e.filter(function () {
            return 1 === this.nodeType
        }).each(t.proxy(function (t, e) {
            e = this.prepare(e), this.$stage.append(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
        }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
    }, o.prototype.add = function (e, i) {
        var o = this.relative(this._current);
        i = i === n ? this._items.length : this.normalize(i, !0), e = e instanceof jQuery ? e : t(e), this.trigger("add", {
            content: e,
            position: i
        }), e = this.prepare(e), 0 === this._items.length || i === this._items.length ? (0 === this._items.length && this.$stage.append(e), 0 !== this._items.length && this._items[i - 1].after(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[i].before(e), this._items.splice(i, 0, e), this._mergers.splice(i, 0, 1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[o] && this.reset(this._items[o].index()), this.invalidate("items"), this.trigger("added", {
            content: e,
            position: i
        })
    }, o.prototype.remove = function (t) {
        (t = this.normalize(t, !0)) !== n && (this.trigger("remove", {
            content: this._items[t],
            position: t
        }), this._items[t].remove(), this._items.splice(t, 1), this._mergers.splice(t, 1), this.invalidate("items"), this.trigger("removed", {
            content: null,
            position: t
        }))
    }, o.prototype.preloadAutoWidthImages = function (e) {
        e.each(t.proxy(function (e, i) {
            this.enter("pre-loading"), i = t(i), t(new Image).one("load", t.proxy(function (t) {
                i.attr("src", t.target.src), i.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
            }, this)).attr("src", i.attr("src") || i.attr("data-src") || i.attr("data-src-retina"))
        }, this))
    }, o.prototype.destroy = function () {
        this.$element.off(".owl.core"), this.$stage.off(".owl.core"), t(i).off(".owl.core"), !1 !== this.settings.responsive && (e.clearTimeout(this.resizeTimer), this.off(e, "resize", this._handlers.onThrottledResize));
        for (var n in this._plugins) this._plugins[n].destroy();
        this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
    }, o.prototype.op = function (t, e, i) {
        var n = this.settings.rtl;
        switch (e) {
            case"<":
                return n ? t > i : t < i;
            case">":
                return n ? t < i : t > i;
            case">=":
                return n ? t <= i : t >= i;
            case"<=":
                return n ? t >= i : t <= i
        }
    }, o.prototype.on = function (t, e, i, n) {
        t.addEventListener ? t.addEventListener(e, i, n) : t.attachEvent && t.attachEvent("on" + e, i)
    }, o.prototype.off = function (t, e, i, n) {
        t.removeEventListener ? t.removeEventListener(e, i, n) : t.detachEvent && t.detachEvent("on" + e, i)
    }, o.prototype.trigger = function (e, i, n, r, s) {
        var a = {item: {count: this._items.length, index: this.current()}},
            l = t.camelCase(t.grep(["on", e, n], function (t) {
                return t
            }).join("-").toLowerCase()),
            c = t.Event([e, "owl", n || "carousel"].join(".").toLowerCase(), t.extend({relatedTarget: this}, a, i));
        return this._supress[e] || (t.each(this._plugins, function (t, e) {
            e.onTrigger && e.onTrigger(c)
        }), this.register({
            type: o.Type.Event,
            name: e
        }), this.$element.trigger(c), this.settings && "function" == typeof this.settings[l] && this.settings[l].call(this, c)), c
    }, o.prototype.enter = function (e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function (t, e) {
            this._states.current[e] === n && (this._states.current[e] = 0), this._states.current[e]++
        }, this))
    }, o.prototype.leave = function (e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function (t, e) {
            this._states.current[e]--
        }, this))
    }, o.prototype.register = function (e) {
        if (e.type === o.Type.Event) {
            if (t.event.special[e.name] || (t.event.special[e.name] = {}), !t.event.special[e.name].owl) {
                var i = t.event.special[e.name]._default;
                t.event.special[e.name]._default = function (t) {
                    return !i || !i.apply || t.namespace && -1 !== t.namespace.indexOf("owl") ? t.namespace && t.namespace.indexOf("owl") > -1 : i.apply(this, arguments)
                }, t.event.special[e.name].owl = !0
            }
        } else e.type === o.Type.State && (this._states.tags[e.name] ? this._states.tags[e.name] = this._states.tags[e.name].concat(e.tags) : this._states.tags[e.name] = e.tags, this._states.tags[e.name] = t.grep(this._states.tags[e.name], t.proxy(function (i, n) {
            return t.inArray(i, this._states.tags[e.name]) === n
        }, this)))
    }, o.prototype.suppress = function (e) {
        t.each(e, t.proxy(function (t, e) {
            this._supress[e] = !0
        }, this))
    }, o.prototype.release = function (e) {
        t.each(e, t.proxy(function (t, e) {
            delete this._supress[e]
        }, this))
    }, o.prototype.pointer = function (t) {
        var i = {x: null, y: null};
        return t = t.originalEvent || t || e.event, (t = t.touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t).pageX ? (i.x = t.pageX, i.y = t.pageY) : (i.x = t.clientX, i.y = t.clientY), i
    }, o.prototype.isNumeric = function (t) {
        return !isNaN(parseFloat(t))
    }, o.prototype.difference = function (t, e) {
        return {x: t.x - e.x, y: t.y - e.y}
    }, t.fn.owlCarousel = function (e) {
        var i = Array.prototype.slice.call(arguments, 1);
        return this.each(function () {
            var n = t(this), r = n.data("owl.carousel");
            r || (r = new o(this, "object" == typeof e && e), n.data("owl.carousel", r), t.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (e, i) {
                r.register({
                    type: o.Type.Event,
                    name: i
                }), r.$element.on(i + ".owl.carousel.core", t.proxy(function (t) {
                    t.namespace && t.relatedTarget !== this && (this.suppress([i]), r[i].apply(this, [].slice.call(arguments, 1)), this.release([i]))
                }, r))
            })), "string" == typeof e && "_" !== e.charAt(0) && r[e].apply(r, i)
        })
    }, t.fn.owlCarousel.Constructor = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    var o = function (e) {
        this._core = e, this._interval = null, this._visible = null, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoRefresh && this.watch()
            }, this)
        }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    o.Defaults = {autoRefresh: !0, autoRefreshInterval: 500}, o.prototype.watch = function () {
        this._interval || (this._visible = this._core.$element.is(":visible"), this._interval = e.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
    }, o.prototype.refresh = function () {
        this._core.$element.is(":visible") !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
    }, o.prototype.destroy = function () {
        var t, i;
        e.clearInterval(this._interval);
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    var o = function (e) {
        this._core = e, this._loaded = [], this._handlers = {
            "initialized.owl.carousel change.owl.carousel resized.owl.carousel": t.proxy(function (e) {
                if (e.namespace && this._core.settings && this._core.settings.lazyLoad && (e.property && "position" == e.property.name || "initialized" == e.type)) for (var i = this._core.settings, n = i.center && Math.ceil(i.items / 2) || i.items, o = i.center && -1 * n || 0, r = (e.property && void 0 !== e.property.value ? e.property.value : this._core.current()) + o, s = this._core.clones().length, a = t.proxy(function (t, e) {
                    this.load(e)
                }, this); o++ < n;) this.load(s / 2 + this._core.relative(r)), s && t.each(this._core.clones(this._core.relative(r)), a), r++
            }, this)
        }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    o.Defaults = {lazyLoad: !1}, o.prototype.load = function (i) {
        var n = this._core.$stage.children().eq(i), o = n && n.find(".owl-lazy");
        !o || t.inArray(n.get(0), this._loaded) > -1 || (o.each(t.proxy(function (i, n) {
            var o, r = t(n), s = e.devicePixelRatio > 1 && r.attr("data-src-retina") || r.attr("data-src");
            this._core.trigger("load", {
                element: r,
                url: s
            }, "lazy"), r.is("img") ? r.one("load.owl.lazy", t.proxy(function () {
                r.css("opacity", 1), this._core.trigger("loaded", {element: r, url: s}, "lazy")
            }, this)).attr("src", s) : (o = new Image, o.onload = t.proxy(function () {
                r.css({"background-image": 'url("' + s + '")', opacity: "1"}), this._core.trigger("loaded", {
                    element: r,
                    url: s
                }, "lazy")
            }, this), o.src = s)
        }, this)), this._loaded.push(n.get(0)))
    }, o.prototype.destroy = function () {
        var t, e;
        for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Lazy = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    var o = function (e) {
        this._core = e, this._handlers = {
            "initialized.owl.carousel refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && this.update()
            }, this), "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && "position" == t.property.name && this.update()
            }, this), "loaded.owl.lazy": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
            }, this)
        }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    o.Defaults = {autoHeight: !1, autoHeightClass: "owl-height"}, o.prototype.update = function () {
        var e = this._core._current, i = e + this._core.settings.items,
            n = this._core.$stage.children().toArray().slice(e, i), o = [], r = 0;
        t.each(n, function (e, i) {
            o.push(t(i).height())
        }), r = Math.max.apply(null, o), this._core.$stage.parent().height(r).addClass(this._core.settings.autoHeightClass)
    }, o.prototype.destroy = function () {
        var t, e;
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.AutoHeight = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    var o = function (e) {
        this._core = e, this._videos = {}, this._playing = null, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.register({type: "state", name: "playing", tags: ["interacting"]})
            }, this), "resize.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.video && this.isInFullScreen() && t.preventDefault()
            }, this), "refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
            }, this), "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" === t.property.name && this._playing && this.stop()
            }, this), "prepared.owl.carousel": t.proxy(function (e) {
                if (e.namespace) {
                    var i = t(e.content).find(".owl-video");
                    i.length && (i.css("display", "none"), this.fetch(i, t(e.content)))
                }
            }, this)
        }, this._core.options = t.extend({}, o.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", t.proxy(function (t) {
            this.play(t)
        }, this))
    };
    o.Defaults = {video: !1, videoHeight: !1, videoWidth: !1}, o.prototype.fetch = function (t, e) {
        var i = t.attr("data-vimeo-id") ? "vimeo" : t.attr("data-vzaar-id") ? "vzaar" : "youtube",
            n = t.attr("data-vimeo-id") || t.attr("data-youtube-id") || t.attr("data-vzaar-id"),
            o = t.attr("data-width") || this._core.settings.videoWidth,
            r = t.attr("data-height") || this._core.settings.videoHeight, s = t.attr("href");
        if (!s) throw new Error("Missing video URL.");
        if ((n = s.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/))[3].indexOf("youtu") > -1) i = "youtube"; else if (n[3].indexOf("vimeo") > -1) i = "vimeo"; else {
            if (!(n[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
            i = "vzaar"
        }
        n = n[6], this._videos[s] = {
            type: i,
            id: n,
            width: o,
            height: r
        }, e.attr("data-video", s), this.thumbnail(t, this._videos[s])
    }, o.prototype.thumbnail = function (e, i) {
        var n, o, r, s = i.width && i.height ? 'style="width:' + i.width + "px;height:" + i.height + 'px;"' : "",
            a = e.find("img"), l = "src", c = "", u = this._core.settings, h = function (t) {
                o = '<div class="owl-video-play-icon"></div>', n = u.lazyLoad ? '<div class="owl-video-tn ' + c + '" ' + l + '="' + t + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>', e.after(n), e.after(o)
            };
        if (e.wrap('<div class="owl-video-wrapper"' + s + "></div>"), this._core.settings.lazyLoad && (l = "data-src", c = "owl-lazy"), a.length) return h(a.attr(l)), a.remove(), !1;
        "youtube" === i.type ? (r = "//img.youtube.com/vi/" + i.id + "/hqdefault.jpg", h(r)) : "vimeo" === i.type ? t.ajax({
            type: "GET",
            url: "//vimeo.com/api/v2/video/" + i.id + ".json",
            jsonp: "callback",
            dataType: "jsonp",
            success: function (t) {
                r = t[0].thumbnail_large, h(r)
            }
        }) : "vzaar" === i.type && t.ajax({
            type: "GET",
            url: "//vzaar.com/api/videos/" + i.id + ".json",
            jsonp: "callback",
            dataType: "jsonp",
            success: function (t) {
                r = t.framegrab_url, h(r)
            }
        })
    }, o.prototype.stop = function () {
        this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
    }, o.prototype.play = function (e) {
        var i, n = t(e.target).closest("." + this._core.settings.itemClass), o = this._videos[n.attr("data-video")],
            r = o.width || "100%", s = o.height || this._core.$stage.height();
        this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), n = this._core.items(this._core.relative(n.index())), this._core.reset(n.index()), "youtube" === o.type ? i = '<iframe width="' + r + '" height="' + s + '" src="//www.youtube.com/embed/' + o.id + "?autoplay=1&rel=0&v=" + o.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === o.type ? i = '<iframe src="//player.vimeo.com/video/' + o.id + '?autoplay=1" width="' + r + '" height="' + s + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === o.type && (i = '<iframe frameborder="0"height="' + s + '"width="' + r + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + o.id + '/player?autoplay=true"></iframe>'), t('<div class="owl-video-frame">' + i + "</div>").insertAfter(n.find(".owl-video")), this._playing = n.addClass("owl-video-playing"))
    }, o.prototype.isInFullScreen = function () {
        var e = i.fullscreenElement || i.mozFullScreenElement || i.webkitFullscreenElement;
        return e && t(e).parent().hasClass("owl-video-frame")
    }, o.prototype.destroy = function () {
        var t, e;
        this._core.$element.off("click.owl.video");
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Video = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    var o = function (e) {
        this.core = e, this.core.options = t.extend({}, o.Defaults, this.core.options), this.swapping = !0, this.previous = n, this.next = n, this.handlers = {
            "change.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" == t.property.name && (this.previous = this.core.current(), this.next = t.property.value)
            }, this), "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(function (t) {
                t.namespace && (this.swapping = "translated" == t.type)
            }, this), "translate.owl.carousel": t.proxy(function (t) {
                t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
            }, this)
        }, this.core.$element.on(this.handlers)
    };
    o.Defaults = {animateOut: !1, animateIn: !1}, o.prototype.swap = function () {
        if (1 === this.core.settings.items && t.support.animation && t.support.transition) {
            this.core.speed(0);
            var e, i = t.proxy(this.clear, this), n = this.core.$stage.children().eq(this.previous),
                o = this.core.$stage.children().eq(this.next), r = this.core.settings.animateIn,
                s = this.core.settings.animateOut;
            this.core.current() !== this.previous && (s && (e = this.core.coordinates(this.previous) - this.core.coordinates(this.next), n.one(t.support.animation.end, i).css({left: e + "px"}).addClass("animated owl-animated-out").addClass(s)), r && o.one(t.support.animation.end, i).addClass("animated owl-animated-in").addClass(r))
        }
    }, o.prototype.clear = function (e) {
        t(e.target).css({left: ""}).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
    }, o.prototype.destroy = function () {
        var t, e;
        for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Animate = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    var o = function (e) {
        this._core = e, this._timeout = null, this._paused = !1, this._handlers = {
            "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "settings" === t.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : t.namespace && "position" === t.property.name && this._core.settings.autoplay && this._setAutoPlayInterval()
            }, this), "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoplay && this.play()
            }, this), "play.owl.autoplay": t.proxy(function (t, e, i) {
                t.namespace && this.play(e, i)
            }, this), "stop.owl.autoplay": t.proxy(function (t) {
                t.namespace && this.stop()
            }, this), "mouseover.owl.autoplay": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
            }, this), "mouseleave.owl.autoplay": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
            }, this), "touchstart.owl.core": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
            }, this), "touchend.owl.core": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this.play()
            }, this)
        }, this._core.$element.on(this._handlers), this._core.options = t.extend({}, o.Defaults, this._core.options)
    };
    o.Defaults = {
        autoplay: !1,
        autoplayTimeout: 5e3,
        autoplayHoverPause: !1,
        autoplaySpeed: !1
    }, o.prototype.play = function (t, e) {
        this._paused = !1, this._core.is("rotating") || (this._core.enter("rotating"), this._setAutoPlayInterval())
    }, o.prototype._getNextTimeout = function (n, o) {
        return this._timeout && e.clearTimeout(this._timeout), e.setTimeout(t.proxy(function () {
            this._paused || this._core.is("busy") || this._core.is("interacting") || i.hidden || this._core.next(o || this._core.settings.autoplaySpeed)
        }, this), n || this._core.settings.autoplayTimeout)
    }, o.prototype._setAutoPlayInterval = function () {
        this._timeout = this._getNextTimeout()
    }, o.prototype.stop = function () {
        this._core.is("rotating") && (e.clearTimeout(this._timeout), this._core.leave("rotating"))
    }, o.prototype.pause = function () {
        this._core.is("rotating") && (this._paused = !0)
    }, o.prototype.destroy = function () {
        var t, e;
        this.stop();
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.autoplay = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    "use strict";
    var o = function (e) {
        this._core = e, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
            next: this._core.next,
            prev: this._core.prev,
            to: this._core.to
        }, this._handlers = {
            "prepared.owl.carousel": t.proxy(function (e) {
                e.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + t(e.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
            }, this), "added.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 0, this._templates.pop())
            }, this), "remove.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 1)
            }, this), "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" == t.property.name && this.draw()
            }, this), "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
            }, this), "refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
            }, this)
        }, this._core.options = t.extend({}, o.Defaults, this._core.options), this.$element.on(this._handlers)
    };
    o.Defaults = {
        nav: !1,
        navText: ["prev", "next"],
        navSpeed: !1,
        navElement: "div",
        navContainer: !1,
        navContainerClass: "owl-nav",
        navClass: ["owl-prev", "owl-next"],
        slideBy: 1,
        dotClass: "owl-dot",
        dotsClass: "owl-dots",
        dots: !0,
        dotsEach: !1,
        dotsData: !1,
        dotsSpeed: !1,
        dotsContainer: !1
    }, o.prototype.initialize = function () {
        var e, i = this._core.settings;
        this._controls.$relative = (i.navContainer ? t(i.navContainer) : t("<div>").addClass(i.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = t("<" + i.navElement + ">").addClass(i.navClass[0]).html(i.navText[0]).prependTo(this._controls.$relative).on("click", t.proxy(function (t) {
            this.prev(i.navSpeed)
        }, this)), this._controls.$next = t("<" + i.navElement + ">").addClass(i.navClass[1]).html(i.navText[1]).appendTo(this._controls.$relative).on("click", t.proxy(function (t) {
            this.next(i.navSpeed)
        }, this)), i.dotsData || (this._templates = [t("<div>").addClass(i.dotClass).append(t("<span>")).prop("outerHTML")]), this._controls.$absolute = (i.dotsContainer ? t(i.dotsContainer) : t("<div>").addClass(i.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "div", t.proxy(function (e) {
            var n = t(e.target).parent().is(this._controls.$absolute) ? t(e.target).index() : t(e.target).parent().index();
            e.preventDefault(), this.to(n, i.dotsSpeed)
        }, this));
        for (e in this._overrides) this._core[e] = t.proxy(this[e], this)
    }, o.prototype.destroy = function () {
        var t, e, i, n;
        for (t in this._handlers) this.$element.off(t, this._handlers[t]);
        for (e in this._controls) this._controls[e].remove();
        for (n in this.overides) this._core[n] = this._overrides[n];
        for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, o.prototype.update = function () {
        var t, e, i = this._core.clones().length / 2, n = i + this._core.items().length, o = this._core.maximum(!0),
            r = this._core.settings, s = r.center || r.autoWidth || r.dotsData ? 1 : r.dotsEach || r.items;
        if ("page" !== r.slideBy && (r.slideBy = Math.min(r.slideBy, r.items)), r.dots || "page" == r.slideBy) for (this._pages = [], t = i, e = 0, 0; t < n; t++) {
            if (e >= s || 0 === e) {
                if (this._pages.push({start: Math.min(o, t - i), end: t - i + s - 1}), Math.min(o, t - i) === o) break;
                e = 0, 0
            }
            e += this._core.mergers(this._core.relative(t))
        }
    }, o.prototype.draw = function () {
        var e, i = this._core.settings, n = this._core.items().length <= i.items,
            o = this._core.relative(this._core.current()), r = i.loop || i.rewind;
        this._controls.$relative.toggleClass("disabled", !i.nav || n), i.nav && (this._controls.$previous.toggleClass("disabled", !r && o <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !r && o >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !i.dots || n), i.dots && (e = this._pages.length - this._controls.$absolute.children().length, i.dotsData && 0 !== e ? this._controls.$absolute.html(this._templates.join("")) : e > 0 ? this._controls.$absolute.append(new Array(e + 1).join(this._templates[0])) : e < 0 && this._controls.$absolute.children().slice(e).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(t.inArray(this.current(), this._pages)).addClass("active"))
    }, o.prototype.onTrigger = function (e) {
        var i = this._core.settings;
        e.page = {
            index: t.inArray(this.current(), this._pages),
            count: this._pages.length,
            size: i && (i.center || i.autoWidth || i.dotsData ? 1 : i.dotsEach || i.items)
        }
    }, o.prototype.current = function () {
        var e = this._core.relative(this._core.current());
        return t.grep(this._pages, t.proxy(function (t, i) {
            return t.start <= e && t.end >= e
        }, this)).pop()
    }, o.prototype.getPosition = function (e) {
        var i, n, o = this._core.settings;
        return "page" == o.slideBy ? (i = t.inArray(this.current(), this._pages), n = this._pages.length, e ? ++i : --i, i = this._pages[(i % n + n) % n].start) : (i = this._core.relative(this._core.current()), n = this._core.items().length, e ? i += o.slideBy : i -= o.slideBy), i
    }, o.prototype.next = function (e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e)
    }, o.prototype.prev = function (e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e)
    }, o.prototype.to = function (e, i, n) {
        var o;
        !n && this._pages.length ? (o = this._pages.length, t.proxy(this._overrides.to, this._core)(this._pages[(e % o + o) % o].start, i)) : t.proxy(this._overrides.to, this._core)(e, i)
    }, t.fn.owlCarousel.Constructor.Plugins.Navigation = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    "use strict";
    var o = function (i) {
        this._core = i, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (i) {
                i.namespace && "URLHash" === this._core.settings.startPosition && t(e).trigger("hashchange.owl.navigation")
            }, this), "prepared.owl.carousel": t.proxy(function (e) {
                if (e.namespace) {
                    var i = t(e.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
                    if (!i) return;
                    this._hashes[i] = e.content
                }
            }, this), "changed.owl.carousel": t.proxy(function (i) {
                if (i.namespace && "position" === i.property.name) {
                    var n = this._core.items(this._core.relative(this._core.current())),
                        o = t.map(this._hashes, function (t, e) {
                            return t === n ? e : null
                        }).join();
                    if (!o || e.location.hash.slice(1) === o) return;
                    e.location.hash = o
                }
            }, this)
        }, this._core.options = t.extend({}, o.Defaults, this._core.options), this.$element.on(this._handlers), t(e).on("hashchange.owl.navigation", t.proxy(function (t) {
            var i = e.location.hash.substring(1), n = this._core.$stage.children(),
                o = this._hashes[i] && n.index(this._hashes[i]);
            void 0 !== o && o !== this._core.current() && this._core.to(this._core.relative(o), !1, !0)
        }, this))
    };
    o.Defaults = {URLhashListener: !1}, o.prototype.destroy = function () {
        var i, n;
        t(e).off("hashchange.owl.navigation");
        for (i in this._handlers) this._core.$element.off(i, this._handlers[i]);
        for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Hash = o
}(window.Zepto || window.jQuery, window, document), function (t, e, i, n) {
    function o(e, i) {
        var o = !1, r = e.charAt(0).toUpperCase() + e.slice(1);
        return t.each((e + " " + a.join(r + " ") + r).split(" "), function (t, e) {
            if (s[e] !== n) return o = !i || e, !1
        }), o
    }

    function r(t) {
        return o(t, !0)
    }

    var s = t("<support>").get(0).style, a = "Webkit Moz O ms".split(" "), l = {
        transition: {
            end: {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd",
                transition: "transitionend"
            }
        },
        animation: {
            end: {
                WebkitAnimation: "webkitAnimationEnd",
                MozAnimation: "animationend",
                OAnimation: "oAnimationEnd",
                animation: "animationend"
            }
        }
    }, c = {
        csstransforms: function () {
            return !!o("transform")
        }, csstransforms3d: function () {
            return !!o("perspective")
        }, csstransitions: function () {
            return !!o("transition")
        }, cssanimations: function () {
            return !!o("animation")
        }
    };
    c.csstransitions() && (t.support.transition = new String(r("transition")), t.support.transition.end = l.transition.end[t.support.transition]), c.cssanimations() && (t.support.animation = new String(r("animation")), t.support.animation.end = l.animation.end[t.support.animation]), c.csstransforms() && (t.support.transform = new String(r("transform")), t.support.transform3d = c.csstransforms3d())
}(window.Zepto || window.jQuery, window, document), (new WOW).init(), $(".button-collapse").sideNav(), $("#testimonials").owlCarousel({
    loop: !0,
    margin: 10,
    nav: !1,
    responsive: {0: {items: 1}, 600: {items: 1}, 1e3: {items: 1}}
});