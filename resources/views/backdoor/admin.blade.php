@extends('backdoor.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Google Analytics</div>

                    <div class="panel-body">
                        Analytics will be here soon :)
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Portfolio
                        <a href="{{ url('/backdoor/projects/create') }}" class="pull-right btn btn-xs btn-primary">
                            Add New One
                        </a>
                    </div>

                    <div class="panel-body">
                        @foreach($projects as $project)
                            {{ $loop->iteration }})
                            <b>{{ $project->name }}</b>
                            <div class="pull-right">
                                <a href="{{ url('backdoor/projects/'. $project->id .'/edit') }}">Edit</a>
                                <a href="{{ url('/backdoor/projects/' . $project->id . '/delete') }}"
                                   class="text-danger deleteLink">
                                    Delete
                                </a>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Testimonials
                        <a href="{{ url('/backdoor/testimonials/create') }}" class="pull-right btn btn-xs btn-primary">
                            Add New One
                        </a>
                    </div>

                    <div class="panel-body">
                        @foreach($testimonials as $testimonial)
                            {{ $loop->iteration }})
                            By <b>{{ $testimonial->name }}</b>
                            <div class="pull-right">
                                <a href="{{ url('backdoor/testimonials/'. $testimonial->id .'/edit') }}">Edit</a>
                                <a href="{{ url('/backdoor/projects/' . $testimonial->id . '/delete')  }}"
                                   class="text-danger deleteLink">
                                    Delete
                                </a>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('.deleteLink').on('click', function () {
            return confirm('Are you sure?');
        });
    </script>
@endsection
