@extends('backdoor.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Project</div>

                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ url('backdoor/projects') }}" method="post"
                              enctype="multipart/form-data">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="name" placeholder="Name"
                                           value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Year</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="year" name="year" placeholder="Year"
                                           value="{{ old('year') }}">

                                    @if ($errors->has('year'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('year') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Main Image</label>
                                <div class="col-md-10">
                                    <input type="file" id="image" name="image">

                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Downloadable File</label>
                                <div class="col-md-10">
                                    <input type="file" id="file" name="file">

                                    @if ($errors->has('file'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('file') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Categories</label>
                                <div class="col-md-10">
                                    <input type="text"
                                           class="form-control"
                                           name="categories"
                                           value="{{ old('categories') }}">

                                    @if ($errors->has('categories'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('categories') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Short Description</label>
                                <div class="col-md-10">
                                    <textarea class="form-control"
                                              rows="6"
                                              name="short_desc"
                                              placeholder="Short Description">{{ old('short_desc') }}</textarea>

                                    @if ($errors->has('desc'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('desc') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Template File</label>
                                <div class="col-md-10">
                                    <select name="template" class="form-control">
                                        <option value="">--</option>
                                        @foreach($bladeFiles as $file)
                                            <option value="$file">{{ $file }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('template'))
                                        <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('template') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-lg-offset-2">
                                    <button class="btn btn-primary" type="submit">Create</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection