@extends('backdoor.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Project</div>

                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ url('backdoor/projects/'.$project->id) }}"
                              method="post"
                              enctype="multipart/form-data">

                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-md-3 control-label">Name</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                                           value="{{ old('name', $project->name) }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Year</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="year" name="year" placeholder="Year"
                                           value="{{ old('year', $project->year) }}">

                                    @if ($errors->has('year'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('year') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Main Image</label>
                                <div class="col-md-9">
                                    @if($project->image)
                                        <img src="{{ $project->image }}" style="max-width: 80%">
                                        <br><br>
                                    @endif
                                    <input type="file" id="image" name="image">

                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Categories</label>
                                <div class="col-md-9">
                                    <input type="text"
                                           class="form-control"
                                           name="categories"
                                           value="{{ old('categories', $project->categories) }}">

                                    @if ($errors->has('categories'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('categories') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Short Description</label>
                                <div class="col-md-9">
                                    <textarea class="form-control"
                                              rows=6
                                              name="short_desc"
                                              placeholder="Short Description">{{ old('short_desc', $project->short_desc) }}</textarea>

                                    @if ($errors->has('short_desc'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('short_desc') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Template File</label>
                                <div class="col-md-9">
                                    <select name="template" class="col-md-10 form-control">
                                        <option value="">--</option>
                                        @foreach($bladeFiles as $file)
                                            <option value="{{ $file }}"
                                                    @if($project->template . '.blade.php' == $file) selected @endif>
                                                {{ $file }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('template'))
                                        <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('template') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-lg-offset-3">
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection