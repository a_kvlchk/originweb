@extends('backdoor.layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Project</div>

                    <div class="panel-body">
                        <form class="form-horizontal"
                              action="{{ url('backdoor/testimonials', $testimonial->id) }}"
                              method="post"
                              enctype="multipart/form-data">

                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-md-3 control-label">Name</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="name" placeholder="Name"
                                           value="{{ old('name', $testimonial->name) }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Company</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="company" placeholder="Company"
                                           value="{{ old('company', $testimonial->company) }}">

                                    @if ($errors->has('company'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('company') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Image</label>
                                <div class="col-md-9">
                                    @if($testimonial->image)
                                        <img src="{{ $testimonial->image }}" style="max-width: 80%">
                                        <br><br>
                                    @endif
                                    <input type="file" name="image">

                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Text</label>
                                <div class="col-md-9">
                                    <textarea class="form-control"
                                              rows="8"
                                              name="text"
                                              placeholder="Text">{{ old('text', $testimonial->text) }}</textarea>

                                    @if ($errors->has('text'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('text') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9 col-lg-offset-3">
                                    <button class="btn btn-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection