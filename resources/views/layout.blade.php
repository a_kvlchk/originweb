<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>OriginWeb</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <meta name="viewport" content="initial-scale=0.8">
    <meta name="description"
          content="OriginWeb - We design and develop stunning websites from scratch, with your customer in mind.">

    <meta property="og:url" content="http://originweb.ca/">
    <meta property="og:type" content="website">
    <meta property="og:title" content="OriginWeb">
    <meta property="og:description"
          content="We design and develop stunning websites from scratch, with your customer in mind.">
    <meta property="og:image" content="http://originweb.ca/dist/ow_logo.png">

    <link rel="icon" href="{{ asset('favicon.ico') }}">
</head>


<body @if(request()->is('projects*')) class="project-page" @endif onload="load()">
<nav>
    <div class="nav-wrapper">
        <div class="container">
            <a href="{{ url('/') }}" class="brand-logo">
                <img src="{{ asset('img/ow_logo.png') }}" alt="OriginWeb">
            </a>

            <ul class="right hide-on-med-and-down">
                <li><a href="{{ request()->is('/') ? '#about' : url('/#about') }}" class="anchor">About</a></li>
                <li><a href="{{ request()->is('/') ? '#projects' : url('/#projects') }}" class="anchor">Projects</a></li>
                <li><a href="{{ request()->is('/') ? '#contact' : url('/#contact') }}" class="anchor">Contact</a></li>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="{{ request()->is('/') ? '#about' : url('/#about') }}" class="anchor">About</a></li>
                <li><a href="{{ request()->is('/') ? '#projects' : url('/#projects') }}" class="anchor">Projects</a></li>
                <li><a href="{{ request()->is('/') ? '#contact' : url('/#contact') }}" class="anchor">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>

@yield('body')

<section class="contact" id="contact">
    <div class="container">
        <h2 class="section-title">{{ request()->is('projects/*') ? 'READY TO START YOUR PROJECT?' : 'CONTACT' }}</h2>
        <div class="row">
            <div class="col-md-5">
                <form id="contactForm">
                    {{ csrf_field() }}
                    <div class="information"><img src="{{ asset('img/icons/mail.png') }}" alt="E-mail">
                        info@originweb.ca
                    </div>
                    <div class="input-container">
                        <label for="name">Your name</label>
                        <div class="input"><input required id="name" name="name" type="text"></div>
                    </div>
                    <div class="input-container">
                        <label for="email">Your e-mail</label>
                        <div class="input"><input required id="email" name="email" type="email"></div>
                    </div>
                    <div class="input-container">
                        <label for="message">Tell us about your project</label>
                        <textarea name="message" id="message"></textarea>
                    </div>
                    <div class="input-container">
                        <input id="copy" type="checkbox" name="sendToMe">
                        <label for="copy">Send a copy to yourself</label>
                    </div>
                    <button type="submit" id="sendContactForm">Send</button>
                </form>
            </div>
            <div class="col-md-7">
                <div class="map">
                    <div class="blueBG pulse"></div>
                    <div id="mapContainer" class="z-depth-5"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="address">240 Richmond Street W Toronto ON M5V 1V6</div>
        <div class="copyright">© 2017 | OriginWeb</div>
    </div>
</footer>

</body>

<script src="{{ mix('js/app.js') }}"></script>

<!--------------------GOOGLE MAP--------------->
<script async type="text/javascript"
        src="https://maps.google.com/maps/api/js?key=AIzaSyA9NtltISVB2SMFxemDCmccpL6mUh6cK_c"></script>
<script type="text/javascript" src="{{ asset('js/google.js') }}"></script>

<!-- Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108463192-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-108463192-1');
</script>

</html>