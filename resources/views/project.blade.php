@extends('layout')

@section('body')
    <section class="main-project" style="background-image: url({{ $project->image_path }})">
        <div class="container">
            <div class="main-project-text">
                <div class="category">{{ $project->categories }}</div>
                <div class="project-title">{{ $project->name }}</div>
                @if($project->short_desc)
                    <div class="project-description">{{ $project->short_desc }}</div>
                @endif
            </div>
        </div>
    </section>

    <div>
        @yield('process')
    </div>

    <div class="container" style="margin-top: 30px">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                @if($prev)
                    <a href="{{ url('projects', $prev->id) }}"><h3>&larr; Prev</h3></a>
                @endif
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                @if($next)
                    <a href="{{ url('projects', $next->id) }}"><h3>Next &rarr;</h3></a>
                @endif
            </div>
        </div>
    </div>

    @if($project->file)
        <!-- File Request -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <form id="file-request" class="modal-body">
                        <div class="modal-information">
                            <p>
                                Write your e-mail below to receive an actual link to file:
                            </p>

                            <div class="input-container">
                                <input type="hidden" name="file_id" value="{{ $project->file_id }}">
                                {{ csrf_field() }}
                                <input type="text" name="email" placeholder="E-mail" class="input">
                            </div>
                            <p id="file-request-result"></p>
                            <button type="submit" href="#" class="blue">SEND</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    @endif
@endsection