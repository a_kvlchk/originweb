@extends('project')

@section('process')
    <img src="{{ asset('projects/vera-crouz/vera_pres.jpg') }}" width="100%">

    <p class="text-center" style="margin-top: 30px">
        <a class="button-white" href="http://demo.originweb.ca/vera-crouz" target="_blank">
            Live Preview
        </a>
    </p>
@endsection