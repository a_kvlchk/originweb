@extends('layout')

@section('body')
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 pull-left">
                    <div class="hero-text">
                        <h1>We build custom websites from scratch to grow your business online.</h1>
                        <div class="mail"><img src="{{ asset('img/icons/mail.png') }}" alt="#"> info@originweb.ca</div>
                        <div class="buttons">
                            <a href="#projects" class="white">Our Projects</a>
                            <a href="#contact" class="blue">Get Free Quote</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 pull-right">
                    <img src="{{ asset('img/p2.gif') }}" class="img-responsive hero-img" alt="#">
                </div>
            </div>
        </div>
    </section>

    <section class="about" id="about">
        <div class="container">
            <h2 class="section-title">About Us</h2>

            <div class="col-lg-10 col-lg-offset-1 text-center">
                <p class="montserrat">
                    OriginWeb is a web design and development agency located in Toronto, Canada.
                </p>

                <p class="montserrat">
                    We specialize in creating beautiful, minimalistic websites based on our clients’ needs.
                    <br>We work mainly with small & medium sized businesses to create or improve their online presence.
                </p>
            </div>
        </div>
    </section>



    <section class="projects" id="projects">
        <h2 class="section-title">PROJECTS</h2>
        <div class="projects-container">
            <div class="projects-element">
                @if(isset($projects[0]))
                    <a href="{{ url('projects', $projects[0]['id']) }}"
                       style="background: url({{ '/projects/thumb_' . $projects[0]['image'] }})"
                       class="project">
                        <div class="project-text">
                            <p>{{ $projects[0]['categories'] }}</p>
                            <h3>{{ $projects[0]['name'] }}</h3>
                        </div>
                    </a>
                @endif
                @if(isset($projects[1]))
                    <a href="{{ url('projects', $projects[1]['id']) }}"
                       style="background: url({{ '/projects/thumb_' . $projects[1]['image'] }})"
                       class="project">
                        <div class="project-text">
                            <p>{{ $projects[1]['categories'] }}</p>
                            <h3>{{ $projects[1]['name'] }}</h3>
                        </div>
                    </a>
                @endif
            </div>
            <div class="projects-element">
                @if(isset($projects[2]))
                    <a href="{{ url('projects', $projects[2]['id']) }}"
                       style="background: url({{ '/projects/thumb_' . $projects[2]['image'] }})"
                       class="project project-center">
                        <div class="project-text">
                            <p>{{ $projects[2]['categories'] }}</p>
                            <h3>{{ $projects[2]['name'] }}</h3>
                        </div>
                    </a>
                @endif
            </div>
            <div class="projects-element">
                @if(isset($projects[3]))
                    <a href="{{ url('projects', $projects[3]['id']) }}"
                       style="background: url({{ '/projects/thumb_' . $projects[3]['image'] }})"
                       class="project">
                        <div class="project-text">
                            <p>{{ $projects[3]['categories'] }}</p>
                            <h3>{{ $projects[3]['name'] }}</h3>
                        </div>
                    </a>
                @endif
                @if(isset($projects[4]))
                    <a href="{{ url('projects', $projects[4]['id']) }}"
                       style="background: url({{ '/projects/thumb_' . $projects[4]['image'] }})"
                       class="project">
                        <div class="project-text">
                            <p>{{ $projects[4]['categories'] }}</p>
                            <h3>{{ $projects[4]['name'] }}</h3>
                        </div>
                    </a>
                @endif
            </div>
        </div>
    </section>


    <section class="how-it-works" id="how-it-works">
        <div class="container">
            <h2 class="section-title">How It Works</h2>

            <div class="row how-it-works-list">
                <div class="col-md-4">
                    <img class="icon" src="{{ asset('img/icons/write.png') }}">
                    <p class="montserrat text-justify">
                        Tell us about your project and select one of our pricing packages based your needs.
                    </p>
                </div>
                <div class="col-md-4">
                    <img class="icon" src="{{ asset('img/icons/presentation.png') }}">
                    <p class="montserrat text-justify">
                        We create design mockups of your future website from scratch. You review and approve.
                    </p>
                </div>
                <div class="col-md-4">
                    <img class="icon" src="{{ asset('img/icons/gift.png') }}">
                    <p class="montserrat text-justify">
                        We build your website and deploy, before you know it your brand new site is live.
                    </p>
                </div>
            </div>
        </div>
    </section>


    <section class="pricing container">
        <h2 class="section-title">Pricing</h2>

        <div class="col-lg-10 col-lg-offset-1 text-center">
            <p class="montserrat">
                All our websites comes with design made from scratch.
                <br>
                We believe that every project is unique and
                requires original design solutions to meet clients' needs.
            </p>
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12">
                <div class="pricing-table">
                    <div class="col-md-4">
                        <h3>Silver</h3>

                        <ul>
                            <li>Original Design</li>
                            <li>Mobile Friendly & Responsive</li>
                            <li class="none"><b>1 round</b> of revisions</li>
                            <li class="none"><b>3</b> pages</li>
                            <li class="none"><b>10</b> days</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="z-depth-4">
                            <h3>Gold</h3>

                            <ul>
                                <li>Original Design</li>
                                <li>Mobile Friendly & Responsive</li>
                                <li>Content Management System</li>
                                <li class="none"><b>2 rounds</b> of revisions</li>
                                <li class="none"><b>5</b> pages</li>
                                <li class="none"><b>15</b> days</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h3>Platinum</h3>

                        <ul>
                            <li>Original Design</li>
                            <li>Mobile Friendly & Responsive</li>
                            <li>Content Management System</li>
                            <li>Social Media Integration</li>
                            <li class="none"><b>3 rounds</b> of revisions</li>
                            <li class="none"><b>10+</b> pages</li>
                            <li class="none"><b>20+</b> days</li>
                        </ul>
                    </div>
                </div>

                <p class="montserrat text-center">
                    Ready to start your project?
                    <a href="#contact" class="button-blue" style="margin-left: 15px">Get Free Quote</a>
                </p>

            </div>
        </div>

    </section>


    <section class="testimonials">
        <div class="container">
            <h2 class="section-title">TESTIMONIALS</h2>
            <div class="owl-carousel owl-theme" id="testimonials">
                @foreach($testimonials as $testimonial)
                    <div class="item">
                        <div class="testimonial z-depth-4">
                            <div class="testimonial-text">
                                {{ $testimonial->text }}
                            </div>
                            <div class="testimonial-user">
                                @if($testimonial->image)
                                    <div class="testimonial-user-photo">
                                        <img src="{{ $testimonial->image }}"
                                             alt="{{ $testimonial->company }}">
                                    </div>
                                @endif
                                <div class="testimonial-user-text @if(!$testimonial->image)text-center @endif">
                                    <div class="name">{{ $testimonial->name }}</div>
                                    <div class="work">{{ $testimonial->company }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

@endsection