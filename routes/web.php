<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('projects/{project}', 'ProjectController@show');

Route::post('files/request', 'FileController@request');
Route::get('files/download/{slug}', 'FileController@download');

Route::prefix('backdoor')->group(function () {
    Route::get('/', 'IndexController@admin');

    Route::get('projects/{project}/delete', 'ProjectController@delete');
    Route::resource('projects', 'ProjectController', ['except' => 'index']);

    Route::resource('testimonials', 'TestimonialController', ['except' => 'index', 'show']);

    Route::resource('files', 'FileController');
});

Route::prefix('api')->group(function () {
    Route::get('projects', 'ApiController@allProjects');
    Route::post('mail', 'ApiController@sendMail');
});

Auth::routes();
