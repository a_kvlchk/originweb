let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/google.js', 'public/js')

    .js('resources/assets/js/backdoor.js', 'public/js')
    .sass('resources/assets/style/backdoor.scss', 'public/css')

    .sass('resources/assets/style/app.scss', 'public/css')
    .options({
        processCssUrls: false
    })
    .version();